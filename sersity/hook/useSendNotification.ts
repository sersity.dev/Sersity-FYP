import prisma from "lib/prismaClient";

const createNoti = async (args) => {
  return await prisma.notification.create({
    data: {
      message: args.message,
      type: args.type,
      data: { count: 1 },
      url: args.url,
      user: { connect: { id: args.userId } },
      fromUser: { connect: { id: args.fromUserId } },
      readAt: null,
    },
  });
};

export default async function useSendNotification(args) {
  let createdNotification;
  if (args.type === "CHAT") {
    const noti: any = await prisma.notification.findFirst({
      where: { fromUserId: args.fromUserId },
      select: { id: true, data: true },
    });
    if (noti) {
      createdNotification = await prisma.notification.update({
        where: { id: noti?.id },
        data: {
          message: args.message,
          type: args.type,
          data: { count: noti.data?.count + 1 },
          url: args.url,
          user: { connect: { id: args.userId } },
          fromUser: { connect: { id: args.fromUserId } },
          readAt: null,
        },
      });
    } else {
      createdNotification = createNoti(args);
    }
  } else {
    createdNotification = createNoti(args);
  }
  return { createdNotification };
}
