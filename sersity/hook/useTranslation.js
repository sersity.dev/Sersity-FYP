import useTranslationNext from "next-translate/useTranslation";

export const useTranslation = (defaultNs) => {
  const globalDefaultNs = "common";

  const { t, ...rest } = useTranslationNext(defaultNs || globalDefaultNs);
  return {
    t: (key, ...args) => {
      return t(key, { fallback: `common:${key}` }, ...args);
    },
    ...rest,
  };
};
