import profanity from "profanity-util";
export function postDetector(post) {
  const title = post.title;
  const content = post.content;

  const badTitle = profanity.check(title);
  const badContent = profanity.check(content);

  const result = {
    badTitle: badTitle,
    badContent: badContent,
  };
  return result;
}

export function commentDetector(comment) {
  var profanity = require("profanity-util");
  const cmt = comment.comment;
  const pureComment = profanity.purify(cmt);
  return pureComment;
}
