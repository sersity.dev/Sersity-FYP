import { useMutation } from "@apollo/client";
import { useToast } from "@chakra-ui/react";
import { useState } from "react";
import { DELETE_DRAFT } from "./graphql/post";

const useDraft = ({ posts }) => {
  const emptyContent = {
    title: "",
    content: "",
    id: undefined,
    tags: [],
  };
  const initDraft = posts || [];
  const [drafts, setDrafts] = useState(initDraft);
  const [selected, setSelected] = useState(emptyContent);
  const [deleteDraft] = useMutation(DELETE_DRAFT);
  const toast = useToast();

  const onDelete = (key) => {
    let updated = drafts;
    updated.splice(key, 1);
    updated && setDrafts([...updated]);
  };

  function handleClickDelete(draftId, i) {
    deleteDraft({
      variables: { draftId },
      update(cache, { data }) {
        if (data) {
          toast({
            title: "Deleted successfully",
            status: "success",
            duration: 1000,
            isClosable: true,
          });
          onDelete(i);
        }
      },
    });
  }

  function updateDraft() {
    const fIndex = drafts.findIndex((e) => e.id === selected.id);
    if (fIndex > -1) {
      let updated = drafts;
      updated[fIndex] = selected;
      setDrafts([...updated]);
    }
    setSelected(emptyContent);
  }

  function selectDraft(e) {
    setSelected({
      title: e.title,
      content: e.content,
      id: e.id,
      tags: e.tags,
    });
  }
  return {
    drafts,
    emptyContent,
    selected,
    setSelected,
    handleClickDelete,
    selectDraft,
    updateDraft,
  };
};

export default useDraft;
