import { useAppContext } from "lib/AppContent";


export function useChatDialog(){
    const {openChat,setOpenChat}= useAppContext();
    const isOpenChat = openChat
    const onOpenChat= () => setOpenChat(true)
    const onCloseChat=()=> setOpenChat(false)
    const onToggleChat=()=> {
        setOpenChat(!openChat)
    }
    return {
        isOpenChat,
        onOpenChat,
        onCloseChat,
        onToggleChat,
    }
}