export function hotSort(data) {
  let list = [];
  data.forEach((d) => {
    let age = d.created_at.getTime();
    let x = d.upvote - d.downvote;
    let y = 1;
    // Y = {1,0,-1}

    if (Math.abs(x) < 1) {
      x = 1;
    }

    if (x > 0) {
      y = 1;
    } else if (x == 0) {
      y = 0;
    } else if (x < 0) {
      y = -1;
    }
    let logz = Math.log10(x);

    let score = logz + (y * age) / 45000; //45000 is equal to 12.5 hours;
    let result = {
      id: d.id,
      score: score,
      age: age,
      logz: logz,
    };
    list.push(result);
  });

  list.sort((a, b) => (a.score > b.score ? -1 : 1));
  console.log(list);
  return list;
}

export function cleanData(data) {
  const data_example = {
    post_id: 1,
    user_id: 1,
    action: "UPVOTE",
    updated_at: new Date(),
  };
  const result = [];

  return result;
}

export default async function handler(req, res) {
  const data = req.body;
  const data_example = [
    {
      id: 1,
      upvote: 100,
      downvote: 25,
      created_at: new Date("2021-3-15"),
    },
    {
      id: 2,
      upvote: 1000,
      downvote: 500,
      created_at: new Date("2021-3-16"),
    },
    {
      id: 3,
      upvote: 120,
      downvote: 3,
      created_at: new Date("2021-3-16"),
    },
    {
      id: 4,
      upvote: 1020,
      downvote: 10,
      created_at: new Date("2021-3-16"),
    },
    {
      id: 5,
      upvote: 300,
      downvote: 100,
      created_at: new Date("2021-3-16"),
    },
    {
      id: 6,
      upvote: 250,
      downvote: 25,
      created_at: new Date("2021-3-16"),
    },
  ];

  const list_id = hotSort(data_example);

  return res.json({
    data: list_id,
  });
}
