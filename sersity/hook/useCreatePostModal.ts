import { useDisclosure } from "@chakra-ui/hooks";

export default function useCreatePostModal() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  return {
    isOpenCreatePostModal: isOpen,
    onOpenCreatePostModal: onOpen,
    onCloseCreatePostModal: onClose,
  };
}
