import { useSubscription } from "@apollo/client";
import { CREATED_NOTIFICATION } from "hook/graphql/notification";
import { useToast } from "@chakra-ui/toast";
import { useMediaQuery } from "@chakra-ui/media-query";

function onSubNoti({ user, isOpen }) {
  const toast = useToast();
  const [isSmallerThan600] = useMediaQuery("(max-width: 600px)");

  useSubscription(CREATED_NOTIFICATION, {
    variables: {
      userId: user?.id,
    },
    onSubscriptionData: ({ client, subscriptionData }) => {
      const newNotification = subscriptionData.data?.createdNotification;
      const chatType = newNotification.type == "CHAT" ? "equals" : "notIn";
      const ref = `notifications:{"where":{"type":{"${chatType}":"CHAT"}}}`;
      client.cache.modify({
        id: client.cache.identify(newNotification.user),
        fields: {
          notifications(e, { storeFieldName }) {
            if (storeFieldName === ref) {
              if (e.length > 0 && e.find((el) => el.id !== newNotification.id))
                return [newNotification, ...e];
            } else {
              return e;
            }
          },
        },
      });
      client.cache.modify({
        id: client.cache.identify({ __typename: "Query" }),
        fields: {
          countNewNotification(e) {
            return { ...e, count: e.count + 1 };
          },
        },
      });
      const notiData = newNotification.userRef.name
        ? newNotification.userRef.name + " " + newNotification.message
        : newNotification.message;
      newNotification && !isSmallerThan600 && !isOpen;
      toast({
        title: "Notification",
        description: notiData,
        status: "info",
        position: "bottom-right",
        duration: 5000,
        isClosable: true,
      });
    },
  });
}

export default function useNotiSub() {
  return { onSubNoti };
}
