import { useTranslation } from "./useTranslation";
const moment = require("moment");

export default function momentD(inp?: moment.MomentInput, strict?: boolean) {
  const { lang } = useTranslation();
  if (lang == "en") {
    moment.updateLocale("en", {
      relativeTime: {
        future: "in %s",
        past: "%s ago",
        s: "1s",
        ss: "%ds",
        m: "1mn",
        mm: "%dmn",
        h: "1h",
        hh: "%dh",
        d: "1d",
        dd: "%dd",
        w: "1w",
        ww: "%dw",
        M: "a month",
        MM: "%d months",
        y: "a year",
        yy: "%d years",
      },
    });
  }
  if (lang == "kh") {
    moment.updateLocale("kh", {
      relativeTime: {
        future: "in %s",
        past: "%sមុន",
        s: "1 វិនាទី",
        ss: "%dវិនាទី",
        m: "1 នាទី",
        mm: "%d នាទី",
        h: "1 ម៉ោង",
        hh: "%d ម៉ោង",
        d: "1 ថ្ងៃ",
        dd: "%d ថ្ងៃ",
        w: "1 សប្តាហ៍",
        ww: "%d សប្តាហ៍",
        M: "1 ខែ",
        MM: "%d ខែ",
        y: "1 ឆ្នាំ",
        yy: "%d ឆ្នាំ",
      },
    });
  }

  return moment(inp, strict);
}
