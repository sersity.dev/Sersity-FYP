import { useMutation } from "@apollo/client";
import { useSession } from "next-auth/client";
import { useEffect } from "react";
import { UPDATE_USER_LAST_SEEN } from "./graphql/user";

const useOnlineUserIndicator = () => {
  const [session] = useSession();
  const [updateUserLastSeen] = useMutation(UPDATE_USER_LAST_SEEN);
  // update user online status
  useEffect(() => {
    const onlineIndicator =
      session?.user && setInterval(() => updateUserLastSeen(), 30000);
    return () => clearInterval(onlineIndicator);
  });

  useEffect(() => {
    if (session) updateUserLastSeen();
  }, []);
};

export default useOnlineUserIndicator;
