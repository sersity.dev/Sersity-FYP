import gql from "graphql-tag";

export const UPDATE_USER_LAST_SEEN = gql`
  # Write your query or mutation here
  mutation updateUserLastSeen {
    updateUserLastSeen {
      id
      lastSeen
    }
  }
`;
// export const ONLINED_USER = gql`
//   subscription($userId: Int!) {
//     onlinedUser(userId: $userId) {
//       id
//       name
//       isOnline
//     }
//   }
// `;

export const GET_USER = gql`
  query getUser($where: UserWhereUniqueInput!) {
    user(where: $where) {
      id
      name
      image
      rank
      score
      followedBy {
        id
        name
      }
      setting {
        anonymous
      }
      isOnline
      countPost
      countFollower
      setting {
        id
        anonymous
      }
    }
  }
`;

// select: {
//   id: true,
//   name: true,
//   rank: true,
//   image: true,
//   score: true,
//   _count: {
//     select: {
//       posts: true,
//     },
//   },
//   followedBy: {
//     where: {
//       id: { equals: "" + session.user.id },
//     },
//     select: {
//       id: true,
//     },
//   },
// },

export const FOLLOW = gql`
  mutation followUser($followingId: String) {
    follow(followingId: $followingId) {
      id
    }
  }
`;

export const UNFOLLOW = gql`
  mutation unfollowUser($followingId: String) {
    unfollow(followingId: $followingId) {
      id
    }
  }
`;

export const GET_USER_POSTS = gql`
  query ($id: String) {
    user(where: { id: $id }) {
      id
      name
      posts(
        where: {
          published: { equals: true }
          status: { equals: PUBLISHED }
          isAnonymous: { equals: false }
        }
        orderBy: { createdAt: desc }
      ) {
        id
        title
        content
        isSaved
        publishedAt
        tags {
          id
          tag
        }
        commentCount
        topComment {
          id
          userRef {
            id
            rank
            name
            image
          }
          replies {
            id
            userRef {
              id
              rank
              name
              image
            }
            comment
            upvote
            downvote
            createdAt
          }
          isVoted {
            isVoted
            action
          }
          comment
          upvote
          downvote
          createdAt
        }
        userRef {
          id
          rank
          name
          image
        }
        isVoted {
          isVoted
          action
        }
        upvote
        downvote
        createdAt
      }
    }
  }
`;

export const CLEAR_KEYWORD = gql`
  mutation {
    clearKeyword {
      id
    }
  }
`;

export const GET_KEYWORDS = gql`
  query {
    getKeyword {
      popularKeywords {
        keyword
        count
      }
      personalKeywords {
        keyword
      }
    }
  }
`;
