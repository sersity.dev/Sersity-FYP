import gql from "graphql-tag";

export const GET_USER_NOTIFICATIONS = gql`
  query UserNotifications(
    $userId: String!
    $where: NotificationWhereInput
    $take: Int
    $skip: Int
  ) {
    user(where: { id: $userId }) {
      id
      name
      notifications(
        take: $take
        skip: $skip
        where: $where
        orderBy: { updatedAt: desc }
      ) {
        id
        type
        message
        url
        data
        readAt
        isRead
        user {
          id
          name
        }
        userRef {
          id
          rank
          name
          image
        }
        isDeleted
        createdAt
        updatedAt
      }
    }
  }
`;
export const READ_NOTIFICATIONS = gql`
  mutation readNotifications($ids: [String]) {
    readNotifications(ids: $ids) {
      count
    }
  }
`;

export const COUNT_NEW_NOTIFICATION = gql`
  query countNewNotification {
    countNewNotification {
      count
    }
  }
`;

export const CREATED_NOTIFICATION = gql`
  subscription ($userId: String!) {
    createdNotification(userId: $userId) {
      id
      type
      message
      url
      data
      readAt
      isRead
      user {
        id
        name
      }
      userRef {
        id
        rank
        name
        image
      }
      isDeleted
      createdAt
      updatedAt
    }
  }
`;
