import gql from "graphql-tag";

export const GET_USER_SETTING = gql`
query($id: String) {
  user(where: { id: $id }) {
    id
    isPasswordless
    name
    image
    email
    rank
    setting{
    	id
      anonymous
      secondVerification
      allowBadWord
      commentNoti
      replyNoti
      newTrendNoti
      followingUserNoti
      chatActiveStatus
      chatSetting
      updatedAt
        
    }
    information{
      id
      bio
      phone
      dateOfBirth
      highSchool
      university
      location
      updatedAt
      
    }
  }
}
`;

export const UPDATE_SETTING = gql`
  mutation($type:String!,$value:Boolean!){
    updateSetting(type:$type,value:$value){
      id
    }
  }

`;


export const UPDATE_CHAT_SETTING = gql`
mutation($value:ChatSettingEnum){
  updateChatSetting(value:$value){
    id
  }
}
`;

export const UPDATE_INFORMATION = gql`
mutation($type: String!, $value: String!){
  updateInformation(type:$type, value:$value){
    id
  }
}
`;

export const UPDATE_PASSWORD = gql`
mutation($oldPw: String!,$newPw: String!, $cfNewPw: String!){
  updatePassword(oldPw: $oldPw, newPw: $newPw, cfNewPw: $cfNewPw){
    msg
    status
  }
}
`;