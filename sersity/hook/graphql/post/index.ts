import { gql } from "graphql-tag";

export const GET_POSTS = gql`
  query ($skip: Int!, $latest: Boolean) {
    posts(skip: $skip, latest: $latest) {
      id
      title
      content
      isSaved
      publishedAt
      tags {
        id
        tag
      }
      commentCount
      topComment {
        id
        userRef {
          id
          name
          rank
          image
        }
        isVoted {
          isVoted
          action
        }
        replies {
          id
          userRef {
            id
            name
            rank
            image
          }
          comment
          upvote
          downvote
          createdAt
        }
        comment
        upvote
        downvote
        createdAt
      }
      userRef {
        id
        name
        rank
        image
      }
      isVoted {
        isVoted
        action
      }
      upvote
      downvote
      createdAt
    }
  }
`;

export const GET_ARRAY_POSTS = gql`
  query ($postIds: String!) {
    arrayPosts(postIds: $postIds) {
      id
      title
      content
      isSaved
      publishedAt
      tags {
        id
        tag
      }
      commentCount
      topComment {
        id
        userRef {
          id
          name
          rank
          image
        }
        isVoted {
          isVoted
          action
        }
        replies {
          id
          userRef {
            id
            name
            rank
            image
          }
          comment
          upvote
          downvote
          createdAt
        }
        comment
        upvote
        downvote
        createdAt
      }
      userRef {
        id
        name
        rank
        image
      }
      isVoted {
        isVoted
        action
      }
      upvote
      downvote
      createdAt
    }
  }
`;

export const GET_POST = gql`
  query ($id: String!) {
    post(id: $id) {
      upvote
      downvote
      isSaved
      publishedAt
      tags {
        id
        tag
      }
      commentCount
      topComment {
        id
        userRef {
          id
          name
          rank
          image
        }
        isVoted {
          isVoted
          action
        }
        replies {
          id
          userRef {
            id
            name
            rank
            image
          }
          comment
          upvote
          downvote
          createdAt
        }
        comment
        upvote
        downvote
        createdAt
      }
      comments {
        replies {
          id
          userRef {
            name
            id
            rank
            image
          }
          isVoted {
            action
            isVoted
          }
          comment
          createdAt
          upvote
          downvote
        }
        id
        userRef {
          name
          id
          rank
          image
        }
        isVoted {
          action
          isVoted
        }
        comment
        createdAt
        upvote
        downvote
      }
      isVoted {
        isVoted
        action
      }
    }
  }
`;

export const SEARCH_POSTS = gql`
  query ($keyword: String!) {
    searchPosts(keyword: $keyword) {
      id
      title
      content
      isSaved
      publishedAt
      tags {
        id
        tag
      }
      userRef {
        id
        name
        rank
        image
      }
      isVoted {
        isVoted
        action
      }
      createdAt
      votes
      upvote
      downvote
      commentCount
      topComment {
        id
        isVoted {
          isVoted
          action
        }
        comment
        votes
        upvote
        downvote
        createdAt
        replies {
          isVoted {
            isVoted
            action
          }
          comment
          userRef {
            name
            rank
            id
            image
          }
          votes
          upvote
          downvote
          createdAt
        }
        userRef {
          name
          rank
          id
          image
        }
      }
    }
  }
`;

export const CREATE_SAVE = gql`
  mutation ($post: String!) {
    createSave(postId: $post) {
      status
      message
    }
  }
`;

export const GET_SAVED_POSTS = gql`
  query {
    savedPost {
      post {
        id
        title
        content
        isSaved
        publishedAt
        tags {
          id
          tag
        }
        userRef {
          id
          name
          rank
          image
        }
        isVoted {
          isVoted
          action
        }
        createdAt
        votes
        upvote
        downvote
        commentCount
        topComment {
          id
          isVoted {
            isVoted
            action
          }
          comment
          votes
          upvote
          downvote
          createdAt
          replies {
            isVoted {
              isVoted
              action
            }
            comment
            userRef {
              name
              rank
              id
              image
            }
            votes
            upvote
            downvote
            createdAt
          }
          userRef {
            name
            rank
            id
            image
          }
        }
      }
    }
  }
`;

export const CREATE_DRAFT = gql`
  mutation ($title: String!, $content: String!, $tags: [Int]) {
    createDraft(title: $title, content: $content, tags: $tags) {
      id
      title
      content
      userRef {
        id
        name
        image
      }
      tags {
        id
        tag
      }
      commentCount
      topComment {
        userRef {
          id
          name
          rank
          image
        }
        upvote
        downvote
        comment
        replies {
          comment
          createdAt
          upvote
          downvote
          createdAt
          userRef {
            id
            name
            rank
            image
          }
        }
        createdAt
      }
      createdAt
    }
  }
`;

export const CREATE_POST = gql`
  mutation ($title: String!, $content: String!, $tags: [Int]) {
    createPost(title: $title, content: $content, tags: $tags) {
      id
      title
      content
      isSaved
      publishedAt
      tags {
        id
        tag
      }
      commentCount
      topComment {
        id
        userRef {
          id
          name
          rank
          image
        }
        isVoted {
          isVoted
          action
        }
        replies {
          id
          userRef {
            id
            name
            rank
            image
          }
          comment
          upvote
          downvote
          createdAt
        }
        comment
        upvote
        downvote
        createdAt
      }
      userRef {
        id
        name
        rank
        image
      }
      isVoted {
        isVoted
        action
      }
      upvote
      downvote
      createdAt
    }
  }
`;
export const GET_DRAFTS = gql`
  query {
    userPosts(status: DRAFTED) {
      id
      title
      tags {
        id
        tag
      }
      content
      createdAt
    }
  }
`;

export const UPDATE_POST = gql`
  mutation (
    $postId: String!
    $status: PostStatus!
    $title: String!
    $content: String!
    $published: Boolean!
    $tags: [Int]
  ) {
    updatePost(
      postId: $postId
      status: $status
      title: $title
      content: $content
      published: $published
      tags: $tags
    ) {
      id
      title
      content
      publishedAt
      userRef {
        id
        name
        image
      }
      tags {
        id
        tag
      }
      commentCount
      topComment {
        userRef {
          id
          name
          image
        }
        upvote
        downvote
        comment
        replies {
          comment
          createdAt
          upvote
          downvote
          createdAt
          userRef {
            id
            name
            image
          }
        }
        updatedAt
        createdAt
      }
      createdAt
    }
  }
`;

export const GET_TAGS = gql`
  query {
    tags {
      id
      tag
    }
  }
`;

export const SEARCH_TAGS = gql`
  query ($keyword: String!) {
    searchTags(keyword: $keyword) {
      id
      tag
    }
  }
`;

export const CREATE_COMMENT = gql`
  mutation ($postId: String!, $comment: String!) {
    createComment(postId: $postId, comment: $comment) {
      id
      userRef {
        id
        image
        rank
        name
      }
      replies {
        id
        comment
        upvote
        downvote
        votes
        createdAt
        isVoted {
          isVoted
          action
        }
      }
      isVoted {
        isVoted
        action
      }
      comment
      upvote
      downvote
      votes
      createdAt
    }
  }
`;

export const CREATE_POST_VOTE = gql`
  mutation ($action: Action!, $postId: String, $isVoted: Boolean!) {
    createPostVote(action: $action, postId: $postId, isVoted: $isVoted) {
      id
      upvote
      downvote
      isVoted {
        action
        isVoted
      }
    }
  }
`;

export const CREATE_COMMENT_VOTE = gql`
  mutation ($action: Action!, $commentId: Int, $isVoted: Boolean!) {
    createCommentVote(
      action: $action
      commentId: $commentId
      isVoted: $isVoted
    ) {
      id
      upvote
      downvote
      isVoted {
        action
        isVoted
      }
    }
  }
`;

export const CREATE_REPLY = gql`
  mutation ($postId: String!, $comment: String!, $parentId: Int!) {
    createReply(postId: $postId, comment: $comment, parentId: $parentId) {
      id
      userRef {
        id
        name
        image
      }
      comment
      upvote
      downvote
      votes
      createdAt
      isVoted {
        isVoted
        action
      }
    }
  }
`;

export const CREATE_REPORTED_POST = gql`
  mutation ($reason: String!, $postId: String!) {
    createReportedPost(reason: $reason, postId: $postId) {
      id
      user {
        name
      }
      reason
      createdAt
    }
  }
`;
export const DELETE_DRAFT = gql`
  mutation ($draftId: String!) {
    deleteDraft(draftId: $draftId) {
      id
    }
  }
`;
export const GET_TAG_POSTS = gql`
  query ($tag: String!) {
    tagPosts(tag: $tag) {
      id
      tag
      posts(orderBy: { score: desc }) {
        id
        title
        content
        publishedAt
        isSaved
        tags {
          id
          tag
        }
        commentCount
        topComment {
          id
          userRef {
            id
            name
            rank
            image
          }
          isVoted {
            isVoted
            action
          }
          replies {
            id
            userRef {
              id
              name
              rank
              image
            }
            comment
            upvote
            downvote
            createdAt
          }
          comment
          upvote
          downvote
          createdAt
        }
        userRef {
          id
          name
          rank
          image
        }
        isVoted {
          isVoted
          action
        }
        upvote
        downvote
        createdAt
      }
    }
  }
`;
