import gql from "graphql-tag";

export const CREATE_MESSAGE = gql`
  mutation CREATE_MESSAGE(
    $content: String!
    $userId: String!
    $chatRoomId: Int!
    $slug: String!
  ) {
    createMessage(
      content: $content
      userId: $userId
      chatRoomId: $chatRoomId
      slug: $slug
    ) {
      id
      content
      createdAt
      chatRoom {
        id
      }
      user {
        image
        name
        id
      }
      seenBy {
        id
        createdAt
        user {
          id
          name
        }
      }
    }
  }
`;

export const UPDATE_LAST_MESSAGE_SEENBY = gql`
  mutation updateLastMessageSeenBy($lastMessageId: Int!) {
    updateLastMessageSeenBy(lastMessageId: $lastMessageId) {
      id
    }
  }
`;

export const CREATED_MESSAGE = gql`
  subscription ($userId: String) {
    createdMessage(userId: $userId) {
      id
      content
      createdAt
      chatRoom {
        id
      }
      user {
        image
        name
        id
      }
      seenBy {
        id
        createdAt
        user {
          id
          name
        }
      }
    }
  }
`;

export const CREATED_CHATROOM = gql`
  subscription ($userId: String) {
    createdChatRoom(userId: $userId) {
      id
      slug
      name
      image
      isOnline
      createdAt
      updatedAt
      members {
        image
        id
        name
        rank
      }
    }
  }
`;

export const GET_CHATROOM_MESSAGES = gql`
  query chatRoomMessages($slug: String!, $skip: Int) {
    chatRoom(where: { slug: $slug }) {
      id
      slug
      name
      image
      isOnline
      messages(orderBy: { createdAt: desc }, skip: $skip, take: 20) {
        id
        content
        createdAt
        chatRoom {
          id
        }
        user {
          image
          name
          id
        }
        seenBy {
          id
          createdAt
          user {
            id
            name
          }
        }
      }
    }
  }
`;

export const CREATE_ROOM = gql`
  mutation CREATE_ROOM($data: ChatRoomCreateInput!) {
    createOneChatRoom(data: $data) {
      id
      slug
      name
      image
      isOnline
      createdAt
      updatedAt
      members {
        image
        id
        name
        rank
      }
    }
  }
`;

export const GET_USER_CHATROOMS = gql`
  query getUserChatRooms($userId: String, $skip: Int) {
    user(where: { id: $userId }) {
      id
      chatRooms(orderBy: { updatedAt: desc }, skip: $skip, take: 10) {
        id
        slug
        name
        image
        isOnline
        createdAt
        updatedAt
        members {
          image
          id
          name
          rank
        }
      }
    }
  }
`;

export const SEARCH_USERS = gql`
  query SEARCH_USERS($searchText: String) {
    searchUsers(searchText: $searchText) {
      id
      name
      email
      image
      isOnline
      rank
    }
  }
`;
