import { useMediaQuery } from "@chakra-ui/react";

export default function useMQ() {
  let isMobile = false;
  let isLargerThan1280 = false;
  const ssrMode = typeof window === "undefined";
  if (!ssrMode) {
    // isMobile = useBreakpointValue({ base: true, md: false });
    [isLargerThan1280] = useMediaQuery("(min-width: 1280px)");
    [isMobile] = useMediaQuery("(max-width: 750px)");
  }
  return { isMobile, isLargerThan1280 };
}
