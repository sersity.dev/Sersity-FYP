import { Post, User } from "../interfaces";

/** Dummy user data. */
export const sampleUserData: User[] = [
  { id: 101, name: "Alice" },
  { id: 102, name: "Bob" },
  { id: 103, name: "Caroline" },
  { id: 104, name: "Dave" },
];

export const posts: Post[] = [
  {
    id: 5,
    user_id: 3,
    title:
      "Magni strenuus vesper ventosus cursim strues conatus succedo balbus adhaero veniam stillicidium vicissitudo ultra carmen valetudo valeo.",
    body:
      "Cuius aggero quia. Quo approbo substantia. Via ager alias. Armarium ver sequi. Cauda stillicidium ulterius. Concido thermae asper. Torrens sollers trepide. Arx turba itaque. Quia auditor thymum. Adultus custodia unde. Depereo sapiente subseco. Debitis minima unus. Culpa eaque cado. Dolorum tepesco sapiente. Explicabo defungo exercitationem. Substantia crux aestas. Asporto theca subito. Velum cras defleo. Cubicularis pecus amor. Vivo abduco tyrannus. Cupiditas sublime corrigo. Auctus conatus impedit. Suus ocer cometes. Convoco strues est. Adhuc aut avaritia. Adfectus ultra denique. Triduana pecunia curo. Viscus tremo porro.",
    created_at: "2021-02-26T03:50:04.263+05:30",
    updated_at: "2021-02-26T03:50:04.263+05:30",
  },
  {
    id: 6,
    user_id: 3,
    title: "Tactus qui quasi valens sperno vestigium quis corpus sum.",
    body:
      "Vulariter articulus quibusdam. Spero quasi deinde. Callide adipisci vulgus. Defleo ancilla ubi. Curiositas ea deserunt. Substantia animi sub. Corrumpo tremo tamdiu. Villa carbo cometes. Quod advenio cerno. Censura ceno tubineus. Absconditus causa triumphus. Comburo sol nulla. Tergiversatio calculus vulnero. Subvenio charisma confero. Cohors utilis vaco. Eum dapifer omnis. Subnecto averto acsi. Defendo conspergo acerbitas. Timor tandem eum. Sono taceo depono. Vivo adsuesco aqua. Dolor crepusculum arca. Vigor adopto vinculum. Tempore calamitas ut. Velum utilis amplexus.",
    created_at: "2021-02-26T03:50:04.273+05:30",
    updated_at: "2021-02-26T03:50:04.273+05:30",
  },
  {
    id: 7,
    user_id: 4,
    title: "Audacia ancilla cura corporis chirographum tolero.",
    body:
      "Stillicidium calamitas strues. Carmen agnitio nemo. Tracto quas avaritia. Cubo demulceo reiciendis. Communis adamo spiculum. Una video atque. Pecus creta casso. Aperte anser concido. Et sit copia. Coerceo sed casus. Qui tam voluptas. Aut despecto solus. Quod verecundia avarus. Celo et tutamen. Conatus accusamus nulla. Aestivus derideo tremo. Celebrer verumtamen vox. Concido casus aut. Vis claudeo sumo. Acidus adficio capio.",
    created_at: "2021-02-26T03:50:04.292+05:30",
    updated_at: "2021-02-26T03:50:04.292+05:30",
  },
  {
    id: 8,
    user_id: 4,
    title:
      "Capitulus et adulescens commemoro congregatio peccatus beneficium voluntarius coaegresco quas reprehenderit nulla cogo decretum vomer consequatur unde.",
    body:
      "Aranea substantia tardus. Itaque cunae amplitudo. Canonicus amplitudo apostolus. Caelestis temporibus trado. Absorbeo vestigium celo. Vilis tersus bis. Amicitia terebro tener. Vaco vulticulus via. Complectus fugiat sum. Summa desino videlicet. Undique sortitus auxilium. Cilicium odit caries. Uberrime voro nostrum. Tego illum aequitas. Iusto sit natus. Thorax quae omnis. Solutio abeo tenus. Dolor tamquam temperantia. Armo delectus censura. Cras tempore curtus. Comptus tricesimus comprehendo. Praesentium aperte beatus. Bonus eos vita. Autem deporto dolor. Ager volaticus deputo. Tempora vulariter soluta. Verecundia accommodo supra.",
    created_at: "2021-02-26T03:50:04.298+05:30",
    updated_at: "2021-02-26T03:50:04.298+05:30",
  },
  {
    id: 12,
    user_id: 7,
    title:
      "Odit temptatio spiculum vel nobis amor undique cognomen provident attollo coadunatio solvo ancilla et.",
    body:
      "Eius sursum nesciunt. Aggero debeo charisma. Deputo sophismata virtus. Capillus peior aspernatur. Amo timor ut. Corroboro considero vulnero. Cura angulus eum. Agnitio tenus curvo. Verbum tepidus unus. Iste strenuus testimonium. Viscus triduana auris. Denique aequitas vox. Quod excepturi vicinus. Caelestis reprehenderit thorax. Video tam viduata. Tandem patruus socius. Benevolentia tactus ut. Ciminatio arbitro canonicus.",
    created_at: "2021-02-26T03:50:04.352+05:30",
    updated_at: "2021-02-26T03:50:04.352+05:30",
  },
  {
    id: 13,
    user_id: 9,
    title:
      "Infit corrigo valens avaritia aspernatur adamo vorax deserunt coerceo nemo caput eaque.",
    body:
      "Amo consequatur voluptates. Vergo dolor vulticulus. Conatus abduco vulgivagus. Ultio comptus curriculum. Acervus voluptatem desolo. Copia angulus demergo. Stipes vomer utroque. Aestivus suus usus. Cenaculum voveo alias. Comes aut bellum. Labore accedo viduo. Damnatio sunt conscendo. Voluptatibus non eveniet. Voluptates arbor iure. Certo impedit occaecati. Dolor absens voluptate. Ratione sed voluptatem. Carus impedit timidus. Valeo saepe ambulo. Casso cogito tutis. Adhuc valetudo tempus. Magnam non cernuus. Beatae derideo spoliatio.",
    created_at: "2021-02-26T03:50:04.374+05:30",
    updated_at: "2021-02-26T03:50:04.374+05:30",
  },
  {
    id: 14,
    user_id: 9,
    title:
      "Verecundia tego creptio vilis a crustulum sulum accedo cito cuppedia uberrime apud subvenio speciosus bardus umbra conspergo verto vociferor.",
    body:
      "Sperno amicitia adimpleo. Numquam vinitor trans. Tabella ancilla utor. Constans turpe stipes. Defaeco usitas vesper. Subiungo adsum tendo. Consuasor conspergo traho. Cunae sed ratione. Aut videlicet uberrime. Tamquam demonstro et. Provident averto tenax. Calco veritatis arbitro. Adsidue suus corrigo. Quia officia virga. Bene pectus tibi. Illo umerus coniuratio. Colo utrimque surculus.",
    created_at: "2021-02-26T03:50:04.382+05:30",
    updated_at: "2021-02-26T03:50:04.382+05:30",
  },
  {
    id: 17,
    user_id: 11,
    title:
      "Stips tepidus conitor alioqui nihil dolorem admoneo arto copiose totidem adeo arceo carpo blanditiis supellex debeo correptius.",
    body:
      "Temeritas theologus patior. Fugit cruciamentum culpa. Testimonium tertius consequuntur. Nisi verbera sit. Cupio voluptas corrumpo. Universe conventus arceo. Stipes contego error. Candidus ea sollers. Cupiditate truculenter audeo. Utrimque desidero deludo. Victus vobis voro. Deprimo carbo sodalitas. Quas tui verus. Tunc titulus derelinquo. Sed cuppedia viduata. Voluptates ex aspicio. Crustulum blandior aqua. Crustulum summisse sed. Utor curvo adhuc. Temptatio tamen beneficium. Crudelis vulgivagus conforto. Tollo confido abutor. Alioqui thymbra toties. Vel amo vigilo. Adeo spectaculum accusamus. Crinis aequitas quia. Solio caelestis omnis. Omnis crepusculum cogito.",
    created_at: "2021-02-26T03:50:04.430+05:30",
    updated_at: "2021-02-26T03:50:04.430+05:30",
  },
  {
    id: 19,
    user_id: 13,
    title:
      "Comburo expedita circumvenio arca casso terminatio pariatur et decerno acceptus.",
    body:
      "Abstergo tribuo vindico. Absque ullus amplitudo. Claudeo tener socius. Consuasor aeternus caritas. Deserunt colo blanditiis. Tracto curto debitis. Arca demoror cuius. Pauci aspicio celebrer. Autem tui adicio. Qui culpa blandior. Tersus ciminatio articulus. Bellicus thema illum. Id pectus pecus. Usus utrum cito. Auctor solitudo apud. Adinventitias iste dolorum. Sol sequi comptus. Cedo delectus error. Accedo adamo est. Deserunt teres numquam. Umbra absum cursim. Clementia theca doloremque. Nam candidus adduco. Speciosus aptus cogo. Vacuus sit voluntarius. Adflicto bardus dolores.",
    created_at: "2021-02-26T03:50:04.456+05:30",
    updated_at: "2021-02-26T03:50:04.456+05:30",
  },
  {
    id: 20,
    user_id: 13,
    title:
      "Voluntarius viridis adsum rem auctor qui delectus ulterius deripio cenaculum considero depromo vacuus custodia alioqui stella.",
    body:
      "Quo qui ventito. Vobis deleniti cresco. Adaugeo amaritudo demitto. Venustas coruscus tertius. Uberrime tristis antea. Doloremque alveus decerno. Coaegresco ceno terebro. Cogo deprecator ceno. Tenax altus benigne. Cornu ratione ipsa. Tabernus demoror deserunt. Cresco velum beneficium. Cruentus laboriosam vulgus. Aiunt et theologus. Asporto adeo ceno. Cornu et administratio. Pariatur templum comitatus. Doloribus circumvenio dolores. Nihil umerus capitulus. Canis ter admoveo.",
    created_at: "2021-02-26T03:50:04.464+05:30",
    updated_at: "2021-02-26T03:50:04.464+05:30",
  },
  {
    id: 21,
    user_id: 17,
    title:
      "Unde minus curto stella cursus corporis vehemens torqueo creo attero est voluptatem verbera vesper earum canonicus et deserunt xiphias.",
    body:
      "Laudantium magnam talus. Curso cognatus pel. Depereo utrimque vero. Strues admiratio ambulo. Utroque coniuratio maxime. Vesica tabesco deduco. Eos claustrum capio. Laboriosam cras dolores. Dicta calcar vomito. Suggero aiunt deficio. Vacuus atrocitas auctus. Trado adversus conicio. Convoco tactus tricesimus. Carbo consequuntur aranea. Tepidus valeo curiositas. Vulariter accendo ut. Atrocitas audax decet.",
    created_at: "2021-02-26T03:50:04.514+05:30",
    updated_at: "2021-02-26T03:50:04.514+05:30",
  },
  {
    id: 22,
    user_id: 17,
    title:
      "Callide summopere avarus adsidue vomica textor advoco vita traho aedificium ocer vigilo angulus aperio sit cohors.",
    body:
      "Carus aranea vitae. Calculus unde absque. Quisquam similique acidus. Spero vado corrigo. Aeternus despecto viriliter. Adsuesco abscido coaegresco. Amor apostolus demonstro. Sono thymum addo. Toties consequuntur tenus. Est coadunatio validus. Tempora peior tam. Amitto deduco demitto. Arguo ut beneficium. Angustus acerbitas sublime. Deporto cibo versus. Cauda considero adstringo. Acquiro coepi comes. Decumbo ut neque. Crustulum triginta audio. Validus curatio cernuus. Inflammatio absque adnuo. Cupiditas virtus contigo. Aufero dolore brevis. Unde officiis cohors. Quia amor et. Toties cognatus vulgus. Ascisco cresco statua.",
    created_at: "2021-02-26T03:50:04.527+05:30",
    updated_at: "2021-02-26T03:50:04.527+05:30",
  },
  {
    id: 23,
    user_id: 18,
    title:
      "Suppono aut animi reiciendis apparatus avoco pecto vindico comptus vilitas absque qui placeat facere curia calco tamen terebro.",
    body:
      "Quas canis varius. Adiuvo doloribus corroboro. Capto turpe cattus. Ratione terebro viduata. Tempus mollitia textor. Crepusculum delectus temporibus. Architecto debitis vicinus. Amaritudo ipsum termes. Totidem claustrum ait. Tardus comes ter. Triduana argentum aut. Fuga quas abstergo. Vomer abbas modi. Vomito corpus utroque. Approbo via conservo. Video auris approbo. Provident tum truculenter. Coerceo adinventitias cum. Ager totidem doloremque. Curso velum ancilla. Clibanus decipio cognatus. Aperiam deserunt ut. Sequi viridis usus. Apto viriliter tolero. Acquiro tripudio carus. Suus amet comptus. Cauda rerum decor. Ducimus conservo peccatus.",
    created_at: "2021-02-26T03:50:04.545+05:30",
    updated_at: "2021-02-26T03:50:04.545+05:30",
  },
  {
    id: 24,
    user_id: 19,
    title:
      "Demergo quo ter texo spes acquiro cumque spiritus thymbra thesis ciminatio aspernatur quo articulus benigne.",
    body:
      "Coerceo reiciendis spectaculum. Terga considero crapula. Tertius theologus articulus. Adaugeo cribro rerum. Deserunt attollo undique. Atrocitas vero curvo. Sponte valetudo decet. Viduo tenus sumptus. Certo delectus cribro. Utrimque cupressus defungo. Vapulus acsi voluptate. Unde textilis confido. Totam et suffragium. Sto accusamus vulariter. Nobis tenetur cultellus. Adinventitias stipes titulus. Timidus thalassinus infit. Attero villa quis. Audeo patruus amo. Damnatio accedo antea. Communis torrens clibanus. Possimus tepidus degusto. Amoveo veritatis cursus. Tametsi canto suspendo. Admoveo cupio comedo. Quis repudiandae tunc. Advoco aureus cupressus. Una socius tibi. Approbo reprehenderit aut.",
    created_at: "2021-02-26T03:50:04.558+05:30",
    updated_at: "2021-02-26T03:50:04.558+05:30",
  },
  {
    id: 28,
    user_id: 23,
    title:
      "Deripio vigor templum surculus comburo tui demens curto tepesco repellat tabula voluptatem delego crustulum theologus libero thymbra celebrer tego.",
    body:
      "Arto sperno conatus. Alius esse bellicus. Atrox ceno provident. Claustrum umerus vix. Abduco voluptas tenetur. Spero constans sapiente. Culpa demitto conicio. Iusto volva aiunt. Voluptatibus vaco tametsi. Ut alveus sit. Caritas corona ater. Tabgo acies clarus. Ter repudiandae nulla. Soluta odit truculenter. Alveus benevolentia quo.",
    created_at: "2021-02-26T03:50:04.624+05:30",
    updated_at: "2021-02-26T03:50:04.624+05:30",
  },
  {
    id: 30,
    user_id: 28,
    title:
      "Defleo est solvo campana astrum aperio spoliatio tolero appono eos aegre voluntarius paens suffoco.",
    body:
      "Tum adeptio crustulum. Vos tricesimus vel. Cumque deporto aestivus. Arcesso teneo apparatus. Charisma ambitus asperiores. Ventosus altus voco. Ter tonsor adeo. Demoror candidus adsuesco. Validus aufero deduco. Acies turbo quia. Stabilis dolores agnosco. Qui adfectus sed. Tenus animi dens. Et cavus sed. Triumphus similique tollo. Adnuo sint asper. Depromo eius nihil. Sit civis advenio. Ad defendo maxime. Blanditiis conventus tametsi. Qui audeo eaque. Inflammatio sui omnis. Ipsam volubilis vestrum. Solus patria adultus. Quaerat corrupti theatrum.",
    created_at: "2021-02-26T03:50:04.673+05:30",
    updated_at: "2021-02-26T03:50:04.673+05:30",
  },
  {
    id: 31,
    user_id: 30,
    title:
      "Tabesco tot eligendi spero et audio depraedor libero adduco tergiversatio.",
    body:
      "Ustulo tergum tondeo. Caput patruus decens. Vesper causa utor. Vicissitudo ipsum subito. Depereo aufero beatus. Charisma copia trado. Truculenter reprehenderit averto. Aliqua advenio viriliter. Verto aestas canto. Appono vulnero rerum. Harum umquam tum. Vaco curo depereo. Vix degero amissio. Arceo tutis demoror. Tepidus error aut.",
    created_at: "2021-02-26T03:50:04.693+05:30",
    updated_at: "2021-02-26T03:50:04.693+05:30",
  },
  {
    id: 32,
    user_id: 30,
    title:
      "Quae barba tristis coniecto abutor deprimo odit subnecto calcar voveo est animus utilis quia.",
    body:
      "Concedo aestivus tero. Aut auctor velut. Suffragium toties commodi. Acerbitas degero avarus. Ver ullam vorago. Validus comitatus admoneo. Aggredior super solitudo. Maiores theatrum ars. Collum nihil absens. Celebrer est defungo. Natus vae vicissitudo. Aperio curriculum coniecto. Aveho molestiae rerum. Caelum sophismata tempora. Calamitas dens angulus. Ante coruscus victus. Spiritus asper velit. Cur suspendo subnecto. Vulnus cras demum. Amplitudo vulgo ultra. Amiculum suffragium viriliter. Vulgaris aufero conor. Quibusdam quas non. Odit abeo sequi. Vel aestas culpa. Comminor adsidue argentum. Creator carpo avoco. Curso clamo commodo.",
    created_at: "2021-02-26T03:50:04.705+05:30",
    updated_at: "2021-02-26T03:50:04.705+05:30",
  },
  {
    id: 33,
    user_id: 31,
    title:
      "Claro quis argumentum depulso provident sit adhuc quisquam ut pax despecto amitto suscipit studio.",
    body:
      "Attonbitus aut capto. Incidunt curo asper. Cupiditas atque apparatus. Cena casus desidero. Annus sonitus tersus. Ex artificiose comptus. Depulso contego cupio. Cometes vomer vulgaris. Deludo voluntarius clamo. Iste cupressus consequatur. Apostolus cunabula thesaurus. Caveo alii cultura. Quaerat succurro asperiores. Sapiente sol accusator. Eos accusamus verecundia. Cum dolores aeternus. Venio deporto tondeo. Bellicus clam soluta. Vitae demergo curo. Tabesco debeo quos. Texo temeritas calco. Libero sit vinco. Avoco et damno.",
    created_at: "2021-02-26T03:50:04.728+05:30",
    updated_at: "2021-02-26T03:50:04.728+05:30",
  },
  {
    id: 34,
    user_id: 32,
    title:
      "Deduco provident cattus venustas depromo curia altus vero clarus victoria somniculosus dedecor defero ut capitulus crastinus tero arto.",
    body:
      "Maxime vorago saepe. Volutabrum corrupti ara. Est abutor artificiose. Nostrum caput tabula. Officia tumultus commodi. Toties studio comprehendo. Comptus stultus damnatio. Cruciamentum conculco agnosco. Templum conventus capillus. Sit eius communis. Synagoga beneficium illum. Aequus commodo adamo. Umquam voluptatum dolor. Stultus conspergo dolor. Custodia textor animus. Sit vinum amplus. Defigo aegrotatio voluptas. Bellicus bestia ara.",
    created_at: "2021-02-26T03:50:04.753+05:30",
    updated_at: "2021-02-26T03:50:04.753+05:30",
  },
];
