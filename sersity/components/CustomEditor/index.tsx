import React, { useEffect, useRef, useState } from 'react';
import {Editor, EditorState, RichUtils} from 'draft-js';
import 'draft-js/dist/Draft.css';
import { Box, Button, Flex } from '@chakra-ui/react';



function CustomEditor() {
  const editorRef = useRef()
  const [editorState, setEditorState] = useState(
    () => EditorState.createEmpty(),
  );
  console.log(editorState.getCurrentContent().getPlainText()); 

  const inlineStyles = [
    { label: "<b>B</b>", style: "BOLD" },
    { label: "<i>I</i>", style: "ITALIC" },
    { label: "<p>U</p>", style: "UNDERLINE" },
    { label: "<strike>S</strike> ", style: "STRIKETHROUGH" },
  ];
  
  const [ready, setReady] = useState(false)
  
  function applyStyle(style,ref) {
       
    // ref.current.focus();
    setEditorState(RichUtils.toggleInlineStyle(editorState, style));
    
  }

  useEffect(() => {
    setReady(true)
  }, )

  return (
      <Box border="1px solid red" background="white" h="500px">
        {ready && (
          <>  
              <Flex justifyContent="space-between" p={3}>
                {inlineStyles.map((e,i) => (
                  <Button key={i} onClick={() => applyStyle(e.style, editorRef)} mb={3}><div  dangerouslySetInnerHTML={{__html:e.label}}></div></Button>
                ))}
              </Flex>
             <Editor 
              editorState={editorState} 
              editorKey="editor" 
              onChange={setEditorState} 
              ref={editorRef}  
              placeholder="Write something!"/>
          </>
        )}
       
     </Box>
    );
}

export default CustomEditor;