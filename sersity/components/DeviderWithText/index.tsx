import {
  Box,
  Divider,
  Flex,
  FlexProps,
  useColorModeValue as mode,
} from "@chakra-ui/react";
import * as React from "react";

export const DividerWithText = (props: FlexProps) => (
  <Flex align="center" color="teal.300" {...props}>
    <Box flex="1">
      <Divider borderColor={mode("teal.400", "teal.300")} />
    </Box>
    <Box
      as="span"
      px="3"
      color={mode("teal.400", "teal.300")}
      fontWeight="medium"
    >
      {props.children}
    </Box>
    <Box flex="1">
      <Divider borderColor={mode("teal.400", "teal.300")} />
    </Box>
  </Flex>
);
