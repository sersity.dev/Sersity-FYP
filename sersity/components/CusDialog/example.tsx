import { Button } from "@chakra-ui/button";
import { useDisclosure } from "@chakra-ui/hooks";
import LoginSupign from "components/LoginSignup";
import CusDialog from ".";

export default function index() {
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    //
    <CusDialog
      isOpen={isOpen}
      onOpen={onOpen}
      onClose={onClose}
      //it will show button for click to open dialog
      ui={<Button onClick={onOpen}> Open</Button>}
      headerText="Sersity"
      header={true}
    >
      <LoginSupign />
      {/* it will show when dialog is opened */}
    </CusDialog>
  );
}
