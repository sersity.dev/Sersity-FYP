import { Button } from "@chakra-ui/button";
import { useDisclosure } from "@chakra-ui/hooks";
import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from "@chakra-ui/modal";
import React from "react";

interface Props {
  children: any;
  isOpen: boolean;
  onOpen: any;
  onClose: any;
  header: boolean;
  headerText: string;
  ui: any;
}

function CusDialog({
  children,
  isOpen,
  onClose,
  header,
  headerText,
  ui,
}: Props) {
  return (
    <>
      {ui}
      <Modal isOpen={isOpen} onClose={onClose} isCentered>
        <ModalOverlay />
        <ModalContent>
          {header && (
            <>
              <ModalHeader>{headerText}</ModalHeader>
              <ModalCloseButton />
            </>
          )}

          <ModalBody mb={4}>{children}</ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
}

export default CusDialog;
