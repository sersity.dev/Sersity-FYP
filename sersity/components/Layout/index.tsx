import { Box } from "@chakra-ui/layout";
import dynamic from "next/dynamic";
import NextNProgress from "nextjs-progressbar";

const Navbar = dynamic(() => import("./Navbar"), { ssr: false });
const Topbar = dynamic(() => import("./Topbar"), { ssr: false });

const TopAndNav = (props) => {
  const layout = props.layout ? props.layout : { navBar: true, topBar: true };

  return (
    <>
      {layout.topBar && (
        <>
          <Topbar layout={layout} />
          <Box h={[58, 74]} />
        </>
      )}

      {layout.navBar && <Navbar />}
    </>
  );
};

const Layout = ({ layout, children, ...rest }) => {
  // useOnlineUserIndicator();
  return (
    <>
      {layout === "none" ? (
        children
      ) : (
        <>
          <NextNProgress
            color="#38b2ac"
            startPosition={0.4}
            stopDelayMs={100}
            height={3}
            options={{ showSpinner: false }}
          />
          <TopAndNav layout={layout} {...rest} />
          {children}
          <Box py={5} mt={7} display={["flex", "none"]} />
        </>
      )}
    </>
  );
};

export * from "./CustomLayout";
export default Layout;
