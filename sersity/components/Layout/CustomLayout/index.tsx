import { Box, Flex, Grid } from "@chakra-ui/react";
import PageMotion from "components/PageMotion";
import useMQ from "hook/useMQ";

CustomLayout.defaultProps = {
  children: null,
  cProps: null,
  right: null,
  left: null,
  rProps: null,
  lProps: null,
  props: null,
  pageMotion: true,
};

export function CustomLayout({
  children,
  cProps,
  right,
  rProps,
  left,
  lProps,
  props,
}) {
  const { isMobile } = useMQ();

  return (
    <PageMotion>
      <Grid
        templateColumns={[
          "1fr",
          "1fr",
          "0.35fr 2fr 1fr",
          "0.5fr 2fr 1fr",
          "1fr 1.25fr 1fr",
        ]}
        gap={0}
        {...props}
      >
        <Flex display={["none", "none", "inherit"]} {...lProps}>
          {left}
        </Flex>
        <Box as="main" {...cProps}>
          {isMobile && (
            <Flex w="100%" {...rProps}>
              {right}
            </Flex>
          )}
          {children}
        </Box>
        <Flex display={["none", "none", "inherit"]} {...rProps}>
          {!isMobile && right}
        </Flex>
      </Grid>

      {/* <Flex display={["none", "none", "inherit"]} {...lProps}>
          {left}
        </Flex>
        <Box as="main">{children}</Box>
        <Box flex={1}> {right}</Box> */}
    </PageMotion>
  );
}
