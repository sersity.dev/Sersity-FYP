import {
  Box,
  Flex,
  IconButton,
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  Spinner,
  useColorModeValue as mode,
  useDisclosure,
} from "@chakra-ui/react";

import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { BiArrowBack, BiSearchAlt } from "react-icons/bi";
import { motion } from "framer-motion";
import { useTranslation } from "hook/useTranslation";
import useMQ from "hook/useMQ";

export function InputSearch(props) {
  const { ...rest } = props;
  const { isOpen, onToggle } = useDisclosure();
  const { isMobile } = useMQ();
  return (
    <>
      {!isMobile ? (
        <InputSearchUI />
      ) : (
        <Box {...rest}>
          <IconButton
            aria-label="search"
            fontSize="1.3em"
            variant="ghost"
            onClick={onToggle}
          >
            <BiSearchAlt color="gray.300" />
          </IconButton>
          <Flex
            zIndex="20"
            bg={mode("white", "gray.700")}
            w="100%"
            h="54px"
            transition="0.2s all ease"
            position="fixed"
            top={isOpen ? 0 : "-54px"}
            left={0}
            // borderRight={isOpen ? 0 : "100%"}
            align="center"
          >
            <IconButton
              ml={2}
              as={motion.button}
              variant="ghost"
              aria-label="back"
              onClick={onToggle}
              display={["inherit", "none"]}
              whileHover={{ scale: 1.1 }}
              whileTap={{ scale: 0.9 }}
            >
              <BiArrowBack fontSize="1.4em" />
            </IconButton>
            <InputSearchUI display="inherit" />
          </Flex>
        </Box>
      )}
    </>
  );
}

const InputSearchUI = (p) => {
  const { ...rest } = p;
  const { t } = useTranslation();
  const { push } = useRouter();
  const [value, setValue] = useState(undefined);
  const [isSearching, setIsSearching] = useState(false);
  useEffect(() => {
    if (value !== undefined) {
      push("/search?keyword=" + value);
      setValue(undefined);
    }
    const delay = require("lodash/delay");
    delay(() => setIsSearching(false), 500);
  }, [value]);
  const debounce = require("lodash/debounce");
  return (
    <InputGroup px={4}>
      <InputLeftElement
        pointerEvents="none"
        children={<BiSearchAlt />}
        fontSize="1.3em"
        color="gray.500"
        ml={5}
        {...rest}
      />
      <Input
        autoComplete="off"
        focusBorderColor="teal.500"
        placeholder={t("search")}
        variant="filled"
        name="search keyword"
        onChange={debounce((e) => {
          setIsSearching(true);
          setValue(e.target.value);
        }, 700)}
        {...rest}
      />
      <InputRightElement
        children={
          <Spinner
            size="sm"
            display={isSearching ? "inherit" : "none"}
            color="teal.500"
            marginRight="10px"
          />
        }
      />
    </InputGroup>
  );
};
