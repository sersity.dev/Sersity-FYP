import { Button, IconButton } from "@chakra-ui/button";

import { Image } from "@chakra-ui/image";
import { InputGroup } from "@chakra-ui/input";
import { Box, Flex, Grid, Text } from "@chakra-ui/layout";

import { useColorModeValue as mode } from "@chakra-ui/react";
import React from "react";

import { useRouter } from "next/router";
import { useSession } from "next-auth/client";
import { InputSearch } from "./InputSearch";
import Notification from "components/Notification";
import MenuUser from "components/MenuUser";
import { BeatLoader } from "react-spinners";
import { useTranslation } from "hook/useTranslation";
import { BiArrowBack } from "react-icons/bi";
import ColorModeSwitcher from "components/ColorModeSwitcher";
import ChangeLocale from "components/ChangeLocale";
import useMQ from "hook/useMQ";
import Loading from "components/Loading";

// 1. Import `extendTheme`

const Topbar = (props) => {
  const { t } = useTranslation();
  const [session, loading] = useSession();
  const { push, back } = useRouter();
  const { isMobile, isLargerThan1280 } = useMQ();

  if (props.layout.isBack && isMobile)
    return (
      <>
        <Flex
          p="2"
          position="fixed"
          top={0}
          zIndex={10}
          w="100%"
          h="58px"
          bg={mode("white", "gray.700")}
          alignItems="center"
          justify="space-between"
        >
          <IconButton
            aria-label="backButton"
            variant="ghost"
            onClick={(e) => {
              e.preventDefault();
              back();
            }}
          >
            <BiArrowBack fontSize="1.3em" />
          </IconButton>
          <Text fontSize={18}>{props.layout.title}</Text>
          <ColorModeSwitcher />
        </Flex>
      </>
    );

  return (
    <Box
      pos="fixed"
      w="100%"
      h={["58px", "74px"]}
      display="flex"
      alignItems="center"
      zIndex={11}
      top={0}
      px={[2, 4]}
      boxShadow={["sm", "none"]}
      bg={mode("white", "gray.700")}
      id="sersity-topbar"
    >
      <Grid
        width="100%"
        templateColumns={[
          "auto 1fr 1fr",
          "2fr 1fr 1fr",
          "auto 1fr 1fr",
          "repeat(3, 2fr)",
        ]}
        gap={0}
      >
        <Flex pl="5px" justify="space-between">
          <Flex
            alignItems="center"
            _hover={{ cursor: "pointer" }}
            onClick={() => push("/home")}
          >
            {/* {isMobile ? } */}

            <Image
              h={["30px", "42px"]}
              w={["30px", "120px"]}
              objectFit="cover"
              fallback={<Loading />}
              src={isMobile ? "/icons/logo.png" : "/images/sersity300.png"}
              alt="Sersity"
            />
          </Flex>
        </Flex>
        <Flex justify="center" w="100%">
          <Box w={isLargerThan1280 ? "container.md" : ["100%"]}>
            <InputSearch />
          </Box>
        </Flex>
        <Flex>
          <InputGroup justifyContent="flex-end">
            <Flex align="center">
              <ChangeLocale />
            </Flex>

            <ColorModeSwitcher />

            {loading ? (
              <Button isLoading spinner={<BeatLoader size={8} color="white" />}>
                SignOut
              </Button>
            ) : !session ? (
              <>
                <Button
                  color="teal.500"
                  // border="2px solid rgba(133, 147, 166, 0.1)"
                  variant="ghost"
                  borderRadius="5px"
                  borderEndRadius={["5px", "0px"]}
                  onClick={() => push("/auth/login")}
                >
                  {t("log in")}
                </Button>
                <Button
                  color="white"
                  bg="teal.500"
                  borderRadius="5px"
                  variant="ghost"
                  display={["none", "inherit"]}
                  onClick={() => push("/auth/signup")}
                >
                  {t("sign up")}
                </Button>
              </>
            ) : (
              <>
                <Box display={["none", "inherit"]}>
                  <Notification />
                </Box>
                <MenuUser user={session.user} />
              </>
            )}
          </InputGroup>
        </Flex>
      </Grid>
    </Box>
  );
};

export default Topbar;
