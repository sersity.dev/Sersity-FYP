import React, { useState } from "react";
import { useColorModeValue as mode } from "@chakra-ui/react";

import { Box, HStack, Stack, VStack, Flex } from "@chakra-ui/layout";
import { FiSettings } from "react-icons/fi";
import { CgLoupe, CgHomeAlt, CgAlbum, CgPoll } from "react-icons/cg";
import { useRouter } from "next/router";
import { useSession } from "next-auth/client";
import { RiChatSmile2Line } from "react-icons/ri";
// import Chat from "./ChatDialog";
import CreatePostDialog from "components/CreatePostDialog";
import { useTranslation } from "hook/useTranslation";
import ListDraft from "components/ListDraft";
import useCreatePostModal from "hook/useCreatePostModal";
import { VscBell } from "react-icons/vsc";
import CountNewNoti from "components/Notification/CountNewNoti";
import useMQ from "hook/useMQ";
import useNotiSub from "hook/useNotiSub";
import { useChatDialog } from "hook/useChatDialog";

const useStyles = () => ({
  navLink: {
    justifyContent: { base: "center", md: "center", lg: "flex-start" },
    // borderRadius: ["8px", "0", "0"],
    borderLeftRadius: 8,
    borderRightRadius: [8, 8, 0],
    width: ["20%", "100%", "100%"],
    height: ["3rem", "3rem", "auto"],
    padding: {
      md: " 0.5rem 1.2rem 0.5rem 0.5rem",
      lg: "0.5rem 1.5rem 0.5rem 0.7rem",
    },

    // color: `${colorMode === "light" ? "gray.600" : "gray.100"}`,
    fontWeight: "500",
    "&:hover": {
      color: mode("teal.500", "teal.200"),
      cursor: "pointer",
    },
    "& svg": {
      fontSize: { base: "1.6rem", md: "1.6rem", lg: "1.4rem" },
    },
  },
});
const Rule = ({ children }) => {
  const { isMobile } = useMQ();
  return isMobile ? (
    <> {children}</>
  ) : (
    <VStack as="ul" w="100%" alignItems="start">
      {children}
    </VStack>
  );
};

const NavTop = (props) => <Rule {...props} />;
const NavMiddle = (props) => <Rule {...props} />;
const NavBottom = (props) => <Rule {...props} />;

function Navbar() {
  const styles = useStyles();
  const { push, pathname } = useRouter();
  const [session] = useSession();
  const { t } = useTranslation();
  const { isMobile } = useMQ();
  const { onSubNoti } = useNotiSub();

  const {
    isOpenCreatePostModal,
    onOpenCreatePostModal,
    onCloseCreatePostModal,
  } = useCreatePostModal();
  // console.log(useCreatePostModal());
  const { isOpenChat, onToggleChat } = useChatDialog();
  const [content, setContent] = useState({
    title: "",
    content: "",
    id: undefined,
    tags: [],
  });

  onSubNoti({ user: session?.user, isOpen: isOpenChat });

  const CreatePostBtn = (
    <>
      <CreatePostDialog
        isOpen={isOpenCreatePostModal}
        onClose={onCloseCreatePostModal}
        content={content}
        setContent={setContent}
      />
      <Flex
        position="relative"
        align="center"
        justify="space-between"
        sx={styles.navLink}
        as="li"
        onClick={() => {
          // console.log(session);
          if (session) {
            setContent({
              title: "",
              content: "",
              id: undefined,
              tags: [],
            });
            onOpenCreatePostModal();
          } else {
            push("/auth/login");
          }
        }}
      >
        {!isMobile && <CgLoupe style={{ zIndex: 1 }} />}
        <Box
          ml={2}
          as="span"
          display={["none", "none", "none", "inline"]}
          textTransform="capitalize"
        >
          {t("create")}
        </Box>
        {isMobile && <CgLoupe style={{ zIndex: 1, fontSize: "1.8rem" }} />}
      </Flex>
    </>
  );
  return (
    <>
      <Stack
        bottom={["0px", "0px", "none"]}
        pos={["fixed"]}
        direction="column"
        zIndex={10}
        h={["auto", "auto", "100%"]}
        w={["100%", "100%", "auto"]}
        pt={["0", "74px"]}
      >
        <Flex
          m={[0, 3]}
          p={[1, 1, 0]}
          h={["auto", "auto", "100%"]}
          pl={[1, 1, 3]}
          borderRadius={10}
          boxShadow={["cus1", "none"]}
          bgColor={mode("white", "gray.700")}
          id="sersity-navbar"
        >
          <Stack
            mt={[0, 0, 3]}
            as="nav"
            w="100%"
            justifyContent="space-between"
            direction={["row", "row", "column"]}
          >
            {!isMobile && <NavTop>{CreatePostBtn}</NavTop>}
            <NavMiddle>
              <HStack
                sx={styles.navLink}
                as="li"
                bg={
                  pathname === "/latest"
                    ? mode("gray.100", "gray.750")
                    : "inherit"
                }
                onClick={() => push("/latest")}
                color={pathname === "/latest" ? "teal.500" : "inherit"}
              >
                <CgPoll />
                <Box
                  as="span"
                  display={["none", "none", "none", "inline"]}
                  textTransform="capitalize"
                >
                  {t("latest")}
                </Box>
              </HStack>

              <HStack
                sx={styles.navLink}
                as="li"
                bg={
                  pathname === "/home"
                    ? mode("gray.100", "gray.750")
                    : "inherit"
                }
                onClick={() => push("/home")}
                color={pathname === "/home" ? "teal.500" : "inherit"}
              >
                <CgHomeAlt />
                <Box
                  as="span"
                  display={["none", "none", "none", "inline"]}
                  textTransform="capitalize"
                >
                  {t("home")}
                </Box>
              </HStack>
              {isMobile && CreatePostBtn}
              {session && (
                <HStack
                  sx={styles.navLink}
                  as="li"
                  bg={
                    pathname === "/chat"
                      ? mode("gray.100", "gray.750")
                      : "inherit"
                  }
                  onClick={!isMobile ? onToggleChat : () => push("/chat")}
                  color={pathname === "/chat" ? "teal.500" : "inherit"}
                >
                  <RiChatSmile2Line />
                  <Box
                    textTransform="capitalize"
                    display={["none", "none", "none", "inline"]}
                  >
                    {t("chat")}
                  </Box>
                </HStack>
              )}
              {session && isMobile && (
                <HStack
                  sx={styles.navLink}
                  as="li"
                  bg={
                    pathname === "/inbox"
                      ? mode("gray.100", "gray.750")
                      : "inherit"
                  }
                  onClick={() => push("/inbox")}
                  display={["inherit", "none"]}
                  position="relative"
                  color={pathname === "/inbox" ? "teal.500" : "inherit"}
                >
                  <Box position="relative">
                    <VscBell />
                    <CountNewNoti top={-1} right={0} />
                  </Box>

                  <Box
                    textTransform="capitalize"
                    display={["none", "none", "none", "inline"]}
                  >
                    {t("inbox")}
                  </Box>
                </HStack>
              )}
              {session && (
                <HStack
                  sx={styles.navLink}
                  as="li"
                  display={["none", "none", "none", "inline"]}
                >
                  <ListDraft
                    openModal={onOpenCreatePostModal}
                    setContent={setContent}
                  />
                </HStack>
              )}
              {session && (
                <HStack
                  display={["none", "inherit"]}
                  sx={styles.navLink}
                  as="li"
                  bg={
                    pathname === "/saved"
                      ? mode("gray.100", "gray.750")
                      : "inherit"
                  }
                  color={pathname === "/saved" ? "teal.500" : "inherit"}
                  onClick={() => push("/saved", undefined, { shallow: true })}
                >
                  <CgAlbum />
                  <Box
                    textTransform="capitalize"
                    display={["none", "none", "none", "inline"]}

                    // onClick={onOpen}
                  >
                    {t("saved")}
                  </Box>
                </HStack>
              )}
            </NavMiddle>
            <NavBottom>
              <HStack
                display={["none", "inherit"]}
                mb={[0, 3]}
                sx={styles.navLink}
                as="li"
                spacing={2}
                bg={
                  pathname === "/setting"
                    ? mode("gray.100", "gray.750")
                    : "inherit"
                }
                color={pathname === "/setting" ? "teal.500" : "inherit"}
                onClick={() => push("/setting")}
              >
                <FiSettings />
                <Box
                  textTransform="capitalize"
                  as="span"
                  display={["none", "none", "none", "inline"]}
                >
                  {t("setting")}
                </Box>
              </HStack>
            </NavBottom>
          </Stack>
        </Flex>
      </Stack>
    </>
  );
}

export default Navbar;
