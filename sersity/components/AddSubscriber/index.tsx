import { Box, Flex } from "@chakra-ui/layout";

import React, { useState } from "react";
import { motion } from "framer-motion";
import { Button } from "@chakra-ui/button";
import { Text } from "@chakra-ui/react";

import { MdNotificationsActive } from "react-icons/md";
import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
} from "@chakra-ui/modal";
import { useDisclosure } from "@chakra-ui/hooks";
import { FormControl, FormLabel } from "@chakra-ui/form-control";
import { Input } from "@chakra-ui/input";

import { gql, useMutation } from "@apollo/client";
import { useToast } from "@chakra-ui/toast";

const ADD_SUB = gql`
  mutation create($data: SubscriberCreateInput!) {
    createOneSubscriber(data: $data) {
      id
    }
  }
`;
function AddSubscriber(props) {
  const toast = useToast();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const initialRef = React.useRef();
  const finalRef = React.useRef();
  const [isSubmited, setIsSubmited] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [email, setEmail] = useState("");
  const [addSubscriber] = useMutation(ADD_SUB, {
    variables: {
      data: {
        email,
      },
    },
    onCompleted() {
      setIsSubmited(true);
      toast({
        title: `Success Subsrcibed`,
        status: "success",
        isClosable: true,
      });
      onClose();
      setIsLoading(false);
    },
    onError(err) {
      console.log(err);
      toast({
        title: `This email address already exists`,
        status: "error",
        isClosable: true,
      });
      onClose();
      setIsLoading(false);
    },
  });

  function handleSubmit(e) {
    e.preventDefault();
    setIsLoading(true);
    addSubscriber();
  }

  return (
    <Box width="280px" {...props}>
      <motion.div
        animate={{
          y: [0, 10, 0, -10],
        }}
        transition={{
          loop: Infinity,
          times: [0, 0.3, 1, 1.5],
          repeatDelay: 0,
          repeatType: "reverse",
        }}
      >
        <Button
          w="100%"
          p="30px"
          bgGradient="linear(to-br, pink.400, red.500)"
          color="white"
          _hover={{ bgGradient: "linear(to-br, pink.600, red.600)" }}
          rightIcon={<MdNotificationsActive fontSize="1.5rem" />}
          fontSize="18px"
          boxShadow="inner"
          onClick={onOpen}
        >
          Get Notified
        </Button>
      </motion.div>

      <Modal
        isCentered
        initialFocusRef={initialRef}
        finalFocusRef={finalRef}
        isOpen={isOpen}
        onClose={onClose}
      >
        <ModalOverlay />
        <ModalContent as="form" onSubmit={handleSubmit}>
          <ModalHeader>Subscription</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            <FormControl>
              <Input
                onChange={(e) => setEmail(e.target.value)}
                required
                type="email"
                focusBorderColor="teal.500"
                ref={initialRef}
                placeholder="Input Email"
                variant="filled"
              />
            </FormControl>
            <Text mt="10px" fontSize="12px" pl="5px" color="red">
              You will get notified when Sersity is officially launched*
            </Text>
          </ModalBody>

          <Flex mx={6} mb={6} justify="space-between">
            <Button
              onClick={() => window.open("https://t.me/sersity", "_blank")}
            >
              Join Telegram Group
            </Button>
            <Button
              isLoading={isLoading}
              ml={4}
              colorScheme="teal"
              type="submit"
            >
              Get Notified
            </Button>
          </Flex>
        </ModalContent>
      </Modal>
    </Box>
  );
}

export default AddSubscriber;
