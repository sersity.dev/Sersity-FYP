
export const helperRank = {
    "& .anime": {
      borderWidth: "3px",
      position: "relative",
      zIndex: 1,
      "&::after": {
        // w: "100%",
        // h: "100%",
        content: `""`,
        borderRadius: "100%",
        position: "absolute",
        top: `calc(-1 * 3px)`,
        left: "calc(-1 * 3px)",
        height: "calc(100% + 3px * 2)",
        width: "calc(100% + 3px * 2)",
        background:
        //   "linear-gradient(60deg, #f79533, #f37055, #ef4e7b, #a166ab, #5073b8, #1098ad, #07b39b, #6fba82)",
          // "linear-gradient(315deg, #7ed6df 0%, #000000 74%)",
          "linear-gradient(315deg, #5ca0f2 0%, #f5f7f6 74%)",
  
  
        zIndex: -1,
        animation: "wufu 3s ease alternate infinite",
        backgroundSize: "300% 300%",
      },
      "@keyFrames  wufu": {
        "0%": {
          backgroundPosition: "0% 50%",
        },
        "50%": {
          backgroundPosition: "100% 50%",
        },
        "100%": {
          backgroundPosition: "0% 50%",
        },
      },
    },

    "& .beginner": {
      borderWidth: "3px",
      position: "relative",
      zIndex: 1,
      "&::after": {
        // w: "100%",
        // h: "100%",
        content: `""`,
        borderRadius: "100%",
        position: "absolute",
        top: `calc(-1 * 3px)`,
        left: "calc(-1 * 3px)",
        height: "calc(100% + 3px * 2)",
        width: "calc(100% + 3px * 2)",
        background:
        //   "linear-gradient(60deg, #f79533, #f37055, #ef4e7b, #a166ab, #5073b8, #1098ad, #07b39b, #6fba82)",
          // "linear-gradient(315deg, #7ed6df 0%, #000000 74%)",
          "linear-gradient(315deg, #5ca0f2 0%, #f5f7f6 74%)",
  
  
        zIndex: -1,
        animation: "wufu 3s ease alternate infinite",
        backgroundSize: "300% 300%",
      },
      "@keyFrames  wufu": {
        "0%": {
          backgroundPosition: "0% 50%",
        },
        "50%": {
          backgroundPosition: "100% 50%",
        },
        "100%": {
          backgroundPosition: "0% 50%",
        },
      },
    },

    "& .anonymous": {
      borderWidth: "3px",
      position: "relative",
      zIndex: 1,
      "&::after": {
        // w: "100%",
        // h: "100%",
        content: `""`,
        borderRadius: "100%",
        position: "absolute",
        top: `calc(-1 * 3px)`,
        left: "calc(-1 * 3px)",
        height: "calc(100% + 3px * 2)",
        width: "calc(100% + 3px * 2)",
        background:
        //   "linear-gradient(60deg, #f79533, #f37055, #ef4e7b, #a166ab, #5073b8, #1098ad, #07b39b, #6fba82)",
          // "linear-gradient(315deg, #7ed6df 0%, #000000 74%)",
          // "linear-gradient(315deg, #5ca0f2 0%, #f5f7f6 74%)",
          // "linear-gradient(147deg, #000000 0%, #04619f 74%)",
          "linear-gradient(147deg, #000000 0%, #434343 74%)",
  
  
        zIndex: -1,
        animation: "wufu 3s ease alternate infinite",
        backgroundSize: "300% 300%",
      },
      "@keyFrames  wufu": {
        "0%": {
          backgroundPosition: "0% 50%",
        },
        "50%": {
          backgroundPosition: "100% 50%",
        },
        "100%": {
          backgroundPosition: "0% 50%",
        },
      },
    },
  }