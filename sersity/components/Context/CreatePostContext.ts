import { createContext } from 'react';

const CreatePostContext = createContext<any>({});

export default CreatePostContext;