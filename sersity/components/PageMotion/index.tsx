import { Box } from "@chakra-ui/layout";
import { usePrefersReducedMotion } from "@chakra-ui/media-query";
import { keyframes } from "@chakra-ui/system";

// interface Props {
//   children: any;
//   pageMotion: boolean;
// }

const fade = keyframes`
  from { opacity: 0;transform: translateY(-8px); }
  to { opacity: 1 ;transform:translateY(0px);}
`;
const PageMotion = (props) => {
  const { children, pageMotion, ...rest } = props;
  const prefersReducedMotion = usePrefersReducedMotion();
  const animation = prefersReducedMotion ? undefined : `${fade} 0.2s ease`;
  if (!pageMotion) return <Box {...rest}>{children}</Box>;
  return (
    <Box animation={animation} {...rest}>
      {children}
    </Box>
  );
};
PageMotion.defaultProps = {
  children: null,
  pageMotion: true,
};
export default PageMotion;
