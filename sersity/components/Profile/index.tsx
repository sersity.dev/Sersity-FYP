import { useMutation } from "@apollo/client";
import { Avatar } from "@chakra-ui/avatar";
import { Button } from "@chakra-ui/button";
import { useColorModeValue as mode } from "@chakra-ui/color-mode";
import { Box, Flex, HStack, Text, VStack } from "@chakra-ui/layout";
import { FOLLOW, UNFOLLOW } from "hook/graphql/user";

import React, { useEffect, useState } from "react";
import { AiOutlineUserAdd, AiOutlineUserDelete } from "react-icons/ai";
import { IoChatbox } from "react-icons/io5";
import _ from "lodash";
import { BiEdit } from "react-icons/bi";
import { RiUserLocationFill, RiFileAddFill } from "react-icons/ri";
import { useRouter } from "next/router";

import { useDisclosure } from "@chakra-ui/react";
import { helperRank } from "components/Context/rankBorder";

import CreatePostDialog from "components/CreatePostDialog";
import { IoIosSchool } from "react-icons/io";
import { BsFillInfoCircleFill } from "react-icons/bs";
import { MdCake } from "react-icons/md";
import moment from "moment";
import { useChatDialog } from "hook/useChatDialog";
import { useSession } from "next-auth/client";
import Loading from "components/Loading";

const ProfileAction = ({ user }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const [content, setContent] = useState({
    title: "",
    content: "",
    id: undefined,
    tags: [],
  });

  const { push } = useRouter();
  const [isAdded, setIsAdded] = useState(user?.isFollowed);

  useEffect(() => {
    setIsAdded(user?.isFollowed);
  }, [user]);

  const [isAdding, setIsAdding] = useState(false);
  const [onFollow] = useMutation(FOLLOW, {
    variables: {
      followingId: user.id,
    },
    onCompleted() {
      setIsAdding(false);
    },
    onError() {
      setIsAdding(false);
    },
  });
  const [onUnFollow] = useMutation(UNFOLLOW, {
    variables: {
      followingId: user.id,
    },
    onCompleted() {
      setIsAdding(false);
    },
    onError() {
      setIsAdding(false);
    },
  });
  // console.log(user);

  const { onToggleChat } = useChatDialog();
  const [session, loading] = useSession();
  return (
    <Flex justify="space-between" w="100%" mt={5}>
      <Flex flex={1} display={user.isMine ? "none" : "block"}>
        <Button
          variant="outline"
          w="100%"
          leftIcon={<IoChatbox fontSize="1.4rem" />}
          isLoading={loading}
          spinner={<Loading />}
          onClick={session ? onToggleChat : () => push("/auth/login")}
        >
          Chat
        </Button>
      </Flex>
      <Flex flex={1} ml={5} display={user.isMine ? "none" : "block"}>
        <Button
          isLoading={isAdding}
          spinner={<Loading />}
          // transform="3s"
          onClick={() => {
            if (user.isAuth) {
              setIsAdding(true);
              !isAdded ? onFollow() : onUnFollow();
              setIsAdded(!isAdded);
            } else {
              push("/auth/login");
            }
          }}
          w="100%"
          colorScheme={isAdded ? "red" : "teal"}
          leftIcon={
            isAdded ? (
              <AiOutlineUserDelete fontSize="1.4rem" />
            ) : (
              <AiOutlineUserAdd fontSize="1.4rem" />
            )
          }
        >
          {isAdded ? "  Unfollow" : "  Follow"}
        </Button>
      </Flex>
      <Flex flex={1} display={user.isMine ? "block" : "none"}>
        <Button
          transform="3s"
          w="100%"
          variant="outline"
          leftIcon={<RiFileAddFill />}
          onClick={() => {
            // isOpenCreatePostModal(true)
            // onOpenCreatePostModal()
            onOpen();
            // console.log(isOpenCreatePostModal());
          }}
        >
          Create Post
          <CreatePostDialog
            isOpen={isOpen}
            onClose={onClose}
            content={content}
            setContent={setContent}
          />
        </Button>
      </Flex>
      <Flex flex={1} ml={5} display={user.isMine ? "block" : "none"}>
        <Button
          transform="3s"
          w="100%"
          colorScheme="teal"
          leftIcon={<BiEdit />}
          onClick={() => push("/setting?option=Profile")}
        >
          Edit Profile
        </Button>
      </Flex>
    </Flex>
  );
};

export default function UserProfile({ user, ...rest }) {
  if (!user) return null;
  // console.log(user);

  return (
    <Flex direction="column" p={[0, 3]} h="100%" w="100%">
      <Flex
        display="flex"
        position="sticky"
        left="0px"
        direction="column"
        p={[3, 5]}
        bg={mode("white", "gray.700")}
        borderRadius="10px"
        align="flex-start"
        {...rest}
      >
        <HStack align="flex-start" w="100%">
          <VStack align="flex-start" justify="space-between" w="100%">
            <Flex alignItems="center" p={3}>
              <Box sx={helperRank}>
                <Avatar
                  boxSize="5rem"
                  name={user.name}
                  src={user?.image}
                  mr={3}
                  className="anime"
                  zIndex={20}
                />
              </Box>
              <Box ml={3}>
                <Text as="h6" fontSize="lg" fontWeight={700}>
                  {user.name}
                </Text>
                <Text fontSize="md" color="gray.500" fontWeight={500}>
                  {user.rank}
                </Text>
              </Box>
            </Flex>
            <Box
              w="100%"
              border="1px solid gray.500"
              borderRadius={5}
              pl={3}
              pr={3}
              pb={3}
              display={user.information?.bio ? "block" : "none"}
            >
              <Text color="gray.600">
                <i> &#8220; {user.information?.bio} &#8221;</i>
              </Text>
              {/* <Text color="gray.800">Bio</Text> */}
            </Box>
            <HStack
              bg={mode("gray.100", "gray.750")}
              p={4}
              justify="space-between"
              borderRadius="10px"
              w="100%"
              px={10}
            >
              <VStack fontWeight={500} spacing={3}>
                <Text color="gray.500" fontSize="sm">
                  Posts
                </Text>
                <Text fontSize="26px" fontWeight={700}>
                  {user?._count?.posts || 0}
                </Text>
              </VStack>
              <VStack>
                <Text color="gray.500" fontSize="sm">
                  Followers
                </Text>
                <Text fontSize="26px" fontWeight={700}>
                  {user?._count?.followers || 0}
                </Text>
              </VStack>
              <VStack>
                <Text color="gray.500" fontSize="sm">
                  Score
                </Text>
                <Text fontSize="26px" fontWeight={700}>
                  {user.score}
                </Text>
              </VStack>
            </HStack>
          </VStack>
        </HStack>
        <ProfileAction user={user} />

        {/* info */}
        <VStack
          maxW="420px"
          // w="410px"
          bg={mode("white", "gray.700")}
          p={6}
          mt={3}
          borderRadius="10px"
          align="flex-start"
          position="relative"
          // border="1px solid gray"
          display={
            user.information?.location ||
            user.information?.dateOfBirth ||
            user.information?.university ||
            user.information?.highSchool
              ? "block"
              : "none"
          }
        >
          <Flex align="center" mb={2}>
            <BsFillInfoCircleFill fontSize="22px" />
            <Text ml={3} fontSize="18px" fontWeight="bold">
              {" "}
              Information
            </Text>
          </Flex>
          <Flex
            align="center"
            pl={7}
            pr={7}
            pt={2}
            display={user.information?.dateOfBirth ? "flex" : "none"}
          >
            <MdCake fontSize="16px" />
            <Text ml={4}>
              {moment(new Date(user.information?.dateOfBirth)).format(
                "MMMM D, yyyy"
              )}
            </Text>
          </Flex>
          <Flex
            align="center"
            pl={7}
            pr={7}
            pt={2}
            display={user.information?.university ? "flex" : "none"}
          >
            <IoIosSchool fontSize="18px" />
            <Text ml={4}>{user.information?.university}</Text>
          </Flex>
          <Flex
            align="center"
            pl={7}
            pr={7}
            pt={2}
            display={user.information?.highSchool ? "flex" : "none"}
          >
            <IoIosSchool fontSize="18px" />
            <Text ml={4}>{user.information?.highSchool}</Text>
          </Flex>
          <Flex
            align="center"
            pl={7}
            pr={7}
            pt={2}
            display={user.information?.location ? "flex" : "none"}
          >
            <RiUserLocationFill fontSize="18px" />
            <Text ml={4}>{user.information?.location}</Text>
          </Flex>
        </VStack>
      </Flex>
    </Flex>
  );
}
