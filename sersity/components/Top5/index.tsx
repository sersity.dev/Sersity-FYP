import { gql, useQuery } from "@apollo/client";
import {
  Avatar,
  Divider,
  Flex,
  Skeleton,
  SkeletonCircle,
  Text,
  Box,
  useColorModeValue as mode,
} from "@chakra-ui/react";
import { helperRank } from "components/Context/rankBorder";
import { useRouter } from "next/dist/client/router";

const SkeletonLoading = ({ no, ...rest }) => {
  return (
    <Flex align="center" justify="space-between" py={3} {...rest}>
      <Flex align="center">
        <SkeletonCircle boxSize="2.8em" />
        <Flex direction="column" ml={2} justify="space-between" h="100%">
          <Skeleton h="10px" borderRadius="15px" w="150px" mb={3} />
          <Skeleton h="10px" w="100" />
        </Flex>
      </Flex>
      <Flex
        w={10}
        borderRadius={10}
        h={10}
        align="center"
        justify="center"
        bg={mode("teal.100", "teal.300")}
        fontStyle="lg"
        fontWeight="bold"
        color="teal.500"
      >
        {no}
      </Flex>
    </Flex>
  );
};

const List = ({ name, score, image, postLength, rank, no, ...rest }) => {
  return (
    <Flex
      align="center"
      justify="space-between"
      py={3}
      {...rest}
      transition="0.3s all ease"
      _hover={{ cursor: "pointer", transform: "translateX(-5px)" }}
    >
      <Flex align="center">
        {/* <Avatar name={name} src={image} boxSize="2.2em" /> */}
        <Box sx={helperRank}>
          <Avatar
            src={image}
            name={name || "author!"}
            bg="teal.500"
            size="md"
            className={rank?.toString().toLowerCase() || "anime"}
          />
        </Box>
        <Flex direction="column" ml={2}>
          <Text fontWeight="bold" textTransform="capitalize">
            {name}
          </Text>
          <Text color="gray.500">
            {postLength} Posts, {score} Scores
          </Text>
        </Flex>
      </Flex>
      <Flex
        w={10}
        borderRadius={10}
        h={10}
        align="center"
        justify="center"
        bg={mode("teal.100", "teal.300")}
        fontStyle="lg"
        fontWeight="bold"
        color="teal.700"
      >
        {no}
      </Flex>
    </Flex>
  );
};

const TOPUSER = gql`
  query {
    users(take: 5, orderBy: { score: desc }) {
      id
      name
      image
      score
      rank
      posts(
        where: {
          AND: [
            { published: { equals: true } }
            { isAnonymous: { equals: false } }
          ]
        }
      ) {
        id
      }
    }
  }
`;
const Top5 = () => {
  const { data, loading } = useQuery(TOPUSER);
  const { push } = useRouter();

  return (
    <Flex
      position="sticky"
      top={79}
      px={4}
      m={3}
      bg={mode("white", "gray.700")}
      maxW="340px"
      borderRadius="10px"
      direction="column"
    >
      <Flex h={10} my={2} justify="center" align="center" fontWeight="bold">
        <Text fontSize="lg" as="h6">
          Top Ranker
        </Text>
      </Flex>
      {loading
        ? [1, 2, 3, 4, 5].map((v, i) => (
            <Box key={i}>
              {i > 0 && <Divider />}
              <SkeletonLoading
                key={i}
                {...{ no: i + 1 }}
                // borderTop={i > 0 && "1px solid rgba(237, 242, 247, 1)"}
              />
            </Box>
          ))
        : data &&
          data.users?.map((e, i) => (
            <Box key={i}>
              {i > 0 && <Divider />}
              <List
                // borderTop={i > 0 && "1px solid rgba(237, 242, 247, 1)"}
                onClick={() => push("/profile/" + e.id)}
                {...{
                  name: e.name,
                  postLength: e.posts.length,
                  score: e.score,
                  image: e.image,
                  rank: e.rank,
                  no: i + 1,
                }}
              />
            </Box>
          ))}
    </Flex>
  );
};

export default Top5;
