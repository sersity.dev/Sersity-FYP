import BeatLoader from "react-spinners/BeatLoader";

export default function Loading(props) {
  const { ...rest } = props;
  return <BeatLoader size={8} color="#38b2ac" {...rest} />;
}
