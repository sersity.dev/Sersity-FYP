import {
  Box,
  SkeletonCircle,
  SkeletonText,
  useColorModeValue as mode,
} from "@chakra-ui/react";
import React from "react";

const LoadingPost = (props) => {
  return (
    <Box padding="6" bg={mode("white", "gray.700")} mt={4} borderRadius="5px">
      <SkeletonCircle size="10" />
      <SkeletonText mt="4" noOfLines={4} spacing="4" />
      <SkeletonText mt="4" noOfLines={4} spacing="4" />
    </Box>
  );
};

export default LoadingPost;
