import {
  Avatar,
  Badge,
  Box,
  Button,
  Collapse,
  Flex,
  HStack,
  Input,
  InputGroup,
  InputRightElement,
  Spacer,
  useColorModeValue as mode,
  useDisclosure,
  VStack,
  Text,
  Tooltip,
  Tag,
  IconButton,
  useToast,
} from "@chakra-ui/react";
import gql from "graphql-tag";
import React, { useEffect, useRef, useState } from "react";
import { CgMailReply } from "react-icons/cg";
import { TiArrowDownThick, TiArrowUpThick } from "react-icons/ti";
import SingleReply from "./reply";

import { CREATE_COMMENT_VOTE, CREATE_REPLY } from "hook/graphql/post";
import moment from "moment";
import { useSession } from "next-auth/client";
import { useRouter } from "next/router";
import { helperRank } from "components/Context/rankBorder";
import { useApolloClient } from "@apollo/client";

// const CREATE_VOTE = gql`
//   mutation($action: Action!, $commentId: Int!) {
//     createVote(action: $action, commentId: $commentId) {
//       id
//     }
//   }
// `;

export default function SingleComent(props) {
  const style = useStyles();
  const { isOpen, onToggle, onOpen } = useDisclosure();
  const commentBg = mode("gray.100", "gray.750");
  const cmdInputBg = mode("white", "gray.700");
  const [session, laoding] = useSession();
  const { push } = useRouter();

  let apolloClient = useApolloClient();

  const replyInputRef = useRef();

  const [reply, setReply] = useState({
    replies: props.comment?.replies,
    isMinimize: false,
    tooltipReplyText: "Minimize the section",
    allReplies: props.comment?.replies,
    isReplying: false,
  });

  const [comment, setComment] = useState({
    score: props.comment?.upvote - props.comment?.downvote,
    isVoted: props.comment?.isVoted?.action,
    action: props.comment?.isVoted?.action,
    isVoting: false,
  });

  useEffect(() => {
    setComment({
      ...comment,
      isVoted: props.comment?.isVoted?.isVoted,
      action: props.comment?.isVoted?.action,
    });
  }, [props.isVoted]);
  // }
  const toast = useToast();
  const createVote = async (option) => {
    setComment({ ...comment, isVoting: true });
    apolloClient.mutate({
      mutation: CREATE_COMMENT_VOTE,
      variables: {
        action: option.action,
        commentId: +option.commentId,
        isVoted: comment.isVoted,
      },
      update: (cache, { data }) => {
        cache.modify({
          id: cache.identify({ __typename: "Comment", id: +option.commentId }),
          fields: {
            isVoted(e, { toReference }) {
              const v = {
                __typename: "IsVoted",
                isVoted: comment.isVoted,
                action: option.action,
              };
              return v;
            },
            upvote(e) {
              return data.createCommentVote.upvote;
            },
            downvote(e) {
              return data.createCommentVote.downvote;
            },
          },
        });
        setComment({
          ...comment,
          isVoting: false,
          isVoted: true,
          action: option.action,
          score:
            data.createCommentVote.upvote - data.createCommentVote.downvote,
        });
        // setIsVoted(true);
        // setIsVoting(false);
      },
    });
  };

  let replyInput = "";
  function handleChange(e) {
    e.preventDefault();
    replyInput = e.target.value;
  }

  const createReply = async (ref) => {
    if (replyInput !== "") {
      setReply({ ...reply, isReplying: true });
      apolloClient.mutate({
        mutation: CREATE_REPLY,
        variables: {
          comment: "" + replyInput,
          postId: props.post_id,
          parentId: +props.comment?.id,
        },
        update: (_, mutationResult) => {
          let data = mutationResult.data.createReply;
          console.log(data);

          setReply({
            ...reply,
            replies: [...reply.replies, data],
            allReplies: [...reply.allReplies, data],
            isReplying: false,
          });

          ref.current.value = "";
          replyInput = "";
        },
      });
    }
  };
  // console.log(props);

  function minimizeReplySection() {
    if (reply.isMinimize === false) {
      setReply({
        ...reply,
        replies: [reply.allReplies[0]],
        isMinimize: true,
        tooltipReplyText: "Maximize the section",
      });
    } else {
      setReply({
        ...reply,
        replies: reply.allReplies,
        isMinimize: false,
        tooltipReplyText: "Maximize the section",
      });
    }
  }

  return (
    <>
      <Flex
        direction="column"
        justify="center"
        borderRadius="10px"
        bgColor={commentBg}
        boxShadow={mode("inherit", "sm")}
        {...props}
      >
        <Flex py={3} align="flex-start">
          <VStack w="45px">
            {/* <IconButton
            aria-label="Call Segun"
            size="md"
            icon={<TiArrowUpThick />}
          /> */}
            <Button
              color={comment.action === "UPVOTE" ? "teal.500" : ""}
              mt={1.5}
              size="sm"
              sx={style.upVoteBtn}
              variant="ghost"
              // disabled={isVoting}
              _hover={comment.isVoting && { cursor: "wait" }}
              leftIcon={<TiArrowUpThick />}
              onClick={(e) => {
                e.preventDefault();
                if (session) {
                  if (comment.isVoting === false) {
                    if (comment.action !== "UPVOTE") {
                      createVote({
                        action: "UPVOTE",
                        commentId: props.comment?.id,
                      });
                    } else {
                      createVote({
                        action: "UNVOTED",
                        commentId: props.comment?.id,
                      });
                    }
                  }
                } else {
                  push("/auth/login");
                }
              }}
            >
              <span>{comment.score}</span>
            </Button>
          </VStack>
          <HStack flexGrow={1} align="flex-start">
            <Box sx={helperRank}>
              <Avatar
                src={props.comment?.userRef?.image}
                name={props.author?.name || "author!"}
                bg="teal.500"
                // boxSize={["2.5rem", "3rem"]}
                size="sm"
                cursor="pointer"
                onClick={() => {
                  if (props.comment?.userRef?.id !== "anonymous") {
                    push("/profile/" + props.comment?.userRef?.id);
                  } else {
                    toast({
                      title: "Anonymous Account",
                      status: "warning",
                      // description: "This account is hidden by the owner",
                      duration: 2000,
                      isClosable: true,
                    });
                  }
                }}
                className="anime"
              />
            </Box>

            <Box ml="3" w="100%">
              <HStack w="100%">
                <Text
                  fontWeight="bold"
                  fontSize={["xs", "md"]}
                  cursor="pointer"
                  onClick={() => push("/profile/" + props.comment?.userRef?.id)}
                >
                  {props.comment?.userRef?.name}
                  <Tag
                    ml={1}
                    fontSize={["0.8rem", "0.8rem", "0.8rem"]}
                    bg="none"
                    color="gray.500"
                  >
                    {moment(props.comment?.createdAt).fromNow()}
                  </Tag>
                </Text>
                <Spacer />
                {props?.reply !== "false" && (
                  <Button
                    onClick={(replyInputRef) => {
                      onOpen();
                      replyInputRef.currentTarget.scrollIntoView({
                        behavior: "smooth",
                      });
                    }}
                    fontSize={["xs", "sm"]}
                    size="sm"
                    boxSize={["1rem 1rem"]}
                    leftIcon={<CgMailReply size="1.3em" />}
                    variant="ghost"
                    colorScheme="teal"
                  >
                    <Text as="a" fontWeight="bold">
                      Reply
                    </Text>
                  </Button>
                )}
              </HStack>

              <Text
                color={mode("gray.600", "gray.200")}
                fontSize={["xs", "md"]}
                noOfLines={props?.type === "single-comment" ? 3 : 1000}
                wordBreak="break-all"
              >
                {props.comment?.comment}
              </Text>
            </Box>
          </HStack>
          <VStack w="45px">
            <IconButton
              aria-label="downvote"
              color={comment.action === "DOWNVOTE" ? "red.400" : ""}
              sx={style.upVoteBtn}
              variant="ghost"
              _hover={comment.isVoting && { cursor: "wait" }}
              // disabled={comment.isVoting}
              leftIcon={<TiArrowDownThick />}
              onClick={(e) => {
                e.preventDefault();
                if (session) {
                  if (comment.isVoting === false) {
                    if (comment.action !== "DOWNVOTE") {
                      createVote({
                        action: "DOWNVOTE",
                        commentId: props.comment?.id,
                      });
                    } else {
                      createVote({
                        action: "UNVOTED",
                        commentId: props.comment?.id,
                      });
                    }
                  }
                } else {
                  push("/auth/login");
                }
              }}
            />
          </VStack>
        </Flex>
        {props.type !== "single-comment" && reply?.replies?.length > 0 && (
          <Flex width="100%" px={3}>
            <Tooltip
              placement="left"
              color={mode("gray.100", "gray.200")}
              bg={mode("", "gray.800")}
              hasArrow
              label={reply.tooltipReplyText}
              aria-label="Minimize reply section"
            >
              <Flex
                width="3%"
                py="30px"
                alignItems="center"
                justifyContent="center"
                _hover={{ cursor: "pointer" }}
                onClick={minimizeReplySection}
              >
                <Box
                  width="3px"
                  bg={
                    !reply.isMinimize ? mode("gray.200", "gray.600") : "red.400"
                  }
                  borderRadius="10px"
                  h="100%"
                ></Box>
              </Flex>
            </Tooltip>

            <Box mb={3} px={3} width="97%">
              {props.type !== "single-comment" &&
                reply &&
                reply.replies.map((r, i) => {
                  return <SingleReply reply={r} key={i} />;
                })}
            </Box>
          </Flex>
        )}
        <Collapse in={isOpen} animateOpacity>
          <Box mb={3} mx={3} rounded="md">
            <InputGroup size="md">
              <Input
                pr="110px"
                variant="outline"
                bgColor={cmdInputBg}
                placeholder={
                  session
                    ? "Reply to this comment"
                    : "Please login to reply on this comment"
                }
                onChange={handleChange}
                ref={replyInputRef}
              />
              <InputRightElement width="4.5rem">
                <Button
                  w="100%"
                  h="2.4rem"
                  colorScheme="teal"
                  size="md"
                  borderLeftRadius="0px "
                  isLoading={reply.isReplying}
                  onClick={() => {
                    if (session) {
                      createReply(replyInputRef);
                    } else {
                      push("/auth/login");
                    }
                  }}
                >
                  {session ? "Reply" : "Login"}
                </Button>
              </InputRightElement>
            </InputGroup>
          </Box>
        </Collapse>
      </Flex>
    </>
  );
}
const useStyles = () => ({
  upVoteBtn: {
    transform: "rotate(90deg)",
    "& span": {
      transform: "rotate(-90deg)",
    },
  },
});
