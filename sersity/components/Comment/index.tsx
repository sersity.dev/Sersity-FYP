import {
  Avatar,
  Box,
  Button,
  Collapse,
  Flex,
  Input,
  InputGroup,
  InputRightElement,
  Spacer,
  useColorModeValue as mode,
  useDisclosure,
  VStack,
  Text,
  Tooltip,
  IconButton,
  useToast,
} from "@chakra-ui/react";

import React, { useEffect, useRef, useState } from "react";

import { TiArrowDownThick, TiArrowUpThick } from "react-icons/ti";
import SingleReply from "./reply";

import { CREATE_COMMENT_VOTE, CREATE_REPLY } from "hook/graphql/post";
import moment from "moment";
import { useSession } from "next-auth/client";
import { useRouter } from "next/router";
import { helperRank } from "components/Context/rankBorder";
import { BiLogInCircle, BiSend } from "react-icons/bi";
import Loading from "components/Loading";
import { useTranslation } from "hook/useTranslation";
import { useApolloClient } from "@apollo/client";

// const CREATE_VOTE = gql`
//   mutation($action: Action!, $commentId: Int!) {
//     createVote(action: $action, commentId: $commentId) {
//       id
//     }
//   }
// `;

export default function SingleComent(props) {
  const style = useStyles();
  const { isOpen, onToggle, onOpen } = useDisclosure();
  const commentBg = mode("gray.100", "gray.750");
  const cmdInputBg = mode("white", "gray.700");
  const [session, laoding] = useSession();
  const { push } = useRouter();

  let apolloClient = useApolloClient();

  const replyInputRef = useRef();

  const [reply, setReply] = useState({
    replies: props.comment?.replies,
    isMinimize: false,
    tooltipReplyText: "Minimize the section",
    allReplies: props.comment?.replies,
    isReplying: false,
  });

  const [comment, setComment] = useState({
    score: props.comment?.upvote - props.comment?.downvote,
    isVoted: props.comment?.isVoted?.action,
    action: props.comment?.isVoted?.action,
    isVoting: false,
  });
  // const [voteState, setVoteState] = useState(
  //   props.comment?.upvote - props.comment?.downvote
  // );

  // const [voteAction, setVoteAction] = useState(props.comment?.isVoted?.action);
  // const [isVoted, setIsVoted] = useState(props.comment?.isVoted?.isVoted);
  // const [isVoting, setIsVoting] = useState(false);

  // if(props.type === "single-post"){
  useEffect(() => {
    setComment({
      ...comment,
      isVoted: props.comment?.isVoted?.isVoted,
      action: props.comment?.isVoted?.action,
    });
    // setIsVoted(props.comment?.isVoted?.isVoted);
    // setVoteAction(props.comment?.isVoted?.action);
  }, [props.isVoted]);
  // }
  const toast = useToast();
  const createVote = async (option) => {
    // setIsVoting(true);
    setComment({ ...comment, isVoting: true });
    apolloClient.mutate({
      mutation: CREATE_COMMENT_VOTE,
      variables: {
        action: option.action,
        commentId: +option.commentId,
        isVoted: comment.isVoted,
      },
      update: (cache, { data }) => {
        cache.modify({
          id: cache.identify({ __typename: "Comment", id: +option.commentId }),
          fields: {
            isVoted(e, { toReference }) {
              const v = {
                __typename: "IsVoted",
                isVoted: comment.isVoted,
                action: option.action,
              };
              return v;
            },
            upvote(e) {
              return data.createCommentVote.upvote;
            },
            downvote(e) {
              return data.createCommentVote.downvote;
            },
          },
        });
        setComment({
          ...comment,
          isVoting: false,
          isVoted: true,
          action: option.action,
          score:
            data.createCommentVote.upvote - data.createCommentVote.downvote,
        });
        // setIsVoted(true);
        // setIsVoting(false);
      },
    });
    // let u = 0;
    // let d = 0;
    // if (option.action === "UPVOTE") {
    //   u = u + 1;

    //   if (voteAction === "DOWNVOTE") {
    //     u = u + 1;
    //     console.log("u", u);
    //   }
    //   setVoteState(voteState + u);
    //   setVoteAction("UPVOTE");
    // } else if (option.action === "DOWNVOTE") {
    //   d = d + 1;
    //   if (voteAction === "UPVOTE") {
    //     d = d + 1;
    //     console.log("d", d);
    //   }
    //   setVoteState(voteState - d);
    //   setVoteAction("DOWNVOTE");
    // } else if (option.action === "UNVOTED") {
    //   if (voteAction === "UPVOTE") {
    //     setVoteState(voteState - 1);
    //   }
    //   if (voteAction === "DOWNVOTE") {
    //     setVoteState(voteState + 1);
    //   }
    //   setVoteAction("UNVOTED");
    // }
  };

  let replyInput = "";
  function handleChange(e) {
    e.preventDefault();
    replyInput = e.target.value;
  }

  const createReply = async (ref) => {
    if (replyInput !== "") {
      setReply({ ...reply, isReplying: true });
      apolloClient.mutate({
        mutation: CREATE_REPLY,
        variables: {
          comment: "" + replyInput,
          postId: props.post_id,
          parentId: +props.comment?.id,
        },
        update: (_, mutationResult) => {
          let data = mutationResult.data.createReply;
          console.log(data);

          setReply({
            ...reply,
            replies: [...reply.replies, data],
            allReplies: [...reply.allReplies, data],
            isReplying: false,
          });

          ref.current.value = "";
          replyInput = "";
        },
      });
    }
  };
  // console.log(props);

  function minimizeReplySection() {
    if (reply.isMinimize === false) {
      setReply({
        ...reply,
        replies: [reply.allReplies[0]],
        isMinimize: true,
        tooltipReplyText: "Maximize the section",
      });
    } else {
      setReply({
        ...reply,
        replies: reply.allReplies,
        isMinimize: false,
        tooltipReplyText: "Maximize the section",
      });
    }
  }

  const { ...rest } = props;
  const { t } = useTranslation();
  function onSend(e) {
    e.preventDefault();
    if (session) {
      createReply(replyInputRef);
    } else {
      push("/auth/login");
    }
  }
  return (
    <>
      <Flex
        direction="column"
        justify="center"
        borderRadius="10px"
        bgColor={commentBg}
        boxShadow={mode("inherit", "sm")}
        {...rest}
      >
        <Flex py={3} align="flex-start">
          <VStack w="45px">
            {/* <IconButton
            aria-label="Call Segun"
            size="md"
            icon={<TiArrowUpThick />}
          /> */}
            <Button
              color={comment.action === "UPVOTE" ? "teal.500" : ""}
              mt={1.5}
              size="sm"
              sx={style.upVoteBtn}
              variant="ghost"
              // disabled={isVoting}
              _hover={comment.isVoting && { cursor: "wait" }}
              leftIcon={<TiArrowUpThick />}
              onClick={(e) => {
                e.preventDefault();
                if (session) {
                  if (comment.isVoting === false) {
                    if (comment.action !== "UPVOTE") {
                      createVote({
                        action: "UPVOTE",
                        commentId: props.comment?.id,
                      });
                    } else {
                      createVote({
                        action: "UNVOTED",
                        commentId: props.comment?.id,
                      });
                    }
                  }
                } else {
                  push("/auth/login");
                }
              }}
            >
              <span>{comment.score}</span>
            </Button>
          </VStack>
          <Flex direction="column" justify="space-between" w="100%">
            <Flex w="100%" align="flex-start" mb={0.5}>
              <Box sx={helperRank} mr={2}>
                <Avatar
                  src={
                    props.comment?.userRef?.image !== "anonymous"
                      ? props.comment?.userRef?.image
                      : "/avatar/anonymous.png"
                  }
                  name={props.author?.name || "author!"}
                  bg="teal.500"
                  boxSize={["1.2em", "1.5em"]}
                  cursor="pointer"
                  onClick={() => {
                    if (props.comment?.userRef?.rank !== "ANONYMOUS") {
                      push("/profile/" + props.comment?.userRef?.id);
                    } else {
                      toast({
                        title: "Anonymous Account",
                        status: "warning",
                        // description: "This account is hidden by the owner",
                        duration: 2000,
                        isClosable: true,
                      });
                    }
                  }}
                  className={
                    props.comment?.userRef?.rank?.toString().toLowerCase() ||
                    "anime"
                  }
                />
              </Box>
              <Flex align="center">
                <Text
                  fontWeight="bold"
                  fontSize={["0.9em", "md"]}
                  cursor="pointer"
                  onClick={() => {
                    if (props.comment?.userRef?.rank !== "ANONYMOUS") {
                      push("/profile/" + props.comment?.userRef?.id);
                    } else {
                      toast({
                        title: "Anonymous Account",
                        status: "warning",
                        // description: "This account is hidden by the owner",
                        duration: 2000,
                        isClosable: true,
                      });
                    }
                  }}
                >
                  {props.comment?.userRef.name}
                </Text>
                <Box mx={0.5} as="span">
                  &#183;
                </Box>
                <Box
                  border="1x solid green"
                  fontSize={["0.7em", "0.8em"]}
                  bg="none"
                  color="gray.500"
                >
                  {moment(props.comment?.createdAt).fromNow()}
                </Box>
              </Flex>

              <Spacer />
              {props?.reply !== "false" && (
                <Button
                  onClick={(replyInputRef) => {
                    onOpen();
                    replyInputRef.currentTarget.scrollIntoView({
                      behavior: "smooth",
                    });
                  }}
                  fontSize={["xs", "sm"]}
                  size="xs"
                  variant="ghost"
                  colorScheme="teal"
                  mr={3}
                >
                  <Text as="a" fontWeight="bold">
                    {t("reply")}
                  </Text>
                </Button>
              )}
            </Flex>
            <Flex justify="space-between">
              <Text
                color={mode("gray.600", "gray.200")}
                fontSize={["0.9em", "md"]}
                noOfLines={props?.type === "single-comment" ? 3 : 1000}
                wordBreak="break-all"
                mr={[0, 2]}
              >
                {props.comment?.comment}
              </Text>
              <VStack w="45px">
                <IconButton
                  mr={3}
                  aria-label="downvote"
                  h="1.4em"
                  color={comment.action === "DOWNVOTE" ? "red.400" : ""}
                  sx={style.upVoteBtn}
                  variant="ghost"
                  _hover={comment.isVoting && { cursor: "wait" }}
                  // disabled={comment.isVoting}
                  leftIcon={<TiArrowDownThick />}
                  onClick={(e) => {
                    e.preventDefault();
                    if (session) {
                      if (comment.isVoting === false) {
                        if (comment.action !== "DOWNVOTE") {
                          createVote({
                            action: "DOWNVOTE",
                            commentId: props.comment?.id,
                          });
                        } else {
                          createVote({
                            action: "UNVOTED",
                            commentId: props.comment?.id,
                          });
                        }
                      }
                    } else {
                      push("/auth/login");
                    }
                  }}
                />
              </VStack>
            </Flex>
          </Flex>
        </Flex>
        {props.type !== "single-comment" && reply?.replies?.length > 0 && (
          <Flex width="100%" px={3}>
            <Tooltip
              placement="left"
              color={mode("gray.100", "gray.200")}
              bg={mode("", "gray.800")}
              hasArrow
              label={reply.tooltipReplyText}
              aria-label="Minimize reply section"
            >
              <Flex
                width="3%"
                py="30px"
                alignItems="center"
                justifyContent="center"
                _hover={{ cursor: "pointer" }}
                onClick={minimizeReplySection}
              >
                <Box
                  width="3px"
                  bg={
                    !reply.isMinimize ? mode("gray.200", "gray.600") : "red.400"
                  }
                  borderRadius="10px"
                  h="100%"
                ></Box>
              </Flex>
            </Tooltip>

            <Box mb={3} px={[2, 3]} pr={0} width="97%">
              {props.type !== "single-comment" &&
                reply &&
                reply.replies.map((r, i) => {
                  return <SingleReply reply={r} key={i} />;
                })}
            </Box>
          </Flex>
        )}
        <Collapse in={isOpen} animateOpacity>
          <Box mb={3} pb={2} mx={3} rounded="md" as="form" onSubmit={onSend}>
            <InputGroup h={["1.8em", "2em"]}>
              <Input
                pr="60px"
                variant="outline"
                bgColor={cmdInputBg}
                focusBorderColor="teal.500"
                placeholder={
                  session
                    ? "Reply to this comment"
                    : "Please login to reply on this comment"
                }
                onChange={handleChange}
                ref={replyInputRef}
              />

              <InputRightElement width="3rem" mr={1}>
                <IconButton
                  aria-label="sumit cmt"
                  isLoading={reply.isReplying}
                  spinner={<Loading size={8} />}
                  w="100%"
                  variant="ghost"
                  color="teal.500"
                  h="2em"
                  type="submit"
                  // onClick={() => {
                  //   if (session) {
                  //     createReply(replyInputRef);
                  //   } else {
                  //     push("/auth/login");
                  //   }
                  // }}
                >
                  {session ? (
                    <BiSend fontSize="1.8em" />
                  ) : (
                    <BiLogInCircle fontSize="1.8em" />
                  )}
                </IconButton>
              </InputRightElement>
            </InputGroup>
          </Box>
        </Collapse>
      </Flex>
    </>
  );
}
const useStyles = () => ({
  upVoteBtn: {
    transform: "rotate(90deg)",
    "& span": {
      transform: "rotate(-90deg)",
    },
  },
  downVoteBtn: {
    transform: "rotate(90deg)",
    "& span": {
      transform: "rotate(-90deg)",
    },
  },
});
