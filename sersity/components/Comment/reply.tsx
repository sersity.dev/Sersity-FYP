import { useColorModeValue } from "@chakra-ui/react";
import React from "react";
import SingleComent from ".";

export default function SingleReply(props) {
  const replyBg = useColorModeValue("white", "gray.700");
  return (
    <SingleComent
      mt={[2, 4]}
      mb={[0, 2]}
      bgColor={replyBg}
      reply="false"
      comment={props.reply}
    />
  );
}
