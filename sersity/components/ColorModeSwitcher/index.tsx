import { IconButton } from "@chakra-ui/button";
import { useColorMode } from "@chakra-ui/color-mode";
import { ImSun } from "react-icons/im";
import { RiMoonClearLine } from "react-icons/ri";

export default function ColorModeSwitcher() {
  const { colorMode, toggleColorMode } = useColorMode();
  return (
    <IconButton
      mr={1}
      aria-label="color mode"
      variant="ghost"
      transition="0.3s all ease"
      transform={colorMode === "light" ? "rotate(0deg)" : "rotate(180deg)"}
      fontSize="1.4em"
      icon={colorMode === "light" ? <RiMoonClearLine /> : <ImSun />}
      onClick={toggleColorMode}
    />
  );
}
