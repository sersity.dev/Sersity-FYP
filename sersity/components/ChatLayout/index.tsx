import { useMutation, useQuery, useSubscription } from "@apollo/client";
import { Avatar, AvatarBadge } from "@chakra-ui/avatar";
import { Box, Flex, HStack, Text } from "@chakra-ui/layout";
import { useColorModeValue as mode } from "@chakra-ui/system";
import {
  CREATED_MESSAGE,
  CREATED_CHATROOM,
  GET_USER_CHATROOMS,
  UPDATE_LAST_MESSAGE_SEENBY,
} from "hook/graphql/chat";
import { useRouter } from "next/router";

import React, { useEffect, useState } from "react";
import { ChatList } from "./ChatList";
import { MessageList } from "./MessageList";
import _ from "lodash";
import useMQ from "hook/useMQ";

export * from "./ChatList";
export * from "./MessageList";

UserUi.defaultProps = {
  state: "none",
};
export function UserUi({ name, image, role, state, isSelect, onSelectRoom }) {
  const colorState = state === "offline" ? "gray.400" : "green.400";
  return (
    <HStack
      mt={1}
      onClick={onSelectRoom}
      bg={["inherit", isSelect ? mode("gray.100", "gray.750") : "none"]}
      p={2.5}
      sx={{
        transition: "all .2s ease-in-out",
      }}
      borderLeftRadius="10px"
    >
      <HStack
        w="100%"
        sx={{
          transition: "all .3s ease-in-out",
          "&:hover": { cursor: "pointer", transform: "scale(0.9)" },
        }}
      >
        <Avatar
          name={name}
          src={image}
          //  boxSize="2.5rem"
          size="md"
        >
          {state != "none" && (
            <AvatarBadge top="0" boxSize="1em" bg={colorState} />
          )}
        </Avatar>
        <Flex direction="column">
          <Text
            fontWeight={600}
            fontSize="0.9rem"
            isTruncated
            color={[
              "inherit",
              isSelect ? mode("teal.500", "teal.200") : "inherit",
            ]}
          >
            {name ? name : " No Name"}
          </Text>
          <Text fontSize="0.8rem"> {role ? role : " No Role"}</Text>
        </Flex>
      </HStack>
    </HStack>
  );
}

function useChatRoom({ user }) {
  const { push, query } = useRouter();
  const [page, setPage] = useState(1);
  const [chatRoom, setChatRoom] = useState({ messages: [] });
  const [hasMore, setHasMore] = useState(true);
  const [updateLastMessageSeenBy] = useMutation(UPDATE_LAST_MESSAGE_SEENBY);
  const { isMobile } = useMQ();
  const { data, fetchMore, error, refetch } = useQuery(GET_USER_CHATROOMS, {
    variables: {
      userId: user.id,
      skip: 0,
    },
    onCompleted(data) {
      const chatRooms = data?.user?.chatRooms;
      if (chatRooms?.length > 0) {
        query.slug && isMobile && push(`/chat/${chatRooms[0]?.slug}`);
        setChatRoom(chatRooms[0]);
      }
    },
  });

  function handlePage() {
    page === 0 ? setPage(1) : setPage(0);
  }

  function onSelectRoom(e) {
    handlePage();
    query.slug === e.slug ? push(`/chat/${e.slug}`) : setChatRoom(e);
  }
  useEffect(() => {
    error || (data?.user && setHasMore(false));
    if (data && data?.user?.chatRooms?.length % 10 !== 0) {
      setHasMore(false);
    }
  }, [data]);

  // if slug and chatRoom.slug, render Message
  useEffect(() => {
    const chatRooms = data?.user?.chatRooms;
    const e = chatRooms && chatRooms.find((e) => e.slug === query?.slug);
    if (e) setChatRoom(e);
  }, [query]);

  function upFetchMoreChatRooms() {
    fetchMore({
      variables: {
        userId: user.id,
        skip: data?.user?.chatRooms.length,
      },
    });
  }

  return {
    user: data?.user,
    upFetchMoreChatRooms,
    hasMore,
    chatRoom,
    setChatRoom,
    onSelectRoom,
    handlePage,
    updateLastMessageSeenBy,
    page,
    slug: query.slug,
    chatRooms: data?.user?.chatRooms || [],
  };
}

function onSubscription({ user, chatProps }) {
  useSubscription(CREATED_MESSAGE, {
    // fetchPolicy: "no-cache",
    variables: {
      userId: user?.id,
    },
    onSubscriptionData: ({ client, subscriptionData }) => {
      const newMessage = subscriptionData.data?.createdMessage;
      const chatRoom = newMessage?.chatRoom;
      client.cache.modify({
        id: client.cache.identify(chatRoom),
        fields: {
          messages(messages) {
            if (messages.find((e) => e.id !== newMessage.id))
              return [newMessage, ...messages];
          },
        },
      });
    },
  });
  useSubscription(CREATED_CHATROOM, {
    // fetchPolicy: "no-cache",
    variables: {
      userId: user?.id,
    },
    onSubscriptionData: ({ client, subscriptionData }) => {
      const newChatRoom = subscriptionData.data?.createdChatRoom;
      client.cache.modify({
        id: client.cache.identify(chatProps?.user),
        fields: {
          chatRooms(chatRooms) {
            if (chatRooms.find((e) => e.id !== newChatRoom.id))
              return [newChatRoom, ...chatRooms];
          },
        },
      });
    },
  });
}
export default function ChatPage({ user, h, w, wl, wr, onClose }) {
  const chatProps = useChatRoom({ user });
  const { isMobile } = useMQ();
  const renderPage = RenderPage({ ...chatProps, isMobile, wl, wr, onClose });
  onSubscription({ user, chatProps });
  return (
    <Box display="flex" flexDirection="row" w={w} h={h || "100vh"}>
      {renderPage}
    </Box>
  );
}

function RenderPage(props) {
  return (
    <>
      {props.isMobile ? (
        props.page === 1 ? (
          <ChatList {...props} />
        ) : (
          <MessageList {...props} />
        )
      ) : (
        <>
          <ChatList {...props} />
          <MessageList {...props} />
        </>
      )}
    </>
  );
}
