import { useLazyQuery, useMutation } from "@apollo/client";
import { Button } from "@chakra-ui/button";
import { useColorModeValue as mode } from "@chakra-ui/color-mode";
import {
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
} from "@chakra-ui/input";
import { Box, Center, Flex } from "@chakra-ui/layout";
import {
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerHeader,
  DrawerOverlay,
} from "@chakra-ui/modal";
import { Spinner } from "@chakra-ui/spinner";
import { Tag, TagCloseButton, TagLabel } from "@chakra-ui/tag";
import { Collapse } from "@chakra-ui/transition";
import EmptyData from "components/EmptyData";
import { CREATE_ROOM, SEARCH_USERS } from "hook/graphql/chat";
import useMQ from "hook/useMQ";
import { useTranslation } from "hook/useTranslation";

import { useRouter } from "next/router";
import { useEffect, useRef, useState } from "react";
import { BiSearchAlt } from "react-icons/bi";
import { RiChatNewLine } from "react-icons/ri";
import { UserUi } from "..";

const useSearchUser = () => {
  const [searchUser, { called, loading, data }] = useLazyQuery(SEARCH_USERS, {
    fetchPolicy: "no-cache",
  });

  return {
    users: data?.searchUsers,
    searchLoading: loading,
    searchCalled: called,
    searchUser,
  };
};

const useAddChat = ({ selectedUsers, setChatRoom, roomName, user }) => {
  const router = useRouter();
  const isMobile = useMQ();
  const [createOneChatRoom, { data }] = useMutation(CREATE_ROOM, {
    onCompleted(data) {
      const newRoom = data?.createOneChatRoom;
      setChatRoom(newRoom);
      router.pathname === "/chat/[slug]" &&
        isMobile &&
        router.push(`/chat/${newRoom?.slug}`);
    },
  });

  function onAddChat({ user }) {
    const ids = selectedUsers.map((e) => ({
      id: e.id,
    }));
    const withAdminIds = [{ id: user?.id }, ...ids];
    const randomName = "G-" + Math.random().toString(36).substring(7);
    let name = selectedUsers.length === 1 ? null : randomName;
    if (roomName.length > 0) name = roomName;
    const data = {
      admin: { connect: { id: user?.id } },
      name: name,
      members: { connect: withAdminIds },
    };

    //input
    createOneChatRoom({ variables: { data } });
  }
  return { onAddChat, newRoom: data?.createOneChatRoom };
};

export default function AddNewChat({
  user,
  setChatRoom,
  chatRooms,
  isOpen,
  onOpen,
  onClose,
}) {
  const [searchText, setSearchText] = useState("");
  const { users, searchUser, searchLoading } = useSearchUser();
  const [selectedUsers, setSelectedUsers] = useState([]);
  const [roomName, setRoomName] = useState("");
  const [isSubmit, setIsSubmit] = useState(false);
  const { onAddChat } = useAddChat({
    selectedUsers,
    setChatRoom,
    roomName,
    user,
  });

  const router = useRouter();
  const isMobile = useMQ();
  useEffect(() => {
    searchText?.length > 0 && searchUser({ variables: { searchText } });
  }, [searchText]);

  function handleAddChat() {
    setIsSubmit(true);
    const isExistContact = chatRooms.find((e) => {
      const exist =
        e?.members.length <= 2 &&
        selectedUsers.length < 2 &&
        e?.members.find((el) => el.id === selectedUsers[0].id);
      return exist;
    });

    console.log(router.pathname);
    if (isExistContact) {
      router.pathname === "/chat/[slug]" && isMobile
        ? router.push(`/chat/${isExistContact?.slug}`)
        : setChatRoom(isExistContact);
      setIsSubmit(false);
      onClose();
    } else {
      onAddChat({ user });
      setIsSubmit(false);
      onClose();
    }
  }

  const debounce = require("lodash/debounce");
  const searchField = useRef();
  const { t } = useTranslation();
  return (
    <>
      <Button
        aria-label="list"
        fontSize="lg"
        size="sm"
        variant="ghost"
        onClick={() => {
          onOpen();
          setSelectedUsers([]);
        }}
        leftIcon={<RiChatNewLine fontSize="1.3rem" />}
      >
        {t("add")}
      </Button>
      <Drawer
        isOpen={isOpen}
        placement="left"
        onClose={onClose}
        initialFocusRef={searchField}
      >
        <DrawerOverlay>
          <DrawerContent>
            <DrawerCloseButton />
            <DrawerHeader fontSize="lg">{t("add chat")}</DrawerHeader>
            <DrawerBody
              sx={{
                display: "flex",
                flexDirection: "column",
                margin: 0,
                padding: 0,
              }}
            >
              <Box d="flex" mx={5} mb={1}>
                <InputGroup>
                  <InputLeftElement
                    borderRightRadius="0px"
                    pointerEvents="none"
                    children={<BiSearchAlt />}
                    fontSize="1.4em"
                    color="gray.300"
                  />
                  <Input
                    ref={searchField}
                    borderEndRadius="0px"
                    variant="filled"
                    focusBorderColor="teal.500"
                    placeholder={`${t("search")}...`}
                    onChange={debounce((e) => {
                      setSearchText(e.target.value);
                    }, 500)}
                  />
                  <InputRightElement
                    children={
                      <Spinner
                        size="sm"
                        display={searchLoading && !users ? "inherit" : "none"}
                        color="teal.500"
                      />
                    }
                  />
                </InputGroup>
                <Button
                  isLoading={isSubmit}
                  onClick={handleAddChat}
                  borderLeftRadius="0px"
                  colorScheme="teal"
                  leftIcon={<RiChatNewLine fontSize="1.3rem" />}
                  disabled={selectedUsers.length > 0 ? false : true}
                >
                  {t("add")}
                </Button>
              </Box>

              <Collapse
                in={selectedUsers?.length > 1 ? true : false}
                animateOpacity
              >
                <Box px={5} py={2}>
                  <Input
                    onChange={(e) => setRoomName(e.target.value)}
                    variant="filled"
                    focusBorderColor="teal.500"
                    placeholder="Group Name"
                  />
                </Box>
              </Collapse>

              <Box mx={5} mt={1}>
                {selectedUsers?.map((e, index) => (
                  <Tag maxW="30%" size="md" mb={2} mr={2} key={index}>
                    <TagLabel>{e.name}</TagLabel>
                    <TagCloseButton
                      onClick={() => {
                        const x = selectedUsers.findIndex(
                          (el) => el.id === e.id
                        );
                        let newArr = selectedUsers;
                        newArr.splice(x, 1);
                        setSelectedUsers([...newArr]);
                      }}
                    />
                  </Tag>
                ))}
              </Box>
              {searchText.length > 0 && users?.length < 1 && (
                <Center py={5}>
                  <EmptyData>Not Found !</EmptyData>
                </Center>
              )}

              <Flex
                direction="column"
                grow={1}
                overflowY="auto"
                sx={{
                  scrollbarWidth: "thin",
                  msOverflowStyle: "none",
                  "&::-webkit-scrollbar": { dispay: "none" },
                }}
              >
                <Box
                  bg={mode("gray.100", "gray.750")}
                  borderRadius="10px"
                  mx={5}
                >
                  {users &&
                    users.map((e, index) => {
                      return (
                        <UserUi
                          image={e?.image}
                          key={index}
                          name={e?.name || "No name"}
                          role={e?.rank}
                          // state={e?.isOnline ? "online" : "offline"}
                          isSelect={false}
                          onSelectRoom={() => {
                            typeof selectedUsers.find(
                              (el) => el.id === e.id
                            ) === "undefined"
                              ? setSelectedUsers([...selectedUsers, e])
                              : console.log("Aready added");
                          }}
                        />
                      );
                    })}
                </Box>
              </Flex>
            </DrawerBody>
          </DrawerContent>
        </DrawerOverlay>
      </Drawer>
    </>
  );
}
