import { Button, IconButton } from "@chakra-ui/button";
import { useColorModeValue as mode } from "@chakra-ui/color-mode";
import { useDisclosure } from "@chakra-ui/hooks";
import { Input } from "@chakra-ui/input";
import { Center, Flex, Heading, HStack, Spacer, Text } from "@chakra-ui/layout";
import { Spinner } from "@chakra-ui/spinner";
import Loading from "components/Loading";
import { useTranslation } from "hook/useTranslation";
import { useRouter } from "next/router";
import { BiArrowBack } from "react-icons/bi";

import InfiniteScroll from "react-infinite-scroll-component";
import { UserUi } from "..";
import AddNew from "./AddNew";
export function ChatList({
  user,
  chatRoom,
  onSelectRoom,
  setChatRoom,
  chatRooms,
  upFetchMoreChatRooms,
  hasMore,
  wl,
}) {
  const router = useRouter();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const disclose = { isOpen, onOpen, onClose };
  const { t } = useTranslation();
  return (
    <Flex
      direction="column"
      minW="200px"
      w={wl || ["100%", "300px"]}
      bgColor={mode("white", "gray.700")}
    >
      <HStack h="58px" p={3}>
        <IconButton
          fontSize="1.4em"
          aria-label="back"
          display={["inherit", "none"]}
          variant="ghost"
          onClick={() => router.push("/home")}
          leftIcon={<BiArrowBack />}
        />

        <Text
          ml={1}
          fontWeight="bold"
          display={["none", "inherit"]}
          fontSize="lg"
        >
          {t("chat")}
        </Text>
        <Spacer />
        <AddNew
          user={user}
          setChatRoom={setChatRoom}
          {...disclose}
          chatRooms={chatRooms}
        />
      </HStack>
      <HStack px={4} pb={2}>
        <Input
          onClick={() => {
            onOpen();
          }}
          placeholder="Search"
          focusBorderColor="teal.500"
          variant="filled"
        />
      </HStack>
      <Flex
        direction="column"
        flexGrow={1}
        overflow="auto"
        pl={4}
        id="scrollableRoom"
        sx={{
          scrollbarWidth: "none",
          msOverflowStyle: "none",
          "&::-webkit-scrollbar": { display: "none" },
        }}
      >
        {chatRooms && (
          <InfiniteScroll
            loader={
              <Center p={3}>
                <Loading />
              </Center>
            }
            dataLength={chatRooms.length}
            next={upFetchMoreChatRooms}
            hasMore={hasMore}
            scrollableTarget="scrollableRoom"
          >
            {chatRooms.map((e, index) => {
              const contact = e?.members.find((e) => e.id !== user.id);

              return (
                <UserUi
                  image={e?.image || (e?.members?.length < 3 && contact?.image)}
                  name={e?.name || contact?.name || "No Name"}
                  key={index}
                  onSelectRoom={() => onSelectRoom(e)}
                  role={e?.members.length > 2 ? "Group" : contact?.rank}
                  // state={e?.isOnline ? "online" : "offline"}
                  isSelect={
                    chatRoom.slug === e.slug || chatRoom.slug == e.slug
                      ? true
                      : false
                  }
                />
              );
            })}
          </InfiniteScroll>
        )}
      </Flex>
    </Flex>
  );
}
