import { IconButton } from "@chakra-ui/button";
import { CloseButton } from "@chakra-ui/close-button";
import { Box, Flex } from "@chakra-ui/layout";
import { useRouter } from "next/router";
import { FiExternalLink } from "react-icons/fi";
import ChatLayout from "components/ChatLayout";

import { useSession } from "next-auth/client";
import { ScaleFade } from "@chakra-ui/transition";
import { useEffect, useState } from "react";
import { browser } from "process";

const ChatDialogData = (props) => {
  const { ...rest } = props;

  const [session] = useSession();
  return (
    <Flex
      boxShadow="cus6"
      borderRadius="10px"
      overflow="hidden"
      w="700px"
      display="relative"
    >
      <Box display={["none", "inherit"]}>
        {props.isOpen && (
          <ChatLayout
            h="450px"
            {...props}
            user={session?.user}
            wl={["100%", "250px"]}
            wr={["100%", "400px"]}
          />
        )}
      </Box>
    </Flex>
  );
};

function ChatDialog({ isOpen, onOpen, onClose, onToggle }) {
  const { locale } = useRouter();
  const ssrMode = typeof window === "undefined";
  const initWidth = locale === "kh" ? 11 : 9;

  const [width, setWidth] = useState(initWidth);

  useEffect(() => {
    const element = document.getElementById("sersity-navbar");
    if (element) setWidth(element.offsetWidth / 16 + 1);
  }, [locale, isOpen]);

  return (
    <Box
      transition="0.2s all ease"
      zIndex={10}
      position="fixed"
      display={isOpen ? "inherit" : "hidden"}
      bottom={0}
      left={`${width}em`}
      // left={locale === "kh" ? "12em" : "10em"}
      m={3}
    >
      <ScaleFade unmountOnExit in={isOpen}>
        <ChatDialogData isOpen={isOpen} onClose={onClose} />
      </ScaleFade>
    </Box>
  );
}

export default ChatDialog;
