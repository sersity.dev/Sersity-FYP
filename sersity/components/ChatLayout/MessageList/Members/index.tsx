import { useDisclosure } from "@chakra-ui/hooks";
import { Box } from "@chakra-ui/layout";
import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
} from "@chakra-ui/modal";
import { UserUi } from "components/ChatLayout";
import { useRouter } from "next/router";

export default Members;
function Members({ members }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { push } = useRouter();

  return (
    <>
      <Box w="100%" onClick={onOpen}>
        Members
      </Box>
      <Modal
        isCentered
        onClose={onClose}
        isOpen={isOpen}
        motionPreset="slideInBottom"
      >
        <ModalOverlay />
        <ModalContent p={0}>
          <ModalHeader>Members</ModalHeader>
          <ModalCloseButton />
          <ModalBody mb={3}>
            {members &&
              members.map((e, i) => (
                <UserUi
                  image={e?.image}
                  name={e?.name || "No Name"}
                  key={`member.${i}`}
                  onSelectRoom={() => push("/profile/" + e.id)}
                  role={e?.rank}
                  // state={e?.isOnline ? "online" : "offline"}
                  isSelect={false}
                />
              ))}
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
}
