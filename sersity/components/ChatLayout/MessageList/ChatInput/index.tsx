import { Button } from "@chakra-ui/button";
import { Input, InputGroup, InputRightElement } from "@chakra-ui/input";
import { Flex } from "@chakra-ui/layout";
import { useState } from "react";
import { BiSend } from "react-icons/bi";

import { useSession } from "next-auth/client";
import { useMutation } from "@apollo/client";
import { CREATE_MESSAGE } from "hook/graphql/chat";
import Loading from "components/Loading";

export default function ChatInput({ chatRoom }) {
  const [session] = useSession();
  const [createMessage] = useMutation(CREATE_MESSAGE, {
    onCompleted: () => setIsSubmitting(false),
    onError: () => setIsSubmitting(false),
  });
  const [inputValue, setInputValue] = useState("");
  const [isSubmitting, setIsSubmitting] = useState(false);

  function onSend(e) {
    e.preventDefault();
    setIsSubmitting(true);
    const data = {
      content: inputValue,
      userId: session?.user.id,
      chatRoomId: +chatRoom?.id,
      slug: chatRoom.slug,
    };
    setInputValue("");
    createMessage({ variables: data });
  }

  return (
    <Flex as="form" onSubmit={onSend}>
      <InputGroup mx={3} ml={[3, 0]} mb={3} size="md">
        <Input
          onChange={(e) => {
            setInputValue(e.target.value);
          }}
          value={inputValue}
          focusBorderColor="teal.500"
          variant="filled"
          placeholder="Enter Text Here ..."
        />
        <InputRightElement width="3.6rem">
          <Button
            isLoading={isSubmitting}
            spinner={<Loading size={8} />}
            disabled={chatRoom?.slug ? !inputValue : true}
            type="submit"
            colorScheme="teal"
            variant="ghost"
            _hover={{ bg: "none" }}
            rightIcon={
              <BiSend
                style={
                  inputValue
                    ? {
                        transform: "rotate(-90deg)",
                        transition: "0.5s all ease",
                      }
                    : {
                        transform: "rotate(0deg)",
                        transition: "0.5s all ease",
                      }
                }
                fontSize="1.8rem"
              />
            }
          ></Button>
        </InputRightElement>
      </InputGroup>
    </Flex>
  );
}
