import { useQuery } from "@apollo/client";
import { Avatar } from "@chakra-ui/avatar";
import { Button, IconButton } from "@chakra-ui/button";
import { useColorModeValue as mode } from "@chakra-ui/color-mode";
import { Box, Center, Flex, HStack, Spacer, Text } from "@chakra-ui/layout";
import { CloseButton } from "@chakra-ui/react";

import EmptyData from "components/EmptyData";
import Loading from "components/Loading";

import { GET_CHATROOM_MESSAGES } from "hook/graphql/chat";
import useMQ from "hook/useMQ";
import moment from "moment";
import { useRouter } from "next/router";

import { useEffect, useState } from "react";
import { BiArrowBack } from "react-icons/bi";
import { FiExternalLink } from "react-icons/fi";
import { RiContactsLine } from "react-icons/ri";
import InfiniteScroll from "react-infinite-scroll-component";
import ChatInput from "./ChatInput";
import RoomMenu from "./RoomMenu";
var isEmpty = require("lodash/isEmpty");
export function MessageList({
  chatRoom,
  handlePage,
  user,
  wr,
  updateLastMessageSeenBy,
  onClose,
}) {
  const [hasMore, setHasMore] = useState(true);
  const { data, loading, fetchMore } = useQuery(GET_CHATROOM_MESSAGES, {
    skip: isEmpty(chatRoom) ? true : false || !chatRoom?.slug,
    variables: {
      slug: chatRoom?.slug,
      skip: chatRoom?.meesages?.length || 0,
    },
  });

  const messages = data?.chatRoom?.messages;

  function upFetchMore() {
    fetchMore({
      variables: {
        slug: chatRoom?.slug,
        skip: messages.length,
      },
    });
  }
  useEffect(() => {
    if (messages && messages?.length % 20 !== 0) {
      setHasMore(false);
    }
    const lastMessage = messages?.length > 0 && messages[0];
    lastMessage &&
      updateLastMessageSeenBy({
        variables: {
          lastMessageId: lastMessage.id,
        },
      });
  }, [data]);

  const renderMessages = () => {
    const keys = Object.keys(messages);
    return keys.map((key, index) => {
      const message = messages[key];
      const lastMessageKey = index === 0 ? null : keys[index - 1];
      const isMyMessage = user?.id === message?.user?.id;

      return (
        <Box key={`msg_${index}`} className="messages-block">
          {isMyMessage ? (
            <MyMessage message={message} />
          ) : (
            <TheirMessage
              message={message}
              lastMessage={messages[lastMessageKey]}
            />
          )}
        </Box>
      );
    });
  };

  if (isEmpty(chatRoom)) return null;
  const contactUser =
    chatRoom?.members?.length === 2 &&
    chatRoom?.members.find((e) => e.id != user.id);

  const chatRoomName = chatRoom?.name
    ? chatRoom?.name
    : contactUser && contactUser.name;

  const { isMobile } = useMQ();
  const router = useRouter();
  const isDialog = router.pathname !== "/chat/[slug]";
  return (
    <Flex
      direction="column"
      flexGrow={1}
      bgColor={mode("white", "gray.700")}
      w={wr || "400px"}
      position="relative"
    >
      <HStack
        position="absolute"
        top={0}
        px={3}
        h={"58px"}
        bgColor={mode("white", "gray.700")}
        w="100%"
        borderTopRadius="10px"
      >
        <Flex align="center" w="100%">
          <IconButton
            aria-label="back"
            variant="ghost"
            onClick={() => {
              isMobile
                ? handlePage()
                : contactUser && router.push("/profile/" + contactUser.id);
            }}
            size="sm"
          >
            {isMobile ? (
              <BiArrowBack fontSize="1.4rem" />
            ) : (
              <RiContactsLine fontSize="1.4rem" />
            )}
          </IconButton>
          <Text ml={2}>{chatRoomName}</Text>
          <Spacer />
          <RoomMenu members={chatRoom?.members} />
          {isDialog && !isMobile && (
            <>
              <IconButton
                borderRadius="5px"
                colorScheme="gray"
                mx={1}
                variant="ghost"
                _hover={{ background: "rgba(45, 55, 72, 0.1)" }}
                size="sm"
                aria-label="ExternalLink "
                icon={<FiExternalLink fontSize="1.2rem" />}
                onClick={() => window.open(`${router.basePath}/chat/default`)}
              />
              <CloseButton onClick={onClose} />
            </>
          )}
        </Flex>
      </HStack>

      <Flex
        borderTopRadius={[0, 10]}
        mt={[3, "54px"]}
        mr={[0, 3]}
        mb={3}
        p={3}
        pt={[3, 10]}
        borderBottomRadius="10px"
        flex={1}
        bgColor={mode("gray.100", "gray.750")}
        overflowY="auto"
        direction="column-reverse"
        id="scrollMessages"
      >
        {!isEmpty(chatRoom) ? (
          loading ? (
            <Center h="100%">
              <Loading />
            </Center>
          ) : (
            <>
              {messages && messages.length > 0 ? (
                <InfiniteScroll
                  loader={
                    <Center p={3}>
                      <Loading />
                    </Center>
                  }
                  style={{
                    marginTop: "2.4em",
                    display: "flex",
                    flexDirection: "column-reverse",
                    overflowX: "hidden",
                  }}
                  dataLength={messages.length}
                  next={upFetchMore}
                  inverse={true}
                  hasMore={hasMore}
                  scrollableTarget="scrollMessages"
                >
                  {/* {messages.map((e, i) => (
                  <PageMotion key={i}>
                    <BubbleMsg user={user} message={e} />
                  </PageMotion>
                ))} */}

                  {renderMessages()}
                </InfiniteScroll>
              ) : (
                <EmptyData h="100%">Empty Messages</EmptyData>
              )}
            </>
          )
        ) : null}
      </Flex>
      <ChatInput chatRoom={chatRoom} />
    </Flex>
  );
}

function MyMessage({ message }) {
  return (
    <Flex w="100%" justify="flex-end">
      <Flex
        align="flex-end"
        _hover={{
          cursor: "pointer",
          "& .createdAt": { display: "inline" },
        }}
      >
        <Text pb={1} mr={2} display="none" className="createdAt" fontSize="xs">
          {/* {moment(message?.createdAt).format("LT")} */}
          {message?.seenBy.length > 0 && (
            <>Seen at {moment(message?.seenBy[0]?.createdAt).format("LT")}</>
          )}
        </Text>
        <Text
          minW="32px"
          p={2}
          my={1}
          borderRadius="6px"
          bg={mode("white", "gray.600")}
          borderRight={
            message?.seenBy.length > 0 && "2px solid rgba(56, 178, 172, 0.8)"
          }
        >
          {message?.content || ""}
        </Text>
      </Flex>
    </Flex>
  );
}

function TheirMessage({ message, lastMessage }) {
  const isFirstMessageByUser =
    !lastMessage || lastMessage.user.id !== message.user.id;

  return (
    <Flex w="100%" align="flex-end">
      <Flex w="56px">
        {isFirstMessageByUser && (
          <Avatar name={message?.user?.name} src={message?.user?.image} />
        )}
      </Flex>
      <Flex ml={2.5} d="column" w="100%">
        <Flex flexGrow={1}>
          <Flex
            align="flex-end"
            _hover={{
              cursor: "pointer",
              "& .createdAt": { display: "inline" },
            }}
          >
            <Text
              minW="32px"
              p={2}
              my={1}
              borderRadius="6px"
              bg={mode("white", "gray.600")}
            >
              {message?.content || ""}
            </Text>
            <Text
              pb={1}
              ml={2}
              display="none"
              className="createdAt"
              fontSize="xs"
            >
              {moment(message?.createdAt).format("LT")}
            </Text>
          </Flex>
        </Flex>
        {isFirstMessageByUser && (
          <Text fontWeight={400} color="gray.500" fontSize="xs">
            {message?.user.name}
          </Text>
        )}
      </Flex>
    </Flex>
  );
}
