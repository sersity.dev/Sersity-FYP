import { IconButton } from "@chakra-ui/button";
import { Menu, MenuButton, MenuItem, MenuList } from "@chakra-ui/menu";
import { BiDotsVerticalRounded } from "react-icons/bi";
import Members from "../Members";
export default function RoomMenu(props) {
  return (
    <Menu>
      <MenuButton
        as={IconButton}
        size="sm"
        fontSize="1.3em"
        variant="ghost"
        alignItems="center"
        borderRadius="5px"
        pl={1.5}
      >
        <BiDotsVerticalRounded />
      </MenuButton>
      <MenuList>
        <MenuItem>
          <Members members={props.members} />
        </MenuItem>
      </MenuList>
    </Menu>
  );
}
