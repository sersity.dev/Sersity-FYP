import { Box, Flex, Text } from "@chakra-ui/layout";
import { useRouter } from "next/router";
import { useColorModeValue as mode } from "@chakra-ui/color-mode";
import { Avatar } from "@chakra-ui/avatar";
import moment from "moment";
import Icon from "@chakra-ui/icon";
import { BsThreeDotsVertical } from "react-icons/bs";
import { useSession } from "next-auth/client";
export default function NotiUi({ notification }) {
  const { push } = useRouter();
  const [session] = useSession();
  const fromName =
    notification?.userRef.id == session?.user?.id ? (
      ""
    ) : (
      <span>{notification.userRef?.name}&nbsp;</span>
    );
  return (
    <>
      <Flex
        borderRadius="8px"
        p={2}
        mb={1}
        _hover={{
          bg: mode("gray.100", "gray.600"),
          "& .TreeDot": {
            display: "inherit",
          },
        }}
        bg={
          notification.isRead
            ? "inherit"
            : mode("gray.100", ["gray.700", "gray.750"])
        }
        w="100%"
        onClick={() => push(notification.url)}
      >
        <Avatar
          mr={2}
          name={notification.userRef?.name}
          src={notification.userRef?.image}
          _hover={{ cursor: "pointer" }}
          boxSize={["2.9rem", "3rem"]}
        />
        <Flex grow={1} _hover={{ cursor: "pointer" }} d="column">
          <Text fontSize="sm">
            <Box as="span" fontWeight={600}>
              {fromName}
            </Box>

            {notification.message}
          </Text>
          <Text fontSize={["xs", "xs"]}>
            {moment(notification.updatedAt).fromNow()}
          </Text>
        </Flex>
        <Box minW={5}>
          <Icon
            _hover={{ cursor: "pointer" }}
            display="none"
            className="TreeDot"
            fontSize={["1.2rem", "1.2rem", "1.4rem"]}
          >
            <BsThreeDotsVertical />
          </Icon>
        </Box>
      </Flex>
    </>
  );
}
