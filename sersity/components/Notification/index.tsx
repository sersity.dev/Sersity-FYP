import { IconButton } from "@chakra-ui/button";
import { useDisclosure } from "@chakra-ui/hooks";
import {
  Popover,
  PopoverArrow,
  PopoverBody,
  PopoverContent,
  PopoverTrigger,
} from "@chakra-ui/popover";

import { TabList, Tab, TabPanel, TabPanels, Tabs } from "@chakra-ui/tabs";

import NotiChatLists from "./NotiChatList";
import NotificationLists from "./NotificationList";
import { VscBell } from "react-icons/vsc";
import { Box } from "@chakra-ui/layout";

import CountNewNoti from "./CountNewNoti";
import { useTranslation } from "hook/useTranslation";

NotificationComponent.defaultProps = {
  isMobileUi: false,
};

export default function NotificationComponent({ isMobileUi }) {
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      {isMobileUi ? (
        <NotiTab />
      ) : (
        <Popover
          isOpen={isOpen}
          onOpen={onOpen}
          onClose={onClose}
          isLazy
          placement="bottom"
        >
          <PopoverTrigger>
            <Box position="relative">
              <IconButton
                aria-label="show notification"
                onClick={onOpen}
                variant="ghost"
                fontSize="1.4em"
                icon={<VscBell />}
              />
              <CountNewNoti />
            </Box>
          </PopoverTrigger>
          <PopoverContent mt={3} mr={2} borderRadius="10px" pt={1} w="320px">
            <PopoverArrow />
            <PopoverBody p={0}>
              <NotiTab />
            </PopoverBody>
          </PopoverContent>
        </Popover>
      )}
    </>
  );
}

const NotiTab = () => {
  const { t } = useTranslation();
  return (
    <Tabs
      isLazy
      variant="soft-rounded"
      w="100%"
      colorScheme="teal"
      isFitted
      display="flex"
      flexDirection="column"
    >
      <TabList mt={2.5} mx={3} display="flex">
        <Tab>{t("notifications")}</Tab>
        <Tab>{t("messages")}</Tab>
      </TabList>
      <TabPanels
        flex={1}
        display="flex"
        flexDirection="column"
        overflow="scroll"
      >
        <TabPanel>
          <NotificationLists />
        </TabPanel>
        <TabPanel h="100%">
          <NotiChatLists />
        </TabPanel>
      </TabPanels>
    </Tabs>
  );
};
