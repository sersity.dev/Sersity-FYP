import { useApolloClient, useMutation, useQuery } from "@apollo/client";
import { Center, Flex } from "@chakra-ui/layout";
import EmptyData from "components/EmptyData";
import Loading from "components/Loading";
import {
  CREATED_NOTIFICATION,
  GET_USER_NOTIFICATIONS,
  READ_NOTIFICATIONS,
} from "hook/graphql/notification";
import { useSession } from "next-auth/client";
import { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import NotiUi from "../NotificationUi";

export default function NotiChatLists() {
  const [session] = useSession();
  const [hasMore, setHasMore] = useState(true);

  const [readNotification] = useMutation(READ_NOTIFICATIONS);

  const { data, fetchMore, loading } = useQuery(GET_USER_NOTIFICATIONS, {
    variables: {
      where: {
        type: {
          equals: "CHAT",
        },
      },
      userId: session?.user.id,
      take: 10,
    },
  });

  const notifications = data?.user?.notifications;
  function downFetchMore() {
    fetchMore({
      variables: {
        where: {
          type: {
            equals: "CHAT",
          },
        },
        userId: session?.user.id,
        skip: notifications?.length,
      },
    });
  }
  const client = useApolloClient();
  useEffect(() => {
    if (notifications)
      setHasMore(notifications.length % 10 === 0 ? true : false);

    if (notifications && notifications.length > 0) {
      const unSeen: [] = notifications.filter((e) => e.readAt === null);
      const clean = unSeen.filter((e: any) => {
        if (e != undefined) return e.id;
      });
      const ids = clean.map((e: any) => e.id);
      if (ids && ids?.length > 0)
        readNotification({
          variables: {
            ids,
          },
          update(cache) {
            cache.modify({
              id: cache.identify({ __typename: "Query" }),
              fields: {
                countNewNotification(e) {
                  return { ...e, count: 0 };
                },
              },
            });
          },
        });
    }
    return () => {
      const NoType = "equals";
      const ref = `notifications:{"where":{"type":{"${NoType}":"CHAT"}}}`;
      data &&
        client.cache.modify({
          id: client.cache.identify(data?.user),
          fields: {
            notifications(e, { storeFieldName }) {
              if (storeFieldName === ref) {
                let newE = e.map((e) => ({ ...e, isRead: true }));
                return newE;
              }
            },
          },
        });
    };
  }, [notifications]);

  return (
    <>
      <Flex d="column" w="100%" maxH="560px" overflow="auto" id="NotiTypeChat">
        {loading ? (
          <Center>
            <Loading />
          </Center>
        ) : notifications && notifications.length > 0 ? (
          <InfiniteScroll
            style={{ overflow: "hidden" }}
            loader={
              <Center>
                <Loading />
              </Center>
            }
            dataLength={notifications.length}
            next={downFetchMore}
            hasMore={hasMore}
            scrollableTarget="NotiTypeChat"
          >
            {notifications.map((e, id) => (
              <NotiUi key={e.id} notification={e} />
            ))}
          </InfiniteScroll>
        ) : (
          <EmptyData minH="300px">Empty Data</EmptyData>
        )}
      </Flex>
    </>
  );
}
