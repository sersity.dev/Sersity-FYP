import { useQuery } from "@apollo/client";
import { Badge } from "@chakra-ui/layout";
import { COUNT_NEW_NOTIFICATION } from "hook/graphql/notification";

export default function CountNewNoti(props) {
  const { ...rest } = props;
  const { data, loading } = useQuery(COUNT_NEW_NOTIFICATION);
  const countNewNotification = data?.countNewNotification;
  return (
    <Badge
      position="absolute"
      top={1}
      right={1}
      variant="solid"
      fontSize="0.6em"
      bg="teal.500"
      {...rest}
    >
      {countNewNotification?.count || (loading && <span>&#183;</span>)}
    </Badge>
  );
}
