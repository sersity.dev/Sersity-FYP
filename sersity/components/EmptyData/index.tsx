import { Center, Flex, Text } from "@chakra-ui/layout";
import { useTranslation } from "hook/useTranslation";
import { FaGg } from "react-icons/fa";

export default function EmptyData(props) {
  const { children, ...rest } = props;
  const { t } = useTranslation();
  return (
    <Center flexDirection="column" {...rest} color="gray.400">
      <FaGg fontSize="2em" />
      <Text>{children || t("empty")}</Text>
    </Center>
  );
}
