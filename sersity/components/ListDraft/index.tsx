import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogCloseButton,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  Box,
  Button,
  Flex,
  IconButton,
  Menu,
  MenuButton,
  MenuDivider,
  MenuGroup,
  MenuList,
  Skeleton,
  Stack,
  Text,
  useColorModeValue,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import React, { useState } from "react";

import { CgFileDocument } from "react-icons/cg";

import { DELETE_DRAFT, GET_DRAFTS } from "hook/graphql/post";

import { useLazyQuery, useMutation } from "@apollo/client";
import { BsFillTrashFill } from "react-icons/bs";
import { useTranslation } from "hook/useTranslation";
import EmptyData from "components/EmptyData";
import useMQ from "hook/useMQ";

function ListDraft(props) {
  const toast = useToast();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const cancelRef = React.useRef();
  const [isDeleting, setIsDeleting] = useState(false);

  const [getDraft, { data }] = useLazyQuery(GET_DRAFTS, {
    fetchPolicy: "network-only",
  });

  const [deleteDraft] = useMutation(DELETE_DRAFT);
  const { t } = useTranslation();
  function deleteDraftFnc(draftId) {
    setIsDeleting(true);
    deleteDraft({
      variables: {
        draftId: draftId,
      },
      update(cache, { data }) {
        cache.modify({
          id: cache.identify({ __typename: "Post", id: draftId }),
          fields(fieldValue, details) {
            return details.DELETE;
          },
        });
        toast({
          title: "Deleted successfully",
          status: "success",
          duration: 1000,
          isClosable: true,
        });
        onClose();
        setIsDeleting(false);
      },
    });
  }

  let content = (
    <>
      <Stack padding="10px">
        <Skeleton height="20px" />
        <Skeleton height="20px" />
        <Skeleton height="20px" />
      </Stack>
      <Stack padding="10px">
        <Skeleton height="20px" />
        <Skeleton height="20px" />
        <Skeleton height="20px" />
      </Stack>
    </>
  );
  if (data) {
    // console.log(data.userPosts);
    const message = t("are you sure you want to delete this draft ?");
    if (data?.userPosts.length > 0) {
      content = data?.userPosts.map((e, i) => (
        <Box key={i} _hover={{ background: "gray.100" }} p={3}>
          <Flex justifyContent="space-between" w="100%" alignItems="center">
            <Box
              padding="10px"
              key={i}
              overflow="hidden"
              maxW="300px"
              onClick={() => {
                props.openModal();
                props.setContent({
                  title: e.title,
                  content: e.content,
                  id: e.id,
                  tags: e.tags,
                  isDrafted: true,
                });
              }}
            >
              <Text fontWeight="bold" noOfLines={3}>
                {e.title}
              </Text>
              <Text noOfLines={3} fontSize="14px">
                {e.content}
              </Text>
            </Box>
            <Box marginLeft="10px">
              <IconButton
                onClick={onOpen}
                aria-label="Delete Draft"
                icon={<BsFillTrashFill size="16px" color="red" />}
                background="transparant"
              />
              <AlertDialog
                motionPreset="slideInBottom"
                leastDestructiveRef={cancelRef}
                onClose={onClose}
                isOpen={isOpen}
                isCentered
              >
                <AlertDialogOverlay />

                <AlertDialogContent>
                  <AlertDialogHeader>{t("delete this")}?</AlertDialogHeader>
                  <AlertDialogCloseButton />
                  <AlertDialogBody>{message}</AlertDialogBody>
                  <AlertDialogFooter>
                    <Button ref={cancelRef} onClick={onClose}>
                      {t("no")}
                    </Button>
                    <Button
                      colorScheme="red"
                      isLoading={isDeleting}
                      ml={3}
                      onClick={() => deleteDraftFnc(e.id)}
                    >
                      {t("yes")}
                    </Button>
                  </AlertDialogFooter>
                </AlertDialogContent>
              </AlertDialog>
            </Box>
          </Flex>
          {/* <MenuDivider /> */}
        </Box>
      ));
    } else {
      content = <EmptyData py={5} />;
    }
  }

  const { isMobile } = useMQ();

  return (
    <>
      {isMobile ? (
        <Box>Draft Mobile</Box>
      ) : (
        <Menu placement="right">
          <MenuButton
            as={Button}
            p={0}
            variant="unstyled"
            h="1.5rem"
            fontWeight={500}
            onClick={() => getDraft()}
          >
            <Flex>
              <CgFileDocument size="1.4rem" />
              <Box ml={2}>{t("drafted")}</Box>
            </Flex>
          </MenuButton>
          <MenuList
            border="none"
            boxShadow="sm"
            marginLeft="40px"
            color="black"
            maxH="500px"
            overflow="auto"
            w="350px"
          >
            <MenuGroup
              title={t("draft posts")}
              fontSize="16px"
              color={useColorModeValue("black", "white")}
              fontWeight="bold"
            >
              <MenuDivider />
              {content}
            </MenuGroup>
          </MenuList>
        </Menu>
      )}
    </>
  );
}

export default ListDraft;
