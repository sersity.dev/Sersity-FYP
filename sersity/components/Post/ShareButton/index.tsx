import { Button } from "@chakra-ui/button";

import { VStack } from "@chakra-ui/layout";
import React from "react";
import { useRouter } from "next/router";
import { VscLiveShare } from "react-icons/vsc";
import {
  Popover,
  PopoverArrow,
  PopoverBody,
  PopoverContent,
  PopoverTrigger,
  Portal,
} from "@chakra-ui/react";

import _ from "lodash";

import {
  EmailIcon,
  EmailShareButton,
  FacebookIcon,
  FacebookShareButton,
  LineIcon,
  LineShareButton,
  LinkedinIcon,
  LinkedinShareButton,
  TelegramIcon,
  TelegramShareButton,
  TwitterIcon,
  TwitterShareButton,
} from "react-share";
// import ShareButton from "react-share/lib/ShareButton";
import { apiUrl } from "api/url";
import { useTranslation } from "hook/useTranslation";

export default function ShareButton(props) {
  const router = useRouter();
  const data = {
    title: props.title,
    text: props.content,
    url: `${apiUrl}${router.basePath}/post/${props.id}`,
  };
  const { t } = useTranslation();
  const shareUrl = `${apiUrl}/post/${props.id}`;
  const title = props.title;
  const canShare = () => {
    let isCan = false;
    try {
      isCan = window.navigator.canShare(data);
    } catch (error) {}
    return isCan;
  };
  const isCanShare = canShare();

  return (
    <>
      {isCanShare ? (
        <Button
          size="sm"
          aria-label="Comment"
          leftIcon={<VscLiveShare fontSize="1.3em" />}
          variant="ghost"
          onClick={() => {
            try {
              window.navigator
                .share(data)
                .then(() => console.log("Share was successful."))
                .catch((error) => console.log("Sharing failed", error));
            } catch (error) {}
          }}
        >
          {t("share")}
        </Button>
      ) : (
        <Popover isLazy placement="right">
          <PopoverTrigger>
            <Button
              size="sm"
              aria-label="Comment"
              leftIcon={<VscLiveShare fontSize="1.3em" />}
              variant="ghost"
              onClick={() => {
                try {
                  window.navigator
                    .share({ title: props.title, text: props.content })
                    .then(() => console.log("Share was successful."))
                    .catch((error) => console.log("Sharing failed", error));
                } catch (error) {}
              }}
            >
              {t("share")}
            </Button>
          </PopoverTrigger>
          <Portal>
            <PopoverContent w="auto" shadow="sm">
              <PopoverArrow />
              <PopoverBody
                sx={{
                  ".Demo__some-network__share-button": {
                    transition: "0.3s all ease",
                    "&:hover": { transform: "scale(1.2)" },
                  },
                }}
              >
                <VStack justify="center">
                  <FacebookShareButton
                    url={shareUrl}
                    quote={title}
                    className="Demo__some-network__share-button"
                  >
                    <FacebookIcon size={32} round />
                  </FacebookShareButton>
                  {/* <FacebookMessengerShareButton
                    url={shareUrl}
                    quote={title}
                    className="Demo__some-network__share-button"
                  >
                    <FacebookMessengerIcon size={32} round />
                  </FacebookMessengerShareButton> */}
                  <TwitterShareButton
                    url={shareUrl}
                    title={title}
                    className="Demo__some-network__share-button"
                  >
                    <TwitterIcon size={32} round />
                  </TwitterShareButton>
                  <TelegramShareButton
                    url={shareUrl}
                    title={title}
                    className="Demo__some-network__share-button"
                  >
                    <TelegramIcon size={32} round />
                  </TelegramShareButton>
                  <LinkedinShareButton
                    url={shareUrl}
                    className="Demo__some-network__share-button"
                  >
                    <LinkedinIcon size={32} round />
                  </LinkedinShareButton>
                  <EmailShareButton
                    url={shareUrl}
                    subject={title}
                    body="body"
                    className="Demo__some-network__share-button"
                  >
                    <EmailIcon size={32} round />
                  </EmailShareButton>
                  <LineShareButton
                    url={shareUrl}
                    title={title}
                    className="Demo__some-network__share-button"
                  >
                    <LineIcon size={32} round />
                  </LineShareButton>
                </VStack>
              </PopoverBody>
            </PopoverContent>
          </Portal>
        </Popover>
      )}
    </>
  );
}
