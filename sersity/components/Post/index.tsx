import { Avatar } from "@chakra-ui/avatar";
import { Button, IconButton } from "@chakra-ui/button";
import { useColorModeValue as mode } from "@chakra-ui/color-mode";
import { Box, Flex, HStack, Text } from "@chakra-ui/layout";
import React, { useEffect, useState } from "react";
import { TiArrowDownThick, TiArrowUpThick } from "react-icons/ti";

import moment from "hook/moment";
import { useRouter } from "next/router";

import {
  BsBookmark,
  BsBookmarkFill,
  BsThreeDotsVertical,
} from "react-icons/bs";
import { HiOutlineChatAlt2 } from "react-icons/hi";

import {
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Tag,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import { CREATE_POST_VOTE, CREATE_SAVE, GET_POST } from "hook/graphql/post";
import _ from "lodash";
import { useSession } from "next-auth/client";
import ReportModal from "components/ReportDialog";
import { helperRank } from "components/Context/rankBorder";

import CommentSection from "./CommentSection";
import { useTranslation } from "hook/useTranslation";
import ShareButton from "./ShareButton";
import { useApolloClient } from "@apollo/client";

function ReportModalDisclosure() {
  const { isOpen, onOpen, onClose } = useDisclosure();

  return {
    isReportModalOpen: isOpen,
    onReportModalOpen: onOpen,
    onReportModelClose: onClose,
  };
}

const Post = (props) => {
  // console.log(props);
  const toast = useToast();
  const cardBG = mode("white", "gray.700");
  const { push } = useRouter();
  const apolloClient = useApolloClient();
  const [session, laoding] = useSession();
  const { t } = useTranslation();
  const [post, setPost] = useState({
    score: props.votes,
    action: props.isVoted.action,
    isVoted: props.isVoted.isVoted,
    isVoting: false,
    comment: props.comment,
    tags: props.tags,
    isSaved: props.isSaved,
  });

  //Report Modal
  const {
    isReportModalOpen,
    onReportModalOpen,
    onReportModelClose,
  } = ReportModalDisclosure();

  useEffect(() => {
    setPost({
      ...post,
      isSaved: props.isSaved,
      score: props.votes,
      action: props.isVoted.action,
      isVoted: props.isVoted.isVoted,
      isVoting: false,
    });
  }, [props.votes, props.isVoted, props.isSaved]);

  const createVote = async (option) => {
    setPost({ ...post, isVoting: true });
    apolloClient.mutate({
      mutation: CREATE_POST_VOTE,
      variables: {
        action: option.action,
        postId: `${option.postId}`,
        isVoted: post.isVoted,
      },
      update: (cache, { data }) => {
        setPost({
          ...post,
          isVoting: false,
          isVoted: true,
          action: option.action,
          score: data.createPostVote.upvote - data.createPostVote.downvote,
        });
        cache.modify({
          id: cache.identify({ __typename: "Post", id: props.id }),
          fields: {
            isVoted(e) {
              const v = {
                __typename: "IsVoted",
                isVoted: data.createPostVote.isVoted.isVoted,
                action: data.createPostVote.isVoted.action,
              };
              return v;
            },
            upvote(e) {
              return data.createPostVote.upvote;
            },
            downvote(e) {
              return data.createPostVote.downvote;
            },
          },
        });
      },
    });
  };

  const save = async (option) => {
    setPost({ ...post, isSaved: !post.isSaved });
    apolloClient.mutate({
      mutation: CREATE_SAVE,
      variables: {
        post: "" + props.id,
      },
      update(cache, { data }) {
        cache.modify({
          id: cache.identify({ __typename: "Post", id: props.id }),
          fields: {
            isSaved: () => !post.isSaved,
          },
        });
      },
    });
  };

  return (
    <Flex
      direction="column"
      bg={cardBG}
      my={3}
      p={[3, 5]}
      borderRadius="10px"
      // onClick={() => push("/post/" + props.id)}
    >
      <Flex justify="space-between">
        <Flex>
          <Box sx={helperRank}>
            <Avatar
              src={
                props.author?.image !== "anonymous"
                  ? props.author?.image
                  : "/avatar/anonymous.png"
              }
              name={props.author?.name || "author!"}
              bg="teal.500"
              size="md"
              cursor="pointer"
              onClick={() => {
                if (props.author.rank !== "ANONYMOUS") {
                  push("/profile/" + props.author.id);
                } else {
                  toast({
                    title: "Anonymous Account",
                    status: "warning",
                    // description: "This account is hidden by the owner",
                    duration: 2000,
                    isClosable: true,
                  });
                }
              }}
              className={
                props.author?.rank?.toString().toLowerCase() || "anime"
              }
            />
          </Box>
          <Box ml="3">
            <Flex align="center">
              <Text
                fontWeight={600}
                fontSize={["0.8em", "sm", "md"]}
                alignItems="center"
                cursor="pointer"
                onClick={() => {
                  if (props.author.rank !== "ANONYMOUS") {
                    push("/profile/" + props.author.id);
                  } else {
                    toast({
                      title: "Anonymous Account",
                      status: "warning",
                      // description: "This account is hidden by the owner",
                      duration: 2000,
                      isClosable: true,
                    });
                  }
                }}
              >
                {props.author.name}
              </Text>
              <Box mx={0.5} as="span">
                &#183;
              </Box>
              <Box
                border="1x solid green"
                fontSize={["0.7em", "0.9em"]}
                bg="none"
                color="gray.500"
              >
                {moment(props.createdAt).fromNow()}
              </Box>
            </Flex>
            <Text
              fontWeight="500"
              color="gray.400"
              fontStyle="italic"
              textTransform="capitalize"
            >
              {props.author.rank.toString().toLowerCase()}
            </Text>
            {/* <Flex mt="2px">
              <HStack spacing={1}>
                {props.tags?.map((e, i) => (
                  <Tag
                    borderRadius="4px"
                    key={i}
                    fontSize={["xs", "12px", "12px"]}
                    fontStyle="italic"
                    color="white"
                    bg="red.400"
                    cursor="pointer"
                    onClick={() => push("/tag/" + e.tag)}
                  >
                    {e.tag}
                  </Tag>
                ))}
                {props.tags?.length < 1 && (
                  <Tag
                    fontSize={["10px", "12px", "12px"]}
                    fontStyle="italic"
                    color="gray.400"
                  >
                    No tag
                  </Tag>
                )}
              </HStack>
            </Flex> */}
            {/* <Text fontSize="sm">UI Engineer</Text> */}
          </Box>
        </Flex>
        <Flex justifyContent="flex-end">
          <IconButton
            // color="teal.500"
            aria-label="Save"
            variant="ghost"
            // boxSize={["1.5rem", "2.5rem"]}
            isLoading={post.score === undefined ? true : false}
            icon={post.isSaved === false ? <BsBookmark /> : <BsBookmarkFill />}
            onClick={(e) => {
              e.preventDefault();
              if (session) {
                save(false);
              } else {
                push("/auth/login");
              }
            }}
          />
          <Menu key={props.id}>
            {/* <IconButton
              aria-label="Options"
              icon={<BsThreeDotsVertical/>}
              variant="outline"
            /> */}
            <MenuButton
              as={IconButton}
              // boxSize={["1.5rem", "2.5rem"]}
              aria-label="Options"
              icon={<BsThreeDotsVertical />}
              variant="ghost"
            />
            <MenuList>
              <MenuItem onClick={onReportModalOpen}>
                <ReportModal
                  isOpen={isReportModalOpen}
                  onClose={onReportModelClose}
                  apolloClient={apolloClient}
                  apolloState={null}
                  postId={props.id}
                />
              </MenuItem>
              {/* <MenuItem>Hide</MenuItem> */}
            </MenuList>
          </Menu>
        </Flex>
      </Flex>
      <Flex direction="column" mt={3}>
        <Text
          fontWeight={600}
          fontSize={["sm", "1.2em"]}
          wordBreak="break-word"
          mb={2}
        >
          {props.title}
        </Text>

        <Text
          color={mode("gray.600", "gray.200")}
          style={{ whiteSpace: "pre-line" }}
          fontSize={["0.95rem", "1.1rem"]}
          noOfLines={props.type === "multi-post" ? 10 : 1000}
          wordBreak="break-word"
          lineHeight={1.65}
        >
          {props.content}
        </Text>
        <Flex mt="10px" justifyContent="end" w="100%">
          <HStack spacing={1} justifyContent="flex-end" w="100%">
            {props.tags?.map((e, i) => (
              <Tag
                borderRadius="25px"
                key={i}
                fontSize={["xs", "12px", "12px"]}
                fontStyle="italic"
                color={mode("gray.700", "gray.100")}
                bg={mode("teal.100", "gray.600")}
                cursor="pointer"
                onClick={() => push("/tag/" + e.tag)}
              >
                {e.tag}
              </Tag>
            ))}
            {props.tags?.length < 1 && (
              <Tag
                fontSize={["10px", "12px", "12px"]}
                fontStyle="italic"
                color="gray.400"
              >
                No tag
              </Tag>
            )}
          </HStack>
        </Flex>
      </Flex>
      <Flex mt={3} justify="space-between">
        {post.score !== undefined ? (
          <Flex>
            <Button
              // color={post.action === "UPVOTE" ? "teal.500" : ""}
              color={post.action === "UPVOTE" ? "white" : "inherit"}
              bgColor={post.action === "UPVOTE" ? "teal.500" : ""}
              borderLeftRadius="30px"
              borderRightRadius="0"
              aria-label="Upvote"
              leftIcon={<TiArrowUpThick />}
              variant="solid"
              // disabled={isVoting}
              _hover={post.isVoting && { cursor: "wait" }}
              size="sm"
              onClick={(e) => {
                e.preventDefault();
                if (session) {
                  if (post.isVoting === false) {
                    if (post.action !== "UPVOTE") {
                      createVote({
                        action: "UPVOTE",
                        postId: props.id,
                      });
                    } else {
                      createVote({
                        action: "UNVOTED",
                        postId: props.id,
                      });
                    }
                  }
                } else {
                  push("/auth/login");
                }
              }}
            >
              {post.score}
            </Button>
            <IconButton
              // color={post.action === "DOWNVOTE" ? "red.400" : ""}
              color={post.action === "DOWNVOTE" ? "white" : "inherit"}
              bgColor={post.action === "DOWNVOTE" ? "red.400" : ""}
              borderLeft="1px solid #CBD5E0"
              borderLeftRadius="0"
              borderRightRadius="30"
              aria-label="Downvote"
              // disabled={isVoting}
              _hover={post.isVoting && { cursor: "wait" }}
              size="sm"
              icon={<TiArrowDownThick />}
              onClick={(e) => {
                e.preventDefault();
                if (session) {
                  if (post.isVoting === false) {
                    if (post.action !== "DOWNVOTE") {
                      createVote({
                        action: "DOWNVOTE",
                        postId: props.id,
                      });
                    } else {
                      createVote({
                        action: "UNVOTED",
                        postId: props.id,
                      });
                    }
                  }
                } else {
                  push("/auth/login");
                }
              }}
            />
          </Flex>
        ) : (
          <Button isLoading={true} borderRadius="30" w="100px" />
        )}

        <Button
          size="sm"
          aria-label="Comment"
          leftIcon={<HiOutlineChatAlt2 fontSize="1.4em" />}
          rightIcon={
            props.commentCount > 0 && (
              <Box fontSize="0.9em" color="gray.500">
                ({props.commentCount})
              </Box>
            )
          }
          variant="ghost"
          // onClick={onToggle}
          onClick={() => push("/post/" + props.id)}
        >
          {t("comment")}
        </Button>
        <ShareButton {...props} />
      </Flex>

      <CommentSection
        type={props.type}
        comment={props.comment}
        setComments={props.setComments}
        postId={props.id}
      />
    </Flex>
  );
};

export default Post;
// export default withApollo(Post, {ssr:false});
