import { InputGroup } from "@chakra-ui/input";
import {
  Box,
  Button,
  Flex,
  IconButton,
  Input,
  InputRightElement,
  SkeletonCircle,
  SkeletonText,
} from "@chakra-ui/react";
import SingleComent from "components/Comment";
import { CREATE_COMMENT } from "hook/graphql/post";
import { initApolloClient } from "lib/apolloClient";
import { useSession } from "next-auth/client";
import { useRouter } from "next/router";
import React, { useRef, useState } from "react";
import { HiOutlineChatAlt2 } from "react-icons/hi";
import { useColorModeValue as mode } from "@chakra-ui/color-mode";
import Loading from "components/Loading";
import { BiLogInCircle, BiSend } from "react-icons/bi";

function CommentSection(props) {
  // console.log(props.comment);

  const { push } = useRouter();
  var commentInputRef = useRef();
  const apolloClient = initApolloClient();
  const [isCommenting, setIsCommenting] = useState(false);
  const commentBg = mode("gray.100", "gray.750");
  const [session, laoding] = useSession();
  const bottomRef = useRef();
  let commentInput = "";
  function handleChange(e) {
    e.preventDefault();
    commentInput = e.target.value;
  }
  const [comment, setComment] = useState(
    props?.comment?.length > 0 ? props.comment[0] : null
  );

  if (props.type === "multi-post") {
    const createComment = async () => {
      if (commentInput !== "") {
        setIsCommenting(true);
        apolloClient.mutate({
          mutation: CREATE_COMMENT,
          variables: {
            postId: props.postId,
            comment: "" + commentInput,
          },
          update: (cache, { data }) => {
            let dataCmt = data.createComment;
            cache.modify({
              id: cache.identify({ __typename: "Post", id: props.postId }),
              fields: {
                topComment(e, { toReference }) {
                  const newCmtRef = toReference(data.createComment, true);
                  return [newCmtRef];
                },
              },
            });
            setComment(dataCmt);
            commentInput = "";
            setIsCommenting(false);
          },
        });
      }
    };

    function onSend(e){
      e.preventDefault()
      if (session) {
        createComment();
      } else {
        push("/auth/login");
      }
    }

    if (comment == undefined) {
      return (
        <Box mt={4} rounded="md" as="form" onSubmit={onSend}>
          <InputGroup mb={3} h={["1.8em", "2em"]}>
            <Input
              pr="60px"
              focusBorderColor="teal.500"
              variant="filled"
              placeholder={
                session
                  ? "Start a first comment"
                  : "Please login to comment on this post"
              }
              onChange={handleChange}
              // ref={commentInputRef}
              // defaultValue={commentInput}
              // value={commentInputRef}
            />
            {/* <InputRightElement width="7.5em">
              <Button
                w="100%"
                borderLeftRadius="0px"
                h="2.4rem"
                colorScheme="teal"
                size="md"
                isLoading={isCommenting}
                rightIcon={<HiOutlineChatAlt2 fontSize="1.2rem" />}
                onClick={() => {
                  if (session) {
                    createComment();
                  } else {
                    push("/auth/login");
                  }
                }}
              >
                {session ? "Comment" : "Login"}
              </Button>
            </InputRightElement> */}
            <InputRightElement width="3rem" mr={1}>
              <IconButton
                aria-label="sumit cmt"
                isLoading={isCommenting}
                spinner={<Loading size={8} />}
                w="100%"
                variant="ghost"
                color="teal.500"
                h="2em"
                type="submit"
                // onClick={() => {
                //   if (session) {
                //     createComment();
                //   } else {
                //     push("/auth/login");
                //   }
                // }}
              >
                {session ? (
                  <BiSend fontSize="1.8em" />
                ) : (
                  <BiLogInCircle fontSize="1.8em" />
                )}
              </IconButton>
            </InputRightElement>
          </InputGroup>
        </Box>
      );
    } else {
      return (
        <SingleComent
          mt={4}
          comment={comment}
          type="single-comment"
          reply="false"
        />
      );
    }
  }
  if (props.type === "single-post") {
    function onSend(e){
      e.preventDefault()
          if (session) {
            createComment(commentInputRef, bottomRef);
          } else {
            push("/auth/login");
          }
    }

    const createComment = async (ref, bottomRef) => {
      if (commentInput !== "") {
        setIsCommenting(true);
        apolloClient.mutate({
          mutation: CREATE_COMMENT,
          variables: {
            postId: props.postId,
            comment: "" + commentInput,
          },
          update: (proxy, mutationResult) => {
            let data = mutationResult.data.createComment;
            props.setComments(props.comment, [data]);
            commentInput = "";
            ref.current.value = "";
            bottomRef.current.scrollIntoView({ behavior: "smooth" });
            setIsCommenting(false);
          },
        });
      }
    };

    return (
      <Box>
        <Box pb={2} mt={3} rounded="md" as="form" onSubmit={onSend}>
          <InputGroup h={["1.8em", "2em"]}>
            <Input
              pr="60px"
              borderRadius="8px"
              focusBorderColor="teal.500"
              variant="filled"
              onChange={handleChange}
              placeholder={
                session
                  ? "Comment your thought ..."
                  : "Please login to comment on this post"
              }
              ref={commentInputRef}
            />
            {/* <InputRightElement width="7.5rem">
              <Button
                w="100%"
                borderLeftRadius="0px"
                colorScheme="teal"
                size="sm"
                isLoading={isCommenting}
                rightIcon={<HiOutlineChatAlt2 fontSize="1.2rem" />}
                onClick={() => {
                  if (session) {
                    createComment(commentInputRef, bottomRef);
                  } else {
                    push("/auth/login");
                  }
                }}
              >
                {session ? "Comment" : "Login"}
              </Button>
            </InputRightElement> */}
            <InputRightElement width="3rem" mr={1}>
              <IconButton
                aria-label="sumit cmt"
                isLoading={isCommenting}
                spinner={<Loading size={8} />}
                w="100%"
                variant="ghost"
                color="teal.500"
                h="2em"
                type="submit"
                // onClick={() => {
                //   if (session) {
                //     createComment(commentInputRef, bottomRef);
                //   } else {
                //     push("/auth/login");
                //   }
                // }}
              >
                <BiSend
                  // style={
                  //   inputValue
                  //     ? {
                  //         transform: "rotate(-90deg)",
                  //         transition: "0.5s all ease",
                  //       }
                  //     : {
                  //         transform: "rotate(0deg)",
                  //         transition: "0.5s all ease",
                  //       }
                  // }
                  fontSize="1.8rem"
                />
              </IconButton>
            </InputRightElement>
          </InputGroup>
        </Box>

        {/* Comment List Section */}
        {props.comment !== undefined &&
          props.comment.length > 0 &&
          props.comment?.map((cmt) => {
            return (
              <SingleComent
                mt={4}
                comment={cmt}
                key={cmt.id}
                post_id={props.postId}
              />
            );
          })}
        {props.comment !== undefined && props.comment.length === 0 && (
          <Box>
            <Flex
              direction="column"
              justify="center"
              borderRadius="10px"
              bgColor={commentBg}
              boxShadow={mode("inherit", "sm")}
              mt={4}
            >
              <Flex py={4} px={4}>
                No comment found
              </Flex>
            </Flex>
          </Box>
        )}
        {props.comment === undefined && (
          <Box>
            <Flex
              direction="column"
              justify="center"
              borderRadius="10px"
              bgColor={commentBg}
              boxShadow={mode("inherit", "sm")}
              mt={4}
            >
              <Flex py={4} px={4} justifyContent="space-between">
                <SkeletonCircle size="10" />
                <SkeletonText noOfLines={4} spacing="4" w="90%" />
              </Flex>
            </Flex>
            <Flex
              direction="column"
              justify="center"
              borderRadius="10px"
              bgColor={commentBg}
              boxShadow={mode("inherit", "sm")}
              mt={4}
            >
              <Flex py={4} px={4} justifyContent="space-between">
                <SkeletonCircle size="10" />
                <SkeletonText noOfLines={4} spacing="4" w="90%" />
              </Flex>
            </Flex>
            <Flex
              direction="column"
              justify="center"
              borderRadius="10px"
              bgColor={commentBg}
              boxShadow={mode("inherit", "sm")}
              mt={4}
            >
              <Flex py={4} px={4} justifyContent="space-between">
                <SkeletonCircle size="10" />
                <SkeletonText noOfLines={4} spacing="4" w="90%" />
              </Flex>
            </Flex>
            <Flex
              direction="column"
              justify="center"
              borderRadius="10px"
              bgColor={commentBg}
              boxShadow={mode("inherit", "sm")}
              mt={4}
            >
              <Flex py={4} px={4} justifyContent="space-between">
                <SkeletonCircle size="10" />
                <SkeletonText noOfLines={4} spacing="4" w="90%" />
              </Flex>
            </Flex>
            <Flex
              direction="column"
              justify="center"
              borderRadius="10px"
              bgColor={commentBg}
              boxShadow={mode("inherit", "sm")}
              mt={4}
            >
              <Flex py={4} px={4} justifyContent="space-between">
                <SkeletonCircle size="10" />
                <SkeletonText noOfLines={4} spacing="4" w="90%" />
              </Flex>
            </Flex>
          </Box>
        )}

        <Box ref={bottomRef}></Box>
      </Box>
    );
  }
}

export default CommentSection;
