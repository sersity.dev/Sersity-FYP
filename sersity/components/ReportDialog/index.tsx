import { useMutation } from "@apollo/client";
import {
  Button,
  Divider,
  Text,
  Flex,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Tag,
  useColorModeValue,
  useToast,
} from "@chakra-ui/react";
import { CREATE_REPORTED_POST } from "hook/graphql/post";
import { useTranslation } from "hook/useTranslation";

import React, { useState } from "react";
import { RiErrorWarningFill } from "react-icons/ri";

function ReportModal(props) {
  const toast = useToast();
  const [selectedReason, setSelectedReason] = useState({
    reason: "",
    selected: -1,
    isLoading: false,
  });

  let reasonInput = "";
  const [createReportPost] = useMutation(CREATE_REPORTED_POST, {
    onError(e) {
      toast({
        title: `There was an error at the moment!`,
        status: "error",
        isClosable: true,
      });
      setSelectedReason({ ...selectedReason, isLoading: false });
    },
  });

  function handleReasonInput(e) {
    reasonInput = e.target.value;
  }
  function report() {
    setSelectedReason({ ...selectedReason, isLoading: true });
    const postId = props.postId;
    let reason = "";
    if (selectedReason.selected !== -1) {
      reason = selectedReason.reason;
    } else {
      reason = reasonInput;
    }
    createReportPost({
      variables: {
        postId: postId,
        reason: reason,
      },
      update() {
        setSelectedReason({ ...selectedReason, isLoading: false });
        props.onClose();
        toast({
          title: `Successfully reported`,
          status: "success",
          isClosable: true,
        });
      },
    });
  }
  const { t } = useTranslation();
  return (
    <>
      {t("report")}
      <Modal onClose={props.onClose} isOpen={props.isOpen} isCentered>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{t("report this post")}</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Flex style={{ flexDirection: "row" }} mb={2}>
              <RiErrorWarningFill fontSize="24px" />
              <Text ml={2}> {t("help us understand what's happening")}.</Text>
            </Flex>
            <Flex flexWrap="wrap" mt={0} justifyContent="center">
              {[
                "hate speech",
                "sexual content",
                "violence",
                "harassment",
                "scam or fake account",
                "unauthorized content",
                "intellectual property",
              ].map((e, i) => (
                <Tag
                  key={i}
                  mt={3}
                  ml={3}
                  p={2}
                  cursor="pointer"
                  _hover={
                    selectedReason.selected !== i && {
                      bg: useColorModeValue("gray.200", "gray.800"),
                    }
                  }
                  bg={
                    selectedReason.selected === i
                      ? "teal.500"
                      : useColorModeValue("gray.100", "gray.600")
                  }
                  color={
                    selectedReason.selected === i
                      ? "white"
                      : useColorModeValue("black", "white")
                  }
                  onClick={() =>
                    setSelectedReason({
                      reason: e,
                      selected: i,
                      isLoading: false,
                    })
                  }
                >
                  {t(e)}
                </Tag>
              ))}
              <Flex w="100%">
                <Divider my={5} />
                <Text my={2} mx={2}>
                  or
                </Text>
                <Divider my={5} />
              </Flex>
              <Text
                fontSize="12px"
                my={3}
                w="100%"
                textAlign="left"
                color="red.400"
              >
                ***
                {t("if your reason is not listed above, please leave it here")}:
              </Text>
              <Input
                placeholder={t("other reason")}
                size="md"
                onFocus={(e) =>
                  setSelectedReason({
                    reason: e.target.value,
                    selected: -1,
                    isLoading: false,
                  })
                }
                onChange={handleReasonInput}
              />
            </Flex>
          </ModalBody>
          <ModalFooter>
            <Flex w="100%" justifyContent="space-between">
              <Button onClick={props.onClose}> {t("close")}</Button>
              <Button
                colorScheme="teal"
                onClick={report}
                isLoading={selectedReason.isLoading}
              >
                {t("report")}
              </Button>
            </Flex>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default ReportModal;
