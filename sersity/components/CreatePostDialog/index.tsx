import {
  Box,
  Button,
  Flex,
  FormControl,
  Icon,
  Input,
  Modal,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  Switch,
  Tag,
  TagCloseButton,
  TagLabel,
  Text,
  Textarea,
} from "@chakra-ui/react";

import React, { useEffect, useState } from "react";
import { BiCloudUpload } from "react-icons/bi";

import { CREATE_DRAFT, CREATE_POST, UPDATE_POST } from "hook/graphql/post";
import AddTag from "./AddTag";
import {
  makeReference,
  useApolloClient,
  useMutation,
  useQuery,
} from "@apollo/client";
import { useRouter } from "next/router";

import { useTranslation } from "hook/useTranslation";
import { SiRedhat } from "react-icons/si";
import { RiBearSmileLine } from "react-icons/ri";
import { GET_USER_SETTING, UPDATE_SETTING } from "hook/graphql/setting";
import { useSession } from "next-auth/client";
const debounce = require("lodash/debounce");
const CreatePostDialog = (props) => {
  const [updateSetting] = useMutation(UPDATE_SETTING);

  const [session] = useSession();
  const { data } = useQuery(GET_USER_SETTING, {
    variables: { id: session?.user.id },
  });
  const [isAnonymous, setIsAnonymous] = useState(
    data?.user?.setting?.anonymous
  );
  // console.log(data?.user?.setting, isAnonymous, session?.user);
  function updateAnonymous() {
    updateSetting({
      variables: {
        type: "anonymous",
        value: !isAnonymous,
      },
      update: (cache) => {
        setIsAnonymous(!isAnonymous);
        cache.modify({
          id: cache.identify({
            __typename: "Setting",
            id: data?.user?.setting?.id,
          }),
          fields: {
            anonymous(e) {
              return !e;
            },
          },
        });
      },
    });
  }
  const AnonyIcon = isAnonymous ? (
    <SiRedhat />
  ) : (
    <RiBearSmileLine fontSize="1.1em" />
  );

  const initialRef = React.useRef();
  const [isLoading, setIsLoading] = useState({
    isDrafting: false,
    isPublishing: false,
  });
  const { push } = useRouter();
  const apolloClient = useApolloClient();
  const [isDrafted, setIsDrafted] = useState(false);
  let tmpTags = [];
  let tagIds = [];
  // setTags([])
  useEffect(() => {
    // getUserSetting()
    setIsAnonymous(data?.user.setting.anonymous);
    setIsDrafted(props.content.isDrafted);
  }, [props.content.isDrafted, data]);

  const { title, content, tags, id } = props.content;

  function handleTitleChange(e) {
    e.preventDefault();
    props.setContent({ ...props.content, title: e.target.value });
    setIsDrafted(false);
  }

  function handleContentChange(e) {
    e.preventDefault();
    props.setContent({ ...props.content, content: e.target.value });
    // console.log("content: " + content);
    setIsDrafted(false);
  }

  const [mutatePost] = useMutation(CREATE_DRAFT);
  const [updatePost] = useMutation(UPDATE_POST);

  const createDraft = () => {
    if (isDrafted === false) {
      tagIds = tags?.map((e, i) => e.id);
      if (title || content) {
        setIsLoading({ isDrafting: true, isPublishing: false });
        setIsDrafted(true);

        if (id === undefined) {
          mutatePost({
            variables: {
              title: "" + title,
              content: "" + content,
              tags: tagIds,
            },
            update(proxy, { data }) {
              // console.log(title, content);
              // props(+data.createDraft.id);
              // console.log("content", data.createDraft.id);
              props.setContent({ ...props.content, id: data.createDraft.id });
              setIsLoading({ isDrafting: false, isPublishing: false });
            },
          });
        }
        if (id) {
          updatePost({
            variables: {
              postId: id,
              title: "" + title,
              content: "" + content,
              status: "DRAFTED",
              published: false,
              tags: tagIds,
            },
            update(cache, { data }) {
              setIsLoading({ isDrafting: false, isPublishing: false });
              if (props.onDraftClick) props.onDraftClick();
            },
          });
        }
      }
    }
  };

  const createPost = () => {
    tagIds = tags?.map((e, i) => e.id);
    if (id === undefined) {
      if (title && content) {
        setIsLoading({ isDrafting: false, isPublishing: true });
        apolloClient.mutate({
          mutation: CREATE_POST,
          variables: {
            title: title,
            content: content,
            tags: tagIds,
          },
          update(cache, { data }) {
            cache.modify({
              id: cache.identify(makeReference("ROOT_QUERY")),
              fields: {
                posts(e, { storeFieldName, toReference }) {
                  const newPostRef = toReference(data.createPost, true);
                  if (storeFieldName === 'posts:{"latest":true}') {
                    return [newPostRef, ...e];
                  } else {
                    return e;
                  }
                },
              },
              optimistic: true,
            });
            setIsLoading({ isDrafting: false, isPublishing: false });
            props.onClose();
            push("/post/" + data.createPost.id);
            // window.location.href = "/post/" + mutationResult.data.createPost.id;
          },
        });
      }
    }
    if (id) {
      if (title || content) {
        apolloClient.mutate({
          mutation: UPDATE_POST,
          variables: {
            postId: id,
            title: "" + title,
            content: "" + content,
            status: "PUBLISHED",
            published: true,
            tags: tagIds,
          },
          update(cache, { data }) {
            cache.modify({
              id: cache.identify(makeReference("ROOT_QUERY")),
              fields: {
                posts(e, { storeFieldName, toReference }) {
                  const newPostRef = toReference(data.updatePost, true);
                  if (storeFieldName === 'posts:{"latest":true}') {
                    return [newPostRef, ...e];
                  } else {
                    return e;
                  }
                },
              },
            });
            setIsLoading({ isDrafting: false, isPublishing: false });
            props.onClose();
            push("/post/" + data.updatePost.id);
            // window.location.href = "/post/" + mutationResult.data.updatePost.id;
          },
        });
      }
    }
  };
  const { t } = useTranslation();
  return (
    <>
      <Modal
        // isOpen={isOpenCreatePostModal()}
        // onClose={onCloseCreatePostModal}
        isOpen={props.isOpen}
        onClose={props.onClose}
        closeOnOverlayClick={false}
        motionPreset="slideInBottom"
        isCentered
      >
        <ModalOverlay />
        <ModalContent borderRadius="10px" maxWidth="container.sm">
          <Flex
            justify="space-between"
            h="60px"
            align="center"
            borderTopRadius="10px"
          >
            <Box ml={5}>
              <Text fontSize="1.2em">{t("create post")}</Text>
            </Box>
            <Box>
              <ModalCloseButton
                sx={{
                  margin: "12px",
                  position: "none",
                  display: "flex",
                  "& button": { margin: 0 },
                }}
                onClick={() => {
                  props.setContent({
                    tags: [],
                    id: undefined,
                    title: "",
                    content: "",
                  });
                  setIsDrafted(false);
                }}
              />
            </Box>
          </Flex>
          <Box p={5} pt={0}>
            <FormControl>
              <Input
                ref={initialRef}
                variant="filled"
                focusBorderColor="teal.500"
                placeholder={t("title")}
                size="lg"
                fontWeight={600}
                borderRadius="10px"
                borderBottomRadius="0px"
                onChange={debounce(handleTitleChange, 500)}
                defaultValue={props.content?.title}
              />
            </FormControl>

            <FormControl mt={1}>
              <Textarea
                variant="filled"
                borderTopRadius="0px"
                focusBorderColor="teal.500"
                borderRadius="10px"
                // size="sm"
                height="270px"
                resize="none"
                placeholder={t("description")}
                onChange={debounce(handleContentChange, 500)}
                defaultValue={props.content?.content}
              />
            </FormControl>
          </Box>
          <Flex justify="start" p={5} pt={0} align="center">
            <Box display="flex" flexWrap="wrap" maxW="100%">
              <Box mr={2}>
                <AddTag
                  setContent={props.setContent}
                  tags={tags}
                  content={props.content}
                  setIsDrafted={setIsDrafted}
                />
              </Box>
              {tags?.map((e, i) => (
                <Tag
                  maxW="80%"
                  py="8px"
                  size="lg"
                  borderColor="gray.100"
                  mb={2}
                  mr={2}
                  key={i}
                >
                  <TagLabel>{e.tag}</TagLabel>
                  <TagCloseButton
                    onClick={() => {
                      // console.log(tags);
                      if (tags.length > 0) {
                        tmpTags = [...tags];
                        tmpTags.splice(i, 1);
                        props.setContent({
                          ...props.content,
                          tags: [...tmpTags],
                        });
                        setIsDrafted(false);
                      }
                    }}
                  />
                </Tag>
              ))}
            </Box>
            {tags?.length === 0 && (
              <Text
                ml="15px"
                color="gray.500"
                fontSize="14px"
                fontStyle="italic"
              >
                *Add tags to help other people quickly understanding your
                context
              </Text>
            )}
          </Flex>
          <Flex justify="space-between" p={5} pt={0} align="center">
            <Flex>
              <Flex align="center">
                <Icon
                  transition="0.5s all ease"
                  transform={isAnonymous ? "rotate(360deg)" : "rotate(0)"}
                  fontSize="1.4em"
                >
                  {AnonyIcon}
                </Icon>
                <Text ml={3} display={["none", "inline"]}>
                  Anonymous
                </Text>
              </Flex>

              <Switch
                ml={3}
                colorScheme="teal"
                size="sm"
                defaultChecked={isAnonymous}
                onChange={debounce(() => updateAnonymous(), 500)}
              />
            </Flex>
            <Box>
              <Button
                variant="outline"
                borderColor="teal.500"
                // color="teal.500"
                fontWeight="bold"
                borderRightRadius="0"
                colorScheme="teal"
                disabled={isDrafted}
                isLoading={isLoading.isDrafting}
                onClick={createDraft}
              >
                {t(!isDrafted ? "draft" : "drafted")}
              </Button>
              <Button
                color="white"
                colorScheme="teal"
                variant="solid"
                fontWeight="bold"
                borderLeftRadius="0"
                bg="teal.500"
                isLoading={isLoading.isPublishing}
                leftIcon={<BiCloudUpload fontSize="22px" />}
                onClick={() => createPost()}
              >
                {t("publish")}
              </Button>
            </Box>
          </Flex>
        </ModalContent>
      </Modal>
    </>
  );
};

export default CreatePostDialog;
