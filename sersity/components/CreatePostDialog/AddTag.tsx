import {
  Box,
  Button,
  Divider,
  Flex,
  Input,
  Popover,
  PopoverArrow,
  PopoverCloseButton,
  PopoverContent,
  PopoverHeader,
  PopoverTrigger,
  Text,
} from "@chakra-ui/react";
import React, { useState } from "react";
import { BsFileMinus, BsPlus, BsPlusCircle } from "react-icons/bs";

import { GET_TAGS, SEARCH_TAGS } from "hook/graphql/post";
import { BiMinus } from "react-icons/bi";
import _ from "lodash";
import { useColorModeValue as mode } from "@chakra-ui/color-mode";
import { useTranslation } from "hook/useTranslation";
import { useApolloClient } from "@apollo/client";
function AddTag(props) {
  // console.log(props.tags);

  const apolloClient = useApolloClient();
  const [tags, setTags] = useState([]);
  let keyword = "";

  async function getTags() {
    const { data, loading, error } = await apolloClient.query({
      query: GET_TAGS,
    });
    if (data) {
      setTags(data.tags);
    }
  }

  async function handleInputSearch(e) {
    keyword = e.target.value;
    const { data, loading, error } = await apolloClient.query({
      query: SEARCH_TAGS,
      variables: { keyword: keyword },
    });
    if (data) {
      setTags(data.searchTags);
    }
  }

  const debounce = _.debounce(handleInputSearch, 100);
  const { t } = useTranslation();
  return (
    <Popover placement="bottom" closeOnBlur={false}>
      <PopoverTrigger>
        <Button
          variant="outline"
          border="2px"
          fontWeight="bold"
          // bg="white"
          // colorScheme="white"
          leftIcon={<BsPlusCircle />}
          onClick={getTags}
        >
          {t("tag")}
        </Button>
      </PopoverTrigger>
      <PopoverContent px={2}>
        <PopoverArrow />
        <Flex align="center" p={2}>
          <Input
            focusBorderColor="teal.500"
            mt={2}
            mr={5}
            variant="filled"
            placeholder="Search tags..."
            onChange={debounce}
          />
          <PopoverCloseButton
            sx={{
              position: "relative",
              bg: mode("gray.100", "gray.600"),
              padding: "20px",
              mr: -2,
            }}
          />
        </Flex>
        <SearchTag {...props} queryTags={tags} />
        <Box
          color="red"
          textColor="red"
          fontSize="14px"
          textAlign="right"
          m={2}
        >
          *You can only put upto 3 tags
        </Box>
      </PopoverContent>
    </Popover>
  );
}

function SearchTag(props) {
  // console.log(props);
  // console.log(props.content.tags);

  return (
    <>
      <Box
        bg={mode("gray.100", "gray.600")}
        borderRadius="10px"
        my={2}
        mx={2}
        maxH="250px"
        overflow="auto"
        sx={{
          scrollbarWidth: "none",
          msOverflowStyle: "none",
          "&::-webkit-scrollbar": { width: "6px" },
          "&::-webkit-scrollbar-thumb": {
            bg: mode("rgba(45, 55, 72, 0.2)", "gray.800"),
          },
        }}
      >
        {props.queryTags.map((e, i) => {
          // let selected = false
          // console.log(props.content.tags);
          const selected =
            typeof props.content.tags.find((el) => el.id === e.id) ===
            "undefined"
              ? false
              : true;

          // if([...props.content.tags].some(tag => tag === e)){
          //   selected = true
          // }
          return <Tag tag={e} key={i} {...props} selected={selected} />;
        })}
      </Box>
    </>
  );
}

function Tag(props) {
  const style = useStyle(props.selected);
  // console.log(props.tag);
  function addTag() {
    if (
      props.tags?.some((tag) => tag["tag"] === props.tag.tag) !== true &&
      props.tags?.length < 3
    ) {
      props.setContent({
        ...props.content,
        tags: [...props.content.tags, props.tag],
      });
      props.setIsDrafted(false);
    }
  }

  function removeTag() {
    if (props.tags.length > 0) {
      let tmpTags = [...props.tags];
      const x = props.tags.findIndex((el) => el.id === props.tag.id);
      tmpTags.splice(x, 1);
      props.setContent({ ...props.content, tags: [...tmpTags] });
      props.setIsDrafted(false);
    }
  }

  return (
    <Box
      padding="10px"
      px="20px"
      sx={style}
      justifyContent="space-between"
      display="flex"
      alignContent="center"
      onClick={() => {
        props.selected ? removeTag() : addTag();
      }}
      bg={props.selected ? "teal.500" : mode("inherit", "gray.800")}
      color={props.selected ? "white" : "inherit"}
    >
      <Text>{props.tag.tag}</Text>
      {!props.selected && <BsPlus fontSize="1.4rem" />}
      {props.selected && <BiMinus fontSize="1.4rem" />}
    </Box>
  );
}

const useStyle = (selected) => ({
  "& svg": {
    display: "none",
  },
  "&:hover": {
    cursor: "pointer",
    background: `${selected ? "teal.500" : mode("gray.200", "gray.600")}`,
    "& svg": {
      display: "inline-block",
    },
  },
});

export default AddTag;
