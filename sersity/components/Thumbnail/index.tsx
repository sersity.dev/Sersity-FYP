import { Avatar, Box, Flex, Text } from "@chakra-ui/react";
import Image from "next/image";

export default function Thumbnail({ post }) {
  // const text = ``
  console.log(post);

  return (
    <Box
      w="1128px"
      h="600px"
      bg="white"
      borderRadius={10}
      p={10}
      border="1px red solid"
      position="relative"
    >
      <Flex position="absolute" bottom="30px" right="30px" align="center">
        <Image
          src="/icons/logo.png"
          alt="Sersity Logo"
          width="30px"
          height="30px"
        />
        <Text ml={3} color="black" fontStyle="italic">
          Powered by Sersity - Online Community
        </Text>
      </Flex>
      <Flex align="center">
        {/* <Flex p={0} align="center" w="35%">
                    <Image src="/icons/icon-512x512.png" alt="Sersity Logo" width="512px" height="512px"/>
                </Flex> */}
        <Flex justifyContent="flex-start" direction="column" h="100%">
          <Flex>
            <Flex align="center">
              <Avatar width="100px" height="100px" />
              <Box ml={5} fontSize="18px">
                <Text fontSize={24} fontWeight={600}>
                  {post?.author?.name}
                </Text>
                <Text fontSize={24} color="gray.400">
                  <i>{post?.author?.rank}</i>
                </Text>
              </Box>
            </Flex>
          </Flex>
          <Box mt={5}>
            <Text fontWeight={600} fontSize={24} wordBreak="break-word">
              {post?.title}
            </Text>
            <Text
              mt={3}
              color="gray.600"
              style={{ whiteSpace: "pre-line" }}
              fontSize={20}
              maxH="300px"
              overflow="hidden"
              textOverflow="ellipsis"
              wordBreak="break-word"
            >
              {post?.content}
            </Text>
          </Box>
        </Flex>
      </Flex>
    </Box>
  );
}
