import { Img } from "@chakra-ui/image";
import { Flex } from "@chakra-ui/layout";
import React from "react";
import SyncLoader from "react-spinners/SyncLoader";

interface Props {}

const SlashScreen = (props: Props) => {
  return (
    <Flex
      align="center"
      position="fixed"
      zIndex={99}
      justify="center"
      h="100vh"
      w="100vw"
      direction="column"
      bg="white"
    >
      <Img
        alt="Sersity"
        objectFit="contain"
        w="50%"
        h="30%"
        src="/images/sersityCrop.png"
      />

      <Flex pt={["0%", "7%"]}>
        <SyncLoader color="#38b2ac" size={15} margin={4} />
      </Flex>
    </Flex>
  );
};

export default SlashScreen;
