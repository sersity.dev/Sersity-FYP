import { useMutation } from "@apollo/client";
import {
  Box,
  Text,
  Button,
  Flex,
  useColorModeValue as mode,
  IconButton,
  useDisclosure,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalFooter,
  ModalBody,
  Input,
  useToast,
} from "@chakra-ui/react";
import DatePicker from "components/DatePicker";
import { UPDATE_INFORMATION } from "hook/graphql/setting";
import moment from "moment";
import { useSession } from "next-auth/client";
import React, { useState } from "react";
import { FiEdit } from "react-icons/fi";

const useStyle = () => ({
  iconBtn: {
    transition: "all .2s ease-in-out",
    "&:hover": { cursor: "pointer", transform: "scale(1.1)" },
    width: "100%",
  },
});
export default function ProfileSetting({ setting }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [settingDialog, setSettingDialog] = useState({ type: "", value: "" });
  const informationData = setting.user.information;
  const sx = useStyle();
  function onOpenSettingDialog(type) {
    switch (type) {
      case "Username":
        setSettingDialog({ type: "Username", value: informationData.username });
        onOpen();
        break;
      case "Bio":
        setSettingDialog({ type: "Bio", value: informationData.bio });
        onOpen();
        break;
      case "University":
        setSettingDialog({
          type: "University",
          value: informationData.university,
        });
        onOpen();
        break;
      case "High School":
        setSettingDialog({
          type: "High School",
          value: informationData.highSchool,
        });
        onOpen();
        break;
      case "Location":
        setSettingDialog({ type: "Location", value: informationData.location });
        onOpen();
        break;
      case "Date of Birth":
        setSettingDialog({
          type: "Date of Birth",
          value: informationData.dateOfBirth,
        });
        onOpen();
        break;
    }
  }

  return (
    <>
      <SettingDialog
        isOpen={isOpen}
        onClose={onClose}
        type={settingDialog.type}
        value={settingDialog.value}
        id={informationData.id}
      />
      <Flex justifyContent="space-between" w="100%" alignItems="center" mb={5}>
        <Box>
          <Text fontWeight="bold" fontSize="16px">
            Username
          </Text>
          <Text color="gray.400">{setting.user.name || (<i>Empty information</i>)}</Text>
        </Box>
        <Box>
          <IconButton
            aria-label="Edit"
            sx={sx.iconBtn}
            icon={<FiEdit />}
            color="gray.600"
            variant="ghost"
            fontSize={["1.3em", "1.3em"]}
            onClick={() => onOpenSettingDialog("Username")}
          />
        </Box>
      </Flex>
      <Flex justifyContent="space-between" w="100%" alignItems="center" mb={5}>
        <Box>
          <Text fontWeight="bold" fontSize="16px">
            Bio
          </Text>
          <Text color="gray.400">{informationData.bio || (<i>Empty information</i>)}</Text>
        </Box>
        <Box>
          <IconButton
            aria-label="Edit"
            sx={sx.iconBtn}
            icon={<FiEdit />}
            color="gray.600"
            variant="ghost"
            fontSize={["1.3em", "1.3em"]}
            onClick={() => onOpenSettingDialog("Bio")}
          />
        </Box>
      </Flex>
      <Flex justifyContent="space-between" w="100%" alignItems="center" mb={5}>
        <Box>
          <Text fontWeight="bold" fontSize="16px">
            College
          </Text>
          <Text color="gray.400">{informationData.university || (<i>Empty information</i>)}</Text>
        </Box>
        <Box>
          <IconButton
            aria-label="Edit"
            sx={sx.iconBtn}
            icon={<FiEdit />}
            color="gray.600"
            variant="ghost"
            fontSize={["1.3em", "1.3em"]}
            onClick={() => onOpenSettingDialog("University")}
          />
        </Box>
      </Flex>
      <Flex justifyContent="space-between" w="100%" alignItems="center" mb={5}>
        <Box>
          <Text fontWeight="bold" fontSize="16px">
            High School
          </Text>
          <Text color="gray.400">{informationData.highSchool || (<i>Empty information</i>)}</Text>
        </Box>
        <Box>
          <IconButton
            aria-label="Edit"
            sx={sx.iconBtn}
            icon={<FiEdit />}
            color="gray.600"
            variant="ghost"
            fontSize={["1.3em", "1.3em"]}
            onClick={() => onOpenSettingDialog("High School")}
          />
        </Box>
      </Flex>
      <Flex justifyContent="space-between" w="100%" alignItems="center" mb={5}>
        <Box>
          <Text fontWeight="bold" fontSize="16px">
            Location
          </Text>
          <Text color="gray.400">{informationData.location || (<i>Empty information</i>)}</Text>
        </Box>
        <Box>
          <IconButton
            aria-label="Edit"
            sx={sx.iconBtn}
            icon={<FiEdit />}
            color="gray.600"
            variant="ghost"
            fontSize={["1.3em", "1.3em"]}
            onClick={() => onOpenSettingDialog("Location")}
          />
        </Box>
      </Flex>
      <Flex justifyContent="space-between" w="100%" alignItems="center" mb={5}>
        <Box>
          <Text fontWeight="bold" fontSize="16px">
            Date of Birth
          </Text>
          <Text color="gray.400">{informationData.dateOfBirth ? moment(informationData.dateOfBirth).format("MMMM D, yyyy") : (<i>Empty information</i>)}</Text>
        </Box>
        <Box>
          <IconButton
            aria-label="Edit"
            sx={sx.iconBtn}
            icon={<FiEdit />}
            color="gray.600"
            variant="ghost"
            fontSize={["1.3em", "1.3em"]}
            onClick={() => onOpenSettingDialog("Date of Birth")}
          />
        </Box>
      </Flex>
    </>
  );
}

function SettingDialog({ isOpen, onClose, type, value, id }) {
  const [updateInformation] = useMutation(UPDATE_INFORMATION);
  const [session] = useSession();
  let infoInput = value;
  
  const [selectedDate, setSelectedDate] = useState(value ? new Date(value) : new Date())

  function handleInfoInputChange(e) {
    infoInput = e.target.value;
  }
  function handleDateValue(e) {
    setSelectedDate(e)
  }
  const [isSaving, setIsSaving] = useState(false)
  const toast = useToast();
  function onUpdateInformation(){
    if(infoInput !== ""){
      setIsSaving(true);
      switch (type) {
        case "Username":
          updateInformation({
            variables: {
              type: "username",
              value: infoInput,
            },
            update: (cache, { data }) => {
              // console.log(cache.data.policies.cache.Root.data)
              cache.modify({
                id: cache.identify({
                  __typename: "User",
                  id: session.user.id,
                }),
                fields: {
                  name(e) {
                    return infoInput;
                  },
                },
              });
            },
          });
          break;
        case "Bio":
          updateInformation({
            variables: {
              type: "bio",
              value: infoInput,
            },
            update: (cache, { data }) => {
              // console.log(cache.data.policies.cache.Root.data)
              cache.modify({
                id: cache.identify({
                  __typename: "Information",
                  id: id,
                }),
                fields: {
                  bio(e) {
                    return infoInput;
                  },
                },
              });
            },
          });
          break;
        case "University":
          updateInformation({
            variables: {
              type: "university",
              value: infoInput,
            },
            update: (cache, { data }) => {
              // console.log(cache.data.policies.cache.Root.data)
              cache.modify({
                id: cache.identify({
                  __typename: "Information",
                  id: id,
                }),
                fields: {
                  university(e) {
                    return infoInput;
                  },
                },
              });
            },
          });
          break;
        case "High School":
          updateInformation({
            variables: {
              type: "highSchool",
              value: infoInput,
            },
            update: (cache, { data }) => {
              // console.log(cache.data.policies.cache.Root.data)
              cache.modify({
                id: cache.identify({
                  __typename: "Information",
                  id: id,
                }),
                fields: {
                  highSchool(e) {
                    return infoInput;
                  },
                },
              });
            },
          });
          break;
        case "Location":
          updateInformation({
            variables: {
              type: "location",
              value: infoInput,
            },
            update: (cache, { data }) => {
              // console.log(cache.data.policies.cache.Root.data)
              cache.modify({
                id: cache.identify({
                  __typename: "Information",
                  id: id,
                }),
                fields: {
                  location(e) {
                    return infoInput;
                  },
                },
              });
            },
          });
          break;
        case "Date of Birth":
          updateInformation({
            variables: {
              type: "dateOfBirth",
              value: selectedDate,
            },
            update: (cache, { data }) => {
              // console.log(cache.data.policies.cache.Root.data)
              cache.modify({
                id: cache.identify({
                  __typename: "Information",
                  id: id,
                }),
                fields: {
                  dateOfBirth(e) {
                    return selectedDate;
                  },
                },
              });
            },
          });
          break;
      }
      setIsSaving(false);
      onClose()
      toast({
        title: "Updated Successfully",
        status: "success",
        duration: 2000,
        isClosable: true,
      })
    }
    
  }
  

  return (
    <>
      <Modal
        closeOnOverlayClick={false}
        isOpen={isOpen}
        onClose={onClose}
        isCentered
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Update {type}</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            {type === "Date of Birth" ? (
              <Box as={DatePicker} 
              id="published-date"
              selectedDate={selectedDate}
              onChange={handleDateValue}
              showPopperArrow={true}
              width="100%"
              />

            ):
            (
              <Input
                variant="flushed"
                placeholder="Update your information here"
                onChange={handleInfoInputChange}
                defaultValue={value !== "Empty information" ? value : null}
              />
            )
            }
            
            
          </ModalBody>

          <ModalFooter>
            <Button onClick={onClose} mr={3}>
              Cancel
            </Button>
            <Button colorScheme="teal" onClick={onUpdateInformation} isLoading={isSaving}>Save</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}
