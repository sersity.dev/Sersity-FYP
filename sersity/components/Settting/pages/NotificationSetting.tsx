import { useMutation } from "@apollo/client";
import {
  Box,
  Text,
  Button,
  Flex,
  useColorModeValue as mode,
  Switch,
} from "@chakra-ui/react";
import { UPDATE_SETTING } from "hook/graphql/setting";

import React, { useState } from "react";

function NotificationSetting({ setting }) {
  const [updateSetting] = useMutation(UPDATE_SETTING);
  const [settingValue, setSettingValue] = useState({
    commentNoti: setting.user.setting.commentNoti,
    replyNoti: setting.user.setting.replyNoti,
    newTrendNoti: setting.user.setting.newTrendNoti,
    followingUserNoti: setting.user.setting.followingUserNoti,
  });

  const updateSettingFnc = (type) => {
    // console.log(settingValue);
    switch (type) {
      case "commentNoti":
        updateSetting({
          variables: {
            type: type,
            value: !settingValue.commentNoti,
          },
          update: (cache, { data }) => {
            // console.log(cache.data.policies.cache.Root.data)
            cache.modify({
              id: cache.identify({
                __typename: "Setting",
                id: setting.user.setting.id,
              }),
              fields: {
                commentNoti(e) {
                  return !settingValue.commentNoti;
                },
              },
            });
          },
        });

        setSettingValue({
          ...settingValue,
          commentNoti: !settingValue.commentNoti,
        });
        break;
      case "replyNoti":
        updateSetting({
          variables: {
            type: type,
            value: !settingValue.replyNoti,
          },
          update: (cache, { data }) => {
            // console.log(cache.data.policies.cache.Root.data)
            cache.modify({
              id: cache.identify({
                __typename: "Setting",
                id: setting.user.setting.id,
              }),
              fields: {
                replyNoti(e) {
                  return !settingValue.replyNoti;
                },
              },
            });
          },
        });

        setSettingValue({
          ...settingValue,
          replyNoti: !settingValue.replyNoti,
        });
        break;
      case "newTrendNoti":
        updateSetting({
          variables: {
            type: type,
            value: !settingValue.newTrendNoti,
          },
          update: (cache, { data }) => {
            // console.log(cache.data.policies.cache.Root.data)
            cache.modify({
              id: cache.identify({
                __typename: "Setting",
                id: setting.user.setting.id,
              }),
              fields: {
                newTrendNoti(e) {
                  return !settingValue.newTrendNoti;
                },
              },
            });
          },
        });

        setSettingValue({
          ...settingValue,
          newTrendNoti: !settingValue.newTrendNoti,
        });
        break;
      case "followingUserNoti":
        updateSetting({
          variables: {
            type: type,
            value: !settingValue.followingUserNoti,
          },
          update: (cache, { data }) => {
            // console.log(cache.data.policies.cache.Root.data)
            cache.modify({
              id: cache.identify({
                __typename: "Setting",
                id: setting.user.setting.id,
              }),
              fields: {
                followingUserNoti(e) {
                  return !settingValue.followingUserNoti;
                },
              },
            });
          },
        });

        setSettingValue({
          ...settingValue,
          followingUserNoti: !settingValue.followingUserNoti,
        });
        break;
    }
  };

  return (
    <>
      <Flex justifyContent="space-between" w="100%" alignItems="center" mb={5}>
        <Box>
          <Text fontWeight="bold" fontSize="16px">
            Comments
          </Text>
          <Text color="gray.400">When somebody comment on your post</Text>
        </Box>
        <Box>
          <Switch
            size="md"
            onChange={() => updateSettingFnc("commentNoti")}
            defaultChecked={settingValue.commentNoti}
            colorScheme="teal"
          />
        </Box>
      </Flex>
      <Flex justifyContent="space-between" w="100%" alignItems="center" mb={5}>
        <Box>
          <Text fontWeight="bold" fontSize="16px">
            Replies
          </Text>
          <Text color="gray.400">When somebody replies to your comment</Text>
        </Box>
        <Box>
          <Switch
            size="md"
            onChange={() => updateSettingFnc("replyNoti")}
            defaultChecked={settingValue.replyNoti}
            colorScheme="teal"
          />
        </Box>
      </Flex>
      <Flex justifyContent="space-between" w="100%" alignItems="center" mb={5}>
        <Box>
          <Text fontWeight="bold" fontSize="16px">
            New Trendings
          </Text>
          <Text color="gray.400">
            Get notified when there is a trending page
          </Text>
        </Box>
        <Box>
          <Switch
            size="md"
            onChange={() => updateSettingFnc("newTrendNoti")}
            defaultChecked={settingValue.newTrendNoti}
            colorScheme="teal"
          />
        </Box>
      </Flex>
      <Flex justifyContent="space-between" w="100%" alignItems="center" mb={5}>
        <Box>
          <Text fontWeight="bold" fontSize="16px">
            Following User
          </Text>
          <Text color="gray.400">
            Get notified when your following user posts
          </Text>
        </Box>
        <Box>
          <Switch
            size="md"
            onChange={() => updateSettingFnc("followingUserNoti")}
            defaultChecked={settingValue.followingUserNoti}
            colorScheme="teal"
          />
        </Box>
      </Flex>
    </>
  );
}

export default NotificationSetting;
