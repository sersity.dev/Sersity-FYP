import { useMutation } from "@apollo/client";
import {
  Box,
  Text,
  Button,
  Flex,
  useColorModeValue as mode,
  Switch,
} from "@chakra-ui/react";
import { UPDATE_SETTING } from "hook/graphql/setting";

import React, { useState } from "react";

function SafetyPrivacySetting({ setting }) {
  const [updateSetting] = useMutation(UPDATE_SETTING);
  const [settingValue, setSettingValue] = useState({
    anonymous: setting.user.setting.anonymous,
    secondVerification: setting.user.setting.secondVerification,
  });

  const updateSettingFnc = (type) => {
    // console.log(settingValue);
    switch (type) {
      case "anonymous":
        updateSetting({
          variables: {
            type: type,
            value: !settingValue.anonymous,
          },
          update: (cache, { data }) => {
            // console.log(cache.data.policies.cache.Root.data)
            cache.modify({
              id: cache.identify({
                __typename: "Setting",
                id: setting.user.setting.id,
              }),
              fields: {
                anonymous(e) {
                  return !settingValue.anonymous;
                },
              },
            });
          },
        });

        setSettingValue({
          ...settingValue,
          anonymous: !settingValue.anonymous,
        });
        break;

      case "secondVerification":
        updateSetting({
          variables: {
            type: type,
            value: !settingValue.secondVerification,
          },
          update: (cache, { data }) => {
            cache.modify({
              id: cache.identify({
                __typename: "Setting",
                id: setting.user.setting.id,
              }),
              fields: {
                secondVerification(e) {
                  return !settingValue.secondVerification;
                },
              },
            });
          },
        });
        setSettingValue({
          ...settingValue,
          secondVerification: !settingValue.secondVerification,
        });
        break;
    }
  };
  return (
    <>
      <Flex justifyContent="space-between" w="100%" alignItems="center" mb={5}>
        <Box>
          <Text fontWeight="bold" fontSize="16px">
            Anonymous Mode
          </Text>
          <Text color="gray.400">Your identity will be not revealed</Text>
        </Box>
        <Switch
          size="md"
          defaultChecked={settingValue.anonymous}
          onChange={() => updateSettingFnc("anonymous")}
          colorScheme="teal"
        />
      </Flex>
      <Flex justifyContent="space-between" w="100%" alignItems="center" mb={5}>
        <Box>
          <Text fontWeight="bold" fontSize={["sm", "sm", "md"]}>
            Second Verification Security
          </Text>
          <Text color="gray.400" fontSize={["sm", "sm", "md"]}>
            When login, you will need to be verified by email
          </Text>
        </Box>
        <Switch
          size="md"
          defaultChecked={settingValue.secondVerification}
          onChange={() => updateSettingFnc("secondVerification")}
          colorScheme="teal"
        />
      </Flex>
      <Flex justifyContent="space-between" w="100%" alignItems="center" mb={5}>
        <Box>
          <Text fontWeight="bold" fontSize="16px">
            Logs
          </Text>
          <Text color="gray.400">Track your activity</Text>
        </Box>
        <Box>
          <Button bg={mode("white", "gray.700")} fontWeight="regular">
            Logs
          </Button>
        </Box>
      </Flex>
    </>
  );
}

export default SafetyPrivacySetting;
