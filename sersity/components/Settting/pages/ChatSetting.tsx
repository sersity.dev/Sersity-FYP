import { useMutation } from "@apollo/client";
import { Box, Text, Button, Flex, useColorModeValue as mode, Switch, Select } from "@chakra-ui/react";
import { updateSetting } from "api/graphql/resolvers";
import { UPDATE_CHAT_SETTING } from "hook/graphql/setting";
import React, { useState } from "react";


export default function ChatSetting({setting}){

    const [updateChatSetting] = useMutation(UPDATE_CHAT_SETTING);

    function updateChatSettingFnc(e){
        updateChatSetting({
            variables:{
                value: e.target.value
            },
            update: (cache, { data }) => {
                // console.log(cache.data.policies.cache.Root.data)
                cache.modify({
                  id: cache.identify({ __typename: "Setting", id: setting.user.setting.id }),
                  fields: {
                    chatSetting() {
                      return e.target.value;
                    },
                  },
                });
              },
        })
    }

    return (
        <>
          <Flex
            justifyContent="space-between"
            w="100%"
            alignItems="center"
            mb={5}
          >
            <Box>
              <Text fontWeight="bold" fontSize="16px">
                Chat
              </Text>
              <Text color="gray.400">Allow who to chat to you</Text>
            </Box>
            <Box>
              <Select
                focusBorderColor="teal.500"
                placeholder=""
                bg={mode("white", "gray.700")}
                defaultValue={setting.user.setting.chatSetting}
                onChange={(e) => updateChatSettingFnc(e)}
              >
                <option value="EVERYONE">Everyone</option>
                <option value="FOLLOWING_FOLLOWER">
                  Following and Follower
                </option>
                <option value="FOLLOWING">Following</option>
              </Select>
              {/* Following & Follower */}
              {/* Follower  */}
              {/* Off  */}
            </Box>
          </Flex>
        </>
    )
}