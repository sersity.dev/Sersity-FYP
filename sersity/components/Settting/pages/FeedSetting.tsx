import { useMutation } from "@apollo/client";
import {
  Box,
  Text,
  Button,
  Flex,
  useColorModeValue as mode,
  Switch,
} from "@chakra-ui/react";
import { UPDATE_SETTING } from "hook/graphql/setting";

import React, { useState } from "react";

function FeedSetting({ setting }) {
  const [updateSetting] = useMutation(UPDATE_SETTING);
  const [settingValue, setSettingValue] = useState({
    allowBadWord: setting.user.setting.allowBadWord,
  });

  const updateSettingFnc = (type) => {
    // console.log(settingValue);
    switch (type) {
      case "allowBadWord":
        updateSetting({
          variables: {
            type: type,
            value: !settingValue.allowBadWord,
          },
          update: (cache, { data }) => {
            // console.log(cache.data.policies.cache.Root.data)
            cache.modify({
              id: cache.identify({
                __typename: "Setting",
                id: setting.user.setting.id,
              }),
              fields: {
                allowBadWord(e) {
                  return !settingValue.allowBadWord;
                },
              },
            });
          },
        });

        setSettingValue({
          ...settingValue,
          allowBadWord: !settingValue.allowBadWord,
        });
        break;
    }
  };
  return (
    <>
      <Flex justifyContent="space-between" w="100%" alignItems="center" mb={5}>
        <Box>
          <Text fontWeight="bold" fontSize="16px">
            Hidden Posts
          </Text>
          <Text color="gray.400">You can check your hidden posts here</Text>
        </Box>
        <Box>
          <Button bg={mode("white", "gray.700")} fontWeight="regular">
            Check
          </Button>
        </Box>
      </Flex>
      <Flex justifyContent="space-between" w="100%" alignItems="center" mb={5}>
        <Box>
          <Text fontWeight="bold" fontSize="16px">
            Bad Word Posts
          </Text>
          <Text color="gray.400">
            Allow posts containing bad words to be on feed
          </Text>
        </Box>
        <Switch
          size="md"
          defaultChecked={settingValue.allowBadWord}
          onChange={() => updateSettingFnc("allowBadWord")}
          colorScheme="teal"
        />
      </Flex>
    </>
  );
}

export default FeedSetting;
