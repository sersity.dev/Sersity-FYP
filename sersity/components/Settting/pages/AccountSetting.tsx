import { useMutation } from "@apollo/client";
import {
  Box,
  Text,
  Button,
  Flex,
  useColorModeValue as mode,
  Icon,
  useToast,
  IconButton,
  Modal,
  ModalOverlay,
  ModalHeader,
  ModalContent,
  ModalCloseButton,
  ModalBody,
  Input,
  ModalFooter,
  useDisclosure,
} from "@chakra-ui/react";
import { UPDATE_PASSWORD } from "hook/graphql/setting";
import { useSession } from "next-auth/client";
import React, { useState } from "react";
import { BsFillTrashFill } from "react-icons/bs";
import { FiEdit } from "react-icons/fi";
import { RiErrorWarningLine } from "react-icons/ri";

const useStyle = () => ({
  iconBtn: {
    transition: "all .2s ease-in-out",
    "&:hover": { cursor: "pointer", transform: "scale(1.05)" },
    width: "100%",
  },
});
export default function AccountSetting({ setting }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const sx = useStyle();
  const toast = useToast()
  return (
    <>
      <SettingDialog
          isOpen={isOpen}
          onClose={onClose}
        />
      <Flex justifyContent="space-between" w="100%" alignItems="center" mb={5}>
        <Box>
          <Text fontWeight="bold" fontSize="16px">
            Email Address
          </Text>
          <Text color="gray.400">{setting.user.email}</Text>
        </Box>
        <Box
           onClick={() => 
            toast({
              title: "This option is not available yet",
              status: "error",
              duration: 1000,
              isClosable: true,
            })
          }
        >
          <IconButton sx={sx.iconBtn} aria-label="Edit" icon={<FiEdit/>} color="gray.600" variant="ghost" opacity={0.3} fontSize={["1.3em","1.3em"]}/>
        </Box>
      </Flex>
      <Flex justifyContent="space-between" w="100%" alignItems="center" mb={5}>
        <Box >
          <Text fontWeight="bold" fontSize="16px">
            Password
          </Text>
          <Text color="gray.400" width="80%">
            Password must be at least 8 characters long
          </Text>
        </Box>
        <Box
           onClick={() => {
            if(setting.user.isPasswordless){
              toast({
                title: "Your account is passwordless",
                status: "error",
                duration: 1000,
                isClosable: true,
              })
            }
           }
          }
        >
        <IconButton opacity={setting.user.isPasswordless ? 0.3 : 1} sx={sx.iconBtn} aria-label="Edit" icon={<FiEdit/>} color="gray.600" variant="ghost" onClick={() => {if(!setting.user.isPasswordless){ onOpen()}}} fontSize={["1.3em","1.3em"]}/>
        </Box>
      </Flex>
      <Flex justifyContent="space-between" w="100%" alignItems="center" mb={5}>
        <Box>
          <Text fontWeight="bold" fontSize="16px">
            Connect to Google
          </Text>
          <Text color="gray.400">Connect account to log in with Google</Text>
        </Box>
        <Box 
        onClick={() => 
          toast({
            title: "This option is not available yet",
            status: "error",
            duration: 1000,
            isClosable: true,
          })
        }>
          <Button bg={mode("white", "gray.700")} opacity={0.3} fontWeight="regular">
            Connect
          </Button>
        </Box>
      </Flex>
      <Flex
        style={{ flexDirection: "row" }}
        position="absolute"
        left="20px"
        bottom="20px"
        cursor="pointer"
        align="center"
        sx={{
          transition: "all .2s ease-in-out",
          "&:hover": { cursor: "pointer", transform: "scale(1.1)" },
        }}
      >
        <Icon fontSize="1.3rem" color="red.500">
          <BsFillTrashFill />
        </Icon>

        <Text ml={2} color="red.500" fontWeight={600}>
          Deactivate Account
        </Text>
      </Flex>
    </>
  );
}

function SettingDialog({ isOpen, onClose}) {
  const [updatePassword] = useMutation(UPDATE_PASSWORD);

  const [password, setPassword] = useState({oldPassword:"",newPassword:"",confirmedNewPassword:"",msg:"",isValid:false})
  const [isSaving, setIsSaving] = useState(false)
  
  function handleOldPasswordInput(e) {
    setPassword({...password,oldPassword:e.target.value,msg:""});
  }

  function handleNewPasswordInput(e) {
    setPassword({...password,newPassword:e.target.value,msg:""});
  }
  
  function handleConfirmedNewPasswordInput(e) {
    setPassword({...password,confirmedNewPassword:e.target.value,msg:""});
  }

  function updatePasswordFnc(){
    console.log(password);
    
    setIsSaving(true)
    validatePassword();

      
      updatePassword({
        variables:{
          oldPw: password.oldPassword,
          newPw: password.newPassword,
          cfNewPw: password.confirmedNewPassword
        },
        update(cache,{data}){
          const resData = data.updatePassword;
          if(resData.status === true){
            onClose()
            setPassword({oldPassword:"",newPassword:"",confirmedNewPassword:"",msg:"",isValid:false})
            toast({
              title: resData.msg,
              status: "success",
              duration: 2000,
              isClosable: true,
            })
          }else{
            setPassword({...password,isValid:false,msg:resData.msg})
          }
          setIsSaving(false)

        }
      })

  }

  

  function validatePassword(){
    if(password.newPassword?.length > 8 && password.confirmedNewPassword?.length >  8 && password.oldPassword?.length >  8){
      if(password.newPassword === password.confirmedNewPassword){
        setPassword({...password , msg:"", isValid:true})
      }else{
        setPassword({...password,  msg:"New password does not match",isValid:false})
      }
    }else{
      setPassword({...password,  msg:"New password length must be greater than 8",isValid:false})
    }
  }


  const toast = useToast();
 
  

  return (
    <>
      <Modal
        closeOnOverlayClick={false}
        isOpen={isOpen}
        onClose={onClose}
        isCentered
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Update Password</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            {!password.isValid && password.msg && (
              <Flex color="red" align="center" mb={4}>
                <RiErrorWarningLine/>
                <Text ml={3} fontSize="14px">{password.msg}</Text>
              </Flex>
            )}
            
              
              <Input
                autoComplete="off"
                variant="flushed"
                type="password"
                fontSize="14px"
                placeholder="Current password"
                onChange={handleOldPasswordInput}
              />

               <Input
                 autoComplete="off"
                variant="flushed"
                type="password"
                fontSize="14px"
                placeholder="New password"
                onChange={handleNewPasswordInput}
                mt={5}
              />
              
              <Input
                autoComplete="off"
                variant="flushed"
                type="password"
                fontSize="14px"
                placeholder="Confirmation of new password"
                onChange={handleConfirmedNewPasswordInput}
                mt={5}
              />
              
              
          </ModalBody>

          <ModalFooter>
            <Button onClick={onClose} mr={3}>
              Cancel
            </Button>
            <Button colorScheme="teal" onClick={updatePasswordFnc} isLoading={isSaving}>Save</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}