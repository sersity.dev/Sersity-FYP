import { Box, HStack, Text, VStack } from "@chakra-ui/layout";
import {
  Flex,
  Icon,
  IconButton,
  useColorModeValue as mode,
} from "@chakra-ui/react";

import { useRouter } from "next/router";
import React, { useState } from "react";
import { AiOutlineProfile, AiOutlineUser } from "react-icons/ai";
import { BiArrowBack } from "react-icons/bi";
import { BsCaretRightFill } from "react-icons/bs";
import { CgFeed } from "react-icons/cg";
import { RiChatSettingsLine, RiNotification2Line } from "react-icons/ri";
import { SiGnuprivacyguard } from "react-icons/si";

import AccountSetting from "./pages/AccountSetting";
import ProfileSetting from "./pages/ProfileSetting";
import FeedSetting from "./pages/FeedSetting";
import ChatSetting from "./pages/ChatSetting";
import NotificationSetting from "./pages/NotificationSetting";
import SafetyPrivacySetting from "./pages/SafetyPrivacySetting";
const useStyle = () => ({
  list: {
    transition: "all .2s ease-in-out",
    "&:hover": { cursor: "pointer", transform: "scale(1.05)" },
    width: "100%",
  },
});
const getList = (name, icon) => ({ name, icon });
const listData = [
  getList("Account", <AiOutlineUser />),
  getList("Profile", <AiOutlineProfile />),
  getList("Safety & Privacy", <SiGnuprivacyguard />),
  getList("Feed", <CgFeed />),
  getList("Notification", <RiNotification2Line />),
  getList("Chat & Messagging", <RiChatSettingsLine />),
];

export default function({ setting }) {
  // console.log(setting);
  const sx = useStyle();
  const router = useRouter();
  const selected = router?.query?.option;
  const indexSelected = listData.findIndex((e) => e.name === selected);
  const [tabIndex, setTabIndex] = useState(
    indexSelected >= 0 ? indexSelected : 0
  );
  const [mobilePage, setMobilePage] = useState({
    tabIndex: 0,
    isSelected: false,
  });
  const Lists = (props) => {
    const { ...rest } = props;
    return (
      <Box {...rest}>
        {listData.map((e, index) => {
          const isActive = tabIndex === index ? true : false;
          return (
            <Flex
              align="center"
              justify="space-between"
              my={[2, 0]}
              color={["inherit", isActive ? "teal.500" : "inherit"]}
              key={index}
              borderLeftRadius="6px"
              borderRightRadius={[6, 0]}
              sx={sx.list}
              p={["1rem 1rem", "0.9rem 0.9rem"]}
              bg={[
                mode("white", "gray.700"),
                isActive ? mode("gray.100", "gray.750") : "inherit",
              ]}
              onClick={() => {
                setTabIndex(index);
                setMobilePage({ tabIndex: index, isSelected: true });
              }}
            >
              <Flex align="center">
                <Icon mr={2} fontSize="1.3em">
                  {e.icon}
                </Icon>
                {e.name}
              </Flex>
              <Icon
                color={mode("gray.300", "gray.500")}
                display={["inherit", isActive ? "inherit" : "none"]}
              >
                <BsCaretRightFill />
              </Icon>
            </Flex>
          );
        })}
      </Box>
    );
  };

  return (
    <Flex
      direction={["column", "row"]}
      minH={["100%", "600px"]}
      bg={["none", mode("white", "gray.700")]}
      w="100%"
      mt={[0, 4]}
      borderRadius="10px"
    >
      <VStack
        fontWeight="bold"
        w={["100%", "22em"]}
        align="flex-start"
        pl={3}
        pr={[3, 0]}
        mt={4}
      >
        <HStack mb={2} w="100%" justify="center">
          <Text fontSize="lg" fontFamily="sans-serif">
            Setting
          </Text>
        </HStack>
        {/* <Divider /> */}
        <Lists w="100%" />
      </VStack>
      <VStack
        // bg={mode("gray.100", "gray.750")}
        w="100%"
        my={[0, 3]}
        mr={[0, 3]}
        h={["100%", "auto"]}
        align="flex-start"
        transition="0.3s all ease"
        position={["fixed", "inherit"]}
        top={["56px"]}
        right={[mobilePage.isSelected ? 0 : "-100%", "0px"]}
      >
        <TabDetail
          index={tabIndex}
          mobilePage={mobilePage}
          setMobilePage={setMobilePage}
          setting={setting}
        />
      </VStack>
    </Flex>
  );
};

function TabDetail({ index, mobilePage, setMobilePage, setting }) {
  const listSettings = [
    "Account",
    "Profile",
    "Safety & Privacy",
    "Feed Setting",
    "Notification",
    "Chat & Messagging",
  ];
  let content;
  switch (index) {
    case 0:
      content = <AccountSetting setting={setting} />;
      break;
    case 1:
      content = <ProfileSetting setting={setting} />;
      break;
    case 2:
      content = <SafetyPrivacySetting setting={setting} />;
      break;
    case 3:
      content = <FeedSetting setting={setting} />;
      break;
    case 4:
      content = <NotificationSetting setting={setting} />;
      break;
    case 5:
      content = <ChatSetting setting={setting} />;
      break;
  }

  return (
    <Box
      p={5}
      h="100%"
      w="100%"
      bg={mode("gray.100", "gray.750")}
      position="relative"
      borderRadius="10px"
    >
      <Flex order={3} align="center" mb={6} justify={["flex-start", "center"]}>
        <IconButton
          mr={3}
          variant="ghost"
          aria-label="back"
          onClick={() =>
            setMobilePage({
              ...mobilePage,
              isSelected: !mobilePage.isSelected,
            })
          }
          display={["inherit", "none"]}
        >
          <BiArrowBack fontSize="1.4em" />
        </IconButton>
        <Text fontSize="lg" fontWeight={600} fontFamily="sans-serif">
          {listSettings[index]} Setting
        </Text>
      </Flex>

      {/* <Divider my={3} size="2px" /> */}
      {content}
    </Box>
  );
}
