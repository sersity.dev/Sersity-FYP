import { Button } from "@chakra-ui/button";
import { FormControl, FormLabel } from "@chakra-ui/form-control";
import {
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
} from "@chakra-ui/input";
import { Flex, Stack, Text, VStack } from "@chakra-ui/layout";
import { DividerWithText } from "components/DeviderWithText";
import React, { useState } from "react";
import { FaFacebook, FaGoogle } from "react-icons/fa";
import { signIn } from "next-auth/client";
import axios from "axios";
import { Spinner } from "@chakra-ui/spinner";
import router from "next/router";
import { Collapse } from "@chakra-ui/transition";
import { useTranslation } from "hook/useTranslation";
import { apiUrl } from "api/url";
import { HiOutlineMail } from "react-icons/hi";
import { RiLockPasswordLine } from "react-icons/ri";
import { FiUser } from "react-icons/fi";

interface Props {}

function SignUp() {
  const [isSubmit, setIsSubmit] = useState({ c: false, fb: false, g: false });
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState([]);
  const [isDone, setIsDone] = useState(false);

  const { t } = useTranslation();
  async function handleLogin({ email, password }) {
    const res = await signIn("credentials", {
      email,
      password,
      callbackUrl: `/home`,
      redirect: false,
    });
    res.error && setError((error) => [{ ...error }, res.error]);
    res.url && router.push(res.url);
  }

  function handleSubmit(e) {
    e.preventDefault();
    setError([]);

    if (password.length < 8) {
      setError((error) => [error, "Password must be at least 8 characters"]);
    }
    if (password.length >= 8 && email !== "") {
      setIsSubmit({
        c: true,
        fb: false,
        g: false,
      });
      // setIsDone
      axios
        .post(`/api/rest/auth/verification`, {
          name,
          email,
          password,
        })
        .then(function ({ data }) {
          if (data.error) setError((error) => [error, data.message]);
          if (data.error == false) setIsDone(true);
          setIsSubmit({
            c: false,
            fb: false,
            g: false,
          });
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  }

  return (
    <>
      {!isDone && (
        <form onSubmit={handleSubmit}>
          <Stack spacing="6">
            <FormControl id="name">
              <FormLabel>{t("name")} </FormLabel>
              <InputGroup>
                <InputLeftElement
                  color="gray.300"
                  pointerEvents="none"
                  children={<FiUser />}
                />

                <Input
                  focusBorderColor="teal.500"
                  name="name"
                  type="name"
                  onChange={(e) => setName(e.target.value)}
                  autoComplete="name"
                  placeholder="example"
                  required
                />
              </InputGroup>
            </FormControl>
            <FormControl id="email">
              <FormLabel>{t("email address")} </FormLabel>
              <InputGroup>
                <InputLeftElement
                  color="gray.300"
                  pointerEvents="none"
                  children={<HiOutlineMail />}
                />

                <Input
                  focusBorderColor="teal.500"
                  name="email"
                  type="email"
                  onChange={(e) => setEmail(e.target.value)}
                  autoComplete="email"
                  placeholder="example@gmail.com"
                  required
                />
              </InputGroup>
            </FormControl>
            <PasswordInput
              placeholder="example@123"
              required
              onChange={(e) => setPassword(e.target.value)}
            />
            <Collapse in={error.length > 0 ? true : false}>
              <Flex
                align="center"
                direction="column"
                border="2px solid red"
                py={2.5}
                borderRadius={5}
              >
                {error.length > 0 &&
                  error.map((err, index) => {
                    const tErr = err.length > 0 ? t(err) : "";
                    return (
                      <Text key={index} color="red" fontWeight="bold">
                        {tErr}
                      </Text>
                    );
                  })}
              </Flex>
            </Collapse>
            <Button
              type="submit"
              isLoading={error.length > 0 ? false : isSubmit.c}
              spinner={<Spinner size="md" />}
              colorScheme="teal"
              size="lg"
              fontWeight="bold"
              fontSize="md"
            >
              {t("create account")}
            </Button>
          </Stack>
          <DividerWithText my="5">OR</DividerWithText>
          <VStack spacing="4">
            <Button
              w="100%"
              size="lg"
              leftIcon={<FaFacebook />}
              colorScheme="facebook"
              isLoading={error.length > 0 ? false : isSubmit.fb}
              spinner={<Spinner size="md" />}
              onClick={() => {
                setIsSubmit({
                  c: false,
                  fb: !isSubmit.fb,
                  g: false,
                });
                signIn("facebook", { callbackUrl: "/" });
              }}
            >
              {t("sign up with facebook")}
            </Button>
            <Button
              w="100%"
              size="lg"
              isLoading={error.length > 0 ? false : isSubmit.g}
              spinner={<Spinner size="md" />}
              onClick={() => {
                setIsSubmit({
                  c: false,
                  fb: false,
                  g: !isSubmit.g,
                });
                signIn("google", { callbackUrl: "/" });
              }}
              leftIcon={<FaGoogle />}
              colorScheme="red"
            >
              {t("sign up with google")}
            </Button>
          </VStack>
        </form>
      )}
      {isDone && (
        <Flex
          direction="column"
          h="300px"
          w="100%"
          justifyContent="center"
          align="center"
        >
          <Text color="green" mb={5} fontWeight="bold">
            Registered successfully
          </Text>
          <Text color="Red" mb={10} textAlign="center">
            Please check your email to verify the account under 5 minutes
          </Text>
          <Button onClick={() => setIsDone(false)} colorScheme="teal">
            Sign up
          </Button>
        </Flex>
      )}
    </>
  );
}

function PasswordInput(props) {
  const [show, setShow] = React.useState(false);
  const handleClick = () => setShow(!show);

  const { t } = useTranslation();
  return (
    <FormControl id="password">
      <FormLabel>{t("password")} </FormLabel>
      <InputGroup size="md">
        <InputLeftElement
          color="gray.300"
          pointerEvents="none"
          children={<RiLockPasswordLine />}
        />
        <Input
          {...props}
          name="password"
          focusBorderColor="teal.500"
          pr="4.5rem"
          type={show ? "text" : "password"}
          placeholder="example@123"
        />
        <InputRightElement width="4.5rem">
          <Button h="1.75rem" size="sm" onClick={handleClick}>
            {t(show ? "hide" : "show")}
          </Button>
        </InputRightElement>
      </InputGroup>
    </FormControl>
  );
}

export default SignUp;
