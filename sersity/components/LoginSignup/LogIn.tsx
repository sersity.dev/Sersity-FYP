import { Button } from "@chakra-ui/button";
import { FormControl, FormLabel } from "@chakra-ui/form-control";
import {
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
} from "@chakra-ui/input";
import { Flex, Stack, Text, VStack } from "@chakra-ui/layout";
import { DividerWithText } from "components/DeviderWithText";
import React, { useState } from "react";
import { FaFacebook, FaGoogle } from "react-icons/fa";

import { signIn } from "next-auth/client";
import { Spinner } from "@chakra-ui/spinner";
import router from "next/router";
import { Collapse } from "@chakra-ui/transition";

import { useTranslation } from "hook/useTranslation";
import { HiOutlineMail } from "react-icons/hi";
import { RiLockPasswordLine } from "react-icons/ri";

function LogIn() {
  const [isSubmit, setIsSubmit] = useState({ c: false, fb: false, g: false });
  const [error, setError] = useState([]);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const { t } = useTranslation();
  function handleError(err) {
    setIsSubmit({
      c: false,
      fb: false,
      g: false,
    });
    setError((error) => [error, err]);
  }
  async function handleSubmit(e) {
    e.preventDefault();
    setError([]);
    if (password.length < 8)
      setError((error) => [error, "Password must be at least 8 characters"]);
    if (password.length >= 8 && email !== "") {
      setIsSubmit({
        c: !isSubmit.c,
        fb: false,
        g: false,
      });
      const res = await signIn("credentials", {
        email,
        password,
        callbackUrl: `/home`,
        redirect: false,
      });

      res.error && handleError(res.error);
      res.url && router.push(res.url);
    }
  }

  return (
    <form onSubmit={handleSubmit}>
      <Stack spacing="6">
        <FormControl id="email">
          <FormLabel>{t("email address")} </FormLabel>

          <InputGroup>
            <InputLeftElement
              color="gray.300"
              pointerEvents="none"
              children={<HiOutlineMail />}
            />
            <Input
              focusBorderColor="teal.500"
              name="email"
              onChange={(e) => setEmail(e.target.value)}
              type="email"
              autoComplete="email"
              placeholder="example@gmail.com"
              required
            />
          </InputGroup>
        </FormControl>

        <PasswordInput
          required
          onChange={(e) => setPassword(e.target.value)}
          placeholder="example"
        />

        <Collapse in={error?.length > 0 ? true : false}>
          <Flex
            align="center"
            direction="column"
            border="2px solid red"
            py={2.5}
            borderRadius={5}
          >
            {error.length > 0 &&
              error.map((err, index) => {
                const tErr = err.length > 0 ? t(err) : "";
                return (
                  <Text key={index} color="red" fontWeight="bold">
                    {tErr}
                  </Text>
                );
              })}
          </Flex>
        </Collapse>

        <Button
          type="submit"
          isLoading={error.length > 0 ? false : isSubmit.c}
          spinner={<Spinner size="md" />}
          colorScheme="teal"
          size="lg"
          fontSize="md"
        >
          {t("log in")}
        </Button>
      </Stack>
      <DividerWithText my="5">OR</DividerWithText>
      <VStack spacing="4">
        <Button
          w="100%"
          size="lg"
          leftIcon={<FaFacebook />}
          colorScheme="facebook"
          isLoading={error.length > 0 ? false : isSubmit.fb}
          onClick={() => {
            setIsSubmit({
              c: false,
              fb: !isSubmit.fb,
              g: false,
            });
            signIn("facebook", { callbackUrl: "/" });
          }}
          spinner={<Spinner size="md" />}
        >
          {t("sign up with facebook")}
        </Button>

        <Button
          w="100%"
          size="lg"
          leftIcon={<FaGoogle />}
          colorScheme="red"
          isLoading={error.length > 0 ? false : isSubmit.g}
          onClick={() => {
            setIsSubmit({
              c: false,
              fb: false,
              g: !isSubmit.g,
            });
            signIn("google", { callbackUrl: "/" });
          }}
          spinner={<Spinner size="md" />}
        >
          {t("sign up with google")}
        </Button>
      </VStack>
    </form>
  );
}

function PasswordInput(props) {
  const [show, setShow] = React.useState(false);
  const handleClick = () => setShow(!show);
  const { t } = useTranslation();

  return (
    <FormControl id="password">
      <FormLabel>{t("password")} </FormLabel>
      <InputGroup size="md">
        <InputLeftElement
          color="gray.300"
          pointerEvents="none"
          children={<RiLockPasswordLine />}
        />
        <Input
          {...props}
          name="password"
          focusBorderColor="teal.500"
          pr="4.5rem"
          type={show ? "text" : "password"}
          placeholder="example@123"
        />
        <InputRightElement width="4.5rem">
          <Button h="1.75rem" size="sm" onClick={handleClick}>
            {t(show ? "hide" : "show")}
          </Button>
        </InputRightElement>
      </InputGroup>
    </FormControl>
  );
}
export default LogIn;
