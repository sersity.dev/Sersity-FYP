import { useColorModeValue as mode } from "@chakra-ui/color-mode";

import { Box, Flex } from "@chakra-ui/layout";
import { Tab, TabList, TabPanel, TabPanels, Tabs } from "@chakra-ui/tabs";
import ChangeLocale from "components/ChangeLocale";
import PageMotion from "components/PageMotion";
import { useTranslation } from "hook/useTranslation";

import React, { useState } from "react";
import LogIn from "./LogIn";
import SignUp from "./SignUp";

const LoginSupign = (props) => {
  const [tabIndex, setTabIndex] = useState(props.tabIndex);

  const handleTabsChange = (index) => {
    setTabIndex(index);
  };

  const { t } = useTranslation();

  return (
    <Box
      {...props}
      borderRadius="10px"
      overflow="auto"
      bg={mode("white", "gray.700")}
    >
      <Tabs
        variant="soft-rounded"
        colorScheme="teal"
        isLazy
        index={tabIndex}
        onChange={handleTabsChange}
      >
        <TabList position="relative">
          <Tab fontWeight="bold" ml={4} mt={4} px={4}>
            {t("log in")}
          </Tab>
          <Tab fontWeight="bold" ml={2} mt={4} px={4}>
            {t("sign up")}
          </Tab>
          <Flex
            position="absolute"
            bottom={0}
            right={0}
            pr={3}
            align="flex-end"
            justify="flex-end"
          >
            <ChangeLocale />
          </Flex>
        </TabList>
        <TabPanels transition=" all 0.5s ease">
          <TabPanel p={6}>
            <PageMotion>
              <LogIn {...props} />
            </PageMotion>
          </TabPanel>
          <TabPanel p={6}>
            <PageMotion>
              <SignUp {...props} />
            </PageMotion>
          </TabPanel>
        </TabPanels>
      </Tabs>
    </Box>
  );
};

export default LoginSupign;
