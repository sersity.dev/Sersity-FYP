import {
  Box,
  useColorModeValue as mode,
  Text,
  Flex,
  Tag,
  IconButton,
} from "@chakra-ui/react";

import React, { memo } from "react";
import { FiEdit, FiTrash } from "react-icons/fi";

function Draft(props) {
  const { onClickEdit, onClickDelete } = props;
  // console.log("in each draft");
  return (
    <Box
      bg={mode("white", "gray.700")}
      w="100%"
      my={3}
      p={[3, 5]}
      borderRadius="10px"
    >
      <Flex justifyContent="space-between" align="center">
        <Box pr={5}>
          <Text fontSize="18px" fontWeight="bold" mb={2}>
            {props.title}
          </Text>
          <Text>{props.content}</Text>
          <Flex mt={5}>
            {props.tags.map((e, i) => (
              <Tag mr={3} key={i}>
                {e.tag}
              </Tag>
            ))}
          </Flex>
        </Box>
        <Flex direction="column" align="center">
          <IconButton
            variant="ghost"
            borderRadius="14px"
            color="#38B2AC"
            border="2px solid rgba(133, 147, 166, 0.1)"
            aria-label="Edit Draft"
            onClick={onClickEdit}
            icon={<FiEdit size="16px" />}
            mb={2}
          />

          <IconButton
            variant="ghost"
            borderRadius="14px"
            color="red.400"
            border="2px solid rgba(133, 147, 166, 0.1)"
            aria-label="Delete Draft"
            onClick={onClickDelete}
            icon={<FiTrash size="16px" />}
          />
        </Flex>
      </Flex>
    </Box>
  );
}

export default memo(
  Draft,
  (prev, next) =>
    prev.title === next.title &&
    prev.tags === next.tags &&
    prev.id === next.id &&
    prev.content === next.content
);
// memo(components,(prev,next)=>prev.[key that change]===next.[key that change])

// that mean only rerender when the value of that key change
// [key that change] the value this one can only string, int, … that can do shallow compare
