import { Avatar } from "@chakra-ui/avatar";
import { Button } from "@chakra-ui/button";
import { useDisclosure } from "@chakra-ui/hooks";
import Icon from "@chakra-ui/icon";
import { Box, Flex, Text } from "@chakra-ui/layout";
import {
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerOverlay,
} from "@chakra-ui/modal";

import { helperRank } from "components/Context/rankBorder";
import { BiChevronDown } from "react-icons/bi";
import PersonalData from "../PersonalData";
import Lists from "../Lists";
export default function MenuDrawer({ user }) {
  const { isOpen, onOpen, onClose } = useDisclosure();

  // const user = session?.user;

  return (
    <>
      <Button onClick={onOpen} pl={2} pr={1} variant="ghost" borderRadius="md">
        <Flex align="center" overflow="hidden">
          <Box sx={helperRank}>
            <Avatar
              mr={2}
              src={user?.image}
              name={user?.name}
              size="sm"
              className="anime"
            />
          </Box>
          <Text fontWeight={600} isTruncated display={["none", "inherit"]}>
            {user?.name}
          </Text>
          <Icon mr={0} ml={1} display={["none", "inline"]} fontSize="1.3rem">
            <BiChevronDown />
          </Icon>
        </Flex>
      </Button>
      <Drawer isOpen={isOpen} placement="left" onClose={onClose}>
        <DrawerOverlay />
        <DrawerContent>
          <DrawerCloseButton />
          <DrawerBody>
            {isOpen && (
              <>
                <PersonalData user={user} />{" "}
                <Lists onClose={onClose} user={user} />{" "}
              </>
            )}
          </DrawerBody>
        </DrawerContent>
      </Drawer>
    </>
  );
}
