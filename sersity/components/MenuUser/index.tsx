import { useQuery } from "@apollo/client";
import { Box } from "@chakra-ui/layout";
import { GET_USER } from "hook/graphql/user";
import useMQ from "hook/useMQ";
import MenuDrawer from "./MenuDrawer";
import MenuDropdown from "./MenuDropdown";

export default function MenuUser({ user }) {
  const { isMobile } = useMQ();
  const { data } = useQuery(GET_USER, {
    variables: {
      where: { id: user?.id },
    },
    fetchPolicy: "network-only",
  });

  return (
    <Box>
      {isMobile ? (
        <MenuDrawer user={data?.user} />
      ) : (
        <MenuDropdown user={data?.user} />
      )}
    </Box>
  );
}
