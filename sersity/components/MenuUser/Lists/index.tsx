import { useMutation, useQuery } from "@apollo/client";
import { Avatar } from "@chakra-ui/avatar";
import { Button } from "@chakra-ui/button";
import { useColorModeValue as mode } from "@chakra-ui/color-mode";
import { useDisclosure } from "@chakra-ui/hooks";
import Icon from "@chakra-ui/icon";

import { Box, Flex, HStack, Text, VStack } from "@chakra-ui/layout";

import {
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerOverlay,
} from "@chakra-ui/modal";
import { Switch } from "@chakra-ui/switch";

import { helperRank } from "components/Context/rankBorder";

import { GET_USER } from "hook/graphql/user";
import { signOut, useSession } from "next-auth/client";
import { useRouter } from "next/router";
import { AiOutlineUser } from "react-icons/ai";
import { BiChevronDown } from "react-icons/bi";
import { BsBookmarks } from "react-icons/bs";
import { CgFeed } from "react-icons/cg";
import { IoSettingsOutline } from "react-icons/io5";
import { MdPowerSettingsNew } from "react-icons/md";

import { SiRedhat } from "react-icons/si";

import { useState } from "react";
import { RiBearSmileLine } from "react-icons/ri";
import {
  Popover,
  PopoverArrow,
  PopoverContent,
  PopoverTrigger,
} from "@chakra-ui/popover";
import { UPDATE_SETTING } from "hook/graphql/setting";
import { useTranslation } from "hook/useTranslation";
import PersonalData from "../PersonalData";

const getList = (name, icon, onClick?) => ({ name, icon, onClick });
export default function Lists({ onClose, user }) {
  const { push } = useRouter();
  // const [isOnline, setIsOnline] = useState(true);
  const [isAnonymous, setIsAnonymous] = useState(user?.setting?.anonymous);
  const [updateSetting] = useMutation(UPDATE_SETTING);

  function updateAnonymous() {
    updateSetting({
      variables: {
        type: "anonymous",
        value: !isAnonymous,
      },
      update: (cache, { data }) => {
        setIsAnonymous(!isAnonymous);
        cache.modify({
          id: cache.identify({
            __typename: "Setting",
            id: user.setting.id,
          }),
          fields: {
            anonymous(e) {
              return isAnonymous.anonymous;
            },
          },
        });
      },
    });
  }

  const AnonyIcon = isAnonymous ? (
    <SiRedhat />
  ) : (
    <RiBearSmileLine fontSize="1.1em" />
  );
  const listData = [
    getList("anonymous browsing", AnonyIcon),
    // getList("Online Status", <GrStatusCriticalSmall />),
    getList("profile", <AiOutlineUser />, () => {
      push("/profile");
      onClose();
    }),
    getList("saved", <BsBookmarks />, () => {
      push("/saved", undefined, { shallow: true });
      onClose();
    }),
    getList("draft", <CgFeed />, () => {
      push("/draft", undefined, { shallow: true });
      onClose();
    }),

    getList("setting", <IoSettingsOutline />, () => {
      push("/setting");
      onClose();
    }),
    getList("log out", <MdPowerSettingsNew />, () => signOut()),
  ];
  const { t } = useTranslation();
  return (
    <VStack align="flex-start" mt={5}>
      {listData.map((e: any, i) => (
        <Flex
          w="100%"
          onClick={() => {
            if (e.onClick) e.onClick();
          }}
          align="center"
          py={i === 0 || i === 1 ? 1 : 2}
          key={i}
          justify={i === 0 || i === 1 ? "space-between" : "flex-start"}
          _hover={{ cursor: "pointer", color: "teal.500" }}
        >
          <Flex>
            <Icon
              transition="0.5s all ease"
              transform={
                e.name === "anonymous browsing" && isAnonymous
                  ? "rotate(360deg)"
                  : "rotate(0)"
              }
              fontSize="1.4em"
            >
              {e.icon}
            </Icon>
            <Text ml={3}>{t(e.name)}</Text>
          </Flex>
          {e.name === "anonymous browsing" && (
            <Switch
              ml={3}
              colorScheme="teal"
              size="sm"
              defaultChecked={isAnonymous}
              onChange={(e) => {
                updateAnonymous();
              }}
            />
          )}
          {/* {e.name === "Online Status" && (
            <Switch
              colorScheme="teal"
              size="sm"
              defaultChecked={isOnline}
              onChange={(e) => setIsOnline(!isOnline)}
            />
          )} */}
        </Flex>
      ))}
    </VStack>
  );
}
