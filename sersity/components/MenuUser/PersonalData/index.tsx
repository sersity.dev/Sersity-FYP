import { Avatar } from "@chakra-ui/avatar";
import { useColorModeValue as mode } from "@chakra-ui/color-mode";

import { Box, Flex, HStack, Text, VStack } from "@chakra-ui/layout";
import { helperRank } from "components/Context/rankBorder";
import { useTranslation } from "hook/useTranslation";

export default function PersonalData({ user }) {
  const { t } = useTranslation();
  return (
    <HStack align="flex-start" mb={3} w="100%" mt={2}>
      <VStack align="flex-start" justify="space-between" w="100%">
        <Flex alignItems="center" p={3}>
          <Box sx={helperRank}>
            <Avatar
              boxSize="5rem"
              name={user?.name}
              src={user?.image}
              mr={3}
              className="anime"
              zIndex={20}
            />
          </Box>
          <Box ml={3}>
            <Text as="h6" fontSize="lg" fontWeight={700}>
              {user?.name}
            </Text>
            <Text fontSize="md" color="gray.500" fontWeight={500}>
              {user?.rank}
            </Text>
          </Box>
        </Flex>

        <HStack
          bg={mode("gray.100", "gray.750")}
          p={4}
          justify="space-between"
          borderRadius="10px"
          w="100%"
          px={5}
        >
          <VStack>
            <Text color="gray.500" fontWeight={600} fontSize="sm">
              {t("posts")}
            </Text>
            <Text fontSize="30px" fontFamily="sans-serif" fontWeight={600}>
              {user?.countPost || 0}
            </Text>
          </VStack>
          <VStack>
            <Text color="gray.500" fontWeight={600} fontSize="sm">
              {t("followers")}
            </Text>
            <Text fontSize="30px" fontFamily="sans-serif" fontWeight={600}>
              {user?.countFollower || 0}
            </Text>
          </VStack>
          <VStack>
            <Text color="gray.500" fontWeight={600} fontSize="sm">
              {t("score")}
            </Text>
            <Text fontSize="30px" fontFamily="sans-serif" fontWeight={600}>
              {user?.score || 0}
            </Text>
          </VStack>
        </HStack>
      </VStack>
    </HStack>
  );
}
