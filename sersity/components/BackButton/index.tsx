import { IconButton } from "@chakra-ui/button";
import { FiChevronLeft } from "react-icons/fi";

export default function BackButton(props) {
  const { ...rest } = props;
  return (
    <IconButton
      variant="ghost"
      aria-label="go back"
      borderRadius="14px"
      color="#38B2AC"
      border="2px solid rgba(133, 147, 166, 0.1)"
      {...rest}
    >
      <FiChevronLeft fontSize="1.6em" />
    </IconButton>
  );
}
