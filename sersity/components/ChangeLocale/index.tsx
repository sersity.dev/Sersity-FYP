import { Button } from "@chakra-ui/button";
import { useBreakpointValue as useBp } from "@chakra-ui/media-query";
import setLanguage from "next-translate/setLanguage";
import { useRouter } from "next/router";

export default function ChangeLocale(props) {
  const { locale } = useRouter();
  let setLng = locale === "en" ? "kh" : "en";
  const displayLng = (setLng) => {
    return setLng === "en"
      ? useBp({ base: "EN", md: "English" })
      : useBp({ base: "ខ្មែរ", md: "ភាសាខ្មែរ" });
  };
  return (
    <Button variant="ghost" onClick={async () => await setLanguage(setLng)}>
      {displayLng(setLng)}
    </Button>
  );
}
