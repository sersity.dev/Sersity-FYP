import ChatDialog from "components/ChatLayout/ChatDialog";
import { createContext, useContext, useState } from "react";

export const AppContext = createContext<any>({});
export function AppWrapper({ children }) {
  const [openChat, setOpenChat] = useState(false);

  return (
    <AppContext.Provider value={{ openChat, setOpenChat }}>
      {children}
      <ChatDialog
        {...{
          isOpen: openChat,
          onOpen: () => setOpenChat(true),
          onClose: () => setOpenChat(false),
          onToggle: () => setOpenChat(!openChat),
        }}
      />
    </AppContext.Provider>
  );
}

export function useAppContext() {
  return useContext(AppContext);
}
