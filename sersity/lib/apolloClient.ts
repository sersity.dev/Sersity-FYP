import { ApolloClient, HttpLink, split, InMemoryCache } from "@apollo/client";
import { sha256 } from "crypto-hash";
import { createPersistedQueryLink } from "@apollo/client/link/persisted-queries";
import { WebSocketLink } from "@apollo/client/link/ws";
import { getMainDefinition } from "@apollo/client/utilities";
import urlBase from "api/url";
import option from "./apolloCache";

let apolloClient = null;
const ssrMode = typeof window === "undefined";

//cache encrypted
const persistedQueriesLink = createPersistedQueryLink({ sha256 });

//enable for client side only
const reHttp = process.env.NODE_ENV === "development" ? "http" : "https";
const reWs = process.env.NODE_ENV === "development" ? "ws" : "wss";

//socket link
const wsLink = !ssrMode
  ? new WebSocketLink({
      // if you instantiate in the server, the error will be thrown
      uri: `${reWs}://${urlBase}/api/graphqlSubscriptions`,
      options: {
        reconnect: true,
        timeout: 30000,
      },
    })
  : null;

//both server and client
const httpLink = new HttpLink({
  credentials: "same-origin",
  uri: `${reHttp}://${urlBase}/api/graphql`,
});

//switch when to use Socket or Http
// SSR only support httpLink
const link = ssrMode
  ? httpLink
  : split(
      //only create the split in the browser
      // split based on operation type
      ({ query }) => {
        const definition = getMainDefinition(query);
        return (
          definition.kind === "OperationDefinition" &&
          definition.operation === "subscription"
        );
      },
      wsLink,
      httpLink
    );

export default function createApolloClient(initialState = {}, option = {}) {
  return new ApolloClient({
    ssrMode,
    link: persistedQueriesLink.concat(link),
    cache: new InMemoryCache(option).restore(initialState),
  });
}

export function initApolloClient(initialState?: {}) {
  // Make sure to create a new client for every server-side request so that data
  // isn't shared between connections (which would be bad)

  if (ssrMode) {
    return createApolloClient(initialState, option);
  }

  // Reuse client on the client-side
  if (!apolloClient) {
    apolloClient = createApolloClient(initialState, option);
  }

  return apolloClient;
}
