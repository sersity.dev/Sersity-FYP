import { makeVar } from "@apollo/client";

export const option = {
  typePolicies: {
    User: {
      fields: {
        chatRooms: {
          keyArgs: false,
          merge(e, i) {
            return e ? [...e, ...i] : i;
          },
        },
        notifications: {
          keyArgs: ["where"],
          merge(e, i) {
            return e ? [...e, ...i] : i;
          },
        },
      },
    },
    ChatRoom: {
      fields: {
        messages: {
          keyArgs: false,
          merge(e, i) {
            return e ? [...e, ...i] : i;
          },
        },
      },
    },
    Query: {
      fields: {
        posts: {
          keyArgs: ["latest"],
          merge(e, i) {
            return e ? [...e, ...i] : i;
          },
        },
      },
    },
  },
};

export default option;

export const globalCache = makeVar({});

export const chatDialogVar = makeVar<any>({});
export const isOpenCreatePostModal = makeVar<any>({});
