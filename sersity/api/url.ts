const domain = "sersity.io";
export const urlBase =
  process.env.NODE_ENV === "development" ? "localhost:3000" : domain;

export const apiUrl =
  process.env.NODE_ENV === "development"
    ? "http://localhost:3000"
    : `https://${domain}`;

export default urlBase;
