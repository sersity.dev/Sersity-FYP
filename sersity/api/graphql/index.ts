import { makeSchema } from "nexus";
import path from "path";
import { nexusPrisma } from "nexus-plugin-prisma";
import * as resolvers from "./resolvers";

export const schema = makeSchema({
  types: { resolvers },
  contextType: {
    module: path.join(process.cwd(), "api/graphql", "context.ts"),
    export: "Context",
  },
  plugins: [
    nexusPrisma({
      paginationStrategy: "prisma",
      experimentalCRUD: true,
      outputs: {
        typegen: path.join(
          process.cwd(),
          "api/graphql/generated",
          "typegen-nexus-plugin-prisma.d.ts"
        ),
      },
      // outputs: {
      //   typegen: path.join(
      //     __dirname,
      //     "../../node_modules/@types/nexus-typegen/index.d.ts"
      //   ),
      // },
    }),
  ],
  outputs: {
    typegen: path.join(
      process.cwd(),
      "api/graphql/generated",
      "nexus-typegen.ts"
    ),
    schema: path.join(process.cwd(), "api/graphql/generated", "schema.graphql"),
  },
});
