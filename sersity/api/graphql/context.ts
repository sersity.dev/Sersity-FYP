import { Notification } from "./resolvers/notification/index";
// graphql/context.ts
import type { PrismaClient } from "@prisma/client";
import { IncomingMessage } from "http";
import { getSession } from "next-auth/client";
import prisma from "lib/prismaClient";
import { PubSub } from "apollo-server";
import { commentDetector, postDetector } from "hook/bad-word-detector";
import Cryptr from "cryptr";
import bcrypt from "bcrypt";
import useSendNotification from "hook/useSendNotification";

export interface Context {
  prisma: PrismaClient;
  session: any;
  pubSub: any;
  commentDetector: any;
  postDetector: any;
  NOTIFICATION_SUBCRIPTION: String;
  sendNotification: any;
  checkRank: any;
  cryptr: any;
  bcrypt: any;
}

function cryptr(key?) {
  return new Cryptr(key);
}

const pubSub: any = new PubSub();
pubSub.ee.setMaxListeners(30);
const NOTIFICATION_SUBCRIPTION = "NOTIFICATION_SUBCRIPTION";

async function sendNotification(args) {
  const { createdNotification } = await useSendNotification(args);
  createdNotification &&
    pubSub.publish(`${NOTIFICATION_SUBCRIPTION}${args.userId}`, {
      createdNotification,
    });
}

async function checkRank(userId, newScore) {
  const user = await prisma.user.findFirst({
    where: {
      id: userId,
    },
  });
  const userScore = user.score + newScore;
  let isNewRank = false;
  let userRank = user.rank;

  if (userScore > 0 && userScore < 100) {
    userRank = "BEGINNER";
  } else if (userScore >= 100 && userScore < 1000) {
    userRank = "HELPER";
  } else if (userScore >= 1000 && userScore < 5000) {
    userRank = "ELITE";
  } else if (userScore >= 5000 && userScore < 10000) {
    userRank = "LEGEND";
  } else if (userScore >= 10000 && userScore < 50000) {
    userRank = "HERO";
  } else if (userScore >= 50000 && userScore < 100000) {
    userRank = "GOD";
  } else if (userScore >= 100000) {
    userRank = "PURITY";
  }

  if (user.rank !== userRank) {
    let message = "";
    if (newScore > 0) {
      message =
        "Congratulation " +
        user.name +
        '! You have reached a new rank "' +
        userRank +
        '".';
    } else {
      message =
        "Oops " +
        user.name +
        '! You have been demoted to rank "' +
        userRank +
        '".';
    }
    const args = {
      message: message,
      type: "SYSTEM",
      data: {
        date: new Date(),
      },
      url: "/profile",
      userId: user.id,
      fromUserId: user.id,
    };
    sendNotification(args);
  }

  return userRank;
}

export const createContext = async ({
  req,
}: {
  req: IncomingMessage;
}): Promise<Context> => {
  const session = await getSession({ req });
  return {
    prisma,
    session,
    pubSub,
    commentDetector,
    postDetector,
    NOTIFICATION_SUBCRIPTION,
    sendNotification,
    checkRank,
    cryptr,
    bcrypt,
  };
};
