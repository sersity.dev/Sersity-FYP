import {
  list,
  mutationField,
  objectType,
  queryField,
  stringArg,
  subscriptionField,
} from "nexus";
import { UserRef } from "..";

export const Notification = objectType({
  name: "Notification",
  definition(t) {
    t.model.id();
    t.model.user();
    t.model.fromUser();
    t.model.message();
    t.model.data();
    t.model.url();
    t.model.type();
    t.model.readAt();
    t.boolean("isRead", {
      resolve: (root, _, ctx): any => (root.readAt === null ? false : true),
    });
    t.model.isDeleted();
    t.model.updatedAt();
    t.model.createdAt();
    t.field("userRef", {
      type: UserRef,
      resolve: async (root: any, _, ctx) => {
        const user = await ctx.prisma.user.findFirst({
          where: {
            id: root.fromUserId,
          },
        });

        let data = {
          id: user.id,
          name: user.name,
          rank: "" + user.rank,
          image: user.image,
        };

        if (root.data?.isAnonymous === true) {
          data = {
            id: "anonymous",
            name: "user" + new Date(user.createdAt).getTime(),
            rank: "ANONYMOUS",
            image: "anonymous",
          };
        }

        return data;
      },
    });
  },
});

export const queryNotification = queryField((t) => {
  t.crud.notification();
  t.crud.notifications({
    filtering: true,
    pagination: true,
  });
  t.field("countNewNotification", {
    type: "CountNewNotification",
    resolve: async (root, _, ctx) => {
      const newNotiCount = await ctx.prisma.notification.count({
        where: {
          AND: [{ userId: ctx.session.user.id }, { readAt: null }],
        },
      });

      const count = { count: newNotiCount };
      return count;
    },
  });
});

export const countNewNotification = objectType({
  name: "CountNewNotification",
  definition(t) {
    t.int("count");
  },
});

export const mutationNotification = mutationField((t) => {
  t.crud.createOneNotification({});
  t.field("readNotifications", {
    type: "PrismaUpdateMany",
    args: {
      ids: list(stringArg()),
    },
    resolve: async (root, { ids }, ctx) => {
      const notification = await ctx.prisma.notification.updateMany({
        where: {
          id: {
            in: ids,
          },
        },
        data: {
          readAt: new Date().toISOString(),
        },
      });
      return notification;
    },
  });
});

const NOTIFICATION_SUBCRIPTION = "NOTIFICATION_SUBCRIPTION";
export const createdNotification = subscriptionField((t) => {
  t.field("createdNotification", {
    type: Notification,
    args: {
      userId: stringArg(),
    },
    subscribe: async (root, { userId }, ctx) => {
      return ctx.pubSub.asyncIterator(`${NOTIFICATION_SUBCRIPTION}${userId}`);
    },
    resolve: ({ createdNotification }) => createdNotification,
  });
});
//   model Notification {
//     id        String            @id @default(cuid())
//     user      User              @relation(fields: [userId], references: [id])
//     fromUser  User              @relation("FromUser", fields: [fromUserId], references: [id])
//     data      Json
//     type      NotificationType?
//     isDeleted Boolean           @default(false)
//     read_at   DateTime?
//     createdAt DateTime          @default(now())
//     updatedAt DateTime          @updatedAt

//     userId     String
//     fromUserId String
//   }
