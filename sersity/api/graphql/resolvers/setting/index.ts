import {
  arg,
  booleanArg,
  enumType,
  mutationField,
  objectType,
  stringArg,
} from "nexus";

import { User } from "../user";

export const Setting = objectType({
  name: "Setting",
  definition(t) {
    t.model.id();
    t.model.anonymous();
    t.model.secondVerification();
    t.model.allowBadWord();
    t.model.commentNoti();
    t.model.replyNoti();
    t.model.newTrendNoti();
    t.model.followingUserNoti();
    t.model.chatActiveStatus();
    t.model.chatSetting();
    t.model.updatedAt();
    t.model.user();
  },
});

export const Information = objectType({
  name: "Information",
  definition(t) {
    t.model.id();
    t.model.phone();
    t.model.dateOfBirth();
    t.model.bio();
    t.model.highSchool();
    t.model.university();
    t.model.location();
    t.model.updatedAt();
    t.model.user();
  },
});

export const ChatSettingEnum = enumType({
  name: "ChatSettingEnum",
  members: {
    FOLLOWING: "FOLLOWING",
    FOLLOWING_FOLLOWER: "FOLLOWING_FOLLOWER",
    EVERYONE: "EVERYONE",
  },
});

export const updateSetting = mutationField("updateSetting", {
  type: User,
  args: {
    type: stringArg(),
    value: booleanArg(),
  },
  resolve: (_, args, ctx) => {
    const uid = ctx.session?.user?.id;
    switch (args.type) {
      case "anonymous":
        return ctx.prisma.user.update({
          where: {
            id: "" + uid,
          },
          data: {
            setting: {
              update: {
                anonymous: args.value,
              },
            },
          },
        });
        break;
      case "secondVerification":
        return ctx.prisma.user.update({
          where: {
            id: "" + uid,
          },
          data: {
            setting: {
              update: {
                secondVerification: args.value,
              },
            },
          },
        });
        break;
      case "allowBadWord":
        return ctx.prisma.user.update({
          where: {
            id: "" + uid,
          },
          data: {
            setting: {
              update: {
                allowBadWord: args.value,
              },
            },
          },
        });
        break;
      case "commentNoti":
        return ctx.prisma.user.update({
          where: {
            id: "" + uid,
          },
          data: {
            setting: {
              update: {
                commentNoti: args.value,
              },
            },
          },
        });
        break;
      case "replyNoti":
        return ctx.prisma.user.update({
          where: {
            id: "" + uid,
          },
          data: {
            setting: {
              update: {
                replyNoti: args.value,
              },
            },
          },
        });
        break;
      case "newTrendNoti":
        return ctx.prisma.user.update({
          where: {
            id: "" + uid,
          },
          data: {
            setting: {
              update: {
                newTrendNoti: args.value,
              },
            },
          },
        });
        break;
      case "followingUserNoti":
        return ctx.prisma.user.update({
          where: {
            id: "" + uid,
          },
          data: {
            setting: {
              update: {
                followingUserNoti: args.value,
              },
            },
          },
        });
        break;
      case "chatActiveStatus":
        return ctx.prisma.user.update({
          where: {
            id: "" + uid,
          },
          data: {
            setting: {
              update: {
                chatActiveStatus: args.value,
              },
            },
          },
        });
        break;
    }
  },
});

export const updateChatSetting = mutationField("updateChatSetting", {
  type: User,
  args: {
    value: arg({ type: ChatSettingEnum }),
  },
  resolve: (_, args, ctx) => {
    const uid = ctx.session?.user?.id;
    return ctx.prisma.user.update({
      where: {
        id: uid,
      },
      data: {
        setting: {
          update: {
            chatSetting: args.value,
          },
        },
      },
    });
  },
});

export const updateInformation = mutationField("updateInformation", {
  type: User,
  args: {
    type: stringArg(),
    value: stringArg(),
  },
  resolve: (_, args, ctx) => {
    const uid = ctx.session?.user?.id;
    switch (args.type) {
      case "username":
        return ctx.prisma.user.update({
          where: {
            id: "" + uid,
          },
          data: {
            name: args.value,
          },
        });
        break;
      case "bio":
        return ctx.prisma.user.update({
          where: {
            id: uid,
          },
          data: {
            information: {
              update: {
                bio: args.value,
              },
            },
          },
        });
      case "phone":
        return ctx.prisma.user.update({
          where: {
            id: uid,
          },
          data: {
            information: {
              update: {
                phone: args.value,
              },
            },
          },
        });
      case "dateOfBirth":
        return ctx.prisma.user.update({
          where: {
            id: uid,
          },
          data: {
            information: {
              update: {
                dateOfBirth: args.value,
              },
            },
          },
        });
      case "highSchool":
        return ctx.prisma.user.update({
          where: {
            id: uid,
          },
          data: {
            information: {
              update: {
                highSchool: args.value,
              },
            },
          },
        });
      case "university":
        return ctx.prisma.user.update({
          where: {
            id: uid,
          },
          data: {
            information: {
              update: {
                university: args.value,
              },
            },
          },
        });
      case "location":
        return ctx.prisma.user.update({
          where: {
            id: uid,
          },
          data: {
            information: {
              update: {
                location: args.value,
              },
            },
          },
        });
      case "dateOfBirth":
        const date = new Date(args.value);
        console.log(date);

        return ctx.prisma.user.update({
          where: {
            id: uid,
          },
          data: {
            information: {
              update: {
                dateOfBirth: date,
              },
            },
          },
        });
    }
  },
});

// export const updateDateOfBirth = mutationField("updateDateOfBirth",{
//     type: User,
//     args:{
//         dob: arg(ty")
//     },
//     resolve: (_,args,ctx) => {

//     }
// })
