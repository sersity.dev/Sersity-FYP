import { prisma } from ".prisma/client";
import { GraphQLList, isEnumType } from "graphql";
import {
  mutationField,
  objectType,
  queryField,
  stringArg,
  arg,
  enumType,
  nonNull,
  intArg,
  booleanArg,
  list,
} from "nexus";
import { count } from "node:console";
import { resolve, resolveAny } from "node:dns";
import { type } from "os";
import post from "pages/post/[id]";
import { Keyword, User } from "../user";

//DEFINE MODEL

export const Post = objectType({
  name: "Post",
  definition(t) {
    t.model.id();
    t.model.title();
    t.model.authorId();
    t.model.content();
    t.model.author();
    t.model.published();
    t.model.votes();
    t.model.status();
    // t.model.comments({ filtering: true });
    t.list.field("comments", {
      type: Comment,
      args: {
        skip: intArg(),
        latest: booleanArg(),
      },
      resolve: (root, _, ctx): any => {
        return ctx.prisma.comment.findMany({
          where: {
            postId: root.id,
          },
          take: 1,
          orderBy: {
            upvote: "desc",
          },
        });
      },
    });
    // t.model.comments({ filtering: true, ordering: true });
    t.model.upvote();
    t.model.downvote();
    t.model.createdAt();
    t.model.tags();
    t.model.saved();
    t.model.isAnonymous();
    t.model.badWordDetection();
    t.model.publishedAt();
    t.int("votes", { resolve: (root): any => root.upvote - root.downvote });
    t.list.field("topComment", {
      type: Comment,
      resolve: (root, _, ctx): any => {
        return ctx.prisma.comment.findMany({
          where: {
            postId: root.id,
          },
          take: 1,
          orderBy: {
            upvote: "desc",
          },
        });
      },
    });

    t.field("isVoted", {
      type: IsVoted,
      resolve: async (root, _, ctx) => {
        const result = {
          isVoted: false,
          action: "",
        };

        if (ctx.session?.user) {
          const uid = ctx.session.user.id;
          const vote = await ctx.prisma.vote.findFirst({
            where: {
              userId: uid,
              postId: "" + root.id,
            },
          });

          if (vote) {
            result.action = vote.action;
            result.isVoted = true;
          }
        }
        // const uid = ctx.session.user.id ;

        return result;
      },
    });
    t.field("isSaved", {
      type: "Boolean",
      resolve: async (root, _, ctx) => {
        let isSaved = false;
        if (ctx.session?.user) {
          const uid = ctx.session.user.id;
          const saved = await ctx.prisma.saved.findFirst({
            where: {
              userId: uid,
              postId: root.id,
            },
          });
          if (saved) {
            isSaved = true;
          } else {
            isSaved = false;
          }
        }
        return isSaved;
      },
    });
    t.field("userRef", {
      type: UserRef,
      resolve: async (root, _, ctx) => {
        const user = await ctx.prisma.user.findFirst({
          where: {
            id: root.authorId,
          },
        });

        let data = {
          id: user.id,
          name: user.name,
          rank: "" + user.rank,
          image: user.image,
        };

        if (root.isAnonymous === true) {
          data = {
            id: "user" + user.createdAt.getTime(),
            name: "user" + user.createdAt.getTime(),
            rank: "ANONYMOUS",
            image: "anonymous",
          };
        }

        return data;
      },
    });
    t.list.field("comments", {
      type: Comment,
      args: {
        where: arg({ type: "CommentWhereInput" }),
      },
      resolve: async (root, args, ctx) => {
        const where = {
          ...args.where,
          postId: root.id,
          parentId: null,
        };
        let comments = await ctx.prisma.comment.findMany({
          where: where,
        });
        comments.sort((a, b) => {
          if (a.upvote - a.downvote > b.upvote - b.downvote) {
            return -1;
          }
        });
        return comments;
      },
    });
    t.int("commentCount", {
      resolve: async (root, args, ctx) =>
        ctx.prisma.comment.count({
          where: { postId: root.id, parentId: null },
        }),
    });
  },
});

export const Saved = objectType({
  name: "Saved",
  definition(t) {
    t.model.id();
    t.model.post();
    t.model.user();
    t.model.createdAt();
  },
});

export const Comment = objectType({
  name: "Comment",
  definition(t) {
    t.model.id();
    t.model.comment();
    t.model.userId();
    t.model.user();
    t.model.votes();
    t.model.post();
    t.model.parent();
    // t.model.replies();
    t.model.upvote();
    t.model.downvote();
    t.model.createdAt();
    t.model.badWordDetection();
    t.model.updatedAt();
    t.model.isAnonymous();
    t.int("votes", { resolve: (root): any => root.upvote - root.downvote });
    t.field("isVoted", {
      type: IsVoted,
      resolve: async (root, _, ctx) => {
        const result = {
          isVoted: false,
          action: "",
        };

        if (ctx.session?.user) {
          // console.log("running?");
          const uid = ctx.session?.user.id;
          const vote = await ctx.prisma.vote.findFirst({
            where: {
              userId: uid,
              commentId: root.id,
            },
          });

          if (vote) {
            result.action = vote.action;
            result.isVoted = true;
          }
        }

        return result;
      },
    });
    t.field("userRef", {
      type: UserRef,
      resolve: async (root, _, ctx) => {
        const user = await ctx.prisma.user.findFirst({
          where: {
            id: root.userId,
          },
        });

        let data = {
          id: user.id,
          name: user.name,
          rank: "" + user.rank,
          image: user.image,
        };

        if (root.isAnonymous === true) {
          data = {
            id: "user" + user.createdAt.getTime(),
            name: "user" + user.createdAt.getTime(),
            rank: "ANONYMOUS",
            image: "anonymous",
          };
        }

        return data;
      },
    });
    t.list.field("replies", {
      type: Comment,
      args: {
        where: arg({ type: "CommentWhereInput" }),
      },
      resolve: async (root, args, ctx) => {
        const where = {
          ...args.where,
          parentId: root.id,
        };
        let comments = await ctx.prisma.comment.findMany({
          where: where,
        });
        comments.sort((a, b) => {
          if (a.upvote - a.downvote > b.upvote - b.downvote) {
            return -1;
          }
        });
        return comments;
      },
    });
  },
});

export const UserRef = objectType({
  name: "UserRef",
  definition(t) {
    t.string("id");
    t.string("name");
    t.string("image");
    t.string("rank");
  },
});

export const Vote = objectType({
  name: "Vote",
  definition(t) {
    t.model.id();
    t.model.user();
    t.model.post();
    t.model.action();
    t.model.createdAt();
    t.model.updatedAt();
  },
});

export const IsVoted = objectType({
  name: "IsVoted",
  definition(t) {
    t.boolean("isVoted");
    t.string("action");
  },
});

export const BadWordDetection = objectType({
  name: "BadWordDetection",
  definition(t) {
    t.model.id();
    t.model.badWord();
    t.model.post();
    t.model.comment();
    t.model.createdAt();
  },
});

export const PostStatus = enumType({
  name: "PostStatus",
  members: {
    DRAFTED: "DRAFTED",
    PUBLISHED: "PUBLISHED",
    DELETED: "DELETED",
    UNDER_REVIEWED: "UNDER_REVIEWED",
    BLOCKED: "BLOCKED",
  },
});

export const Action = enumType({
  name: "Action",
  members: {
    UPVOTE: "UPVOTE",
    DOWNVOTE: "DOWNVOTE",
    UNVOTED: "UNVOTED",
  },
});

export const ReturnMessage = objectType({
  name: "ReturnMessage",
  definition(t) {
    t.int("status");
    t.string("message");
    t.field("data", { type: ReturnMessageData });
  },
});

export const ReturnMessageData = objectType({
  name: "ReturnMessageData",
  definition(t) {
    t.field("Post", { type: Post });
    t.field("Comment", { type: Comment });
    t.field("Vote", { type: Vote });
  },
});

export const Tag = objectType({
  name: "Tag",
  definition(t) {
    t.model.id();
    t.model.posts({ filtering: true, ordering: true });
    t.model.tag();
  },
});

export const ReportedPost = objectType({
  name: "ReportedPost",
  definition(t) {
    t.model.id();
    t.model.user();
    t.model.post();
    t.model.reason();
    t.model.createdAt();
  },
});

export const UserLog = objectType({
  name: "UserLog",
  definition(t) {
    t.model.id();
    t.model.user();
    t.model.log();
    t.model.createdAt();
  },
});

export const KeywordCount = objectType({
  name: "KeywordCount",
  definition(t) {
    t.string("keyword");
    t.int("count");
  },
});

export const KeywordList = objectType({
  name: "KeywordList",
  definition(t) {
    t.list.field("popularKeywords", {
      type: KeywordCount,
    });
    t.list.field("personalKeywords", {
      type: Keyword,
    });
  },
});

// QUERY FEILD

export const query = queryField((t) => {
  //Get all post on homepage
  t.list.field("posts", {
    type: "Post",
    args: {
      skip: intArg(),
      latest: booleanArg(),
    },
    resolve: async (_, args, ctx) => {
      const uid = ctx.session?.user?.id;
      let badword = true;
      if (uid) {
        const setting = await ctx.prisma.setting.findFirst({
          where: {
            userId: uid,
          },
        });

        if (setting.allowBadWord === false) {
          badword = false;
        }
      }
      let posts;

      if (badword === false) {
        if (args.latest === true) {
          posts = ctx.prisma.post.findMany({
            where: {
              status: "PUBLISHED",
              published: true,
            },
            orderBy: {
              createdAt: "desc",
            },
            take: 10,
            skip: args.skip,
          });
        }

        if (!args.latest) {
          posts = ctx.prisma.post.findMany({
            where: {
              status: "PUBLISHED",
              published: true,
            },
            orderBy: {
              score: "desc",
            },
            take: 10,
            skip: args.skip,
          });
        }
      } else {
        if (args.latest === true) {
          posts = ctx.prisma.post.findMany({
            where: {
              status: "PUBLISHED",
              published: true,
              badWordDetection: {
                every: {
                  badWord: { isEmpty: true },
                },
              },
            },
            orderBy: {
              createdAt: "desc",
            },
            take: 10,
            skip: args.skip,
          });
        }

        if (!args.latest) {
          posts = ctx.prisma.post.findMany({
            where: {
              status: "PUBLISHED",
              published: true,
              badWordDetection: {
                every: {
                  badWord: { isEmpty: true },
                },
              },
            },
            orderBy: {
              score: "desc",
            },
            take: 10,
            skip: args.skip,
          });
        }
      }

      return posts;
    },
  });

  t.list.field("arrayPosts", {
    type: "Post",
    args: {
      postIds: stringArg(),
    },
    resolve: async (_, args, ctx) => {
      // let posts = []
      const postIds = args.postIds.split(",");
      const posts = await ctx.prisma.post.findMany({
        where: {
          id: {
            in: postIds,
          },
        },
      });

      return posts;
    },
  });
  //Get post by id
  t.list.field("post", {
    type: Post,
    args: {
      id: nonNull(stringArg()),
    },
    resolve: (_, args, ctx) => {
      return ctx.prisma.post.findMany({
        where: {
          id: args.id,
          published: true,
          status: "PUBLISHED",
        },
        include: {
          comments: {
            orderBy: {
              upvote: "asc",
            },
          },
        },
        take: 1,
      });
    },
  });
  //Get posts by user id and status
  t.list.field("userPosts", {
    type: Post,
    args: {
      status: PostStatus,
    },
    resolve: (_, args, ctx) => {
      const uid = ctx.session.user.id;
      return ctx.prisma.post.findMany({
        where: {
          authorId: uid,
          status: args.status,
        },
        orderBy: {
          createdAt: "desc",
        },
      });
    },
  });

  //Search posts
  t.list.field("searchPosts", {
    type: Post,
    args: {
      keyword: stringArg(),
    },
    resolve: (_, args, ctx) => {
      return ctx.prisma.post.findMany({
        where: {
          published: true,
          status: "PUBLISHED",
          OR: [
            { content: { contains: args.keyword, mode: "insensitive" } },
            { title: { contains: args.keyword, mode: "insensitive" } },
          ],
        },
      });
    },
  });

  t.list.field("savedPost", {
    type: Saved,
    resolve: (_, __, ctx) => {
      const uid = ctx.session.user.id;
      return ctx.prisma.saved.findMany({
        where: {
          userId: uid,
          post: {
            published: true,
            status: "PUBLISHED",
          },
        },
        orderBy: {
          createdAt: "desc",
        },
      });
    },
  });
  t.list.field("tags", {
    type: Tag,
    resolve: (_, args, ctx) => {
      return ctx.prisma.tag.findMany();
    },
  });

  t.list.field("searchTags", {
    type: Tag,
    args: {
      keyword: stringArg(),
    },
    resolve: (_, args, ctx) => {
      return ctx.prisma.tag.findMany({
        where: {
          tag: {
            contains: args.keyword,
            mode: "insensitive",
          },
        },
      });
    },
  });

  t.field("tagPosts", {
    type: Tag,
    args: {
      tag: stringArg(),
    },
    resolve: (_, args, ctx) => {
      return ctx.prisma.tag.findFirst({
        where: {
          tag: {
            equals: args.tag,
            mode: "insensitive",
          },
        },
      });
    },
  });

  t.field("getKeyword", {
    type: "KeywordList",
    resolve: async (_, __, ctx) => {
      let keywords = {
        popularKeywords: [],
        personalKeywords: [],
      };
      const uid = ctx.session?.user?.id;
      const popKeywords = await ctx.prisma.keyword.groupBy({
        by: ["keyword"],
        _count: {
          keyword: true,
        },
      });
      let sortedPopKeywords = popKeywords.sort((a, b) =>
        a._count.keyword > b._count.keyword
          ? -1
          : a._count.keyword < b._count.keyword
          ? 1
          : 0
      );
      sortedPopKeywords = sortedPopKeywords.slice(0, 10);
      sortedPopKeywords.map((e, i) => {
        let keywordCount = {
          keyword: e.keyword,
          count: e._count.keyword,
        };
        keywords.popularKeywords.push(keywordCount);
      });
      if (uid) {
        let user = await ctx.prisma.user.findFirst({
          where: {
            id: uid,
          },
          include: {
            keywords: {
              orderBy: {
                createdAt: "desc",
              },
              take: 10,
            },
          },
        });

        keywords.personalKeywords = user.keywords;
      }

      return keywords;
    },
  });
});

//MUTATION FIELD

export const createComment = mutationField("createComment", {
  type: Comment,
  args: {
    postId: nonNull(stringArg()),
    comment: stringArg(),
  },
  resolve: async (_, args, ctx) => {
    let uid = ctx.session.user.id;
    const pureComment = ctx.commentDetector({ comment: args.comment });
    const post = await ctx.prisma.post.findFirst({
      where: { id: args.postId },
    });

    const rank = await ctx.checkRank(uid, 1);
    const user = await ctx.prisma.user.update({
      where: {
        id: uid,
      },
      data: {
        score: {
          increment: 1,
        },
        rank: rank,
      },
      include: {
        setting: true,
      },
    });

    if (uid !== post.authorId) {
      ctx.sendNotification({
        type: "COMMENT",
        url: "/post/" + args.postId,
        data: { isAnonymous: user.setting.anonymous },
        message: "commented on your post",
        userId: post.authorId,
        fromUserId: uid,
      });
    }

    if (pureComment[1].length > 0) {
      ctx.sendNotification({
        type: "COMMENT",
        url: "/post/" + args.postId,
        data: {
          isAnonymous: user.setting.anonymous,
          badWords: pureComment[1],
        },
        message: (ctx.session.user.name =
          "We have detected bad words in your comment. Your comment will be filled out on these bad words: " +
          pureComment[1].toString()),
        userId: uid,
        fromUserId: uid,
      });
    }

    return ctx.prisma.comment.create({
      data: {
        user: {
          connect: {
            id: uid,
          },
        },
        post: {
          connect: {
            id: "" + args.postId,
          },
        },
        isAnonymous: user.setting.anonymous,
        upvote: 1,
        votes: {
          create: {
            user: {
              connect: { id: uid },
            },
            action: "UPVOTE",
          },
        },
        comment: pureComment[0],
        badWordDetection: {
          create: {
            badWord: pureComment[1],
          },
        },
      },
    });
  },
});

export const createReply = mutationField("createReply", {
  type: Comment,
  args: {
    postId: nonNull(stringArg()),
    parentId: intArg(),
    comment: stringArg(),
  },
  resolve: async (_, args, ctx) => {
    const pureComment = ctx.commentDetector({ comment: args.comment });
    let uid = ctx.session?.user.id;

    const comment = await ctx.prisma.comment.findFirst({
      where: { id: args.parentId },
      select: {
        post: {
          select: {
            authorId: true,
          },
        },
        user: {
          select: {
            id: true,
          },
        },
      },
    });
    const rank = await ctx.checkRank(uid, 1);
    const user = await ctx.prisma.user.update({
      where: {
        id: uid,
      },
      data: {
        score: {
          increment: 1,
        },
        rank: rank,
      },
      include: {
        setting: true,
      },
    });

    //Send Post Author
    if (
      uid !== comment.post.authorId &&
      comment.post.authorId !== comment.user.id
    ) {
      ctx.sendNotification({
        type: "COMMENT",
        url: "/post/" + args.postId,
        data: { isAnonymous: user.setting.anonymous },
        message: "replied to a comment on your post",
        userId: comment.post.authorId,
        fromUserId: uid,
      });
    }
    //Sned to comment owner
    if (uid !== comment.user.id) {
      ctx.sendNotification({
        type: "COMMENT",
        url: "/post/" + args.postId,
        data: { isAnonymous: user.setting.anonymous },
        message: "replied to your comment",
        userId: comment.user.id,
        fromUserId: uid,
      });
    }

    if (pureComment[1].length > 0) {
      ctx.sendNotification({
        type: "COMMENT",
        url: "/post/" + args.postId,
        data: {
          isAnonymous: user.setting.anonymous,
          badWords: pureComment[1],
        },
        message: (ctx.session.user.name =
          "We have detected bad words in your comment. Your comment will be filled out on these bad words: " +
          pureComment[1].toString()),
        userId: uid,
        fromUserId: uid,
      });
    }

    return ctx.prisma.comment.create({
      data: {
        user: {
          connect: {
            id: uid,
          },
        },
        post: {
          connect: {
            id: "" + args.postId,
          },
        },
        upvote: 1,
        isAnonymous: user.setting.anonymous,
        votes: {
          create: {
            user: {
              connect: { id: uid },
            },
            action: "UPVOTE",
          },
        },
        comment: pureComment[0],
        parent: {
          connect: {
            id: args.parentId ? args.parentId : null,
          },
        },
        badWordDetection: {
          create: {
            badWord: pureComment[1],
          },
        },
      },
    });
  },
});

export const createDraft = mutationField("createDraft", {
  type: Post,
  args: {
    title: stringArg(),
    content: stringArg(),
    tags: list(intArg()),
  },
  resolve: (_, args, ctx) => {
    let uid = ctx.session?.user.id;
    const ids = args.tags.map((e, i) => ({
      id: e,
    }));
    return ctx.prisma.post.create({
      data: {
        title: args.title,
        content: args.content,
        status: "DRAFTED",
        author: {
          connect: {
            id: uid,
          },
          // create:{
          //   logs:{
          //     create:{log:"You drafted a new post"}
          //   }
          // }
        },
        tags: {
          connect: ids,
        },
      },
    });
  },
});

export const createPost = mutationField("createPost", {
  type: Post,
  args: {
    title: stringArg(),
    content: stringArg(),
    tags: list(intArg()),
  },
  resolve: async (_, args, ctx) => {
    let uid = ctx.session?.user.id;
    const ids = args.tags.map((e, i) => ({
      id: e,
    }));
    const post = {
      title: args.title,
      content: args.content,
    };
    const badWord = ctx.postDetector(post);
    const badWordArray = badWord.badTitle.concat(badWord.badContent);
    const rank = await ctx.checkRank(uid, 1);
    const user = await ctx.prisma.user.update({
      where: {
        id: uid,
      },
      data: {
        score: {
          increment: 1,
        },
        rank: rank,
      },
      include: {
        setting: true,
      },
    });

    // ctx.sendNotification({
    //   type: "CHAT",
    //   url: `/chat/${chatRoom.slug}`,
    //   // data: { message: createdMessage },
    //   message: "messages you",
    //   userId: e.id,
    //   fromUserId: userId,
    // });

    const createdPost = await ctx.prisma.post.create({
      data: {
        title: args.title,
        content: args.content,
        published: true,
        status: "PUBLISHED",
        publishedAt: new Date(),
        score: Math.log10(1 + user.score) + new Date().getTime() / 45000000,
        author: {
          connect: {
            id: uid,
          },
          // create:{
          //   logs:{
          //     create:{log:"You drafted a new post"}
          //   }
          // }
        },
        isAnonymous: user.setting.anonymous,
        upvote: 1,
        votes: {
          create: {
            user: {
              connect: { id: uid },
            },
            action: "UPVOTE",
          },
        },
        tags: {
          connect: ids,
        },
        badWordDetection: {
          create: {
            badWord: badWordArray,
          },
        },
      },
    });

    const { followedBy } =
      uid &&
      (await ctx.prisma.user.findFirst({
        where: { id: uid },
        select: {
          followedBy: {
            select: {
              id: true,
            },
          },
        },
      }));
    console.log(createdPost);
    followedBy &&
      followedBy.map((e, i) => {
        ctx.sendNotification({
          type: "POST",
          url: `/post/${createdPost.id}`,
          // data: { message: createdMessage },
          message: `has published a post, ${createdPost.title}`,
          userId: e.id,
          fromUserId: uid,
        });
      });
    return createdPost;
  },
});

export const updatePostStatus = mutationField("updatePostStatus", {
  type: Post,
  args: {
    postId: stringArg(),
    status: PostStatus,
  },

  resolve: async (_, args, ctx) => {
    if (args.status === "PUBLISHED") {
      const post = await ctx.prisma.post.findFirst({
        where: { id: args.postId },
      });
      const badWord = ctx.postDetector(post);
      const badWordArray = badWord.badTitle.concat(badWord.badContent);
      const rank = await ctx.checkRank(post.authorId, 1);
      const user = await ctx.prisma.user.update({
        where: {
          id: post.authorId,
        },
        data: {
          score: {
            increment: 1,
          },
          rank: rank,
        },
        include: {
          setting: true,
        },
      });
      return ctx.prisma.post.update({
        where: {
          id: "" + args.postId,
        },
        data: {
          status: args.status,
          isAnonymous: user.setting.anonymous,
          publishedAt: new Date(),
          upvote: 1,
          score: Math.log10(1 + user.score) + new Date().getTime() / 45000000,
          votes: {
            create: {
              user: {
                connect: { id: post.authorId },
              },
              action: "UPVOTE",
            },
          },
          badWordDetection: {
            create: {
              badWord: badWordArray,
            },
          },
        },
      });
    } else {
      return ctx.prisma.post.update({
        where: {
          id: "" + args.postId,
        },
        data: {
          status: args.status,
        },
      });
    }
  },
});

export const updatePost = mutationField("updatePost", {
  type: Post,
  args: {
    postId: stringArg(),
    status: arg({ type: PostStatus }),
    published: booleanArg(),
    title: stringArg(),
    content: stringArg(),
    tags: list(intArg()),
  },
  resolve: async (_, args, ctx) => {
    const ids = args.tags.map((e, i) => ({
      id: e,
    }));
    if (args.status === "PUBLISHED") {
      const post = await ctx.prisma.post.findFirst({
        where: { id: args.postId },
      });
      const badWord = ctx.postDetector(post);
      const badWordArray = badWord.badTitle.concat(badWord.badContent);
      const rank = await ctx.checkRank(post.authorId, 1);
      const user = await ctx.prisma.user.update({
        where: {
          id: post.authorId,
        },
        data: {
          score: {
            increment: 1,
          },
          rank: rank,
        },
      });
      return ctx.prisma.post.update({
        where: {
          id: "" + args.postId,
        },
        data: {
          published: args.published,
          status: args.status,
          title: args.title,
          content: args.content,
          score: Math.log10(1 + user.score) + new Date().getTime() / 45000000,
          tags: {
            set: [],
            connect: ids,
          },
          upvote: 1,
          votes: {
            create: {
              user: {
                connect: { id: post.authorId },
              },
              action: "UPVOTE",
            },
          },
          badWordDetection: {
            create: {
              badWord: badWordArray,
            },
          },
        },
      });
    } else {
      return ctx.prisma.post.update({
        where: {
          id: "" + args.postId,
        },
        data: {
          published: args.published,
          status: args.status,
          title: args.title,
          content: args.content,
          tags: {
            set: [],
            connect: ids,
          },
        },
      });
    }
  },
});

export const createPostVote = mutationField("createPostVote", {
  type: Post,
  args: {
    postId: stringArg(),
    action: arg({ type: Action }),
    isVoted: booleanArg(),
  },
  resolve: async (_, args, ctx: any) => {
    let u = 0;
    let d = 0;
    let uid = ctx.session.user.id;
    let vote;

    if (args.action === "UPVOTE") {
      u = 1;
    } else if (args.action === "DOWNVOTE") {
      d = 1;
    }

    if (args.isVoted === true) {
      vote = await ctx.prisma.vote.findFirst({
        where: {
          postId: args.postId,
          userId: uid,
        },
        include: {
          post: {
            select: {
              author: {
                select: { id: true },
              },
            },
          },
        },
      });

      if (args.action === "UPVOTE") {
        if (vote.action === "DOWNVOTE") {
          d = -1;
        }
      } else if (args.action === "DOWNVOTE") {
        if (vote.action === "UPVOTE") {
          u = -1;
        }
      } else if (args.action === "UNVOTED") {
        if (vote.action === "UPVOTE") {
          u = -1;
        }
        if (vote.action === "DOWNVOTE") {
          d = -1;
        }
      }
    } else {
      vote = await ctx.prisma.vote.create({
        data: {
          user: {
            connect: { id: uid },
            // create:{
            //   logs:{
            //     create:{log:"You "+args.action + " a post."}
            //   }
            // }
          },
          post: {
            connect: {
              id: "" + args.postId,
            },
          },
          action: args.action,
        },
        include: {
          post: {
            select: {
              author: true,
            },
          },
        },
      });
    }

    const rank = await ctx.checkRank(vote.post.author.id, u - d);
    console.log(rank);

    return ctx.prisma.post.update({
      where: {
        id: args.postId,
      },
      data: {
        author: {
          update: {
            score: {
              increment: u - d,
            },
            rank: rank,
          },
        },
        upvote: {
          increment: u,
        },
        downvote: {
          increment: d,
        },
        votes: {
          update: {
            where: {
              id: vote.id,
            },
            data: {
              action: args.action,
            },
          },
        },
      },
    });
  },
});

export const createCommentVote = mutationField("createCommentVote", {
  type: Comment,
  args: {
    commentId: intArg(),
    action: arg({ type: Action }),
    isVoted: booleanArg(),
  },
  resolve: async (_, args, ctx: any) => {
    let u = 0;
    let d = 0;
    let uid = ctx.session.user.id;
    let vote;

    if (args.action === "UPVOTE") {
      u = 1;
    } else if (args.action === "DOWNVOTE") {
      d = 1;
    }

    if (args.isVoted === true) {
      vote = await ctx.prisma.vote.findFirst({
        where: {
          commentId: args.commentId,
          userId: uid,
        },
        include: {
          comment: {
            select: {
              user: {
                select: {
                  id: true,
                },
              },
            },
          },
        },
      });

      if (args.action === "UPVOTE") {
        if (vote.action === "DOWNVOTE") {
          d = -1;
        }
      } else if (args.action === "DOWNVOTE") {
        if (vote.action === "UPVOTE") {
          u = -1;
        }
      } else if (args.action === "UNVOTED") {
        if (vote.action === "UPVOTE") {
          u = -1;
        }
        if (vote.action === "DOWNVOTE") {
          d = -1;
        }
      }
    } else {
      vote = await ctx.prisma.vote.create({
        data: {
          user: {
            connect: { id: uid },
            // create:{
            //   logs:{
            //     create:{log:"You "+args.action + " a post."}
            // //   }
            // }
          },
          comment: {
            connect: {
              id: args.commentId,
            },
          },
          action: args.action,
        },
        include: {
          comment: {
            select: {
              user: true,
            },
          },
        },
      });
    }
    console.log(vote.comment.user);

    const rank = await ctx.checkRank(vote.comment.user.id, u - d);
    return ctx.prisma.comment.update({
      where: {
        id: +args.commentId,
      },
      data: {
        user: {
          update: {
            score: {
              increment: u - d,
            },
            rank: rank,
          },
        },
        upvote: {
          increment: u,
        },
        downvote: {
          increment: d,
        },
        votes: {
          update: {
            where: {
              id: vote.id,
            },
            data: {
              action: args.action,
            },
          },
        },
      },
    });
  },
});

export const createSave = mutationField("createSave", {
  type: ReturnMessage,
  args: {
    postId: stringArg(),
  },
  resolve: async (_, args, ctx) => {
    let message = {
      message: "",
      status: 0,
    };
    const uid = ctx.session.user.id;
    const saved = await ctx.prisma.saved.findFirst({
      where: {
        userId: uid,
        postId: "" + args.postId,
      },
    });
    if (saved) {
      await ctx.prisma.saved.delete({
        where: {
          id: saved.id,
        },
      });
      message.status = 200;
      message.message = "Successfully unsaved!";
    } else {
      await ctx.prisma.saved.create({
        data: {
          userId: uid,
          postId: "" + args.postId,
        },
      });
      message.status = 200;
      message.message = "Successfully saved!";
    }
    return message;
  },
});

export const createReportedPost = mutationField("createReportedPost", {
  type: ReportedPost,
  args: {
    postId: nonNull(stringArg()),
    reason: nonNull(stringArg()),
  },
  resolve: (_, args, ctx) => {
    return ctx.prisma.reportedPost.create({
      data: {
        user: {
          connect: { id: ctx.session.user.id },
          // create:{
          //   logs:{
          //     create:{log:"You reported a post."}
          //   }
          // }
        },
        post: {
          connect: {
            id: args.postId,
          },
        },
        reason: args.reason,
      },
    });
  },
});

export const deleteDraft = mutationField("deleteDraft", {
  type: User,
  args: {
    draftId: nonNull(stringArg()),
  },
  resolve: (_, args, ctx) => {
    const uid = ctx.session?.user.id;
    return ctx.prisma.user.update({
      where: {
        id: uid,
      },
      data: {
        posts: {
          update: {
            where: {
              id: args.draftId,
            },
            data: {
              status: "DELETED",
            },
          },
        },
      },
    });
  },
});
