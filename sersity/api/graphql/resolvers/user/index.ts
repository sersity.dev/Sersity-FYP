import { Hash } from "crypto";
import { database } from "faker";
import { session } from "next-auth/client";
import {
  booleanArg,
  mutationField,
  objectType,
  queryField,
  stringArg,
  subscriptionField,
} from "nexus";
const ONLINEUSERS_SUBCRIPTION = "ONLINEUSERS_SUBCRIPTION";
export const User = objectType({
  name: "User",
  definition(t) {
    t.model.id();
    t.model.name();
    t.model.email();
    t.model.posts({ filtering: true, ordering: true });
    t.model.image();
    t.model.votes();
    t.model.keywords({ordering: true, filtering:true});
    t.model.lastSeen();
    t.model.notifications({ filtering: true, ordering: true });
    t.model.chatRooms({ filtering: true, ordering: true });
    t.model.followedBy();
    t.model.following();
    t.model.rank();
    t.model.score();
    t.model.setting();
    t.model.logs();
    t.model.information();
    t.boolean("isOnline", {
      resolve: (root, _, ctx): any => {
        if (root.lastSeen === null) return false;
        //current time  exceed last seen by 2minutes is offline
        return Date.now() - new Date(root.lastSeen).getTime() > 40000
          ? false
          : true;
      },
    });
    t.boolean("isPasswordless", {
      resolve: async (root, _, ctx) => {
        const account = await ctx.prisma.account.findFirst({
          where: {
            userId: root.id,
          },
        });
        if (account) {
          return true;
        } else {
          return false;
        }
      },
    });
    t.int("countPost", {
      resolve: (root,_,ctx) => {
        return ctx.prisma.post.count({
          where:{
            authorId: root.id,
            status: "PUBLISHED",
            published: true,
            isAnonymous: false
          }
        })
      }
    })
    t.int("countFollower", {
      resolve: (root,_,ctx) => {
        return ctx.prisma.user.count({
          where: {
            status: "ACTIVE",
            following: {
              some:{
                id: root.id
              }
            }
          },
        })
      }
    })
  },
});



export const Keyword = objectType({
  name: "Keyword",
  definition(t){
    t.model.id()
    t.model.keyword()
    t.model.createdAt()
    t.model.user()
  }
})

export const queryUser = queryField((t) => {
  t.crud.user();
  t.crud.users({ filtering: true, ordering: true });
});

export const mutationUser = mutationField((t) => {
  t.crud.createOneUser();
  t.field("updateUserLastSeen", {
    type: "User",
    resolve: async (parent, _, ctx) => {
      const userId = ctx.session?.user?.id;
      let onlinedUsers = [];
      const onlineUser =
        ctx.session &&
        (await ctx.prisma.user.update({
          where: { id: userId },
          data: { lastSeen: new Date().toISOString() },
          include: {
            followedBy: {
              select: {
                id: true,
                name: true,
              },
            },
          },
        }));

      if (onlineUser) {
        onlinedUsers.push(onlineUser);

        //notify to check who is online
        ctx.pubSub.publish(`${ONLINEUSERS_SUBCRIPTION}${userId}`, {
          onlinedUsers,
        });

        //notify my follower that I am onlined
        onlineUser?.followedBy?.map((e) => {
          console.log(
            "who",
            ctx.session.user.name,
            `${ONLINEUSERS_SUBCRIPTION}${e.id}`,
            e.name
          );
          ctx.pubSub.publish(`${ONLINEUSERS_SUBCRIPTION}${e.id}`, {
            onlinedUsers,
          });
        });
      }

      return onlineUser;
    },
  });
  t.field("follow", {
    type: "User",
    args: {
      followingId: stringArg(),
    },
    resolve: async (parent, { followingId }, ctx) => {
      const following = await ctx.prisma.user.update({
        where: { id: ctx.session?.user?.id },
        data: { following: { connect: { id: followingId } } },
      });

      // following && ctx.sendNotification({
      //   type: "SYSTEM",
      //   url: "/profile/" + followingId,
      //   // data: { isAnonymous: user.setting.anonymous },
      //   message: "has followed you",
      //   userId: followingId,
      //   fromUserId: ctx.session.user.id
      // });
      return following
    },
  });
  t.field("unfollow", {
    type: "User",
    args: {
      followingId: stringArg(),
    },
    resolve: (parent, { followingId }, ctx) => {
      return ctx.prisma.user.update({
        where: { id: ctx.session?.user?.id },
        data: { following: { disconnect: { id: followingId } } },
      });
    },
  });
});

export const Subscriber = objectType({
  name: "Subscriber",
  definition(t) {
    t.model.id();
    t.model.email();
    t.model.createdAt();
  },
});

export const querySub = queryField((t) => {
  t.crud.subscriber();
  t.crud.subscribers({ filtering: true, ordering: true });
});

export const subMutation = mutationField((t) => {
  t.crud.createOneSubscriber();
});

export const OnlinedUsers = subscriptionField((t) => {
  t.list.field("OnlinedUsers", {
    type: User,
    args: {
      userId: stringArg(),
    },
    subscribe: async (root, { userId }, ctx) => {
      return ctx.pubSub.asyncIterator(`${ONLINEUSERS_SUBCRIPTION}${userId}`);
    },
    resolve: ({ onlinedUsers }) => {
      return onlinedUsers;
    },
  });
});

export const UpdatePasswordMessage = objectType({
  name: "UpdatePasswordMessage",
  definition(t) {
    t.string("msg");
    t.boolean("status");
  },
});

export const updatePassword = mutationField("updatePassword", {
  type: UpdatePasswordMessage,
  args: {
    oldPw: stringArg(),
    newPw: stringArg(),
    cfNewPw: stringArg(),
  },
  resolve: async (_, args, ctx) => {
    const uid = ctx.session.user.id;
    const user = await ctx.prisma.user.findFirst({
      where: {
        id: uid,
      },
    });
    if (args.newPw === args.cfNewPw) {
      const match = await ctx.bcrypt.compare(args.oldPw, user.password);
      if (match) {
        ctx.bcrypt.hash(args.newPw, 10, async function (err, hash) {
          await ctx.prisma.user.update({
            where: {
              id: uid,
            },
            data: {
              password: hash,
            },
          });
        });
        return {
          msg: "Password has been updated successfully",
          status: true,
        };
      } else {
        return {
          msg: "Old password is incorrect",
          status: false,
        };
      }
    } else {
      return {
        msg: "New password does not match",
        status: false,
      };
    }
  },
});

export const clearKeyword = mutationField("clearKeyword",{
  type: User,
  resolve: (_,__,ctx) => {
    const uid = ctx.session.user.id
    return ctx.prisma.user.update({
      where: {
        id: uid
      },
      data:{
        keywords: {
          set: []
        }
      }
    })
  }
})
