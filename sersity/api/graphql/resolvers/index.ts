import {
  mutationField,
  objectType,
  stringArg,
  queryField,
  queryType,
} from "nexus";

//use prisma update many need return count
export const prismaUpdateMany = objectType({
  name: "PrismaUpdateMany",
  definition(t) {
    t.int("count");
  },
});

//we can write any where
export * from "./notification";
export * from "./chat";
export * from "./post";
export * from "./user";
export * from "./setting";
