import {
  enumType,
  intArg,
  mutationField,
  objectType,
  queryField,
  stringArg,
  subscriptionField,
} from "nexus";

export const ChatRoom = objectType({
  name: "ChatRoom",
  definition(t) {
    t.model.id();
    t.model.name();
    t.model.admin();
    t.model.image();
    t.model.slug();
    t.model.members();
    t.model.messages({
      type: "Message",
      ordering: true,
      filtering: true,
      pagination: true,
    });
    t.boolean("isOnline", {
      resolve: async (root, _, ctx) => {
        const { members } = await ctx.prisma.chatRoom.findFirst({
          where: { id: root.id },
          select: { members: { select: { lastSeen: true } } },
        });

        const isChatOnline = () => {
          let isOn = false;
          members.map((e) => {
            const value = Date.now() - new Date(e?.lastSeen).getTime();

            isOn = value < 30000 ? true : false;
          });
          return isOn;
        };

        return isChatOnline();
      },
    });
    t.model.createdAt();
    t.model.updatedAt();
  },
});

export const SeenBy = objectType({
  name: "SeenBy",
  definition(t) {
    t.model.id();
    t.model.user();
    t.model.createdAt();
  },
});

export const Message = objectType({
  name: "Message",
  definition(t) {
    t.model.id();
    t.model.chatRoom();
    t.model.user();
    t.model.seenBy();
    t.model.createdAt();
    t.model.updatedAt();
    t.field("content", {
      type: "String",
      resolve: async (root: any, _, ctx) => {
        const msg = await ctx.prisma.message.findFirst({
          where: { id: root.id },
          select: {
            chatRoom: {
              select: {
                id: true,
                slug: true,
              },
            },
          },
        });

        const key = msg.chatRoom.id + msg.chatRoom.slug;
        return msg && key && ctx.cryptr(key).decrypt(root.content);
      },
    });
  },
});

export const queryCruds = queryField((t) => {
  t.crud.message();
  t.crud.messages({
    filtering: true,
    pagination: true,
  });
  t.crud.chatRoom();
  t.crud.chatRooms({ filtering: true, pagination: true, ordering: true });
});

export const searchUsers = queryField((t) => {
  t.list.field("searchUsers", {
    type: "User",
    args: {
      searchText: stringArg(),
    },
    resolve: async (root, { searchText }, ctx) => {
      const uid = ctx.session.user.id;
      const users = await ctx.prisma.user.findMany({
        where: {
          id: {
            not: uid,
          },
          OR: [
            { name: { contains: searchText, mode: "insensitive" } },
            // { email: { contains: searchText, mode: "insensitive" } },
          ],
        },
        orderBy: {
          lastSeen: "desc",
        },
      });
      return users;
    },
  });
});

//store whom need to notice
const CHATROOM_SUBCRIPTION = "CHATROOM_SUBSCRIPTION";
const MESSAGE_SUBCRIPTION = "MESSAGE_SUBCRIPTION";

export const NotificationType = enumType({
  name: "NotificationType",
  members: {
    SYSTEM: "SYSTEM",
    COMMENT: "COMMENT",
    CHAT: "CHAT",
    POST: "POST",
  },
});

export const createMessageSeen = mutationField((t) => {
  t.field("updateLastMessageSeenBy", {
    type: Message,
    args: {
      lastMessageId: intArg(),
    },
    resolve: async (root, { lastMessageId }, ctx) => {
      return ctx.prisma.message.update({
        where: { id: lastMessageId },
        data: {
          seenBy: {
            create: {
              user: { connect: { id: ctx?.session?.user?.id } },
            },
          },
        },
      });
    },
  });

  t.field("createMessage", {
    type: Message,
    args: {
      content: stringArg(),
      userId: stringArg(),
      chatRoomId: intArg(),
      slug: stringArg(),
    },
    resolve: async (root, { content, userId, chatRoomId, slug }, ctx) => {
      const key = chatRoomId + slug;
      const createdMessage = await ctx.prisma.message.create({
        data: {
          content: ctx.cryptr(key).encrypt(content),
          user: { connect: { id: userId } },
          chatRoom: { connect: { id: chatRoomId } },
        },
      });
      const chatRoom = await ctx.prisma.chatRoom.findFirst({
        where: { id: chatRoomId },
        include: {
          members: {
            select: {
              id: true,
            },
          },
        },
      });

      //store
      chatRoom &&
        createdMessage &&
        chatRoom.members.map((e) => {
          if (e.id != userId) {
            ctx.sendNotification({
              type: "CHAT",
              url: `/chat/${chatRoom.slug}`,
              // data: { message: createdMessage },
              message: "messages you",
              userId: e.id,
              fromUserId: userId,
            });
          }
          ctx.pubSub.publish(`${MESSAGE_SUBCRIPTION}${e.id}`, {
            createdMessage,
          });
        });

      return createdMessage;
    },
  });
});

export const createdMessage = subscriptionField((t) => {
  t.field("createdMessage", {
    type: Message,
    args: {
      userId: stringArg(),
    },
    subscribe: async (root, { userId }, ctx) => {
      return ctx.pubSub.asyncIterator(`${MESSAGE_SUBCRIPTION}${userId}`);
    },
    resolve: ({ createdMessage }) => {
      return createdMessage;
    },
  });
});

export const createOneRoom = mutationField((t) => {
  t.crud.createOneChatRoom({
    async resolve(root, args, ctx, info, originalResolve) {
      const res: any = await originalResolve(root, args, ctx, info);
      const chatRoom = await ctx.prisma.chatRoom.findFirst({
        where: { id: res.id },
        include: {
          members: {
            select: {
              id: true,
            },
          },
        },
      });

      res &&
        chatRoom?.members.map((e) => {
          ctx.pubSub.publish(`${CHATROOM_SUBCRIPTION}${e.id}`, {
            createdChatRoom: res,
          });
        });
      return res;
    },
  });
});

export const createdChatRoom = subscriptionField((t) => {
  t.field("createdChatRoom", {
    type: ChatRoom,
    args: {
      userId: stringArg(),
    },
    subscribe: async (root, { userId }, ctx) => {
      return ctx.pubSub.asyncIterator(`${CHATROOM_SUBCRIPTION}${userId}`);
    },
    resolve: ({ createdChatRoom }) => createdChatRoom,
  });
});
