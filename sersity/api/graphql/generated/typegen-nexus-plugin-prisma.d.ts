import * as Typegen from 'nexus-plugin-prisma/typegen'
import * as Prisma from '@prisma/client';

// Pagination type
type Pagination = {
    take?: boolean
    skip?: boolean
    cursor?: boolean
}

// Prisma custom scalar names
type CustomScalars = 'DateTime' | 'Json'

// Prisma model type definitions
interface PrismaModels {
  Account: Prisma.Account
  Session: Prisma.Session
  Subscriber: Prisma.Subscriber
  User: Prisma.User
  Keyword: Prisma.Keyword
  Hidden: Prisma.Hidden
  Setting: Prisma.Setting
  Information: Prisma.Information
  ExtraInfo: Prisma.ExtraInfo
  Notification: Prisma.Notification
  ChatRoom: Prisma.ChatRoom
  SeenBy: Prisma.SeenBy
  Message: Prisma.Message
  VerificationRequest: Prisma.VerificationRequest
  Post: Prisma.Post
  Comment: Prisma.Comment
  Vote: Prisma.Vote
  Saved: Prisma.Saved
  BadWordDetection: Prisma.BadWordDetection
  Tag: Prisma.Tag
  ReportedPost: Prisma.ReportedPost
  UserLog: Prisma.UserLog
  OTPVerification: Prisma.OTPVerification
  Rank: Prisma.Rank
}

// Prisma input types metadata
interface NexusPrismaInputs {
  Query: {
    accounts: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'compoundId' | 'userId' | 'providerType' | 'providerId' | 'providerAccountId' | 'refreshToken' | 'accessToken' | 'accessTokenExpires' | 'createdAt' | 'updatedAt'
      ordering: 'id' | 'compoundId' | 'userId' | 'providerType' | 'providerId' | 'providerAccountId' | 'refreshToken' | 'accessToken' | 'accessTokenExpires' | 'createdAt' | 'updatedAt'
    }
    sessions: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'userId' | 'expires' | 'sessionToken' | 'accessToken' | 'createdAt' | 'updatedAt'
      ordering: 'id' | 'userId' | 'expires' | 'sessionToken' | 'accessToken' | 'createdAt' | 'updatedAt'
    }
    subscribers: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'email' | 'createdAt' | 'updatedAt'
      ordering: 'id' | 'email' | 'createdAt' | 'updatedAt'
    }
    users: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'name' | 'email' | 'emailVerified' | 'image' | 'createdAt' | 'updatedAt' | 'status' | 'password' | 'rank' | 'score' | 'posts' | 'lastSeen' | 'comments' | 'chatRooms' | 'notifications' | 'information' | 'setting' | 'message' | 'followedBy' | 'following' | 'votes' | 'saved' | 'reportingPosts' | 'logs' | 'chatRoom' | 'notification' | 'seenBy' | 'seenById' | 'keywords'
      ordering: 'id' | 'name' | 'email' | 'emailVerified' | 'image' | 'createdAt' | 'updatedAt' | 'status' | 'password' | 'rank' | 'score' | 'lastSeen' | 'seenById'
    }
    keywords: {
      filtering: 'AND' | 'OR' | 'NOT' | 'user' | 'id' | 'keyword' | 'createdAt' | 'userId'
      ordering: 'id' | 'keyword' | 'createdAt' | 'userId'
    }
    hiddens: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'post' | 'createdAt' | 'postId'
      ordering: 'id' | 'createdAt' | 'postId'
    }
    settings: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'anonymous' | 'secondVerification' | 'allowBadWord' | 'notification' | 'commentNoti' | 'replyNoti' | 'newTrendNoti' | 'followingUserNoti' | 'chatSetting' | 'chatActiveStatus' | 'createdAt' | 'updatedAt' | 'user' | 'userId'
      ordering: 'id' | 'anonymous' | 'secondVerification' | 'allowBadWord' | 'notification' | 'commentNoti' | 'replyNoti' | 'newTrendNoti' | 'followingUserNoti' | 'chatSetting' | 'chatActiveStatus' | 'createdAt' | 'updatedAt' | 'userId'
    }
    information: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'user' | 'userId' | 'phone' | 'dateOfBirth' | 'bio' | 'highSchool' | 'university' | 'location' | 'extraInfo' | 'createdAt' | 'updatedAt'
      ordering: 'id' | 'userId' | 'phone' | 'dateOfBirth' | 'bio' | 'highSchool' | 'university' | 'location' | 'createdAt' | 'updatedAt'
    }
    extraInfos: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'Information' | 'informationId' | 'title' | 'description' | 'createdAt' | 'updatedAt'
      ordering: 'id' | 'informationId' | 'title' | 'description' | 'createdAt' | 'updatedAt'
    }
    notifications: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'user' | 'fromUser' | 'data' | 'message' | 'url' | 'type' | 'isDeleted' | 'readAt' | 'createdAt' | 'updatedAt' | 'userId' | 'fromUserId'
      ordering: 'id' | 'data' | 'message' | 'url' | 'type' | 'isDeleted' | 'readAt' | 'createdAt' | 'updatedAt' | 'userId' | 'fromUserId'
    }
    chatRooms: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'admin' | 'name' | 'image' | 'slug' | 'members' | 'messages' | 'deletedAt' | 'createdAt' | 'updatedAt' | 'userId'
      ordering: 'id' | 'name' | 'image' | 'slug' | 'deletedAt' | 'createdAt' | 'updatedAt' | 'userId'
    }
    seenBies: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'user' | 'createdAt' | 'Message' | 'messageId'
      ordering: 'id' | 'createdAt' | 'messageId'
    }
    messages: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'content' | 'chatRoomId' | 'user' | 'chatRoom' | 'seenBy' | 'deletedAt' | 'createdAt' | 'updatedAt' | 'userId'
      ordering: 'id' | 'content' | 'chatRoomId' | 'deletedAt' | 'createdAt' | 'updatedAt' | 'userId'
    }
    verificationRequests: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'identifier' | 'token' | 'expires' | 'createdAt' | 'updatedAt'
      ordering: 'id' | 'identifier' | 'token' | 'expires' | 'createdAt' | 'updatedAt'
    }
    posts: {
      filtering: 'AND' | 'OR' | 'NOT' | 'content' | 'id' | 'title' | 'published' | 'status' | 'viewCount' | 'upvote' | 'downvote' | 'score' | 'publishedAt' | 'createdAt' | 'updatedAt' | 'isBuilt' | 'author' | 'isAnonymous' | 'comments' | 'votes' | 'tags' | 'saved' | 'badWordDetection' | 'reportedPosts' | 'authorId' | 'Hidden'
      ordering: 'content' | 'id' | 'title' | 'published' | 'status' | 'viewCount' | 'upvote' | 'downvote' | 'score' | 'publishedAt' | 'createdAt' | 'updatedAt' | 'isBuilt' | 'isAnonymous' | 'authorId'
    }
    comments: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'postId' | 'userId' | 'parentId' | 'comment' | 'upvote' | 'downvote' | 'isAnonymous' | 'createdAt' | 'updatedAt' | 'parent' | 'post' | 'badWordDetection' | 'replies' | 'user' | 'votes'
      ordering: 'id' | 'postId' | 'userId' | 'parentId' | 'comment' | 'upvote' | 'downvote' | 'isAnonymous' | 'createdAt' | 'updatedAt'
    }
    votes: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'action' | 'updatedAt' | 'createdAt' | 'post' | 'postId' | 'comment' | 'commentId' | 'user' | 'userId'
      ordering: 'id' | 'action' | 'updatedAt' | 'createdAt' | 'postId' | 'commentId' | 'userId'
    }
    saveds: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'userId' | 'postId' | 'createdAt' | 'user' | 'post'
      ordering: 'id' | 'userId' | 'postId' | 'createdAt'
    }
    badWordDetections: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'postId' | 'commentId' | 'badWord' | 'createdAt' | 'post' | 'comment'
      ordering: 'id' | 'postId' | 'commentId' | 'badWord' | 'createdAt'
    }
    tags: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'tag' | 'posts'
      ordering: 'id' | 'tag'
    }
    reportedPosts: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'userId' | 'postId' | 'reason' | 'createdAt' | 'post' | 'user'
      ordering: 'id' | 'userId' | 'postId' | 'reason' | 'createdAt'
    }
    userLogs: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'userId' | 'log' | 'createdAt' | 'user'
      ordering: 'id' | 'userId' | 'log' | 'createdAt'
    }
    otpVerifications: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'code' | 'userData' | 'status' | 'createdAt' | 'expiredAt'
      ordering: 'id' | 'code' | 'userData' | 'status' | 'createdAt' | 'expiredAt'
    }
    ranks: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'name' | 'score' | 'createdAt'
      ordering: 'id' | 'name' | 'score' | 'createdAt'
    }
  },
  Account: {

  }
  Session: {

  }
  Subscriber: {

  }
  User: {
    posts: {
      filtering: 'AND' | 'OR' | 'NOT' | 'content' | 'id' | 'title' | 'published' | 'status' | 'viewCount' | 'upvote' | 'downvote' | 'score' | 'publishedAt' | 'createdAt' | 'updatedAt' | 'isBuilt' | 'author' | 'isAnonymous' | 'comments' | 'votes' | 'tags' | 'saved' | 'badWordDetection' | 'reportedPosts' | 'authorId' | 'Hidden'
      ordering: 'content' | 'id' | 'title' | 'published' | 'status' | 'viewCount' | 'upvote' | 'downvote' | 'score' | 'publishedAt' | 'createdAt' | 'updatedAt' | 'isBuilt' | 'isAnonymous' | 'authorId'
    }
    comments: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'postId' | 'userId' | 'parentId' | 'comment' | 'upvote' | 'downvote' | 'isAnonymous' | 'createdAt' | 'updatedAt' | 'parent' | 'post' | 'badWordDetection' | 'replies' | 'user' | 'votes'
      ordering: 'id' | 'postId' | 'userId' | 'parentId' | 'comment' | 'upvote' | 'downvote' | 'isAnonymous' | 'createdAt' | 'updatedAt'
    }
    chatRooms: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'admin' | 'name' | 'image' | 'slug' | 'members' | 'messages' | 'deletedAt' | 'createdAt' | 'updatedAt' | 'userId'
      ordering: 'id' | 'name' | 'image' | 'slug' | 'deletedAt' | 'createdAt' | 'updatedAt' | 'userId'
    }
    notifications: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'user' | 'fromUser' | 'data' | 'message' | 'url' | 'type' | 'isDeleted' | 'readAt' | 'createdAt' | 'updatedAt' | 'userId' | 'fromUserId'
      ordering: 'id' | 'data' | 'message' | 'url' | 'type' | 'isDeleted' | 'readAt' | 'createdAt' | 'updatedAt' | 'userId' | 'fromUserId'
    }
    message: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'content' | 'chatRoomId' | 'user' | 'chatRoom' | 'seenBy' | 'deletedAt' | 'createdAt' | 'updatedAt' | 'userId'
      ordering: 'id' | 'content' | 'chatRoomId' | 'deletedAt' | 'createdAt' | 'updatedAt' | 'userId'
    }
    followedBy: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'name' | 'email' | 'emailVerified' | 'image' | 'createdAt' | 'updatedAt' | 'status' | 'password' | 'rank' | 'score' | 'posts' | 'lastSeen' | 'comments' | 'chatRooms' | 'notifications' | 'information' | 'setting' | 'message' | 'followedBy' | 'following' | 'votes' | 'saved' | 'reportingPosts' | 'logs' | 'chatRoom' | 'notification' | 'seenBy' | 'seenById' | 'keywords'
      ordering: 'id' | 'name' | 'email' | 'emailVerified' | 'image' | 'createdAt' | 'updatedAt' | 'status' | 'password' | 'rank' | 'score' | 'lastSeen' | 'seenById'
    }
    following: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'name' | 'email' | 'emailVerified' | 'image' | 'createdAt' | 'updatedAt' | 'status' | 'password' | 'rank' | 'score' | 'posts' | 'lastSeen' | 'comments' | 'chatRooms' | 'notifications' | 'information' | 'setting' | 'message' | 'followedBy' | 'following' | 'votes' | 'saved' | 'reportingPosts' | 'logs' | 'chatRoom' | 'notification' | 'seenBy' | 'seenById' | 'keywords'
      ordering: 'id' | 'name' | 'email' | 'emailVerified' | 'image' | 'createdAt' | 'updatedAt' | 'status' | 'password' | 'rank' | 'score' | 'lastSeen' | 'seenById'
    }
    votes: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'action' | 'updatedAt' | 'createdAt' | 'post' | 'postId' | 'comment' | 'commentId' | 'user' | 'userId'
      ordering: 'id' | 'action' | 'updatedAt' | 'createdAt' | 'postId' | 'commentId' | 'userId'
    }
    saved: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'userId' | 'postId' | 'createdAt' | 'user' | 'post'
      ordering: 'id' | 'userId' | 'postId' | 'createdAt'
    }
    reportingPosts: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'userId' | 'postId' | 'reason' | 'createdAt' | 'post' | 'user'
      ordering: 'id' | 'userId' | 'postId' | 'reason' | 'createdAt'
    }
    logs: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'userId' | 'log' | 'createdAt' | 'user'
      ordering: 'id' | 'userId' | 'log' | 'createdAt'
    }
    chatRoom: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'admin' | 'name' | 'image' | 'slug' | 'members' | 'messages' | 'deletedAt' | 'createdAt' | 'updatedAt' | 'userId'
      ordering: 'id' | 'name' | 'image' | 'slug' | 'deletedAt' | 'createdAt' | 'updatedAt' | 'userId'
    }
    notification: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'user' | 'fromUser' | 'data' | 'message' | 'url' | 'type' | 'isDeleted' | 'readAt' | 'createdAt' | 'updatedAt' | 'userId' | 'fromUserId'
      ordering: 'id' | 'data' | 'message' | 'url' | 'type' | 'isDeleted' | 'readAt' | 'createdAt' | 'updatedAt' | 'userId' | 'fromUserId'
    }
    keywords: {
      filtering: 'AND' | 'OR' | 'NOT' | 'user' | 'id' | 'keyword' | 'createdAt' | 'userId'
      ordering: 'id' | 'keyword' | 'createdAt' | 'userId'
    }
  }
  Keyword: {

  }
  Hidden: {

  }
  Setting: {

  }
  Information: {
    extraInfo: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'Information' | 'informationId' | 'title' | 'description' | 'createdAt' | 'updatedAt'
      ordering: 'id' | 'informationId' | 'title' | 'description' | 'createdAt' | 'updatedAt'
    }
  }
  ExtraInfo: {

  }
  Notification: {

  }
  ChatRoom: {
    members: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'name' | 'email' | 'emailVerified' | 'image' | 'createdAt' | 'updatedAt' | 'status' | 'password' | 'rank' | 'score' | 'posts' | 'lastSeen' | 'comments' | 'chatRooms' | 'notifications' | 'information' | 'setting' | 'message' | 'followedBy' | 'following' | 'votes' | 'saved' | 'reportingPosts' | 'logs' | 'chatRoom' | 'notification' | 'seenBy' | 'seenById' | 'keywords'
      ordering: 'id' | 'name' | 'email' | 'emailVerified' | 'image' | 'createdAt' | 'updatedAt' | 'status' | 'password' | 'rank' | 'score' | 'lastSeen' | 'seenById'
    }
    messages: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'content' | 'chatRoomId' | 'user' | 'chatRoom' | 'seenBy' | 'deletedAt' | 'createdAt' | 'updatedAt' | 'userId'
      ordering: 'id' | 'content' | 'chatRoomId' | 'deletedAt' | 'createdAt' | 'updatedAt' | 'userId'
    }
  }
  SeenBy: {

  }
  Message: {
    seenBy: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'user' | 'createdAt' | 'Message' | 'messageId'
      ordering: 'id' | 'createdAt' | 'messageId'
    }
  }
  VerificationRequest: {

  }
  Post: {
    comments: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'postId' | 'userId' | 'parentId' | 'comment' | 'upvote' | 'downvote' | 'isAnonymous' | 'createdAt' | 'updatedAt' | 'parent' | 'post' | 'badWordDetection' | 'replies' | 'user' | 'votes'
      ordering: 'id' | 'postId' | 'userId' | 'parentId' | 'comment' | 'upvote' | 'downvote' | 'isAnonymous' | 'createdAt' | 'updatedAt'
    }
    votes: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'action' | 'updatedAt' | 'createdAt' | 'post' | 'postId' | 'comment' | 'commentId' | 'user' | 'userId'
      ordering: 'id' | 'action' | 'updatedAt' | 'createdAt' | 'postId' | 'commentId' | 'userId'
    }
    tags: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'tag' | 'posts'
      ordering: 'id' | 'tag'
    }
    saved: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'userId' | 'postId' | 'createdAt' | 'user' | 'post'
      ordering: 'id' | 'userId' | 'postId' | 'createdAt'
    }
    badWordDetection: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'postId' | 'commentId' | 'badWord' | 'createdAt' | 'post' | 'comment'
      ordering: 'id' | 'postId' | 'commentId' | 'badWord' | 'createdAt'
    }
    reportedPosts: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'userId' | 'postId' | 'reason' | 'createdAt' | 'post' | 'user'
      ordering: 'id' | 'userId' | 'postId' | 'reason' | 'createdAt'
    }
  }
  Comment: {
    badWordDetection: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'postId' | 'commentId' | 'badWord' | 'createdAt' | 'post' | 'comment'
      ordering: 'id' | 'postId' | 'commentId' | 'badWord' | 'createdAt'
    }
    replies: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'postId' | 'userId' | 'parentId' | 'comment' | 'upvote' | 'downvote' | 'isAnonymous' | 'createdAt' | 'updatedAt' | 'parent' | 'post' | 'badWordDetection' | 'replies' | 'user' | 'votes'
      ordering: 'id' | 'postId' | 'userId' | 'parentId' | 'comment' | 'upvote' | 'downvote' | 'isAnonymous' | 'createdAt' | 'updatedAt'
    }
    votes: {
      filtering: 'AND' | 'OR' | 'NOT' | 'id' | 'action' | 'updatedAt' | 'createdAt' | 'post' | 'postId' | 'comment' | 'commentId' | 'user' | 'userId'
      ordering: 'id' | 'action' | 'updatedAt' | 'createdAt' | 'postId' | 'commentId' | 'userId'
    }
  }
  Vote: {

  }
  Saved: {

  }
  BadWordDetection: {

  }
  Tag: {
    posts: {
      filtering: 'AND' | 'OR' | 'NOT' | 'content' | 'id' | 'title' | 'published' | 'status' | 'viewCount' | 'upvote' | 'downvote' | 'score' | 'publishedAt' | 'createdAt' | 'updatedAt' | 'isBuilt' | 'author' | 'isAnonymous' | 'comments' | 'votes' | 'tags' | 'saved' | 'badWordDetection' | 'reportedPosts' | 'authorId' | 'Hidden'
      ordering: 'content' | 'id' | 'title' | 'published' | 'status' | 'viewCount' | 'upvote' | 'downvote' | 'score' | 'publishedAt' | 'createdAt' | 'updatedAt' | 'isBuilt' | 'isAnonymous' | 'authorId'
    }
  }
  ReportedPost: {

  }
  UserLog: {

  }
  OTPVerification: {

  }
  Rank: {

  }
}

// Prisma output types metadata
interface NexusPrismaOutputs {
  Query: {
    account: 'Account'
    accounts: 'Account'
    session: 'Session'
    sessions: 'Session'
    subscriber: 'Subscriber'
    subscribers: 'Subscriber'
    user: 'User'
    users: 'User'
    keyword: 'Keyword'
    keywords: 'Keyword'
    hidden: 'Hidden'
    hiddens: 'Hidden'
    setting: 'Setting'
    settings: 'Setting'
    information: 'Information'
    information: 'Information'
    extraInfo: 'ExtraInfo'
    extraInfos: 'ExtraInfo'
    notification: 'Notification'
    notifications: 'Notification'
    chatRoom: 'ChatRoom'
    chatRooms: 'ChatRoom'
    seenBy: 'SeenBy'
    seenBies: 'SeenBy'
    message: 'Message'
    messages: 'Message'
    verificationRequest: 'VerificationRequest'
    verificationRequests: 'VerificationRequest'
    post: 'Post'
    posts: 'Post'
    comment: 'Comment'
    comments: 'Comment'
    vote: 'Vote'
    votes: 'Vote'
    saved: 'Saved'
    saveds: 'Saved'
    badWordDetection: 'BadWordDetection'
    badWordDetections: 'BadWordDetection'
    tag: 'Tag'
    tags: 'Tag'
    reportedPost: 'ReportedPost'
    reportedPosts: 'ReportedPost'
    userLog: 'UserLog'
    userLogs: 'UserLog'
    otpVerification: 'OTPVerification'
    otpVerifications: 'OTPVerification'
    rank: 'Rank'
    ranks: 'Rank'
  },
  Mutation: {
    createOneAccount: 'Account'
    updateOneAccount: 'Account'
    updateManyAccount: 'AffectedRowsOutput'
    deleteOneAccount: 'Account'
    deleteManyAccount: 'AffectedRowsOutput'
    upsertOneAccount: 'Account'
    createOneSession: 'Session'
    updateOneSession: 'Session'
    updateManySession: 'AffectedRowsOutput'
    deleteOneSession: 'Session'
    deleteManySession: 'AffectedRowsOutput'
    upsertOneSession: 'Session'
    createOneSubscriber: 'Subscriber'
    updateOneSubscriber: 'Subscriber'
    updateManySubscriber: 'AffectedRowsOutput'
    deleteOneSubscriber: 'Subscriber'
    deleteManySubscriber: 'AffectedRowsOutput'
    upsertOneSubscriber: 'Subscriber'
    createOneUser: 'User'
    updateOneUser: 'User'
    updateManyUser: 'AffectedRowsOutput'
    deleteOneUser: 'User'
    deleteManyUser: 'AffectedRowsOutput'
    upsertOneUser: 'User'
    createOneKeyword: 'Keyword'
    updateOneKeyword: 'Keyword'
    updateManyKeyword: 'AffectedRowsOutput'
    deleteOneKeyword: 'Keyword'
    deleteManyKeyword: 'AffectedRowsOutput'
    upsertOneKeyword: 'Keyword'
    createOneHidden: 'Hidden'
    updateOneHidden: 'Hidden'
    updateManyHidden: 'AffectedRowsOutput'
    deleteOneHidden: 'Hidden'
    deleteManyHidden: 'AffectedRowsOutput'
    upsertOneHidden: 'Hidden'
    createOneSetting: 'Setting'
    updateOneSetting: 'Setting'
    updateManySetting: 'AffectedRowsOutput'
    deleteOneSetting: 'Setting'
    deleteManySetting: 'AffectedRowsOutput'
    upsertOneSetting: 'Setting'
    createOneInformation: 'Information'
    updateOneInformation: 'Information'
    updateManyInformation: 'AffectedRowsOutput'
    deleteOneInformation: 'Information'
    deleteManyInformation: 'AffectedRowsOutput'
    upsertOneInformation: 'Information'
    createOneExtraInfo: 'ExtraInfo'
    updateOneExtraInfo: 'ExtraInfo'
    updateManyExtraInfo: 'AffectedRowsOutput'
    deleteOneExtraInfo: 'ExtraInfo'
    deleteManyExtraInfo: 'AffectedRowsOutput'
    upsertOneExtraInfo: 'ExtraInfo'
    createOneNotification: 'Notification'
    updateOneNotification: 'Notification'
    updateManyNotification: 'AffectedRowsOutput'
    deleteOneNotification: 'Notification'
    deleteManyNotification: 'AffectedRowsOutput'
    upsertOneNotification: 'Notification'
    createOneChatRoom: 'ChatRoom'
    updateOneChatRoom: 'ChatRoom'
    updateManyChatRoom: 'AffectedRowsOutput'
    deleteOneChatRoom: 'ChatRoom'
    deleteManyChatRoom: 'AffectedRowsOutput'
    upsertOneChatRoom: 'ChatRoom'
    createOneSeenBy: 'SeenBy'
    updateOneSeenBy: 'SeenBy'
    updateManySeenBy: 'AffectedRowsOutput'
    deleteOneSeenBy: 'SeenBy'
    deleteManySeenBy: 'AffectedRowsOutput'
    upsertOneSeenBy: 'SeenBy'
    createOneMessage: 'Message'
    updateOneMessage: 'Message'
    updateManyMessage: 'AffectedRowsOutput'
    deleteOneMessage: 'Message'
    deleteManyMessage: 'AffectedRowsOutput'
    upsertOneMessage: 'Message'
    createOneVerificationRequest: 'VerificationRequest'
    updateOneVerificationRequest: 'VerificationRequest'
    updateManyVerificationRequest: 'AffectedRowsOutput'
    deleteOneVerificationRequest: 'VerificationRequest'
    deleteManyVerificationRequest: 'AffectedRowsOutput'
    upsertOneVerificationRequest: 'VerificationRequest'
    createOnePost: 'Post'
    updateOnePost: 'Post'
    updateManyPost: 'AffectedRowsOutput'
    deleteOnePost: 'Post'
    deleteManyPost: 'AffectedRowsOutput'
    upsertOnePost: 'Post'
    createOneComment: 'Comment'
    updateOneComment: 'Comment'
    updateManyComment: 'AffectedRowsOutput'
    deleteOneComment: 'Comment'
    deleteManyComment: 'AffectedRowsOutput'
    upsertOneComment: 'Comment'
    createOneVote: 'Vote'
    updateOneVote: 'Vote'
    updateManyVote: 'AffectedRowsOutput'
    deleteOneVote: 'Vote'
    deleteManyVote: 'AffectedRowsOutput'
    upsertOneVote: 'Vote'
    createOneSaved: 'Saved'
    updateOneSaved: 'Saved'
    updateManySaved: 'AffectedRowsOutput'
    deleteOneSaved: 'Saved'
    deleteManySaved: 'AffectedRowsOutput'
    upsertOneSaved: 'Saved'
    createOneBadWordDetection: 'BadWordDetection'
    updateOneBadWordDetection: 'BadWordDetection'
    updateManyBadWordDetection: 'AffectedRowsOutput'
    deleteOneBadWordDetection: 'BadWordDetection'
    deleteManyBadWordDetection: 'AffectedRowsOutput'
    upsertOneBadWordDetection: 'BadWordDetection'
    createOneTag: 'Tag'
    updateOneTag: 'Tag'
    updateManyTag: 'AffectedRowsOutput'
    deleteOneTag: 'Tag'
    deleteManyTag: 'AffectedRowsOutput'
    upsertOneTag: 'Tag'
    createOneReportedPost: 'ReportedPost'
    updateOneReportedPost: 'ReportedPost'
    updateManyReportedPost: 'AffectedRowsOutput'
    deleteOneReportedPost: 'ReportedPost'
    deleteManyReportedPost: 'AffectedRowsOutput'
    upsertOneReportedPost: 'ReportedPost'
    createOneUserLog: 'UserLog'
    updateOneUserLog: 'UserLog'
    updateManyUserLog: 'AffectedRowsOutput'
    deleteOneUserLog: 'UserLog'
    deleteManyUserLog: 'AffectedRowsOutput'
    upsertOneUserLog: 'UserLog'
    createOneOTPVerification: 'OTPVerification'
    updateOneOTPVerification: 'OTPVerification'
    updateManyOTPVerification: 'AffectedRowsOutput'
    deleteOneOTPVerification: 'OTPVerification'
    deleteManyOTPVerification: 'AffectedRowsOutput'
    upsertOneOTPVerification: 'OTPVerification'
    createOneRank: 'Rank'
    updateOneRank: 'Rank'
    updateManyRank: 'AffectedRowsOutput'
    deleteOneRank: 'Rank'
    deleteManyRank: 'AffectedRowsOutput'
    upsertOneRank: 'Rank'
  },
  Account: {
    id: 'Int'
    compoundId: 'String'
    userId: 'String'
    providerType: 'String'
    providerId: 'String'
    providerAccountId: 'String'
    refreshToken: 'String'
    accessToken: 'String'
    accessTokenExpires: 'DateTime'
    createdAt: 'DateTime'
    updatedAt: 'DateTime'
  }
  Session: {
    id: 'Int'
    userId: 'String'
    expires: 'DateTime'
    sessionToken: 'String'
    accessToken: 'String'
    createdAt: 'DateTime'
    updatedAt: 'DateTime'
  }
  Subscriber: {
    id: 'Int'
    email: 'String'
    createdAt: 'DateTime'
    updatedAt: 'DateTime'
  }
  User: {
    id: 'String'
    name: 'String'
    email: 'String'
    emailVerified: 'DateTime'
    image: 'String'
    createdAt: 'DateTime'
    updatedAt: 'DateTime'
    status: 'AccountStatus'
    password: 'String'
    rank: 'String'
    score: 'Int'
    posts: 'Post'
    lastSeen: 'DateTime'
    comments: 'Comment'
    chatRooms: 'ChatRoom'
    notifications: 'Notification'
    information: 'Information'
    setting: 'Setting'
    message: 'Message'
    followedBy: 'User'
    following: 'User'
    votes: 'Vote'
    saved: 'Saved'
    reportingPosts: 'ReportedPost'
    logs: 'UserLog'
    chatRoom: 'ChatRoom'
    notification: 'Notification'
    seenBy: 'SeenBy'
    seenById: 'Int'
    keywords: 'Keyword'
  }
  Keyword: {
    user: 'User'
    id: 'Int'
    keyword: 'String'
    createdAt: 'DateTime'
    userId: 'String'
  }
  Hidden: {
    id: 'Int'
    post: 'Post'
    createdAt: 'DateTime'
    postId: 'String'
  }
  Setting: {
    id: 'Int'
    anonymous: 'Boolean'
    secondVerification: 'Boolean'
    allowBadWord: 'Boolean'
    notification: 'Boolean'
    commentNoti: 'Boolean'
    replyNoti: 'Boolean'
    newTrendNoti: 'Boolean'
    followingUserNoti: 'Boolean'
    chatSetting: 'ChatSetting'
    chatActiveStatus: 'Boolean'
    createdAt: 'DateTime'
    updatedAt: 'DateTime'
    user: 'User'
    userId: 'String'
  }
  Information: {
    id: 'Int'
    user: 'User'
    userId: 'String'
    phone: 'String'
    dateOfBirth: 'DateTime'
    bio: 'String'
    highSchool: 'String'
    university: 'String'
    location: 'String'
    extraInfo: 'ExtraInfo'
    createdAt: 'DateTime'
    updatedAt: 'DateTime'
  }
  ExtraInfo: {
    id: 'Int'
    Information: 'Information'
    informationId: 'Int'
    title: 'String'
    description: 'String'
    createdAt: 'DateTime'
    updatedAt: 'DateTime'
  }
  Notification: {
    id: 'String'
    user: 'User'
    fromUser: 'User'
    data: 'Json'
    message: 'String'
    url: 'String'
    type: 'NotificationType'
    isDeleted: 'Boolean'
    readAt: 'DateTime'
    createdAt: 'DateTime'
    updatedAt: 'DateTime'
    userId: 'String'
    fromUserId: 'String'
  }
  ChatRoom: {
    id: 'Int'
    admin: 'User'
    name: 'String'
    image: 'String'
    slug: 'String'
    members: 'User'
    messages: 'Message'
    deletedAt: 'DateTime'
    createdAt: 'DateTime'
    updatedAt: 'DateTime'
    userId: 'String'
  }
  SeenBy: {
    id: 'Int'
    user: 'User'
    createdAt: 'DateTime'
    Message: 'Message'
    messageId: 'Int'
  }
  Message: {
    id: 'Int'
    content: 'String'
    chatRoomId: 'Int'
    user: 'User'
    chatRoom: 'ChatRoom'
    seenBy: 'SeenBy'
    deletedAt: 'DateTime'
    createdAt: 'DateTime'
    updatedAt: 'DateTime'
    userId: 'String'
  }
  VerificationRequest: {
    id: 'Int'
    identifier: 'String'
    token: 'String'
    expires: 'DateTime'
    createdAt: 'DateTime'
    updatedAt: 'DateTime'
  }
  Post: {
    content: 'String'
    id: 'String'
    title: 'String'
    published: 'Boolean'
    status: 'PostStatus'
    viewCount: 'Int'
    upvote: 'Int'
    downvote: 'Int'
    score: 'Float'
    publishedAt: 'DateTime'
    createdAt: 'DateTime'
    updatedAt: 'DateTime'
    isBuilt: 'Boolean'
    author: 'User'
    isAnonymous: 'Boolean'
    comments: 'Comment'
    votes: 'Vote'
    tags: 'Tag'
    saved: 'Saved'
    badWordDetection: 'BadWordDetection'
    reportedPosts: 'ReportedPost'
    authorId: 'String'
    Hidden: 'Hidden'
  }
  Comment: {
    id: 'Int'
    postId: 'String'
    userId: 'String'
    parentId: 'Int'
    comment: 'String'
    upvote: 'Int'
    downvote: 'Int'
    isAnonymous: 'Boolean'
    createdAt: 'DateTime'
    updatedAt: 'DateTime'
    parent: 'Comment'
    post: 'Post'
    badWordDetection: 'BadWordDetection'
    replies: 'Comment'
    user: 'User'
    votes: 'Vote'
  }
  Vote: {
    id: 'Int'
    action: 'Action'
    updatedAt: 'DateTime'
    createdAt: 'DateTime'
    post: 'Post'
    postId: 'String'
    comment: 'Comment'
    commentId: 'Int'
    user: 'User'
    userId: 'String'
  }
  Saved: {
    id: 'Int'
    userId: 'String'
    postId: 'String'
    createdAt: 'DateTime'
    user: 'User'
    post: 'Post'
  }
  BadWordDetection: {
    id: 'Int'
    postId: 'String'
    commentId: 'Int'
    badWord: 'String'
    createdAt: 'DateTime'
    post: 'Post'
    comment: 'Comment'
  }
  Tag: {
    id: 'Int'
    tag: 'String'
    posts: 'Post'
  }
  ReportedPost: {
    id: 'Int'
    userId: 'String'
    postId: 'String'
    reason: 'String'
    createdAt: 'DateTime'
    post: 'Post'
    user: 'User'
  }
  UserLog: {
    id: 'Int'
    userId: 'String'
    log: 'String'
    createdAt: 'DateTime'
    user: 'User'
  }
  OTPVerification: {
    id: 'String'
    code: 'String'
    userData: 'Json'
    status: 'Boolean'
    createdAt: 'DateTime'
    expiredAt: 'DateTime'
  }
  Rank: {
    id: 'Int'
    name: 'String'
    score: 'Int'
    createdAt: 'DateTime'
  }
}

// Helper to gather all methods relative to a model
interface NexusPrismaMethods {
  Account: Typegen.NexusPrismaFields<'Account'>
  Session: Typegen.NexusPrismaFields<'Session'>
  Subscriber: Typegen.NexusPrismaFields<'Subscriber'>
  User: Typegen.NexusPrismaFields<'User'>
  Keyword: Typegen.NexusPrismaFields<'Keyword'>
  Hidden: Typegen.NexusPrismaFields<'Hidden'>
  Setting: Typegen.NexusPrismaFields<'Setting'>
  Information: Typegen.NexusPrismaFields<'Information'>
  ExtraInfo: Typegen.NexusPrismaFields<'ExtraInfo'>
  Notification: Typegen.NexusPrismaFields<'Notification'>
  ChatRoom: Typegen.NexusPrismaFields<'ChatRoom'>
  SeenBy: Typegen.NexusPrismaFields<'SeenBy'>
  Message: Typegen.NexusPrismaFields<'Message'>
  VerificationRequest: Typegen.NexusPrismaFields<'VerificationRequest'>
  Post: Typegen.NexusPrismaFields<'Post'>
  Comment: Typegen.NexusPrismaFields<'Comment'>
  Vote: Typegen.NexusPrismaFields<'Vote'>
  Saved: Typegen.NexusPrismaFields<'Saved'>
  BadWordDetection: Typegen.NexusPrismaFields<'BadWordDetection'>
  Tag: Typegen.NexusPrismaFields<'Tag'>
  ReportedPost: Typegen.NexusPrismaFields<'ReportedPost'>
  UserLog: Typegen.NexusPrismaFields<'UserLog'>
  OTPVerification: Typegen.NexusPrismaFields<'OTPVerification'>
  Rank: Typegen.NexusPrismaFields<'Rank'>
  Query: Typegen.NexusPrismaFields<'Query'>
  Mutation: Typegen.NexusPrismaFields<'Mutation'>
}

interface NexusPrismaGenTypes {
  inputs: NexusPrismaInputs
  outputs: NexusPrismaOutputs
  methods: NexusPrismaMethods
  models: PrismaModels
  pagination: Pagination
  scalars: CustomScalars
}

declare global {
  interface NexusPrismaGen extends NexusPrismaGenTypes {}

  type NexusPrisma<
    TypeName extends string,
    ModelOrCrud extends 'model' | 'crud'
  > = Typegen.GetNexusPrisma<TypeName, ModelOrCrud>;
}
  