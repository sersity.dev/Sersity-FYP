/*
  Warnings:

  - You are about to drop the column `anouymous` on the `Setting` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Setting" DROP COLUMN "anouymous",
ADD COLUMN     "anonymous" BOOLEAN DEFAULT false;
