/*
  Warnings:

  - The `rank` column on the `User` table would be dropped and recreated. This will lead to data loss if there is data in the column.
  - You are about to drop the `RankRequirement` table. If the table is not empty, all the data it contains will be lost.

*/
-- AlterTable
ALTER TABLE "User" DROP COLUMN "rank",
ADD COLUMN     "rank" TEXT NOT NULL DEFAULT E'BEGINNER';

-- DropTable
DROP TABLE "RankRequirement";

-- DropEnum
DROP TYPE "Rank";

-- CreateTable
CREATE TABLE "Rank" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL DEFAULT E'BEGINNER',
    "score" INTEGER NOT NULL DEFAULT 0,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY ("id")
);
