-- AlterTable
ALTER TABLE "Accounts" ALTER COLUMN "user_id" SET DATA TYPE TEXT;

-- AlterTable
ALTER TABLE "Sessions" ALTER COLUMN "user_id" SET DATA TYPE TEXT;
