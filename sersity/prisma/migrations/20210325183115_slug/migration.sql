/*
  Warnings:

  - The migration will add a unique constraint covering the columns `[slug]` on the table `ChatRoom`. If there are existing duplicate values, the migration will fail.
  - Made the column `slug` on table `ChatRoom` required. The migration will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "ChatRoom" ALTER COLUMN "slug" SET NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "ChatRoom.slug_unique" ON "ChatRoom"("slug");
