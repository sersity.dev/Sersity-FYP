-- CreateEnum
CREATE TYPE "ChatSetting" AS ENUM ('EVERYONE', 'FOLLOWING_FOLLOWER', 'FOLLOWER');

-- CreateTable
CREATE TABLE "Subscriber" (
    "id" SERIAL NOT NULL,
    "email" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Keyword" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "userId" TEXT NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Hidden" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "postId" TEXT NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Setting" (
    "id" SERIAL NOT NULL,
    "anouymous" BOOLEAN DEFAULT false,
    "secondVerification" BOOLEAN DEFAULT false,
    "allowBadWord" BOOLEAN DEFAULT false,
    "notification" BOOLEAN DEFAULT false,
    "commentNoti" BOOLEAN DEFAULT false,
    "replyNoti" BOOLEAN DEFAULT false,
    "newTrendNoti" BOOLEAN DEFAULT false,
    "followingUserNoti" BOOLEAN DEFAULT false,
    "chatSetting" "ChatSetting" NOT NULL DEFAULT E'FOLLOWING_FOLLOWER',
    "chatActiveStatus" BOOLEAN DEFAULT false,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "userId" TEXT NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Infomation" (
    "id" SERIAL NOT NULL,
    "userId" TEXT NOT NULL,
    "phone" INTEGER,
    "dateOfBirth" TIMESTAMP(3),
    "bio" TEXT,
    "highSchool" TEXT,
    "university" TEXT,
    "location" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "ExtraInfo" (
    "id" SERIAL NOT NULL,
    "infomationId" INTEGER NOT NULL,
    "title" TEXT,
    "description" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Subscriber.email_unique" ON "Subscriber"("email");

-- CreateIndex
CREATE UNIQUE INDEX "Keyword_userId_unique" ON "Keyword"("userId");

-- CreateIndex
CREATE UNIQUE INDEX "Hidden_postId_unique" ON "Hidden"("postId");

-- CreateIndex
CREATE UNIQUE INDEX "Setting_userId_unique" ON "Setting"("userId");

-- CreateIndex
CREATE UNIQUE INDEX "Infomation_userId_unique" ON "Infomation"("userId");

-- AddForeignKey
ALTER TABLE "Keyword" ADD FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Hidden" ADD FOREIGN KEY ("postId") REFERENCES "Post"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Setting" ADD FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Infomation" ADD FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ExtraInfo" ADD FOREIGN KEY ("infomationId") REFERENCES "Infomation"("id") ON DELETE CASCADE ON UPDATE CASCADE;
