/*
  Warnings:

  - Added the required column `keyword` to the `Keyword` table without a default value. This is not possible if the table is not empty.

*/
-- DropIndex
DROP INDEX "Keyword_userId_unique";

-- AlterTable
ALTER TABLE "Keyword" ADD COLUMN     "keyword" TEXT NOT NULL,
ALTER COLUMN "userId" DROP NOT NULL;
