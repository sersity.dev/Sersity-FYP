/*
  Warnings:

  - You are about to drop the column `infomationId` on the `ExtraInfo` table. All the data in the column will be lost.
  - You are about to drop the `Infomation` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `informationId` to the `ExtraInfo` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "Infomation" DROP CONSTRAINT "Infomation_userId_fkey";

-- DropForeignKey
ALTER TABLE "ExtraInfo" DROP CONSTRAINT "ExtraInfo_infomationId_fkey";

-- AlterTable
ALTER TABLE "ExtraInfo" DROP COLUMN "infomationId",
ADD COLUMN     "informationId" INTEGER NOT NULL;

-- DropTable
DROP TABLE "Infomation";

-- CreateTable
CREATE TABLE "Information" (
    "id" SERIAL NOT NULL,
    "userId" TEXT NOT NULL,
    "phone" INTEGER,
    "dateOfBirth" TIMESTAMP(3),
    "bio" TEXT,
    "highSchool" TEXT,
    "university" TEXT,
    "location" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Information_userId_unique" ON "Information"("userId");

-- AddForeignKey
ALTER TABLE "Information" ADD FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ExtraInfo" ADD FOREIGN KEY ("informationId") REFERENCES "Information"("id") ON DELETE CASCADE ON UPDATE CASCADE;
