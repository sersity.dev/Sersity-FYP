/*
  Warnings:

  - You are about to drop the column `read_at` on the `Notification` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Notification" DROP COLUMN "read_at",
ADD COLUMN     "readAt" TIMESTAMP(3);
