/*
  Warnings:

  - The values [FOLLOWER] on the enum `ChatSetting` will be removed. If these variants are still used in the database, this will fail.

*/
-- AlterEnum
BEGIN;
CREATE TYPE "ChatSetting_new" AS ENUM ('EVERYONE', 'FOLLOWING_FOLLOWER', 'FOLLOWING');
ALTER TABLE "Setting" ALTER COLUMN "chatSetting" DROP DEFAULT;
ALTER TABLE "Setting" ALTER COLUMN "chatSetting" TYPE "ChatSetting_new" USING ("chatSetting"::text::"ChatSetting_new");
ALTER TABLE "Setting" ALTER COLUMN "chatSetting" SET  DEFAULT E'FOLLOWING_FOLLOWER';
ALTER TYPE "ChatSetting" RENAME TO "ChatSetting_old";
ALTER TYPE "ChatSetting_new" RENAME TO "ChatSetting";
DROP TYPE "ChatSetting_old";
COMMIT;
