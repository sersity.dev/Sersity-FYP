/*
  Warnings:

  - Added the required column `commentId` to the `BadWordDetection` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "BadWordDetection" ADD COLUMN     "commentId" INTEGER NOT NULL;

-- AddForeignKey
ALTER TABLE "BadWordDetection" ADD FOREIGN KEY ("commentId") REFERENCES "Comment"("id") ON DELETE CASCADE ON UPDATE CASCADE;
