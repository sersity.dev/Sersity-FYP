import { PostStatus, PrismaClient } from "@prisma/client";
import * as faker from "faker/locale/en";
import { hash } from "bcrypt";

//http://marak.github.io/faker.js/
// enum Action{
//   UPVOTE,
//   DOWNVOTE
// }
const tags = [
  "Computer Science",
  "Business",
  "Internship",
  "Tips & Tricks",
  "Food",
  "Friends",
  "Family",
  "Friends",
  "Gaming",
  "Knowledge",
  "Lifestyle",
  "Music",
  "Highschool",
  "University",
  "Relationship",
  "Opportunity",
  "Mathematics",
  "Physic",
  "Khmer Literature",
  "English",
  "Places",
  "Design",
  "Community",
  "Music",
  "Information Technology",
  "Architecture",
  "Industry Engineering",
  "Civil Engineering",
  "Construction Management",
  "Architectural Engineering",
  "Management of Information Science",
  "International Relations",
  "Finance",
  "Logistic",
  "Need Help",
  "General Question",
  "Other",   
]

var userIds = [];
var postIds = [];

// const data_upvote = Array.from({ length: 20 }).map(() => ({
//   postId: faker.random.arrayElement(postIds),
//   userId: faker.random.arrayElement(userIds),
//   updatedAt: faker.date.recent(),
// }));

// const data_comment = Array.from({ length: 20 }).map(() => ({
//   postId: faker.random.arrayElement(postIds),
//   userId: faker.random.arrayElement(userIds),
//   comment: faker.random.words(Math.floor(Math.random() * 20) + 1),
// }));


const data_rank = [
  {
    name: "BEGINNER",
    score: 0
  },
  {
    name: "HELPER",
    score: 100
  },
  {
    name: "ELITE",
    score: 1000
  },
  {
    name: "LEGEND",
    score: 5000
  },
  {
    name: "HERO",
    score: 10000
  },
  {
    name: "GOD",
    score: 50000
  },
  {
    name: "PURIRTY",
    score: 100000
  }
]



const data_tag = tags.map((e) => ({
  tag: e,
}));
// const data_tag = tags;

const prisma = new PrismaClient();
async function main() {

    // Prepare search
    await prisma.$executeRaw(
      `ALTER TABLE "Post" ADD COLUMN IF NOT EXISTS "post_with_weight" tsvector;`
    );
  
    await prisma.$executeRaw(
      `CREATE INDEX  IF NOT EXISTS post_with_weight_idx ON "Post" USING GIN (post_with_weight);`
    );
  
    await prisma.$executeRaw(
      `
      CREATE OR REPLACE FUNCTION  post_tsvector_trigger() RETURNS TRIGGER AS $$
      BEGIN
        new.post_with_weight := 
        setweight(to_tsvector('english',new.title),'A') || setweight(to_tsvector('english',new.content),'B');
        RETURN new;
      END
      
      $$ LANGUAGE plpgsql;
      `
    )

    await prisma.$executeRaw(
      `
      DROP TRIGGER IF EXISTS tsvectorupdate on "Post";
      `
    )
  
    await prisma.$executeRaw(
      `
      CREATE TRIGGER tsvectorupdate BEFORE INSERT OR UPDATE ON "Post" FOR EACH ROW EXECUTE PROCEDURE post_tsvector_trigger();
      `
    )

    const countRank = await prisma.rank.count();
    if(countRank === 0){
      await prisma.rank.createMany({data:data_rank})
    }
    
    hash("123123123", 10, async function (err, hash) {
            const user = await prisma.user.findFirst({
              where: {
                email: "sersity.dev@gmail.com"
              }
            })
            if (!user){
              await prisma.user.create({
                data: {
                  email: "sersity.dev@gmail.com",
                  name: "Sersity Dev",
                  password: hash,
                  setting:{
                    create: {anonymous:false}
                  },
                  information:{
                    create:{
                      bio:null,
                    }
                  }
                },
               
              });
            }
            
      });

  const data_user = Array.from({ length: 2 }).map(() => ({
    name: faker.internet.userName(),
    email: faker.internet.email(),
    password: faker.internet.password(),
    image: faker.image.people(100, 100),
  }));

  await prisma.user.createMany({ data: data_user });
  const users = await prisma.user.findMany();
  userIds = users.map((u) => u.id);
  // console.log(userIds);
  
  const data_post = Array.from({ length: 30 }).map(() => ({
    title: faker.random.words(Math.floor(Math.random() * 8) + 1),
    published: faker.datatype.boolean(),
    status: PostStatus["PUBLISHED"],
    viewCount: 0,
    upvote: faker.datatype.number({ min: 100, max: 200 }),
    downvote: faker.datatype.number({ min: 0, max: 100 }),
    authorId: faker.random.arrayElement(userIds),
    content: faker.random.words(Math.floor(Math.random() * 100) + 1),
    createdAt: faker.date.recent(),
    updatedAt: faker.date.recent(),
  }));
  // console.log(data_post);

  // await prisma.post.create({
  //   data:{
  //     title: "a",
  //     content:"b", 
  //     author: {
  //       connect:{
  //         id:userIds[0]
  //       }
  //     }
  //   }
  // });

  await prisma.post.createMany({ data: data_post });
  const posts = await prisma.post.findMany();
  const postIds = posts.map((p) => p.id);

  const data_comment = Array.from({ length: 20 }).map(() => ({
    postId: faker.random.arrayElement(postIds),
    userId: faker.random.arrayElement(userIds),
    comment: faker.random.words(Math.floor(Math.random() * 20) + 1),
  }));

  await prisma.comment.createMany({ data: data_comment });
  const tags = await prisma.tag.count()
  if(tags == 0){
    await prisma.tag.createMany({ data: data_tag });
  }

  userIds.map(async (e) => {
    await prisma.setting.create({
      data: {
        user: {
          connect: {
            id: e,
          },
        },
      },
    });
  });

  

}



main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
