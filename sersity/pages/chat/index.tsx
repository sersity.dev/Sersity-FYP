import React, { useEffect } from "react";

import { useColorModeValue as mode } from "@chakra-ui/color-mode";
import ChatLayout from "components/ChatLayout";
import { getSession } from "next-auth/client";
import { Flex } from "@chakra-ui/layout";
import { useRouter } from "next/router";
import useMQ from "hook/useMQ";

function ChatPage(props) {
  const { push } = useRouter();
  const { isMobile } = useMQ();

  useEffect(() => {
    if (!isMobile) push("/chat/default");
  }, []);
  return (
    <Flex
      pb="58px"
      bg={mode("white", "gray.750")}
      position="fixed"
      top="0"
      bottom="0"
      w="100%"
      h="100%"
      overflow="none"
    >
      <ChatLayout {...props} w="100%" h="100%" />
    </Flex>
  );
}

export async function getServerSideProps(ctx) {
  const session = await getSession(ctx);
  if (!session) {
    ctx.res.setHeader("Location", `/auth/login`);
    ctx.res.statusCode = 302;
    ctx.res.end();
  }
  return {
    props: { layout: { topBar: false, navBar: true }, user: session?.user }, // will be passed to the page component as props
  };
}

export default ChatPage;
