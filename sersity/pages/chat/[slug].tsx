import React from "react";

import ChatLayout from "components/ChatLayout";
import { getSession } from "next-auth/client";
import Head from "next/head";

function ChatPage(props) {
  return (
      <>
        <Head>
          <title>Chat - Sersity</title>
        </Head>
        <ChatLayout {...props} />
      </>
    );
}

export async function getServerSideProps(ctx) {
  const session = await getSession(ctx);
  if (!session) {
    ctx.res.setHeader("Location", `/auth/login`);
    ctx.res.statusCode = 302;
    ctx.res.end();
  }
  return {
    props: { layout: "none", user: session?.user }, // will be passed to the page component as props
  };
}

export default ChatPage;
