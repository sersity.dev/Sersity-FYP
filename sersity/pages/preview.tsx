import { Box, Button, Container } from "@chakra-ui/react";
import { CustomLayout } from "components/Layout";
import Thumbnail from "components/Thumbnail";
import React, { useEffect, useRef, useState } from "react";
import html2canvas from 'html2canvas';

function Preview(props){
    function saveAs(uri, filename) {

        var link = document.createElement('a');
    
        if (typeof link.download === 'string') {
    
            link.href = uri;
            link.download = filename;
    
            //Firefox requires the link to be in the body
            document.body.appendChild(link);
    
            //simulate click
            link.click();
    
            //remove the link when done
            document.body.removeChild(link);
    
        } else {
    
            window.open(uri);
    
        }
    }
    
    const ref = useRef(null)
    
    const capture = () => {
        console.log(ref.current)
        html2canvas(ref.current, {
            scrollX: 0,
            scrollY: 0,
            width: 1200,
            height: 630
            }).then(function(canvas) {
                // document.body.appendChild(canvas)
                // const image = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");;
                saveAs(canvas.toDataURL(), 'testing.jpeg');
      })
    }
    // const [isReady, setIsReady] = useState(false)
    // if(isReady){
    //     console.log(ref.current);
        
        
    // }
    // useEffect(() => {
    //     setIsReady(true)
    // }, [])
    

    return(
        <Box>
            <Button onClick={capture}>Download</Button>
            <Box ref={ref} p={0} m={0}>
                <Thumbnail post={props.post} />
            </Box>
        </Box>
        
        
    )   
}

export const getServerSideProps = async (ctx) => {
    const post = {
        title: "How to Get A Good Grade",
        content: `Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.

        The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham. Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.

        The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.`,
        author:{
            name: "Limhorng Sok",
            rank: "BEGINNER"
        }
    }
    return {
      props: {
        post:post,
        layout: "none",
      },
    };
  };

export default Preview;