import { useColorModeValue as mode } from "@chakra-ui/color-mode";
import { Box, Flex, Grid, VStack } from "@chakra-ui/layout";
import LoginSignup from "components/LoginSignup";
import PageMotion from "components/PageMotion";
// import Image from "next/image";
import { Img, useToast } from "@chakra-ui/react";
import { useRouter } from "next/router";

import React, { useEffect } from "react";
import { useSession } from "next-auth/client";
import Head from "next/head";
import { useTranslation } from "hook/useTranslation";

import useMQ from "hook/useMQ";
import BackButton from "components/BackButton";

export const AuthPage = (props) => {
  const router = useRouter();
  const authType = router.query.index;
  const [session] = useSession();
  const { isMobile } = useMQ();
  const toast = useToast();
  // console.log(authType);
  const tabIndex = authType == "login" ? 0 : 1;
  useEffect(() => {
    // cannot enter if you have already log in
    const user = session?.user;

    if (user) {
      router.push("/home");
      toast({
        title: "Authentication",
        description: "You're already Logged in.",
        status: "info",
        position: "top-right",
        duration: 1000,
        isClosable: true,
      });
    }
  }, [session]);
  const { t } = useTranslation();
  const pageTitle = authType == "login" ? "log in" : "sign up";
  const title = `Seristy - ${t(pageTitle)}`;
  return (
    <PageMotion>
      <Head>
        <title>{title}</title>
        <meta property="title" content={title} key="title" />
        <meta name="description" content={title} />
        <meta name="keywords" content={title} />
        <meta
          property="og:image"
          content="https://sersity.io/images/sersity.png"
        />
        <meta property="og:url" content="https://sersity.io" />
      </Head>
      <Grid
        templateColumns={{ base: "repeat(1, 1fr)", md: "repeat(4, 1fr)" }}
        gap={0}
      >
        <Flex
          direction="column"
          h="100vh"
          bg={{ base: mode("white", "gray.700"), md: "teal.500" }}
          w="100%"
        />
        <span />
        <span />
        <span />
        <Grid
          position="absolute"
          templateColumns={{ base: "repeat(1, 1fr)", md: "repeat(2, 1fr)" }}
          gap={0}
          h="100%"
          w="100%"
        >
          <Flex
            direction="column"
            justify={{ base: "center", md: "center" }}
            align="center"
          >
            {isMobile && (
              <Box position="absolute" top={5} left={5}>
                <BackButton onClick={() => router.push("/")} />
              </Box>
            )}

            {isMobile && (
              <Box my={8} onClick={() => router.push("/")}>
                <Img
                  alt="sersity"
                  objectFit="contain"
                  h={100}
                  w={180}
                  src="/images/sersityCrop.png"
                />
              </Box>
            )}

            <LoginSignup
              tabIndex={tabIndex}
              w={{ base: "100%", sm: "70%", md: "sm" }}
            />
          </Flex>
          <VStack
            display={{ base: "none", md: "flex" }}
            justify="center"
            align="flex-start"
            mr={10}
          >
            <Img
              alt="Sersity"
              objectFit="contain"
              w="600px"
              h="600px"
              src="/images/sersityCrop.png"
            />
          </VStack>
        </Grid>
      </Grid>
    </PageMotion>
  );
};

//set we dont wnat topbar and navbae
// LoginPage.getInitialProps = async ({ locale }) => {
//   const layout = "none";

//   return {, ...(await serverSideTranslations(locale, ["common"])) };
// };

export const getStaticProps = async (ctx) => {
  return {
    props: {
      layout: "none",
    },
  };
};

// it required slug lists to generate static
export async function getStaticPaths(ctx) {
  return {
    paths: [
      { params: { index: "login" }, locale: "kh" },
      { params: { index: "signup" }, locale: "kh" },
      { params: { index: "login" } },
      { params: { index: "signup" } },
    ],
    fallback: false,
  };
}

export default AuthPage;
