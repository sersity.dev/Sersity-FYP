import { useMutation, useQuery } from "@apollo/client";
import { gql } from "@apollo/client";

import {
  Box,
  Container,
  useColorModeValue as mode,
  useDisclosure,
} from "@chakra-ui/react";

import { CustomLayout } from "components/Layout";

import LoadingPost from "components/Loading/LoadingPost";
import UserProfile from "components/Profile";
import { GET_USER_POSTS } from "hook/graphql/user";
import prisma from "lib/prismaClient";
import _ from "lodash";

import React from "react";
import { getSession } from "next-auth/client";

// const Post = dynamic(() => import("components/Post"), { ssr: false });
import Post from "components/Post";
import Head from "next/head";
import ChatDialog from "components/ChatLayout/ChatDialog";
const UserPostClient = (props) => {
  const { data, loading, error } = useQuery(GET_USER_POSTS, {
    variables: { id: props?.user?.id },
  });

  return (
    <Box width="100%">
      {loading ? (
        <>
          <LoadingPost key="1" mt={4} />
          <LoadingPost key="2" mt={4} />
          <LoadingPost key="3" mt={4} />
          <LoadingPost key="4" mt={4} />
        </>
      ) : (
        data &&
        data.user?.posts.map((e, i) => (
          <Box key={i}>
            <Post
              key={e.id}
              // content={e.content}
              // title={e.title}
              // author={e.userRef}
              // id={e.id}
              // comment={e.topComment}
              // createdAt={e.publishedAt}
              // isVoted={e.isVoted}
              // isSaved={e.isSaved}
              // votes={e.upvote - e.downvote}
              // tags={e.tags}
              // type="multi-post"
              {...{
                author: e.userRef,
                comment: e.topComment,
                createdAt: e.publishedAt,
                votes: e.upvote - e.downvote,
                type: "multi-post",
                ...e,
              }}
            />
          </Box>
        ))
      )}
    </Box>
  );
};

function Profile(props) {
  return (
    <Box>
      <Head>
        <title>{props?.user.name} - Sersity</title>
      </Head>
      <CustomLayout
        right={
          <UserProfile
            mt={0}
            p={5}
            user={props?.user}
            w={["100%", "100%", "360px"]}
          />
        }
      >
        <Container maxW="container.md" p={0}>
          <UserPostClient user={props?.user} />
        </Container>
      </CustomLayout>
    </Box>
  );
}

export default Profile;

export async function getServerSideProps(ctx) {
  const session = await getSession(ctx);
  let modifiedUser = {
    isMine: false,
    _count: { followers: 0, posts: 0 },
    isFollowed: false,
    isAuth: false,
  };
  if (session?.user) {
    const user = await prisma.user.findFirst({
      where: { id: ctx?.query?.slug },
      select: {
        id: true,
        name: true,
        rank: true,
        image: true,
        score: true,
        followedBy: {
          where: {
            id: { equals: "" + session.user.id },
          },
          select: {
            id: true,
          },
        },
        information: {
          select: {
            bio: true,
            dateOfBirth: true,
            university: true,
            location: true,
            highSchool: true,
          },
        },
      },
    });

    if (!user) {
      return {
        notFound: true,
      };
    }

    const countPost = await prisma.post.count({
      where: {
        authorId: user.id,
        status: "PUBLISHED",
        published: true,
        isAnonymous: false,
      },
    });

    const countFollower = await prisma.user.count({
      where: {
        status: "ACTIVE",
        following: {
          some: {
            id: user.id,
          },
        },
      },
    });
    modifiedUser = {
      ...user,
      isAuth: true,
      _count: { followers: countFollower, posts: countPost },
      isFollowed: user?.followedBy?.length > 0 ? true : false,
      isMine: user?.id === "" + session.user.id ? true : false,
    };
    // console.log(user);
    // if (!user) return { notFound: true };
  } else {
    const user = await prisma.user.findFirst({
      where: { id: ctx?.query?.slug },
      select: {
        id: true,
        name: true,
        rank: true,
        image: true,
        score: true,
        // _count: {
        //   select: {
        //     posts: true,
        //   },
        // },
        information: {
          select: {
            bio: true,
            dateOfBirth: true,
            university: true,
            location: true,
            highSchool: true,
          },
        },
      },
    });

    if (!user) {
      return {
        notFound: true,
      };
    }

    const countFollower = await prisma.user.count({
      where: {
        status: "ACTIVE",
        following: {
          some: {
            id: user.id,
          },
        },
      },
    });

    const countPost = await prisma.post.count({
      where: {
        authorId: user.id,
        status: "PUBLISHED",
        published: true,
        isAnonymous: false,
      },
    });

    modifiedUser = {
      ...modifiedUser,
      _count: { followers: countFollower, posts: countPost },
      ...user,
    };
  }

  return {
    props: {
      layout: { topBar: true, navBar: true },
      user: modifiedUser,
    },
  };
}
