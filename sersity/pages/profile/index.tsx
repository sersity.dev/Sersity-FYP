import { getSession, useSession } from "next-auth/client";
import { useRouter } from "next/router";

export default function MyProfile() {
  const [session] = useSession();
  const { push } = useRouter();
  if (session?.user) push(`profile/${session.user?.id}`);
  return null;
}
