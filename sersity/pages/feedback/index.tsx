import { Container } from "@chakra-ui/layout";
import { Box } from "@chakra-ui/react";
import React from "react";

export default function IndexPage(props) {
  return (
    <Container maxW="container.md">
      <Box w="100%" display="flex" align="center">
        <Box w="100%" h="895px" mt="75px">
          <iframe
            src="https://docs.google.com/forms/d/e/1FAIpQLSeIay3HxSTYp2-AKHqr1Sojbp0vzKZk-g31Iah7w0g1JDCbHg/viewform?embedded=true"
            width="100%"
            height="895px"
            frameBorder="0"
          >
            Loading…
          </iframe>
        </Box>
      </Box>
    </Container>
  );
}
