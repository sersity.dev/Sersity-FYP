import { Container } from "@chakra-ui/layout";
import React from "react";
import Setting from "components/Settting";
import { useQuery } from "@apollo/client";
import { GET_USER_SETTING } from "hook/graphql/setting";
import { useSession } from "next-auth/client";
import Head from "next/head";

function SettingPage(props) {
  const [session] = useSession();

  const { data, loading, error } = useQuery(GET_USER_SETTING, {
    variables: { id: session?.user?.id },
    fetchPolicy: "cache-first",
  });

  return (
      <>
        <Head>
          <title>Setting - Sersity</title>
        </Head>
        <Container maxW="container.md" display="relative" h="100%" p={[0, 6, 7]}>
          {data && <Setting setting={data} />}
        </Container>
      </>
  );
}
export default SettingPage;

//improve performent
// export function getStaticProps() {
//   return { props: {} };
// }
