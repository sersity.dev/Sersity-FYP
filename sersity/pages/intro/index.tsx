import { Box, Center, Flex, Grid, Stack, VStack } from "@chakra-ui/layout";

import PageMotion from "components/PageMotion";
import Image from "next/image";
import { useRouter } from "next/router";

import React, { useEffect, useState } from "react";
import { InferGetStaticPropsType } from "next";
import { Button, Heading, Text } from "@chakra-ui/react";
import { RiArrowLeftSLine, RiArrowRightSLine } from "react-icons/ri";
import AddSubscriber from "components/AddSubscriber";
import { motion } from "framer-motion";
import { FcExternal } from "react-icons/fc";
import { FaFacebook } from "react-icons/fa";
import { initApolloClient } from "lib/apolloClient";
import { Img } from "@chakra-ui/react";

export const IntroPage = (
  props: InferGetStaticPropsType<typeof getStaticProps>
) => {
  const router = useRouter();

  // Make sure we're in the browser

  useEffect(() => {
    if (typeof window !== "undefined") {
      const localData = localStorage.getItem("isVisited");
      !localData && localStorage.setItem("isVisited", "true");
    }
    document.body.style.overflow = "auto";
  }, []);

  const [stepIndex, setStepIndex] = useState(0);
  const animatedWidth = [
    ["0%", "-100%", "-100%", "-100%"],
    ["-70%", "70%", "70%", "-70%"],
    ["-100%", "-30%", "-30%", "30%"],
    ["-100%", "-100%", "-100%", "-70%"],
  ];

  function nextStep() {
    if (stepIndex === 3) {
      window.location.replace(`${router.basePath}/home`);
      setStepIndex(0);
    } else {
      setStepIndex(stepIndex + 1);
    }
  }

  function prevStep() {
    if (stepIndex === 0) {
      setStepIndex(3);
    } else {
      setStepIndex(stepIndex - 1);
    }
  }

  return (
    <Flex w="100%" position="fixed" h="100%">
      <Flex
        w="100%"
        h="100%"
        transform={
          stepIndex === 2 || stepIndex === 3
            ? "rotate(180deg)"
            : "rotate:(0deg)"
        }
        transitionDuration="0.5s"
      >
        <Box
          h="100%"
          w="100%"
          bg="transparant"
          transitionDuration="0.5s"
          position="fixed"
          right={animatedWidth[0][stepIndex]}
          zIndex="10"
        ></Box>
        <Box
          h="100%"
          w="100%"
          bg={stepIndex === 2 || stepIndex === 3 ? "transparant" : "teal.500"}
          transitionDuration="0.5s"
          position="fixed"
          right={animatedWidth[1][stepIndex]}
          zIndex="10"
        ></Box>
        <Box
          h="100%"
          w="100%"
          bg={stepIndex === 2 || stepIndex === 3 ? "teal.500" : "transparant"}
          transitionDuration="0.5s"
          position="fixed"
          right={animatedWidth[2][stepIndex]}
          zIndex="10"
        ></Box>
        <Box
          h="100%"
          w="100%"
          bg="teal.500"
          transitionDuration="0.5s"
          position="fixed"
          right={animatedWidth[3][stepIndex]}
          zIndex="10"
        ></Box>
      </Flex>
      {/* First Slide */}
      <Box
        position="fixed"
        bg={["teal.500", "transparent"]}
        width={["100%", "100%", "30%"]}
        display={stepIndex === 0 ? "flex" : "none"}
        zIndex="20"
        right={0}
      >
        <VStack
          w="100%"
          color="white"
          fontSize={["32px", "40px", "34px", "42px", "44px"]}
          fontWeight="semi-bold"
          letterSpacing={[10, 20]}
          justify="center"
          h="100vh"
        >
          <Text textAlign="center">
            WELCOME <br /> TO
          </Text>
          <Text color="gray.700" fontWeight="bold">
            SERSITY
          </Text>
        </VStack>
      </Box>

      <Box
        position="fixed"
        zIndex="20"
        bottom="15%"
        style={{ left: "calc(35% - 319.5px)" }}
        display={stepIndex === 0 ? "block" : "none"}
        transitionDuration="0.5s"
      >
        <Img
          display={["none", "none", "inherit"]}
          w="639px"
          h="500px"
          objectFit="contain"
          alt="sersity logo"
          src="/icons/women_thinking.png"
        />
      </Box>
      {/* Second Slide */}
      <Box
        position="fixed"
        zIndex="20"
        style={{ right: "calc(35% - 373px)", top: "calc(50% - 131px)" }}
        display={stepIndex === 1 ? "block" : "none"}
      >
        <Img
          display={["none", "inherit"]}
          alt="Sersity"
          objectFit="contain"
          w="600px"
          h="250px"
          src="/images/sersityCrop.png"
        />
      </Box>
      <Flex
        display={stepIndex === 1 ? "inherit" : "none"}
        position="fixed"
        top={0}
        zIndex="20"
        bgColor={["teal.500", "teal.500", "transparent"]}
        w={["100%", "100%", "30%"]}
        h="100%"
        align="center"
        justify="center"
        direction="column"
      >
        <PageMotion>
          <Center>
            <Text
              color="gray.700"
              fontSize={["32px", "40px", "34px", "42px", "44px"]}
              fontWeight="bold"
            >
              What is Sersity?
            </Text>
          </Center>

          <br></br>
          <Text
            fontFamily="Raleway"
            fontSize={["20px", "24px", "30px"]}
            fontWeight="normal"
            m="10%"
            color="white"
          >
            Sersity is an online community where students find answers to their
            questions on diverse topics.
          </Text>
        </PageMotion>
      </Flex>
      {/* <Box
        position="absolute"
        zIndex="20"
        top="37%"
        style={{ left: "calc(15% - 300px)" }}
        fontSize="32px"
        fontWeight="semibold"
        color="white"
        display={stepIndex === 1 ? "block" : "none"}
        letterSpacing={3}
        width="600px"
        textAlign="left"
        transitionDuration="0.5s"
      >
        <Text color="gray.700" fontSize="48px" fontWeight="bold">
          What is Sersity?
        </Text>
        <br></br>
        <Text fontFamily="Raleway" fontWeight="normal">
          Sersity is an online community where students find answers to their
          questions on diverse topics.
        </Text>
      </Box> */}

      {/* Third Slide */}
      <Box
        position="fixed"
        zIndex="20"
        style={{ right: "calc(15% - 200px)", top: "calc(50% - 200px)" }}
        display={stepIndex === 2 ? "block" : "none"}
      >
        <Img
          display={["none", "inherit"]}
          w="400px"
          h="400px"
          alt="sersity logo"
          src="/icons/sersity_logox3600.png"
        />
      </Box>
      {/* <Box
        position="absolute"
        zIndex="20"
        top="37%"
        style={{ left: "calc(35% - 450px)" }}
        fontSize="32px"
        fontWeight="semibold"
        color="white"
        display={stepIndex === 2 ? "block" : "none"}
        letterSpacing={3}
        width="900px"
        textAlign="left"
        transitionDuration="0.5s"
      > */}
      <Flex
        display={stepIndex === 2 ? "inherit" : "none"}
        position="fixed"
        zIndex="20"
        bgColor={["teal.500", "teal.500", "transparent"]}
        w={["100%", "100%", "70%"]}
        h="100%"
        align="center"
        justify="center"
        direction="column"
      >
        <PageMotion>
          <Center>
            <Text
              as="h1"
              color="gray.700"
              fontSize={["32px", "40px", "34px", "42px", "44px"]}
              fontWeight="bold"
            >
              Features
            </Text>
          </Center>
          <Stack
            fontSize={["20", "24px", "30px"]}
            as="ul"
            fontFamily="Raleway"
            fontWeight="normal"
            mt={10}
            mx="10%"
            color="white"
            spacing={3}
          >
            <Text as="li">
              Upvote and Downvote button for each post and comment
            </Text>
            <Text as="li">
              Earning point from your upvotes and get rewarded
            </Text>
            <Text as="li">
              Khmer version combining its culture, language and norm
            </Text>
          </Stack>
        </PageMotion>
      </Flex>

      {/* Forth Page */}
      <ForthPage stepIndex={stepIndex}></ForthPage>

      <Flex
        position="fixed"
        w="100%"
        justifyContent="center"
        bottom="40px"
        alignItems="center"
        zIndex="100"
      >
        <Button
          leftIcon={<RiArrowLeftSLine />}
          background="transparent"
          py="0"
          _hover={{
            textDecoration: "underline",
            backgroundColor: "transparent",
          }}
          onClick={prevStep}
        >
          Prev
        </Button>
        {[1, 2, 3, 4].map((e, i) => (
          <Box onClick={() => setStepIndex(i)} key={i}>
            <CircleStep isActive={i === stepIndex ? true : false} />
          </Box>
        ))}
        <Button
          rightIcon={<RiArrowRightSLine />}
          background="transparent"
          py="0"
          _hover={{
            textDecoration: "underline",
            backgroundColor: "transparent",
          }}
          onClick={nextStep}
        >
          Next
        </Button>
      </Flex>
    </Flex>
  );
};

CircleStep.defaultProps = { isActive: false };

function ForthPage({ stepIndex }) {
  const client = initApolloClient();
  if (stepIndex !== 3) {
    return null;
  }
  const router = useRouter();
  return (
    <VStack
      w="100%"
      h="100%"
      align="center"
      // bgGradient="linear(to-r,teal.500, teal.400, teal.400)"
      background="transparent"
      dispay={stepIndex === 3 ? "block" : "none"}
      position="fixed"
      zIndex="20"
      justify="center"
    >
      <PageMotion>
        <Stack mb="100px" align="center" justify="center">
          <Heading
            mb={[4, 5, 10]}
            color="gray.700"
            letterSpacing={20}
            fontSize={["30px", "40px", "42px", "44px"]}
          >
            SERSITY
          </Heading>
          <Text
            my={3}
            w={["70vw", "70vw", "700px"]}
            color="white"
            fontSize={["18px", "24px", "30px"]}
          >
            Extend your arms in welcome to the future. The best is yet to come!
          </Text>
        </Stack>
      </PageMotion>
      <Flex align="center" justify="center" mt={10} pb={10}>
        <Stack spacing={10} direction={["column", "column", "row"]} mx={4}>
          <AddSubscriber
            apolloClient={client || null}
            apolloState={null}
            width="280px"
          />
          <motion.div
            animate={{
              y: [0, 10, 0, -10],
            }}
            transition={{
              loop: Infinity,
              times: [0, 0.5, 1, 1.5],
              repeatDelay: 0,
              repeatType: "reverse",
            }}
          >
            <Button
              w="280px"
              p="30px"
              bgGradient="linear(to-r,teal.600,teal.400, teal.500, )"
              color="white"
              _hover={{ background: "rgba(0, 208, 179, 0.2)" }}
              rightIcon={<FcExternal fontSize="2rem" />}
              fontSize="18px"
              boxShadow="inner"
              border="2px solid white"
              onClick={() => window.location.replace(`${router.basePath}/home`)}
            >
              Go to Website
            </Button>
          </motion.div>
          <motion.div
            animate={{
              y: [0, 10, 0, -10],
            }}
            transition={{
              loop: Infinity,
              times: [0, 0.7, 1, 1.5],
              repeatDelay: 0,
              repeatType: "reverse",
            }}
          >
            <Button
              w="280px"
              p="30px"
              bgGradient="linear(to-r, #1880cf, blue.400)"
              color="white"
              _hover={{ background: "rgba(0, 208, 179, 0.2)" }}
              rightIcon={<FaFacebook />}
              fontSize="18px"
              boxShadow="inner"
              onClick={() =>
                window.open("https://www.facebook.com/sersity.online.community")
              }
            >
              Facebook Page
            </Button>
          </motion.div>
        </Stack>
      </Flex>
    </VStack>
  );
}

function LeftSide({ step }) {
  let content = (
    <>
      <Box h="100vh" w="30%" bg="teal.500"></Box>
      <Box h="100vh" w="70%" bg="white"></Box>
    </>
  );

  switch (step) {
    case 1:
      content = (
        <>
          <Box h="100vh" w="70%" bg="teal.500"></Box>
          <Box h="100vh" w="30%" bg="white"></Box>
        </>
      );
      break;
    case 2:
      content = (
        <>
          <Box h="100vh" w="30%" bg="teal.500"></Box>
          <Box h="100vh" w="70%" bg="white"></Box>
        </>
      );
      break;
    case 3:
      content = (
        <>
          <Box h="100vh" w="70%" bg="teal.500"></Box>
          <Box h="100vh" w="30%" bg="white"></Box>
        </>
      );
      break;
    default:
      content = (
        <>
          <Box h="100vh" w="30%" bg="teal.500"></Box>
          <Box h="100vh" w="70%" bg="white"></Box>
        </>
      );
      break;
  }
  return content;
}

function RightSide({ step }) {
  let content = (
    <>
      <Box h="100vh" w="30%" bg="teal.500"></Box>
      <Box h="100vh" w="70%" bg="white"></Box>
    </>
  );

  switch (step) {
    case 1:
      content = (
        <>
          <Box h="100vh" w="70%" bg="teal.500"></Box>
          <Box h="100vh" w="30%" bg="white"></Box>
        </>
      );
      break;
    case 2:
      content = (
        <>
          <Box h="100vh" w="30%" bg="teal.500"></Box>
          <Box h="100vh" w="70%" bg="white"></Box>
        </>
      );
      break;
    case 3:
      content = (
        <>
          <Box h="100vh" w="70%" bg="teal.500"></Box>
          <Box h="100vh" w="30%" bg="white"></Box>
        </>
      );
      break;
    default:
      content = (
        <>
          <Box h="100vh" w="30%" bg="teal.500"></Box>
          <Box h="100vh" w="70%" bg="white"></Box>
        </>
      );
      break;
  }
  return content;
}

function CircleStep({ isActive }) {
  return (
    <Box
      w="18px"
      h="18px"
      border="1px"
      borderColor="gray.600"
      borderRadius="100%"
      backgroundColor={isActive ? "gray.600" : "transparent"}
      m="1"
      cursor="pointer"
    />
  );
}

export const getStaticProps = async () => {
  return {
    props: {
      layout: "none",
    },
  };
};

export default IntroPage;
