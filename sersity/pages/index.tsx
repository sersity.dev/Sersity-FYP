import SlashScreen from "components/SlashScreen";
import delay from "lodash/delay";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
const IndexPage = (props) => {
  const router = useRouter();

  useEffect(() => {
    delay(() => {
      const localData = localStorage.getItem("isVisited");
      localData ? router.push("/home") : router.push("intro");
    }, 500);
  }, []);
  return (
    <>
      <Head>
        <title>Sersity - Online Student Community</title>
        <meta
          property="title"
          content="Sersity - Online Student Community"
          key="title"
        />
        <meta
          property="og:title"
          content="Sersity - Online Student Community"
          key="title"
        />
        <meta
          name="description"
          content="SERSITY has been created in the view of helping students to find their relevant information regarding education. High-school students and University students can ask, answer, post and comment throughout the website. Having a specific platform for them, could unlock their hidden potential to the fullest and generate unlimited benefits to all students across the country."
        />
        <meta
          name="og:description"
          content="SERSITY has been created in the view of helping students to find their relevant information regarding education. High-school students and University students can ask, answer, post and comment throughout the website. Having a specific platform for them, could unlock their hidden potential to the fullest and generate unlimited benefits to all students across the country."
        />
        <meta name="keywords" content="Sersity - Online Student Community" />
        <meta
          property="og:image"
          content="https://sersity.io/images/sersity.png"
        />
        <meta property="og:url" content="https://sersity.io" />
      </Head>
      <SlashScreen />
    </>
  );
};

export const getServerSideProps = async () => {
  return {
    props: {
      layout: "none",
    },
  };
};

export default IndexPage;
