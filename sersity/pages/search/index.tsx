import {
  Box,
  Flex,
  Tag,
  TagLabel,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";

import { Container } from "@chakra-ui/layout";
import React, { useEffect } from "react";
import { useRouter } from "next/router";

import { GET_ARRAY_POSTS } from "hook/graphql/post";
import Post from "components/Post";
import LoadingPost from "components/Loading/LoadingPost";
import { CustomLayout } from "components/Layout";
import { useLazyQuery, useMutation } from "@apollo/client";

import axios from "axios";
import { apiUrl } from "api/url";
import { CLEAR_KEYWORD, GET_KEYWORDS } from "hook/graphql/user";
import { useSession } from "next-auth/client";
import Head from "next/head";

function Search() {
  const [session] = useSession();
  const uid = session?.user?.id;
  const router = useRouter();
  const keyword = router.query.keyword;
  const [getArrayPosts, { data, loading }] = useLazyQuery(GET_ARRAY_POSTS);
  const [getKeywordFnc, { data: getKeywordData, loading: getKeywordLoading }] =
    useLazyQuery(GET_KEYWORDS, { fetchPolicy: "network-only" });
  const { push } = useRouter();
  let content;
  const cardBG = useColorModeValue("white", "gray.700");
  // const { getKeywordFnc, getKeywordData, getKeywordLoading } = useGetUser();
  const [clearKeyword] = useMutation(CLEAR_KEYWORD, {
    update(cache, { data }) {
      cache.modify({
        id: cache.identify({ __typename: "User", id: uid }),
        fields: {
          keywords(e) {
            return [];
          },
        },
      });
    },
  });

  function clearKeywordFnc() {
    clearKeyword();
  }

  useEffect(() => {
    getKeywordFnc();
    if (keyword) {
      axios.get(`${apiUrl}/api/search?keyword=${keyword}`).then(({ data }) => {
        // console.log(data);

        const postIds = data.map((p) => p.postid);
        getArrayPosts({
          variables: {
            postIds: postIds.toString(),
          },
        });
      });
    }
  }, [keyword]);

  if (keyword === "") {
    content = (
      <>
        <Box
          fontSize="16px"
          fontWeight="bold"
          lineHeight="110%"
          letterSpacing="-2%"
          display={
            getKeywordData?.getKeyword?.popularKeywords ? "flex" : "none"
          }
        >
          Popular Search:
        </Box>
        <Flex mb={3} mt={5} pb={3} flexWrap="wrap">
          {getKeywordData?.getKeyword?.popularKeywords?.map((e, i) => (
            <Tag
              key={i}
              size="md"
              variant="outline"
              // colorScheme="teal"
              mr={3}
              mb={3}
              _hover={{
                cursor: "pointer",
                backgroundColor: useColorModeValue("gray.100", "inherit"),
              }}
              onClick={() => push("/search?keyword=" + e.keyword)}
              bg={cardBG}
              p={3}
              borderRadius="5px"
              boxShadow="xs"
            >
              <TagLabel>{e.keyword}</TagLabel>
            </Tag>
          ))}
        </Flex>
        <Flex
          justifyContent="space-between"
          alignContent="center"
          display={
            getKeywordData?.getKeyword?.personalKeywords?.length > 0
              ? "flex"
              : "none"
          }
        >
          <Box
            fontSize="16px"
            fontWeight="bold"
            lineHeight="110%"
            letterSpacing="-2%"
          >
            Recent Search:
          </Box>
          <Text
            color="teal.500"
            cursor="pointer"
            fontSize="14px"
            _hover={{
              cursor: "pointer",
              textDecoration: "underline",
              color: "teal.600",
              fontStyle: "italic",
            }}
            onClick={clearKeywordFnc}
          >
            Clear
          </Text>
        </Flex>

        <Flex mb={3} mt={5} pb={3} flexWrap="wrap">
          {getKeywordData?.getKeyword?.personalKeywords?.map((e, i) => (
            <Tag
              key={i}
              size="md"
              variant="outline"
              // colorScheme="teal"
              mr={3}
              mb={3}
              _hover={{
                cursor: "pointer",
                backgroundColor: useColorModeValue("gray.100", "inherit"),
              }}
              onClick={() => push("/search?keyword=" + e.keyword)}
              bg={cardBG}
              p={3}
              borderRadius="5px"
              boxShadow="xs"
            >
              <TagLabel>{e.keyword}</TagLabel>
            </Tag>
          ))}
        </Flex>
      </>
    );
  }

  if (!data && loading && keyword !== "") {
    content = (
      <>
        <Text mx={3}>
          Search keyword: <b>{keyword}</b>
        </Text>
        <LoadingPost key="1" mt={4} />
        <LoadingPost key="2" mt={4} />
        <LoadingPost key="3" mt={4} />
        <LoadingPost key="4" mt={4} />
      </>
    );
  }

  if (data && !loading && keyword !== "") {
    // const results =
    content = (
      <Box>
        <Text mx={3}>
          Search keyword: <b>{keyword}</b>
        </Text>
        {data?.arrayPosts?.map((e, i) => {
          return (
            <Box key={i}>
              <Post
                key={e.id}
                // content={e.content}
                // title={e.title}
                // author={e.userRef}
                // id={e.id}
                // createdAt={e.publishedAt}
                // isVoted={e.isVoted}
                // isSaved={e.isSaved}
                // votes={e.upvote - e.downvote}
                // tags={e.tags}
                // type="multi-post"
                {...{
                  author: e.userRef,
                  comment: e.topComment,
                  createdAt: e.publishedAt,
                  votes: e.upvote - e.downvote,
                  type: "multi-post",
                  ...e,
                }}
              />
            </Box>
          );
        })}
      </Box>
    );
  }

  if (data?.arrayPosts?.length === 0 && keyword !== "") {
    content = (
      <>
        <Box
          fontSize="16px"
          fontWeight="bold"
          lineHeight="110%"
          letterSpacing="-2%"
          display={
            getKeywordData?.getKeyword?.popularKeywords ? "flex" : "none"
          }
        >
          Popular Search:
        </Box>
        <Flex mb={3} mt={5} pb={3} flexWrap="wrap">
          {getKeywordData?.getKeyword?.popularKeywords?.map((e, i) => (
            <Tag
              key={i}
              size="md"
              variant="outline"
              // colorScheme="teal"
              mr={3}
              mb={3}
              _hover={{
                cursor: "pointer",
                backgroundColor: useColorModeValue("gray.100", "inherit"),
              }}
              onClick={() => push("/search?keyword=" + e.keyword)}
              bg={cardBG}
              p={3}
              borderRadius="5px"
              boxShadow="xs"
            >
              <TagLabel>{e.keyword}</TagLabel>
            </Tag>
          ))}
        </Flex>
        <Flex
          justifyContent="space-between"
          alignContent="center"
          display={
            getKeywordData?.getKeyword?.personalKeywords?.length > 0
              ? "flex"
              : "none"
          }
        >
          <Box
            fontSize="16px"
            fontWeight="bold"
            lineHeight="110%"
            letterSpacing="-2%"
          >
            Recent Search:
          </Box>
          <Text
            color="teal.500"
            cursor="pointer"
            fontSize="14px"
            _hover={{
              cursor: "pointer",
              textDecoration: "underline",
              color: "teal.600",
              fontStyle: "italic",
            }}
            onClick={clearKeywordFnc}
          >
            Clear
          </Text>
        </Flex>

        <Flex mb={3} mt={5} pb={3} flexWrap="wrap">
          {getKeywordData?.getKeyword?.personalKeywords?.map((e, i) => (
            <Tag
              key={i}
              size="md"
              variant="outline"
              // colorScheme="teal"
              mr={3}
              mb={3}
              _hover={{
                cursor: "pointer",
                backgroundColor: useColorModeValue("gray.100", "inherit"),
              }}
              onClick={() => push("/search?keyword=" + e.keyword)}
              bg={cardBG}
              p={3}
              borderRadius="5px"
              boxShadow="xs"
            >
              <TagLabel>{e.keyword}</TagLabel>
            </Tag>
          ))}
        </Flex>
        <Box
          fontSize="16px"
          // fontWeight="bold"
          lineHeight="110%"
          letterSpacing="-2%"
        >
          {keyword && (
            <>
              No result for keyword: <b>{keyword}</b>
            </>
          )}
        </Box>
      </>
    );
  }

  return (
    <CustomLayout>
      <Head>
        <title>Sersity - Search</title>
        <meta property="title" content="Sersity - Search" key="title" />
        <meta property="og:title" content="Sersity - Search" key="title" />
        <meta
          name="description"
          content="SERSITY allows students to search for the answer without creating a post to ask."
        />
        <meta
          name="og:description"
          content="SERSITY allows students to search for the answer without creating a post to ask."
        />
        <meta name="keywords" content="Sersity - Search" />
        <meta
          property="og:image"
          content="https://sersity.io/images/sersity.png"
        />
        <meta property="og:url" content="https://sersity.io" />
      </Head>
      <Container maxW="container.md" pt={5}>
        {content}
      </Container>
    </CustomLayout>
  );
}

export default Search;
