import { Flex } from "@chakra-ui/layout";

import { useRouter } from "next/router";

import React, { useEffect, useState } from "react";
import { InferGetStaticPropsType } from "next";
import { Text } from "@chakra-ui/react";

import { Img } from "@chakra-ui/react";
import axios from "axios";
import { signIn } from "next-auth/client";
import { PulseLoader } from "react-spinners";

export const VerificationPage = (
  props: InferGetStaticPropsType<typeof getStaticProps>
) => {
  const router = useRouter();
  const [isValid, setIsValid] = useState(false);
  const [state, setstate] = useState("Verifying");
  const [error, setError] = useState({});
  let params = router.query;
  let id;
  let code;
  let otp;

  async function handleLogin({ email, password }) {
    const res = await signIn("credentials", {
      email,
      password,
      callbackUrl: `/home`,
      redirect: false,
    });
    res.error && setError((error) => [{ ...error }, res.error]);
    res.url && router.push(res.url);
  }

  useEffect(() => {
    //  console.log(params);
    if (params.id) {
      id = params.id;
      code = params.code;
      // setIsValid(true)
      otp = axios
        .post(`/api/rest/auth/verify-otp`, { id, code })
        .then(function ({ data }) {
          console.log(data);
          if (data.status !== 404) {
            const name = data.userData.name;
            const password = data.userData.password;
            const email = data.userData.email;
            if (name && password && email) {
              setstate("Creating an account");
              axios
                .post(`/api/rest/auth/signup`, { name, email, password })
                .then(function ({ data }) {
                  console.log(data);
                  setstate("Signing in");
                  if (data.error) setError((error) => [error, data.message]);
                  if (data.error == false) handleLogin({ email, password });
                })
                .catch(function (error) {
                  console.log(error);
                });
            } else {
              setstate("There was an error, please try again");
            }
          } else {
            setstate(data.msg);
            window.location.href = "/auth/signup";
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  }, [params]);

  return (
    <>
      {params?.id && (
        <Flex
          flexDirection="column"
          w="100%"
          position="fixed"
          h="100%"
          align="center"
          justifyContent="center"
          bg="gray.700"
        >
          {/* {error && (
                  <Text>{error}</Text>
                )} */}
          <Img
            alt="Sersity"
            objectFit="contain"
            w="40%"
            h="20%"
            src="/images/sersityCrop.png"
            mt="-100px"
          />
          <Flex pt={["0%", "5%"]}>
            <PulseLoader color="white" margin={8} />
          </Flex>
          <Text
            disabled
            colorScheme="teal"
            bg="transparant"
            color="teal"
            fontSize="24px"
            mt="50px"
            mb="20px"
          >
            Verifying for <b>{params.email}</b>
          </Text>
          <Text
            colorScheme="teal"
            bg="transparant"
            color="white"
            fontSize="24px"
          >
            {state}
          </Text>
        </Flex>
      )}
    </>
  );
};

export const getStaticProps = async () => {
  return {
    props: {
      layout: "none",
    },
  };
};

export default VerificationPage;
