import { useQuery } from "@apollo/client";
import { gql } from "@apollo/client";
import { Button } from "@chakra-ui/button";
import { useDisclosure } from "@chakra-ui/hooks";
import { Container } from "@chakra-ui/layout";
import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Box,
  Flex,
  Spinner,
  useColorModeValue as mode,
} from "@chakra-ui/react";
import SingleComent from "components/Comment";
import CusDialog from "components/CusDialog";
import { CustomLayout } from "components/Layout";

import LoadingPost from "components/Loading/LoadingPost";
import LoginSupign from "components/LoginSignup";
import Top5 from "components/Top5";

import { GET_POSTS } from "hook/graphql/post";
import useMQ from "hook/useMQ";

import dynamic from "next/dynamic";
import Head from "next/head";
import React, { useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
const Post = dynamic(() => import("components/Post"), { ssr: false });

const PostLists = () => {
  const { data, loading, error, fetchMore } = useQuery(GET_POSTS, {
    variables: { skip: 0, latest: true },
    // fetchPolicy:"cache-and-network"
  });

  // console.log(data);
  const [hasMore, setHasMore] = useState(true);

  function scrollDownFetchMore() {
    if (data) {
      fetchMore({
        variables: { skip: data?.posts?.length },
      });
      if (data?.posts?.length % 10 !== 0) setHasMore(false);
    }
  }

  return (
    <>
      {loading ? (
        <Box px={3}>
          <LoadingPost key="1" mt={4} />
          <LoadingPost key="2" mt={4} />
          <LoadingPost key="3" mt={4} />
          <LoadingPost key="4" mt={4} />
        </Box>
      ) : (
        <>
          {data && (
            <InfiniteScroll
              loader={
                <Flex
                  mt={3}
                  mb={3}
                  overflow="hidden"
                  align="center"
                  justify="center"
                >
                  <Spinner
                    thickness="4px"
                    speed="0.65s"
                    emptyColor="gray.200"
                    color="teal.500"
                    size="md"
                  />
                </Flex>
              }
              dataLength={data?.posts.length}
              next={scrollDownFetchMore}
              hasMore={hasMore}
            >
              {data?.posts.map((e, i) => (
                <Post
                  key={e.id}
                  // content={e.content}
                  // title={e.title}
                  // author={e.userRef}
                  // id={e.id}
                  // comment={e.topComment}
                  // createdAt={e.publishedAt}
                  // votes={e.upvote - e.downvote}
                  // isVoted={e.isVoted}
                  // isSaved={e.isSaved}
                  // tags={e.tags}
                  // type="multi-post"
                  {...{
                    author: e.userRef,
                    comment: e.topComment,
                    createdAt: e.publishedAt,
                    votes: e.upvote - e.downvote,
                    type: "multi-post",
                    ...e,
                  }}
                />
              ))}
            </InfiniteScroll>
          )}
        </>
      )}
    </>
  );
};

const RigthSide = () => (
  <Flex direction="column">
    <Top5 />
  </Flex>
);
export default function LatestPage(props) {
  const { isMobile } = useMQ();
  return (
    <CustomLayout right={isMobile ? null : <RigthSide />}>
      <Head>
        <title>Sersity - Latest</title>
        <meta property="title" content="Sersity - Latest" key="title" />
        <meta property="og:title" content="Sersity - Latest" key="title" />
        <meta
          name="description"
          content="SERSITY has been created in the view of helping students to find their relevant information regarding education. High-school students and University students can ask, answer, post and comment throughout the website. Having a specific platform for them, could unlock their hidden potential to the fullest and generate unlimited benefits to all students across the country."
        />
        <meta
          name="og:description"
          content="SERSITY has been created in the view of helping students to find their relevant information regarding education. High-school students and University students can ask, answer, post and comment throughout the website. Having a specific platform for them, could unlock their hidden potential to the fullest and generate unlimited benefits to all students across the country."
        />
        <meta name="keywords" content="Sersity - Latest" />
        <meta
          property="og:image"
          content="https://sersity.io/images/sersity.png"
        />
        <meta property="og:url" content="https://sersity.io" />
      </Head>
      <Container maxW="container.md" p={0}>
        <PostLists />
      </Container>
    </CustomLayout>
  );
}
