import { NextApiRequest, NextApiResponse } from "next";
import { hotSort } from "hook/hot-algorithm";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const data = req.body;
  const data_example = [
    {
      id: 1,
      upvote: 100,
      downvote: 25,
      created_at: new Date("2021-3-15"),
    },
    {
      id: 2,
      upvote: 1000,
      downvote: 500,
      created_at: new Date("2021-3-16"),
    },
    {
      id: 3,
      upvote: 120,
      downvote: 3,
      created_at: new Date("2021-3-16"),
    },
    {
      id: 4,
      upvote: 1020,
      downvote: 10,
      created_at: new Date("2021-3-16"),
    },
    {
      id: 5,
      upvote: 300,
      downvote: 100,
      created_at: new Date("2021-3-16"),
    },
    {
      id: 6,
      upvote: 250,
      downvote: 25,
      created_at: new Date("2021-3-16"),
    },
  ];

  const list_id = hotSort(data_example);

  return res.json({
    data: list_id,
  });
}
