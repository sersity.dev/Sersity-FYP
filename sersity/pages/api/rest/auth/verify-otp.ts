import axios from "axios";
import { hash } from "bcrypt";
import prisma from "lib/prismaClient";
import moment from "moment";
import { NextApiRequest, NextApiResponse } from "next";
import nodemailer from "nodemailer"

export default async function verification(
  req: NextApiRequest,
  res: NextApiResponse
) {
//   const prisma = new PrismaClient();

  if (req.method === "POST") {
        const otp = await prisma.oTPVerification.findFirst({
            where:{
                id: req.body.id,
                code: req.body.code,
                status: true
            }
        })
        if(otp && otp?.expiredAt > new Date()){
            const otp = await prisma.oTPVerification.update({
                where:{
                    id: req.body.id,
                },
                data:{
                    status:false
                }
            })
            res.json(otp)
        }else{
            // await prisma.$disconnect();
            res.json({status: 404,data:{},msg:"The session is not valid or expired"})
        }
    }
}

