import bcrypt from "bcrypt";
import { sign } from "jsonwebtoken";
import { NextApiRequest, NextApiResponse } from "next";
import prisma from "lib/prismaClient";
export default async function login(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === "POST") {
    const person = await prisma.user.findUnique({
      where: {
        email: req.body.email.toLowerCase(),
      },
    });
    if (person) {
      prisma.$disconnect();
      const secret = process.env.JWT_SECRET;
      const claims = {
        sub: `${person.id}`,
        name: person.name,
        email: person.email,
        picture: person.image,
        iat: Math.floor(Date.now() / 1000),
        exp: Math.floor(Date.now() / 1000) + 30 * 24 * 60 * 60,
      };

      const match = await bcrypt.compare(req.body.password, person.password);
      const token = sign(claims, secret);
      const data = { ...claims, accessToken: token };

      if (match) {
        res.status(200).json(data);
        res.end();
      } else {
        res.status(401).json({ erorr: "Authentication failed" });
        res.end();
      }
    } else {
      prisma.$disconnect();
      res.status(401).json({ erorr: "Account doesn't exists" });
      res.end();
    }
  } else {
    prisma.$disconnect();
    res.status(405).json({ error: "We only support POST" });
    res.end();
  }
}
