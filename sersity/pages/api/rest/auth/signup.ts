import { hash } from "bcrypt";
import prisma from "lib/prismaClient";
import { NextApiRequest, NextApiResponse } from "next";

export default async function signup(
  req: NextApiRequest,
  res: NextApiResponse
) {
  // const prisma = new PrismaClient();

  if (req.method === "POST") {
    const { email, name, password } = req.body;

    const userExists = await prisma.user.findUnique({
      where: { email: email.toLowerCase() },
      select: { email: true },
    });

    if (userExists) {
      res.status(200).json({
        error: true,
        message: "This email address is already being used",
      });
      res.end();
    } else {
      hash(password, 10, async function (err, hash) {
        const user = await prisma.user.create({
          data: {
            email: email.toLowerCase(),
            name: name,
            password: hash,
            setting:{
              create: {anonymous:false}
            },
            information:{
              create:{
                bio:null,
              }
            }
          },
         
        });
        res.status(200).json({
          error: false,
          data: { id: user.id, name: user.name, email: user.email },
          message: "Successful",
        });
        res.end();
      });
    }
  } else {
    res.status(405).json({ message: "We only support POST" });
    res.end();
  }
}
