import { PrismaClient } from ".prisma/client";
import { posts } from "utils/sample-data";

const prisma = new PrismaClient();
export default async function handle(req, res) {
  const posts = await prisma.post.findMany();

  res.json(posts);
}
