import NextAuth from "next-auth";
import Providers from "next-auth/providers";
import Adapters from "next-auth/adapters";
import prisma from "lib/prismaClient";
import { sign } from "jsonwebtoken";
import { apiUrl } from "api/url";
import bcrypt from "bcrypt";

const providers = [
  Providers.Facebook({
    clientId: process.env.FACEBOOK_ID,
    clientSecret: process.env.FACEBOOK_SECRET,
  }),
  Providers.Google({
    clientId: process.env.GOOGLE_ID,
    clientSecret: process.env.GOOGLE_SECRET,
  }),
  Providers.Email({
    server: {
      host: process.env.EMAIL_SERVER_HOST,
      port: process.env.EMAIL_SERVER_PORT,
      auth: {
        user: process.env.EMAIL_SERVER_USER,
        pass: process.env.EMAIL_SERVER_PASSWORD,
      },
    },
    from: process.env.EMAIL_FROM,
  }),
  Providers.Credentials({
    name: "Credentials",
    authorize: async ({ password, email }) => {
      const person = await prisma.user.findUnique({
        where: {
          email: email,
        },
      });

      if (person) {
        prisma.$disconnect();
        const secret = process.env.JWT_SECRET;
        const claims = {
          sub: `${person.id}`,
          name: person.name,
          email: person.email,
          picture: person.image,
          iat: Math.floor(Date.now() / 1000),
          exp: Math.floor(Date.now() / 1000) + 30 * 24 * 60 * 60,
        };
        const match = await bcrypt.compare(password, person.password);
        const token = sign(claims, secret);
        const user = { ...claims, accessToken: token };
        if (match) {
          return user;
        } else {
          throw new Error("Authentication failed");
        }
      } else {
        prisma.$disconnect();
        throw new Error("Account doesn't exists");
      }
    },
  }),
];

const callbacks = {
  async jwt(token, user, account, profile, isNewUser) {
    // Add access_token to the token right after signin
    if (account?.accessToken) {
      token.accessToken = account.accessToken;
    } else if (user?.accessToken) {
      token = user;
      token.accessToken = user.accessToken;
    }
    return token;
  },
  async session(session, token, user) {
    // Add property to session, like an access_token from a provider.
    session.user.id = token.sub;
    const setting = await prisma.setting.findFirst({
      where: {
        userId: "" + session.user.id,
      },
    });
    const information = await prisma.information.findFirst({
      where: {
        userId: "" + session.user.id,
      },
    });
    if (!setting) {
      await prisma.setting.create({
        data: {
          user: {
            connect: { id: "" + session.user.id },
          },
        },
      });
    }
    if (!information) {
      await prisma.information.create({
        data: {
          user: {
            connect: { id: "" + session.user.id },
          },
        },
      });
    }
    // session.t = token;
    return session;
  },
};

export default NextAuth({
  providers,
  callbacks,
  debug: process.env.NODE_ENV === "development",
  secret: process.env.AUTH_SECRET,
  jwt: {
    secret: process.env.JWT_SECRET,
  },
  session: {
    jwt: true,
    // How long until an idle session expires and is no longer valid.
    maxAge: 30 * 24 * 60 * 60, // 30 days
    updateAge: 24 * 60 * 60,
  },

  database: process.env.DATABASE_URL,
  adapter: Adapters.Prisma.Adapter({ prisma }),
});
