import { schema } from "api/graphql";
import { createContext } from "api/graphql/context";
import { ApolloServer } from "apollo-server-micro";

export const config = {
  api: {
    bodyParser: false,
  },
};

const apolloServer = new ApolloServer({
  context: createContext,
  schema,
  tracing: process.env.NODE_ENV === "development",
  subscriptions: {
    path: "/api/graphqlSubscriptions",
    keepAlive: 9000,
    onConnect: () => console.log("connected"),
    onDisconnect: () => console.log("disconnected"),
  },
  playground:
    process.env.NODE_ENV === "development"
      ? {
          subscriptionEndpoint: "/api/graphqlSubscriptions",
          settings: {
            "request.credentials": "same-origin",
          },
        }
      : false,
});

// type CustomSocket = Exclude<NextApiResponse<any>["socket"], null> & {
//   server: Parameters<ApolloServer["installSubscriptionHandlers"]>[0] & {
//     apolloServer?: ApolloServer;
//     apolloServerHandler?: any;
//   };
// };

// type CustomNextApiResponse<T = any> = NextApiResponse<T> & {
//   socket: CustomSocket;
// };

const graphqlWithSubscriptionHandler = (req, res) => {
  const oldOne = res.socket.server.apolloServer;
  if (
    //     //we need compare old apolloServer with newOne, becasue after hot-reload are
    // not equals
    oldOne &&
    oldOne !== apolloServer
  ) {
    console.warn("FIXING HOT RELOAD !!!!!!!!!!!!!!! ");
    delete res.socket.server.apolloServer;
  }

  if (!res.socket.server.apolloServer) {
    console.log(`* apolloServer (re)initialization *`);

    apolloServer.installSubscriptionHandlers(res.socket.server);
    res.socket.server.apolloServer = apolloServer;
    const handler = apolloServer.createHandler({ path: "/api/graphql" });
    res.socket.server.apolloServerHandler = handler;
    //clients losts old connections, but clients are able to reconnect
    oldOne?.stop();
  }

  return res.socket.server.apolloServerHandler(req, res);
};

export default graphqlWithSubscriptionHandler;

// export default apolloServer.createHandler({
//   path: "/api/graphql",
// });

// export default apolloServer.listen(80).then(({ url }) => {
//   console.log(`🚀  Server ready at ${url}`);
// });
