import { Post } from "@prisma/client";
import prisma from "lib/prismaClient";
import { NextApiRequest, NextApiResponse } from "next";
import { getSession} from 'next-auth/client'

export default async function verification(
  req,
  res
) {
    
    const session = await getSession({ req })
    // console.log(session);
    
    const uid = session?.user?.id
    // console.log(session);
    if(req.query.keyword){
        let keyword = req.query.keyword.toString()
        if(uid){
            await prisma.keyword.create({
                data:{
                    user: {
                        connect:{
                            id: ""+uid
                        }
                    },
                    keyword: keyword
                }
            })
        }else{
            await prisma.keyword.create({
                data:{
                    keyword: keyword
                }
            })
        }
        
        const arrKeyword = keyword.split(' ');
        keyword=arrKeyword[0];
        arrKeyword.map((e,i) => {
            if(i > 1)
            keyword = keyword + " | " + e
        })
        const result = await prisma.$queryRaw(
            `
            SELECT P.id as postId,title, content, P."createdAt" FROM "Post" as P
            INNER JOIN "User" as U ON P."authorId" = U.id
            LEFT JOIN "_PostToTag" as PT ON P.id = PT."A"
            LEFT JOIN "Tag" as T ON PT."B" = T.id
            WHERE P.post_with_weight @@ plainto_tsquery('${keyword}')
            AND P.published = true AND P.status = 'PUBLISHED'
            GROUP BY P.id, U."name", U.id
            Order by ts_rank(P.post_with_weight, plainto_tsquery('${keyword}')) desc
            `
        )
        
        res.json(result);
    }else{
        res.json({msg: "No keyword found!"});
    } 
   
    
}

