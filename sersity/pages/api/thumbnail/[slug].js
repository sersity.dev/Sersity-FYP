import fs from "fs";
import chromium from "chrome-aws-lambda";
import puppeteer from "puppeteer-core";
import prisma from "lib/prismaClient";
import path from "path";
import ReactDOMServer from "react-dom/server";
import Thumbnail from "components/Thumbnail";

export default async (req, res) => {
  const postSlug = req.query.slug.replace(".jpg", "");
  let post = await prisma.post.findFirst({
    where: {
      id: postSlug,
    },
    include: {
      tags: true,
      author: {
        select: {
          name: true,
          id: true,
          rank: true,
          image: true,
          createdAt: true
        },
      },
    },
  });

  const htmlString = ReactDOMServer.renderToString(<Thumbnail post={post} />);

  const imageLogo = fs.readFileSync("./public/icons/logo.png");
  const imageAvatar = fs.readFileSync("./public/avatar/anonymous.png");
  const base64Image = new Buffer.from(imageLogo).toString("base64");
  const dataURI = "data:image/jpeg;base64," + base64Image;
  const Avabase64Image = new Buffer.from(imageAvatar).toString("base64");
  const dataURIAva = "data:image/jpeg;base64," + Avabase64Image;
  const originalDate = new Date(post.createdAt);

  const formattedDate = `${originalDate.getDate()}/${(
    "0" +
    (originalDate.getMonth() + 1)
  ).slice(-2)}/${originalDate.getFullYear()}`;

  const browser = await puppeteer.launch({
    args: [...chromium.args, "--hide-scrollbars", "--disable-web-security"],
    defaultViewport: chromium.defaultViewport,
    executablePath: "/usr/bin/chromium-browser",
    headless: true,
    ignoreHTTPSErrors: true,
  });

  const tags = post.tags
    ? post.tags
        .map((tag) => {
          return `#${tag.tag}`;
        })
        .join(" | ")
    : "";

  if (post.isAnonymous == true) {
    post = {
      ...post,
      author: {
        name: "Anonymous",
        id: "user" + post?.author.createdAt.getTime(),
        rank: "Anonymous",
        image: "anonymous",
        createdAt: NaN
      },
    };
  }
  let screenShotBuffer;
  try {
    const page = await browser.newPage();
    page.setViewport({ width: 1128, height: 600 });
    page.setContent(getHTML(post, formattedDate, dataURI, dataURIAva, tags));
    // page.setContent(htmlString);
    screenShotBuffer = await page.screenshot();
  }
  catch(err) {
    console.log(err)
  }
  finally{
    await browser.close()
  }
 
  res.writeHead(200, {
    "Content-Type": "image/png",
    "Content-Length": Buffer.byteLength(screenShotBuffer),
  });
  res.end(screenShotBuffer);
};

function getHTML(post, date, dataURI, dataURIAva, tags) {
  return `<html>
    <body>
        <div class="social-image-content">
             <div class="author" style="display: flex; align-items: center">
                <div class="avatar">
                    <img src="${dataURIAva}"/>
                </div>
                <div class="author-name">
                    <h3>${post?.author?.name}<h3>
                    <h4 style="">${post?.author?.rank}</h4>
                    
                </div>
            </div>
            <div class="title">
                ${post.title}
            </div>
            <div class="description" style="white-space: pre-line;">
                ${post.content}
            </div>
            
            
            <div class="social-image-footer">
                <div class="social-image-footer-left">
                    ${tags} <i>${date}</i>
                </div>
                <div class="social-image-footer-right">
                    <img src="${dataURI}" />
                    <i>Powered by Sersity - Online Commuinity</i>
                    
                </div>
            </div>
        </div>
    </body>
    <style>
        .description{
            text-overflow: ellipsis;
            display: block;/* or inline-block */
            word-wrap: break-word;
            overflow: hidden;
            font-size: 24px;
            max-height: 9em;
            line-height: 1.5em;
        }
        .title{
            margin-top: 30px;
            font-size: 28px;
            text-overflow: ellipsis;
            display: block;/* or inline-block */
            word-wrap: break-word;
            overflow: hidden;
            max-height: 3em;
            line-height: 1.5em;
            margin-bottom: 15px;
            font-weight: 600;
        }
    
        .author{
            height: 80px;
            width: 100%;
        }
        .avatar{
            height: 80px;
            width: 80px !important;
            border-radius: 100%;
            background: teal
        }
        .avatar img{
            height: 80px;
            width: 80px !important;
            border-radius: 100%;
        }
        .author-name{
            margin-left: 20px;
            
        }
        .author-name h4{
            font-style: italic;
            margin-top: -20px;
        }
        .author-name h3{
            font-weight: 600;
            font-size: 24px !important;
            margin-bottom: 0px
        }
        html, body {
            height : 100%;
        }
        body {
            align-items : center;
            display : flex;
            height : 600px;
            justify-content : center;
            margin: 0;
            width : 1128px;
            background-color: #008080;
        }
        .social-image-content {
            border : 1px solid teal;
            border-radius : 5px;
            box-sizing: border-box;
            display : flex;
            flex-direction : column;
            height : calc(100% - 10px);
            margin : 5px;
            padding : 30px;
            width : calc(100%);
            position: relative;
            background-color: white;
        }
        
        .social-image-content h1 {
            font-size: 72px;
            margin-top: 90px;
        }
        .social-image-footer {
            display : flex;
            flex-direction : row;
            margin-top : auto;
        }
        .social-image-footer-left {
            align-items: center;
            display: flex;
            flex-direction: row;
            font-size : 16px;
            font-weight : 600;
            justify-content: center;
            line-height: 40px;
        }
        .social-image-footer-right img {
            // border : 2px solid black;
            border-radius : 50%;
            height : 30px;
            margin-right : 10px;
            width : 30px;
        }
        .social-image-footer-right {
            align-items: center;
            display: flex;
            flex-direction: row;
            height : 40px;
            justify-content: center;
            margin-left : auto;
            font-size : 16px;
        }
        * {
            font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
            Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
            font-weight : 400;
        }
    </style>
    </html>`;
}

// const html = `
//     <div class="css-cu9xnh">
//         <div class="css-h1vzk8">
//             <div class="css-15epm7e">
//                 <div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
//                 <div style="box-sizing: border-box; display: block; max-width: 100%;">
//                 <img alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMzAiIGhlaWdodD0iMzAiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" style="max-width: 100%; display: block; margin: 0px; border: none; padding: 0px;">
//                 </div>
//                 <img alt="Sersity Logo" src="/_next/image?url=%2Ficons%2Flogo.png&amp;w=64&amp;q=75" decoding="async" srcset="/_next/image?url=%2Ficons%2Flogo.png&amp;w=32&amp;q=75 1x, /_next/image?url=%2Ficons%2Flogo.png&amp;w=64&amp;q=75 2x" style="position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
//                 </div>
//                 <p class="chakra-text css-dol0sb">Powered by Sersity - Online Community</p></div><div class="css-70qvj9">
//                 <div class="css-m18wkb"><div class="css-k008qs"><div class="css-70qvj9"><span class="chakra-avatar css-127yab">
//                 <svg viewBox="0 0 128 128" class="chakra-avatar__svg css-16ite8i" role="img" aria-label=" avatar">
//                 <path fill="currentColor" d="M103,102.1388 C93.094,111.92 79.3504,118 64.1638,118 C48.8056,118 34.9294,111.768 25,101.7892 L25,95.2 C25,86.8096 31.981,80 40.6,80 L87.4,80 C96.019,80 103,86.8096 103,95.2 L103,102.1388 Z">
//                 </path>
//                 <path fill="currentColor" d="M63.9961647,24 C51.2938136,24 41,34.2938136 41,46.9961647 C41,59.7061864 51.2938136,70 63.9961647,70 C76.6985159,70 87,59.7061864 87,46.9961647 C87,34.2938136 76.6985159,24 63.9961647,24">
//                 </path>
//                 </svg>
//                 </span>
//                 <div class="css-t5co4c">
//                 <p class="chakra-text css-1xyplge">${name}</p><p class="chakra-text css-1t0irce"><i>${rank}</i></p></div></div></div><div class="css-17qnj1m"><p class="chakra-text css-dvkfu1">How to Get A Good Grade</p><p class="chakra-text css-viespw" style="white-space: pre-line;">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.

//         The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham. Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.

//         The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p></div></div></div></div></div>
// `
