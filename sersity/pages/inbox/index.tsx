import { useColorModeValue as mode } from "@chakra-ui/color-mode";
import { Flex } from "@chakra-ui/layout";
import Notification from "components/Notification";

function InboxPage() {
  return (
    <Flex
      py="58px"
      position="fixed"
      top={0}
      h="100vh"
      w="100vw"
      bg={mode("white", "gray.750")}
    >
      <Notification isMobileUi={true} />
    </Flex>
  );
}

export default InboxPage;

export const getStaticProps = async (ctx) => {
  return {
    props: {
      layout: { navBar: true, topBar: true, isBack: true, title: "Inbox" },
    },
  };
};
