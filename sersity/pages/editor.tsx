import { Container } from "@chakra-ui/layout";
import { CustomLayout } from "components/Layout";

import dynamic from "next/dynamic";
const CustomEditor = dynamic(() => import("components/CustomEditor"));

function EditorPage(props) {
  return (
    <CustomLayout>
      <Container maxW="container.md">
        <CustomEditor></CustomEditor>
      </Container>
    </CustomLayout>
  );
}
export default EditorPage;
