// import App from "next/app";

// import { Provider } from "react-redux";

import "styles/notFound.css";
import { Provider } from "next-auth/client";
import CSSReset from "@chakra-ui/css-reset";
import { ChakraProvider } from "@chakra-ui/react";
import theme from "styles/theme";
import "focus-visible/dist/focus-visible";
import "styles/global.css";
import "styles/date-picker.css";

import usePersistLocaleCookie from "hook/usePersistLocale";
import Layout from "components/Layout";
import { ApolloProvider } from "@apollo/client";
import { initApolloClient } from "lib/apolloClient";
import { AppWrapper } from "lib/AppContent";
function MyApp({ Component, pageProps }) {
  usePersistLocaleCookie();
  const apolloClient = initApolloClient();
  return (
    <div>
      <ChakraProvider theme={theme}>
        <Provider session={pageProps.session}>
          <ApolloProvider client={apolloClient}>
            <AppWrapper>
              <Layout layout={pageProps.layout}>
                <Component {...pageProps} />
              </Layout>
            </AppWrapper>
          </ApolloProvider>
          <CSSReset />
        </Provider>
      </ChakraProvider>
    </div>
  );
}

export default MyApp;
