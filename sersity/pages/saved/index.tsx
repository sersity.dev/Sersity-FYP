import { Box, Container, useColorModeValue } from "@chakra-ui/react";
import PageMotion from "components/PageMotion";
import React from "react";
import initializeApollo from "lib/apolloClient";
import { GET_SAVED_POSTS } from "hook/graphql/post";
import LoadingPost from "components/Loading/LoadingPost";
import { useQuery } from "@apollo/client";

import dynamic from "next/dynamic";
import { CustomLayout } from "components/Layout";

import Post from "components/Post";
import Head from "next/head";

function SavedPage(props) {
  const cardBG = useColorModeValue("white", "gray.700");

  let content;

  const { data, loading, error } = useQuery(GET_SAVED_POSTS, {
    fetchPolicy: "network-only",
  });

  if (data && !loading && !error) {
    if (data.savedPost?.length > 0) {
      content = data.savedPost.map((e, i) => (
        <Box key={i}>
          <Post
            key={e.post.id}
            // content={e.post.content}
            // title={e.post.title}
            // author={e.post.userRef}
            // id={e.post.id}
            // comment={e.post.topComment}
            // createdAt={e.post.publishedAt}
            // isVoted={e.post.isVoted}
            // isSaved={e.post.isSaved}
            // votes={e.post.upvote - e.post.downvote}
            // tags={e.post.tags}
            // type="multi-post"
            {...{
              author: e.post.userRef,
              comment: e.post.topComment,
              createdAt: e.post.publishedAt,
              votes: e.post.upvote - e.post.downvote,
              type: "multi-post",
              ...e.post,
            }}
          />
        </Box>
      ));
    } else {
      content = (
        <Box bg={cardBG} my={3} mx={[0, 3]} p={[3, 5]}>
          No saved post found
        </Box>
      );
    }
  }

  if (loading && !data) {
    content = (
      <>
        <LoadingPost key="1" mt={4} />
        <LoadingPost key="2" mt={4} />
        <LoadingPost key="3" mt={4} />
        <LoadingPost key="4" mt={4} />
      </>
    );
  }

  return (
    <>
      <Head>
        <title>Saved - Sersity</title>
      </Head>
      <CustomLayout>
        <Container maxW="container.md" pt={5}>
          <Box fontWeight="bold" marginLeft="10px" fontSize="16px">
            My Saved Lists:
          </Box>
          {content}
        </Container>
      </CustomLayout>
    </>
  );
}

export default SavedPage;
