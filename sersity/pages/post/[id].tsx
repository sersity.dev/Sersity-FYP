import React, { useEffect, useState } from "react";
import { Box, Container } from "@chakra-ui/layout";
import Post from "components/Post";

import initializeApollo from "lib/apolloClient";
import { InferGetStaticPropsType } from "next";
import { GET_POST } from "hook/graphql/post";
import { useRouter } from "next/router";
import LoadingPost from "components/Loading/LoadingPost";
import Head from "next/head";
import prisma from "lib/prismaClient";
import { CustomLayout } from "components/Layout";
import { Flex } from "@chakra-ui/react";
import Top5 from "components/Top5";
import useMQ from "hook/useMQ";

// const ClientSideData = withApollo(
//   (props) => {
//     return <div></div>;
//   },
//   { ssr = false }
// );

const RigthSide = () => (
  <Flex direction="column">
    <Top5 />
  </Flex>
);
export function post(props: InferGetStaticPropsType<typeof getStaticProps>) {
  // const { data, loading, error } = props;
  const apolloClient = initializeApollo();
  const postData = props?.post;
  const [comments, setComments] = useState(undefined);
  const [isVoted, setIsVoted] = useState({ isVoted: false, action: "" });
  const [isSaved, setIsSaved] = useState(false);
  const [post, setPost] = useState({ tags: [], votes: undefined });
  // console.log(comments)
  useEffect(() => {
    async function getPost() {
      const { data, loading, error } = await apolloClient.query({
        query: GET_POST,
        variables: {
          id: postData.id,
        },
      });
      if (!loading && !error && data) {
        setComments([...data.post[0].comments]);
        setIsVoted(data.post[0].isVoted);
        setIsSaved(data.post[0].isSaved);
        setPost({
          ...post,
          tags: data.post[0].tags,
          votes: data.post[0].upvote - data.post[0].downvote,
        });
        // console.log(data.post[0]);
      } else if (error) {
        console.log(error);
      }
    }

    if (postData) getPost();
  }, [postData]);
  // console.log(loading);
  // console.log(data);

  function handleSetComments(exist, income) {
    setComments([...exist, ...income]);
  }

  const router = useRouter();
  const { isMobile } = useMQ();

  if (router.isFallback) {
    // your loading indicator
    return (
      <CustomLayout right={isMobile ? null : <RigthSide />}>
        <Container maxW="container.md">
          <LoadingPost></LoadingPost>
        </Container>
      </CustomLayout>
    );
  }

  return (
    <div>
      <Head>
        <meta
          name="description"
          content={
            postData.content.length >= 155
              ? postData.content.slice(0, 155)
              : postData.content
          }
        />
        <title>
          {postData.title >= 70 ? postData.title.slice(0, 70) : postData.title}
        </title>

        {/* <!-- Twitter Card data --> */}
        <meta name="twitter:card" content="article" />
        <meta name="twitter:site" content="https://sersity.io/" />
        <meta name="twitter:title" content={postData.title} />
        <meta
          name="twitter:description"
          content={
            postData.content.length >= 200
              ? postData.content.slice(0, 200)
              : postData.content
          }
        />
        <meta name="twitter:creator" content={postData.author.name} />
        <meta
          name="twitter:image"
          content={"https://sersity.io/api/thumbnail/" + postData.id + ".jpg"}
        />

        {/* <!-- Open Graph data --> */}
        <meta name="og:title" content={postData.title} />
        <meta name="og:type" content="article" />
        <meta
          name="og:url"
          content={"https://sersity.io/post/" + postData.id}
        />
        <meta
          name="og:image"
          content={"https://sersity.io/api/thumbnail/" + postData.id + ".jpg"}
        />
        <meta
          name="og:description"
          content={
            postData.content.length >= 155
              ? postData.content.slice(0, 155)
              : postData.content
          }
        />
        <meta name="og:site_name" content="Sersity" />
        <meta name="keywords" content={postData.title} />
        <meta name="author" content={postData.author.name} />
        <meta name="og:author" content={postData.author.name} />

        {/* <meta
          property="og:image"
          content={`https://sersity.io/api/thumbnail/${postData.id}.jpg`}
        /> */}
      </Head>
      <CustomLayout right={isMobile ? null : <RigthSide />}>
        <Container maxW="container.md" p={0}>
          {/* {!props.error && ( */}
          <Post
            content={postData.content}
            title={postData.title}
            author={postData.author}
            id={postData.id}
            createdAt={postData.publishedAt}
            comment={comments}
            setComments={handleSetComments}
            isVoted={isVoted}
            isSaved={isSaved}
            votes={post.votes}
            tags={post.tags}
            type="single-post"
          />
          {/* )} */}
          {/* {props.error === true && <Box>404</Box>} */}
        </Container>
      </CustomLayout>
    </div>
  );
}

export const getStaticProps = async (ctx) => {
  const { id } = ctx.params;
  const post = await prisma.post.findFirst({
    where: {
      id: id,
      published: true,
      status: "PUBLISHED",
    },
    include: {
      author: {
        select: {
          name: true,
          rank: true,
          id: true,
          image: true,
          createdAt: true,
          setting: true,
        },
      },
    },
  });

  let modifiedPost;

  if (post.isAnonymous === true) {
    modifiedPost = {
      ...post,
      author: {
        id: "anonymous",
        rank: "ANONYMOUS",
        image: "anonymous",
        name: "user" + new Date(post.author.createdAt).getTime(),
      },
    };
  } else {
    modifiedPost = post;
  }

  prisma.$disconnect();
  if (!post) {
    return {
      props: { post: null, error: true },
      notFound: true,
    };
  }

  return {
    props: {
      post: modifiedPost,
      layout: { navBar: true, topBar: true, isBack: true },
      revalidate: 5, // In seconds
    },
  };
};

export const getStaticPaths = async () => {
  const posts = await prisma.post.findMany({
    where: {
      published: true,
      status: "PUBLISHED",
      // isBuilt: false,
    },
  });

  await prisma.post.updateMany({
    where: {
      published: true,
      status: "PUBLISHED",
      isBuilt: false,
    },
    data: {
      isBuilt: true,
    },
  });

  const ids = posts.map((e) => e.id);

  const enPaths = ids.map((id) => ({
    params: { id: id.toString() },
  }));

  const khPaths = ids.map((id) => ({
    params: { id: id.toString() },
    locale: "kh",
  }));

  prisma.$disconnect();
  return {
    paths: [...enPaths, ...khPaths],
    fallback: "blocking",
  };
};

export default post;
// export default withApollo(post);
