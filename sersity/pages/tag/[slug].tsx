import {
  Box,
  Flex,
  Tag,
  TagLabel,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";

import { Container } from "@chakra-ui/layout";
import React, { useEffect } from "react";
import { useRouter } from "next/router";

import { GET_TAG_POSTS } from "hook/graphql/post";
import Post from "components/Post";
import LoadingPost from "components/Loading/LoadingPost";
import { CustomLayout } from "components/Layout";
import { useLazyQuery } from "@apollo/client";

import { tags } from "hook/config";

function TagPost() {
  const router = useRouter();
  const tag = router.query.slug;
  const [getTagPosts, { data, loading }] = useLazyQuery(GET_TAG_POSTS);
  const { push } = useRouter();
  let content;
  const cardBG = useColorModeValue("white", "gray.700");

  useEffect(() => {
    if (tag) {
      getTagPosts({
        variables: {
          tag: tag,
        },
      });
    }
  }, [tag]);

  if (loading) {
    content = (
      <>
        <Flex mx={3} align="center">
          Tag:{" "}
          <Text
            fontWeight="bold"
            ml={3}
            bg="red.400"
            color="white"
            px={3}
            py={1}
            fontStyle="italic"
            borderRadius={5}
          >
            {tag}
          </Text>
        </Flex>
        <LoadingPost key="1" mt={4} />
        <LoadingPost key="2" mt={4} />
        <LoadingPost key="3" mt={4} />
        <LoadingPost key="4" mt={4} />
      </>
    );
  }

  if (data && !loading) {
    // const results =
    content = (
      <Box>
        <Flex mx={3} align="center">
          Tag:{" "}
          <Text
            fontWeight="bold"
            ml={3}
            bg="red.400"
            color="white"
            px={3}
            py={1}
            fontStyle="italic"
            borderRadius={5}
          >
            {tag}
          </Text>
        </Flex>
        {data?.tagPosts?.posts.map((e, i) => {
          return (
            <Box key={i}>
              <Post
                key={e.id}
                // content={e.content}
                // title={e.title}
                // author={e.userRef}
                // id={e.id}
                // createdAt={e.publishedAt}
                // isVoted={e.isVoted}
                // isSaved={e.isSaved}
                // votes={e.upvote - e.downvote}
                // tags={e.tags}
                // type="multi-post"
                {...{
                  author: e.userRef,
                  comment: e.topComment,
                  createdAt: e.publishedAt,
                  votes: e.upvote - e.downvote,
                  type: "multi-post",
                  ...e,
                }}
              />
            </Box>
          );
        })}
      </Box>
    );
  }

  if (data?.tagPosts?.posts.length === 0 && !loading) {
    // console.log("length");

    content = (
      <>
        <Box
          fontSize="16px"
          fontWeight="bold"
          lineHeight="110%"
          letterSpacing="-2%"
        >
          Popular Search:
        </Box>
        <Flex mb={3} mt={5} pb={3} flexWrap="wrap">
          {[
            "Good Unvisity in Cambodia",
            "How to be good at Programming",
            "How to get good grade",
            "How to be a good man",
            "Lifestyle",
          ].map((e, i) => (
            <Tag
              key={i}
              size="md"
              variant="outline"
              // colorScheme="teal"
              mr={3}
              mb={3}
              _hover={{
                cursor: "pointer",
                backgroundColor: useColorModeValue("gray.100", "inherit"),
              }}
              onClick={() => push("/search?keyword=" + e)}
              bg={cardBG}
              p={3}
              borderRadius="5px"
              boxShadow="xs"
            >
              <TagLabel>{e}</TagLabel>
            </Tag>
          ))}
        </Flex>
        <Flex justifyContent="space-between" alignContent="center">
          <Box
            fontSize="16px"
            fontWeight="bold"
            lineHeight="110%"
            letterSpacing="-2%"
          >
            Available Tags:
          </Box>
        </Flex>

        <Flex mb={3} mt={5} pb={3} flexWrap="wrap">
          {tags.map((e, i) => (
            <Tag
              key={i}
              size="md"
              variant="outline"
              // colorScheme="teal"
              mr={3}
              mb={3}
              _hover={{
                cursor: "pointer",
                backgroundColor: useColorModeValue("gray.100", "inherit"),
              }}
              onClick={() => push("/tag/" + e)}
              bg={cardBG}
              p={3}
              borderRadius="5px"
              boxShadow="xs"
            >
              <TagLabel>{e}</TagLabel>
            </Tag>
          ))}
        </Flex>
        <Box
          fontSize="16px"
          // fontWeight="bold"
          lineHeight="110%"
          letterSpacing="-2%"
        >
          {tag && (
            <>
              No result for tag: <b>{tag}</b>
            </>
          )}
        </Box>
      </>
    );
  }

  if (data?.tagPosts?.id === undefined && !loading) {
    content = (
      <>
        <Box
          fontSize="16px"
          fontWeight="bold"
          lineHeight="110%"
          letterSpacing="-2%"
        >
          Popular Search:
        </Box>
        <Flex mb={3} mt={5} pb={3} flexWrap="wrap">
          {[
            "Good Unvisity in Cambodia",
            "How to be good at Programming",
            "How to get good grade",
            "How to be a good man",
            "Lifestyle",
          ].map((e, i) => (
            <Tag
              key={i}
              size="md"
              variant="outline"
              // colorScheme="teal"
              mr={3}
              mb={3}
              _hover={{
                cursor: "pointer",
                backgroundColor: useColorModeValue("gray.100", "inherit"),
              }}
              onClick={() => push("/search?keyword=" + e)}
              bg={cardBG}
              p={3}
              borderRadius="5px"
              boxShadow="xs"
            >
              <TagLabel>{e}</TagLabel>
            </Tag>
          ))}
        </Flex>
        <Flex justifyContent="space-between" alignContent="center">
          <Box
            fontSize="16px"
            fontWeight="bold"
            lineHeight="110%"
            letterSpacing="-2%"
          >
            Available Tags:
          </Box>
        </Flex>

        <Flex mb={3} mt={5} pb={3} flexWrap="wrap">
          {tags.map((e, i) => (
            <Tag
              key={i}
              size="md"
              variant="outline"
              // colorScheme="teal"
              mr={3}
              mb={3}
              _hover={{
                cursor: "pointer",
                backgroundColor: useColorModeValue("gray.100", "inherit"),
              }}
              onClick={() => push("/tag/" + e)}
              bg={cardBG}
              p={3}
              borderRadius="5px"
              boxShadow="xs"
            >
              <TagLabel>{e}</TagLabel>
            </Tag>
          ))}
        </Flex>
        <Box
          fontSize="16px"
          // fontWeight="bold"
          lineHeight="110%"
          letterSpacing="-2%"
        >
          {tag && (
            <>
              There is no such a tag called: <b>{tag}</b>
            </>
          )}
        </Box>
      </>
    );
  }

  return (
    <CustomLayout>
      <Container maxW="container.md" pt={5}>
        {content}
      </Container>
    </CustomLayout>
  );
}

export default TagPost;
