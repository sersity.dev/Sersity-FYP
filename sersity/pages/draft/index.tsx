import { Container, Flex, Text } from "@chakra-ui/react";
import CreatePostDialog from "components/CreatePostDialog";
import Draft from "components/Draft";
import { CustomLayout } from "components/Layout";

import useDraft from "hook/useDraft";
import useMQ from "hook/useMQ";

import prisma from "lib/prismaClient";
import { getSession } from "next-auth/client";
import React from "react";

//CSR
const Lists = (props) => {
  const { posts } = props;
  const {
    drafts,
    emptyContent,
    selected,
    setSelected,
    handleClickDelete,
    selectDraft,
    updateDraft,
  } = useDraft({ posts });

  return (
    <>
      <CreatePostDialog
        isOpen={selected?.title?.length > 0}
        onClose={() => setSelected(emptyContent)}
        content={selected}
        setContent={setSelected}
        onDraftClick={updateDraft}
      />
      {drafts &&
        drafts.map((e, i) => (
          <Draft
            key={i}
            {...{
              ...e,
              onClickEdit: () => selectDraft(e),
              onClickDelete: () => handleClickDelete(e.id, i),
            }}
          />
        ))}
    </>
  );
};

//SSR
export default function DraftPage(props) {
  const { isMobile } = useMQ();
  return (
    <CustomLayout>
      <Container maxW="container.md" p={0} pt={5}>
        {!isMobile && (
          <Flex fontWeight="bold" mx={3} align="center" justify="space-between">
            <Text as="h1">My Draft Lists:</Text>
          </Flex>
        )}

        <Lists {...props} />
      </Container>
    </CustomLayout>
  );
}

export async function getServerSideProps(ctx) {
  const session = await getSession(ctx);
  const posts = await prisma.post.findMany({
    where: {
      status: "DRAFTED",
      authorId: session?.user?.id,
    },
    include: {
      tags: true,
    },
  });
  return {
    props: {
      posts: posts,
      layout: {
        navBar: true,
        topBar: true,
        isBack: true,
        title: "Draft Lists",
      },
    },
  };
}
