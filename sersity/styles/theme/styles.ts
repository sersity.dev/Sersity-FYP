const styles = {
  global: (props) => ({
    "html, body": {
      // overflow: "hidden",
      fontFamily:
        "Kantumruy,Inter,Whitney,Lato,system-ui,BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Oxygen,Ubuntu,Cantarell,Fira Sans,Droid Sans,Helvetica Neue,sans-serif",
      fontSize: "md",
      color: props.colorMode === "dark" ? "white" : "gray.600",
      bg: props.colorMode === "dark" ? "gray.750" : "gray.100",
      // lineHeight: "tall",
    },
    Fonts,
    "& *": {
      scrollbarWidth: "thin",
      msOverflowStyle: "none",
      "&::-webkit-scrollbar": { width: "6px" },
      "&::-webkit-scrollbar-thumb": {
        bg: "rgba(45, 55, 72, 0.2)",
        borderRadius: 5,
      },
    },
    a: {
      color: props.colorMode === "dark" ? "teal.300" : "teal.500",
      "&:focus": {
        outline: " none",
      },
    },
    button: {
      "&:focus": {
        outline: " none",
      },
    },
    // ".chakra-modal__overlay": {
    //   filter: "blur(8px)",
    // },
  }),
};

export default styles;

const Fonts = () =>
  `
  @font-face {
    font-family: "CUS";
    font-style: normal;
    font-display: swap;
    font-weight: 400;
    src: url("/fonts/CUS-Regular.ttf") format("truetype");
  }
  @font-face {
    font-family: "CUS";
    font-weight: 700;
    src: url("/fonts/CUS-Bold.ttf") format("truetype");
  }
  @font-face {
    font-family: "Kantumruy";
    font-style: normal;
    font-display: swap;
    font-weight: 400;
    unicode-range: U+17DD, 17f9;
    src: url("/fonts/Kantumruy-Regular.ttf") format("truetype");
  }
  @font-face {
    font-family: "Kantumruy";
    font-style: normal;
    font-display: swap;
    font-weight: 500;
    unicode-range: U+17DD, 17f9;
    src: url("/fonts/Kantumruy-Bold.ttf") format("truetype");
  }
  @font-face {
    font-family: "Kantumruy";
    font-style: normal;
    font-display: swap;
    font-weight: 600;
    unicode-range: U+17DD, 17f9;
    src: url("/fonts/Kantumruy-Bold.ttf") format("truetype");
  }
  @font-face {
    font-family: "Open Sans";
    font-style: normal;
    font-weight: 700;
    font-display: swap;
    src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN7rgOXOhpKKSTj5PW.woff2)
      format("woff2");
    unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB,
      U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
  }
  /* latin-ext */
  @font-face {
    font-family: "Raleway";
    font-style: normal;
    font-weight: 400;
    font-display: swap;
    src: url(https://fonts.gstatic.com/s/raleway/v18/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVvaorCGPrcVIT9d0c-dYA.woff)
      format("woff");
    unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB,
      U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
  }
  /* latin */
  `;
