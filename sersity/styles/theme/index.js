import { extendTheme } from "@chakra-ui/react";
// Global style overrides
import styles from "./styles";
// Foundational style overrides
import layerStyles from "./foundations/layerStyles";
import textStyles from "./foundations/textStyles";
import borders from "./foundations/borders";
import colors from "./foundations/colors";
import shadows from "./foundations/boxShadow";
// Component style overrides
import Button from "./components/button";
import DrawerOverlay from "./components/DrawerOverlay";
const overrides = {
  config: {
    initialColorMode: "light",
    useSystemColorMode: false,
  },
  fonts: {
    heading: "Open Sans",
    body: "Raleway",
  },
  styles,
  borders,
  shadows,
  layerStyles,
  textStyles,
  colors,
  // Other foundational style overrides go here
  components: {
    // Button,
    DrawerOverlay,
    // Other components go here
  },
  breakpoints: {
    sm: "30em",
    md: "48em",
    lg: "62em",
    xl: "80em",
    "2xl": "96em",
  },
};

export default extendTheme(overrides);
