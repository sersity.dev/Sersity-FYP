const ButtonStyle = {
  // style object for base or default style
  baseStyle: {
    background: "blackAlpha-800",
  },
  // // styles for different sizes ("sm", "md", "lg")
  // sizes: {},
  // // styles for different visual variants ("outline", "solid")
  // variants: {},
  // // default values for `size` and `variant`
};

export default ButtonStyle;
