// const boxShadow = {
//   cus0: "box-shadow: rgba(149, 157, 165, 0.2) 0px 8px 24px",
//   cus1: "box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px",
//   cus2: "box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px",
//   cus3: "box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px",
//   cus4: "box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 12px",
//   cus5:
//     "box-shadow: rgba(0, 0, 0, 0.05) 0px 6px 24px 0px, rgba(0, 0, 0, 0.08) 0px 0px 0px 1px",

//   cus6: "box-shadow: rgba(17, 12, 46, 0.15) 0px 48px 100px 0px",
// };

const shadows = {
  // xs: "0 0 0 1px rgba(0, 0, 0, 0.05)",
  // sm: "0 1px 2px 0 rgba(0, 0, 0, 0.05)",
  // base: "0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)",
  // md: "0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)",
  // lg: "0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)",
  // xl: "0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04)",
  // "2xl": "0 25px 50px -12px rgba(0, 0, 0, 0.25)",
  // outline: "0 0 0 3px rgba(66, 153, 225, 0.6)",
  // inner: "inset 0 2px 4px 0 rgba(0,0,0,0.06)",
  // none: "none",
  // "dark-lg": "rgba(0, 0, 0, 0.1) 0px 0px 0px 1px, rgba(0, 0, 0, 0.2) 0px 5px 10px, rgba(0, 0, 0, 0.4) 0px 15px 40px"
  cus0: "rgba(149, 157, 165, 0.2) 0px 8px 24px",
  cus1: " rgba(100, 100, 111, 0.2) 0px 7px 29px 0px",
  cus2: "rgba(0, 0, 0, 0.16) 0px 1px 4px",
  cus3: " rgba(99, 99, 99, 0.2) 0px 2px 8px 0px",
  cus4: "rgba(0, 0, 0, 0.1) 0px 4px 12px",
  cus5:
    " rgba(0, 0, 0, 0.05) 0px 6px 24px 0px, rgba(0, 0, 0, 0.08) 0px 0px 0px 1px",

  cus6: " rgba(17, 12, 46, 0.15) 0px 48px 100px 0px",
  cus7:
    " rgba(136, 165, 191, 0.48) 6px 2px 16px 0px, rgba(255, 255, 255, 0.8) -6px -2px 16px 0px",
};

export default shadows;
