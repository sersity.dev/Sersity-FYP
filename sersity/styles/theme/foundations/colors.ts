const colors = {
  brand: {
    100: "#f7fafc",
    // ...
    900: "#1202c",
  },
  teal: { 500: "#38B2AC" },
  red: {
    400: "rgba(245, 101, 101, 0.9)",
    450: "#f56565",
  },
  gray: {
    750: "rgba(41, 50, 66, 1)",
  },
  orange: {
    450: "#FF9501",
  },
};

export default colors;
