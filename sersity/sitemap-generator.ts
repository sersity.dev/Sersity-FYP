import { configureSitemap } from '@sergeymyssak/nextjs-sitemap';
import { PrismaClient } from "@prisma/client";
import path from "path";

const prisma = new PrismaClient();

async function getDynamicPaths() {
  const dataPost = await prisma.post.findMany({where:{published:true,status:"PUBLISHED"}});
  let data=[];
  dataPost.map((e) => {
      data.push(e.id)
  })
  return data.map((item) => `/post/${item}`);
}

getDynamicPaths().then((paths) => {
  const Sitemap = configureSitemap({
    baseUrl: 'https://sersity.io',
    include: [...paths],
    exclude: ['/post/[id]','/intro/','/editor','/editor/','/api/*','/api\\*','/preview/','/preview','/inbox/','/feedback/','/draft/','/chat/*','/auth/[index]','/profile/*','/tag/*','/verification/','/404','/saved/','/setting/'],
    // excludeIndex: true,
    pagesConfig: {
      '/post/*': {
        priority: '0.5',
        changefreq: 'daily',
      },
    },
    isTrailingSlashRequired: true,
    targetDirectory: path.join(process.cwd(), "/public"),
    pagesDirectory: path.join(process.cwd(), '/pages'),
  });
  prisma.$disconnect();
  Sitemap.generateSitemap();

});