Link gitlab: https://gitlab.com/sersity.dev/Sersity-FYP

**How to run in development ?**

' - ' : required.
' + ' : optional.
' # ' : comment.


**Requirement**:

    - NodeJS
    - NPM Installer
    - Database (PostgresQL only)
    + Google Service API
    + Facebook Service API
    + Email Service Provider


Step 1: Download and Install 

    - download
    - npm install

Step 2: Setup env. file

    - duplicate or copy env.example
    - rename env.example to .env.local or .env

    #deployment
    + rename to .env.production

Step 3: Verify environment

    ! must has DATABASE_URL in env file
    + correct value in env file 
    # To use credentials log in /sign up
    + AUTH_SECRET
    + JWT_SECRET

Step 4: Run script

    - create sql schema
      `npm run migrate && num run generate`
    - run development mode
      `num run dev`
    #deployment
    - run build
      `npm run build`
    - run start
      `npm run start`
